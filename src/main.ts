import { recordInitUrl } from '@alauda/common-snippet';
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ajax } from 'rxjs/ajax';
import { retry } from 'rxjs/operators';

import { globalEnvironments } from './app/app-global';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import './vendor.entry';

if (environment.production) {
  enableProdMode();
}

ajax
  .getJSON('api/v1/envs')
  .pipe(retry(3))
  .subscribe(envs => {
    Object.assign(globalEnvironments, envs);
    recordInitUrl();
    platformBrowserDynamic()
      .bootstrapModule(AppModule)
      .catch(err => console.error(err));
  });
