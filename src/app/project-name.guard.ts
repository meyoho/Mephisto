import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class ProjectNameGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot) {
    if (!route.queryParams.project) {
      const project = window.localStorage.getItem('project');
      if (!!project) {
        return this.router.createUrlTree(['/home'], {
          queryParams: { project },
        });
      }
    }
    return true;
  }
}
