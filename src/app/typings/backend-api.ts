import { K8sResourceAction, KubernetesResource } from '@alauda/common-snippet';

import {
  Application,
  DaemonSet,
  Deployment,
  HorizontalPodAutoscaler,
  Service,
  StatefulSet,
} from 'app/typings/raw-k8s';

export type K8sComponent = PodController | HorizontalPodAutoscaler | Service;

export type PodController = Deployment | DaemonSet | StatefulSet;

export interface K8sResourceWithActions<
  T extends KubernetesResource = KubernetesResource
> {
  kubernetes: T;
  resource_actions?: string[];
}

export interface K8sApplicationPayload {
  resource?: {
    name: string;
    namespace: string;
    displayName?: string;
    createMethod?: string; // UI/YAML
    owners?: {
      name: string;
      phone: string;
    }[];
  };
  crd?: Application;
  kubernetes: KubernetesResource[];
}

export interface Pagination<T> {
  count: number;
  page_size: number;
  num_pages: number;
  page: number;
  results: T[];
}

export interface TopologyResponse {
  nodes: {
    [key: string]: KubernetesResource;
  };
  edges: {
    type: string;
    from: string;
    to: string;
  }[];
}

export interface ResourceListParams {
  limit?: string;
  continue?: string;
  labelSelector?: string;
  fieldSelector?: string;
}

/**
 * API resource types
 * refers to: http://confluence.alauda.cn/pages/viewpage.action?pageId=44663323#id-%E6%96%B0%E5%BA%94%E7%94%A8API-1.2.0GET/acp/v1/resources/{cluster}/resourcetypes
 */
export interface APIResource {
  kind: string;
  name: string;
  singularName?: string;
  shortNames?: string[];
  namespaced: boolean;
  verbs: K8sResourceAction[];
  // copied from APIResourceList
  group?: string;
  groupVersion?: string;
}

export interface APIResourceList {
  kind: 'APIResourceList';
  apiVersion?: string;
  groupVersion: string;
  resources: APIResource[];
}
