export interface SerieChartData {
  name: string | number | Date;
  value: number;
}

export interface QuerySection {
  start_time: number;
  end_time: number;
}
