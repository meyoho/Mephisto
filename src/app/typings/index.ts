export * from './backend-api';
export * from './raw-k8s';
export * from './status';
export * from './chart.types';

// Defines core types here

export interface WorkspaceBaseParams {
  project?: string;
  cluster?: string;
  namespace?: string;
}
export interface RouteParams extends WorkspaceBaseParams {
  name?: string;
  [key: string]: any;
}

export interface I18nManifest {
  en: { [key: string]: string };
  zh_cn: { [key: string]: string };
}

export interface Environments {
  debug?: boolean;
  sock_server_url?: string;
  label_base_domain: string;
  docker_versions?: string;
  log_ttl?: string;
  event_ttl?: string;
}

export type Environment = keyof Environments;

/**
 * Console related status.
 *
 * All feature related status should be mapped into one of the categories
 */
export enum UnifiedStatus {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
  PENDING = 'PENDING',
  IN_PROGRESS = 'IN_PROGRESS',
  INACTIVE = 'INACTIVE',
}
