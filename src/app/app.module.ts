import { CodeEditorIntl, CodeEditorModule } from '@alauda/code-editor';
import { DEFAULT_REMOTE_URL, TranslateModule } from '@alauda/common-snippet';
import {
  MessageModule,
  NotificationModule,
  PaginatorIntl,
  TooltipCopyIntl,
} from '@alauda/ui';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ASMModule } from '@asm/asm.module';

import { MonacoProviderService } from 'ng-monaco-editor';

import { AppPaginatorIntl } from './app-paginator-intl';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {
  CustomCodeEditorIntlService,
  CustomTooltipIntlService,
  ServicesModule,
} from './services';
import { CustomMonacoProviderService } from './services/custom-monaco-provider';
import { SharedModule } from './shared';
import { en } from './translate/en.i18n';
import { zh } from './translate/zh.i18n';

export const DEFAULT_MONACO_OPTIONS = {
  fontSize: 12,
  folding: true,
  scrollBeyondLastLine: false,
  minimap: { enabled: false },
  find: { seedSearchStringFromSelection: false, autoFindInSelection: false },
  mouseWheelZoom: true,
  scrollbar: {
    vertical: 'visible' as 'auto' | 'visible' | 'hidden',
    horizontal: 'visible' as 'auto' | 'visible' | 'hidden',
  },
  fixedOverflowWidgets: true,
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ServicesModule,
    SharedModule,
    MessageModule,
    NotificationModule,
    // App routing module should stay at the bottom
    AppRoutingModule,
    ASMModule,
    CodeEditorModule.forRoot({
      baseUrl: 'monaco_lib/v1',
      defaultOptions: DEFAULT_MONACO_OPTIONS,
    }),
    TranslateModule.forRoot({
      loose: true,
      translations: { zh, en },
      remoteUrl: DEFAULT_REMOTE_URL,
    }),
  ],
  providers: [
    {
      provide: MonacoProviderService,
      useClass: CustomMonacoProviderService,
    },
    {
      provide: PaginatorIntl,
      useClass: AppPaginatorIntl,
    },
    {
      provide: TooltipCopyIntl,
      useClass: CustomTooltipIntlService,
    },
    {
      provide: CodeEditorIntl,
      useClass: CustomCodeEditorIntlService,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
