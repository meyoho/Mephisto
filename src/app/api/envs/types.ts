export interface Environments {
  ALAUDA_AUDIT_TTL?: string;
  SYNC_TKE?: string;
  LOGO_URL?: string;
  LABEL_BASE_DOMAIN?: string;
  GLOBAL_NAMESPACE?: string;
}
