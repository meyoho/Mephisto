import { ResourceListParams } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from '@asm/api/project/project.service';

import { isEqual } from 'lodash-es';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

import { ResourceList } from 'app/utils/resource-list';
@Component({
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectListComponent {
  list: ResourceList;
  columns = ['name', 'cluster', 'creationTimestamp'];
  keywords$ = this.route.queryParamMap.pipe(
    map(queryParams => queryParams.get('keywords')),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  fetchParams$ = this.keywords$.pipe(
    map(keywords => ({
      fieldSelector: keywords ? `metadata.name=${keywords}` : '',
    })),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly api: ProjectService,
  ) {
    this.list = new ResourceList({
      fetchParams$: this.fetchParams$,
      fetcher: this.fetchProjects.bind(this),
    });
  }

  fetchProjects = (params: ResourceListParams) => {
    params.limit = '20';
    return this.api.getProjects(params);
  };

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords },
      queryParamsHandling: 'merge',
    });
  }

  clustersPure = (clusters: Array<{ [key: string]: string }>) => {
    return clusters.map(item => item.name);
  };
}
