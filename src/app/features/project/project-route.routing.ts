import { RouterModule, Routes } from '@angular/router';

import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectListComponent } from './project-list/project-list.component';
const routes: Routes = [
  {
    path: '',
    component: ProjectListComponent,
  },
  {
    path: ':name',
    component: ProjectDetailComponent,
  },
];

export const ProjectRouteRoutes = RouterModule.forChild(routes);
