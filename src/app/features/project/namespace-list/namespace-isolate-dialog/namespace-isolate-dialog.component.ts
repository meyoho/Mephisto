import {
  K8sUtilService,
  KubernetesResource,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { Sidecar } from '@asm/api/namespace-isolate/namespace-isolate.types';
import { ProjectService } from '@asm/api/project/project.service';

import { get } from 'lodash-es';

@Component({
  templateUrl: './namespace-isolate-dialog.component.html',
  styleUrls: ['./namespace-isolate-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceIsolateDialogComponent implements OnInit {
  isOpenNamespace = false;
  loading = false;
  submitting = false;
  sidecar: Sidecar;

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      cluster: string;
      namespace: KubernetesResource;
    },
    private readonly dialogRef: DialogRef<NamespaceIsolateDialogComponent>,
    private readonly message: MessageService,
    private readonly api: ProjectService,
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    this.isOpenNamespace = this.checkNamespaceIsolate(this.data.namespace);
    this.cdr.markForCheck();
  }

  confirm() {
    const name = this.k8sUtil.getName(this.data.namespace);
    const annotations = {
      metadata: {
        annotations: {
          'sidecar-egress': this.isOpenNamespace ? 'enable' : 'disable',
        },
      },
    };
    this.submitting = true;
    this.api.updateNamespace(name, this.data.cluster, annotations).subscribe(
      () => {
        this.message.success(
          this.translate.get('set_namespace_visibility_succeeded'),
        );
        this.dialogRef.close(true);
      },
      () => {
        this.submitting = false;
        this.cdr.markForCheck();
      },
    );
  }

  switchValue() {
    this.isOpenNamespace = !this.isOpenNamespace;
  }

  private checkNamespaceIsolate(namespace: KubernetesResource) {
    return (
      get(namespace, 'metadata.annotations.sidecar-egress', '') === 'enable'
    );
  }
}
