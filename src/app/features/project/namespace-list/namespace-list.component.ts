import {
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  KubernetesResource,
  ResourceListParams,
  StringMap,
  TOKEN_BASE_DOMAIN,
  TranslateService,
} from '@alauda/common-snippet';
import {
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavControlService } from '@app/services';
import { RESOURCE_TYPES } from '@app/utils';
import { ResourceList } from '@app/utils/resource-list';
import { ProjectService } from '@asm/api/project/project.service';
import {
  NamespaceResponse,
  ResultResponse,
} from '@asm/api/project/project.types';

import { flattenDeep, isEqual } from 'lodash-es';
import { forkJoin, of, Subject } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  startWith,
} from 'rxjs/operators';

import { NamespaceIsolateDialogComponent } from './namespace-isolate-dialog/namespace-isolate-dialog.component';

@Component({
  selector: 'alo-namespace-list',
  templateUrl: './namespace-list.component.html',
  styleUrls: ['./namespace-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceListComponent implements OnInit {
  list: ResourceList;
  deploymented = false;
  submitting = false;
  loading = false;
  columns = [
    'name',
    'cluster',
    'serviceMesh',
    'create_by',
    'creationTimestamp',
    'action',
  ];

  @ViewChild('enableConfirmDialog', { static: true })
  enableConfirmDialog: TemplateRef<any>;

  enabled = false;
  dialogRef: DialogRef;
  destroy$ = new Subject<void>();
  namespace: KubernetesResource;
  @Input()
  projectName: string;

  @Input()
  clusters: string[];

  selectedCluster: string;
  nodata = true;
  cluster = this.route.snapshot.paramMap.get('cluster') || '';
  deployedMenu: StringMap = {};
  get dialogText() {
    return this.enabled
      ? {
          title: 'disable_service_mesh',
          content: 'disable_service_mesh_confirm',
          value: 'disabled',
          button: 'disable',
          icon: 'exclamation_triangle_s',
        }
      : {
          title: 'enable_service_mesh',
          content: 'enable_service_mesh_confirm',
          value: 'enabled',
          button: 'enable',
          icon: 'exclamation_circle_s',
        };
  }

  keywords$ = this.route.queryParamMap.pipe(
    map(queryParams => queryParams.get('keywords')),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  fetchParams$ = this.keywords$.pipe(
    map(keywords => ({
      fieldSelector: keywords ? `metadata.name=${keywords}` : '',
    })),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly dialog: DialogService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly api: ProjectService,
    private readonly navControl: NavControlService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {}

  updated$ = this.k8sPermission
    .isAllowed({
      type: RESOURCE_TYPES.NAMESPACE,
      action: K8sResourceAction.UPDATE,
    })
    .pipe(publishReplay(1), refCount());

  fetch = (params: ResourceListParams) => {
    return forkJoin(
      this.clusters.map(cluster => this.getNamespace(cluster, params)),
    ).pipe(
      map((result: NamespaceResponse[]) => {
        const data = {
          metadata: {},
          items: flattenDeep(result.map(ns => flattenDeep(ns.items))),
        };
        this.nodata = data.items.length === 0;
        this.loading = false;
        return data;
      }),
    );
  };

  ngOnInit(): void {
    this.getDeployed();
    this.list = new ResourceList({
      fetchParams$: this.fetchParams$,
      fetcher: this.fetch.bind(this),
    });
  }

  getDeployed() {
    this.loading = true;
    forkJoin((this.clusters || []).map(cluster => this.getApis(cluster)))
      .pipe(
        map((result: [{}]) =>
          result.reduce((prev, curr) => ({ ...curr, ...prev }), {}),
        ),
        catchError(() => of({})),
      )
      .subscribe(result => {
        this.deployedMenu = result;
        this.loading = false;
        this.cdr.markForCheck();
      });
  }

  private getApis(cluster: string) {
    return this.navControl.getClusterDeployed(cluster).pipe(
      catchError(() => of(false)),
      map(value => ({
        [cluster]: !!value,
      })),
    );
  }

  getNamespace(cluster: string, params: ResourceListParams) {
    return this.api.getNamespaces(this.projectName, cluster, params).pipe(
      catchError(() => of({ items: [] })),
      startWith({ items: [] }),
      map((res: NamespaceResponse) => {
        return {
          ...res,
          items: res.items.map(ns => {
            ns.metadata.labels.currendCluster = cluster;
            return ns;
          }),
        };
      }),
    );
  }

  enableConfirm(item: ResultResponse) {
    this.namespace = item.__original;
    this.selectedCluster = item.currendCluster;
    this.enabled = item.serviceMesh === 'enabled';
    this.dialogRef = this.dialog.open(this.enableConfirmDialog, {
      size: DialogSize.Medium,
    });
  }

  setVisibility(item: ResultResponse) {
    this.dialog
      .open(NamespaceIsolateDialogComponent, {
        size: DialogSize.Medium,
        data: {
          cluster: item.cluster,
          namespace: item.__original,
        },
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.list.reload();
        }
      });
  }

  serviceMeshStatus(item: ResultResponse, deployedMenu: StringMap) {
    const type = item.serviceMesh;
    if (!deployedMenu[item.cluster || item.currendCluster]) {
      return {
        status: 'cancel',
        text: 'not_deployed',
      };
    }
    return type === 'enabled'
      ? {
          status: 'success',
          text: 'enabled',
        }
      : {
          status: 'stopped',
          text: 'not_enabled',
        };
  }

  enableOrDisableServiceMesh(enabled: string) {
    this.submitting = true;
    const label = {
      metadata: {
        labels: {
          [`${this.baseDomain}/serviceMesh`]: enabled,
          'istio-injection': enabled,
        },
      },
    };
    this.updateNamespace(this.selectedCluster, label).subscribe(
      () => {
        this.list.reload();
        this.submitting = false;
        let massage = 'enable_service_success';
        if (enabled === 'disabled') {
          massage = 'disabled_service_success';
        }
        this.message.success(this.translate.get(massage));
        this.cdr.markForCheck();
        this.dialogRef.close();
      },
      () => {
        this.submitting = false;
        this.cdr.markForCheck();
      },
    );
  }

  updateNamespace(cluster: string, label: KubernetesResource) {
    const name = this.namespace.metadata.name;
    return this.api.updateNamespace(name, cluster, label);
  }

  dataPure = (resource: KubernetesResource[]) => {
    return resource && resource.map(this.toModelNamespace);
  };

  toModelNamespace = (resource: KubernetesResource) => {
    const meta = resource.metadata || {};
    const labels = meta.labels || {};
    return {
      currendCluster: labels.currendCluster,
      name: this.k8sUtil.getName(resource),
      displayName: this.k8sUtil.getDisplayName(resource),
      cluster: labels[`${this.baseDomain}/cluster`],
      serviceMesh: labels[`${this.baseDomain}/serviceMesh`],
      creator: this.k8sUtil.getCreator(resource),
      creationTimestamp: this.k8sUtil.getCreationTimestamp(resource),
      __original: resource,
    };
  };
}
