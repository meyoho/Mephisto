import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FeatureSharedCommonModule } from '@app/features-shared/common';
import { SharedModule } from '@app/shared';
import { ProjectService } from '@asm/api/project/project.service';

import { ClusterTagsLabelComponent } from './components/cluster-tags-label/cluster-tags-label.component';
import { NamespaceIsolateDialogComponent } from './namespace-list/namespace-isolate-dialog/namespace-isolate-dialog.component';
import { NamespaceListComponent } from './namespace-list/namespace-list.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectRouteRoutes } from './project-route.routing';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    FeatureSharedCommonModule,
    RouterModule,
    ProjectRouteRoutes,
  ],
  declarations: [
    ProjectListComponent,
    ProjectDetailComponent,
    NamespaceListComponent,
    ClusterTagsLabelComponent,
    NamespaceIsolateDialogComponent,
  ],
  providers: [ProjectService],
  entryComponents: [NamespaceIsolateDialogComponent],
})
export class ProjectModule {}
