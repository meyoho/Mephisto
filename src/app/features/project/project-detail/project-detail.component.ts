import { AsyncDataLoader } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from '@asm/api/project/project.service';

import { map, publishReplay, refCount } from 'rxjs/operators';
@Component({
  selector: 'alo-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectDetailComponent {
  name$ = this.route.paramMap.pipe(map(paramMap => paramMap.get('name')));
  clusters: string[];
  keywords$ = this.route.queryParamMap.pipe(
    map(paramMap => paramMap.get('keywords') || ''),
    publishReplay(1),
    refCount(),
  );
  constructor(
    private api: ProjectService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  fetchProject = (name: string) => this.api.getProjectDetail(name);

  dataLoader = new AsyncDataLoader({
    params$: this.name$,
    fetcher: this.fetchProject,
  });

  clustersPure = (clusters: { [key: string]: string }[]) => {
    if (!clusters) {
      return;
    }
    const clustersName = clusters.map(item => item.name);
    this.clusters = clustersName;
    return clustersName;
  };

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords },
      queryParamsHandling: 'merge',
    });
  }
}
