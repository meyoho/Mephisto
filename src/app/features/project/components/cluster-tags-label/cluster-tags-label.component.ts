import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'alo-cluster-tags-label',
  templateUrl: './cluster-tags-label.component.html',
  styleUrls: ['./cluster-tags-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterTagsLabelComponent {
  @Input()
  tagList: [string, string][] | string[];
  @Input()
  showAllCap = 2;
}
