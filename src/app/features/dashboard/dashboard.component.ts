import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent {
  project$ = this.route.paramMap.pipe(
    map(paramMap => paramMap.get('project')),
    publishReplay(1),
    refCount(),
  );

  constructor(private route: ActivatedRoute) {}
}
