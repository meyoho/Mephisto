import {
  K8sApiService,
  K8sUtilService,
  KubernetesResource,
} from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavControlService } from '@app/services';
import { ResourceType, RESOURCE_TYPES } from '@app/utils';
import { ProjectService } from '@asm/api/project/project.service';
import {
  NamespaceResponse,
  ProjectResponse,
} from '@asm/api/project/project.types';
import { PermissionType, permissionUrl, UrlParams } from '@asm/utils';

import { get } from 'lodash-es';
import { combineLatest, forkJoin, merge, Observable, of, Subject } from 'rxjs';
import {
  catchError,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import { Cluster } from 'app/typings';

interface ClusterOption {
  project_name: string;
  name: string;
  display_name: string;
}

interface NamespaceEntity {
  cluster: string;
  project: string;
  namespace: KubernetesResource;
}
interface NamespaceOption {
  name: string;
  display_name?: string;
  cluster_name: string;
  cluster_display_name?: string;
  create_time: string;
  service_mesh: {
    status: string;
    text: string;
  };
}

@Component({
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent implements OnInit, OnDestroy {
  private project$: Observable<ProjectResponse>;
  private readonly clusterSelect$ = new Subject<ClusterOption>();
  private readonly onDestroy$ = new Subject<void>();
  private projectName: string;

  columns = ['name', 'cluster', 'serviceMesh', 'create_time', 'action'];
  currentCluster: ClusterOption;
  projects: ProjectResponse[];

  projects$: Observable<ProjectResponse[]>;
  projectName$: Observable<string>;

  clusters$: Observable<ClusterOption[]>;
  clustersDeployedServiceMesh$: Observable<{}>;
  clustersLength$: Observable<number>;

  namespacesLength$: Observable<number>;
  filteredNamespaces$: Observable<NamespaceOption[]>;
  search$ = new Subject<string>();

  clustersDeployedStatus: { [key: string]: boolean };

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
    private readonly projectApi: ProjectService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly navControl: NavControlService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    this.initObservables();
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.project) {
        window.localStorage.setItem('project', params.project);
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  nameCompare(ns1: NamespaceOption, ns2: NamespaceOption) {
    return ns1.name.localeCompare(ns2.name);
  }

  trackByClusterName(_index: number, cluster: any) {
    return cluster.name;
  }

  trackByNamespaceUuid(_index: number, namespace: NamespaceOption) {
    return `${namespace.cluster_name}-${namespace.name}`;
  }

  projectSelected(project: string) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        project,
      },
    });
  }

  enterNamespace(namespace: NamespaceOption) {
    const params: UrlParams = {
      project: this.projectName,
      cluster: namespace.cluster_name,
      namespace: namespace.name,
    };
    this.router.navigate([
      `${permissionUrl(PermissionType.Servicemesh, params)}`,
    ]);
  }

  clusterSelected(cluster: ClusterOption) {
    if (
      (this.currentCluster &&
        cluster &&
        this.currentCluster.name === cluster.name) ||
      (!this.currentCluster && !cluster)
    ) {
      return;
    }
    this.clusterSelect$.next(cluster);
    this.currentCluster = cluster;
  }

  private initObservables() {
    this.projects$ = this.projectApi.getProjectsStore().pipe(
      map(res => res.items),
      tap(projects => {
        this.projects = projects;
      }),
    );

    this.projectName$ = this.activatedRoute.queryParams.pipe(
      map(params => {
        return params.project;
      }),
      tap(name => (this.projectName = name)),
    );

    this.project$ = combineLatest([this.projectName$, this.projects$]).pipe(
      switchMap(([name, projects]) => {
        const project = projects.find(
          item => this.k8sUtil.getName(item) === name,
        );
        if (project) {
          return of(project);
        }
      }),
      tap(() => {
        this.currentCluster = null;
      }),
    );

    this.clusters$ = combineLatest([
      this.project$,
      this.k8sApi
        .getGlobalResourceList<Cluster>({
          type: RESOURCE_TYPES.CLUSTER,
          namespaced: true,
        })
        .pipe(map(res => res.items)),
    ]).pipe(
      map(([project, clusters]) => {
        const clusterRefs = project.spec.clusters || [];
        return clusters
          .filter(c => {
            return clusterRefs.find(ref => ref.name === c.metadata.name);
          })
          .map(cluster => ({
            project_name: project.metadata.name,
            name: cluster.metadata.name,
            display_name: this.k8sUtil.getDisplayName(cluster),
          }));
      }),
      publishReplay(1),
      refCount(),
    );

    this.clustersLength$ = this.clusters$.pipe(map(r => r.length));

    this.clustersDeployedServiceMesh$ = this.clusters$.pipe(
      switchMap(res => {
        return forkJoin(
          (res || []).map(cluster => this.getApis(cluster.name)),
        ).pipe(
          map(result =>
            result.reduce((prev, curr) => ({ ...curr, ...prev }), {}),
          ),
          catchError(() => of({})),
        );
      }),
    );

    this.clustersDeployedServiceMesh$.subscribe(res => {
      this.clustersDeployedStatus = res;
      this.cdr.markForCheck();
    });

    const namespaces$ = merge(
      this.clusterSelect$,
      this.clusters$.pipe(map(() => null)),
    ).pipe(
      withLatestFrom(this.clusters$),
      switchMap(([cluster, clusters]) => {
        if (clusters.length) {
          const entities$ = cluster
            ? this.getNamespacesByCluster(cluster)
            : this.getNamespacesByAllClusters(clusters);

          return entities$.pipe(
            filter(r => !!r),
            map(entities => {
              return entities.map(entity =>
                this.getNamespaceOption(entity, clusters),
              );
            }),
          );
        } else {
          return of([]);
        }
      }),
      startWith([]),
      publishReplay(1),
      refCount(),
    );

    this.namespacesLength$ = namespaces$.pipe(
      map(namespaces => namespaces.length),
      publishReplay(1),
      refCount(),
    );

    this.filteredNamespaces$ = combineLatest([
      namespaces$,
      this.search$.pipe(startWith('')),
    ]).pipe(
      map(([namespaces, search]) =>
        search
          ? namespaces
              .filter((item: any) => item.name.includes(search.trim()))
              .sort(this.nameCompare)
          : namespaces,
      ),
      tap(() => {
        this.cdr.markForCheck();
      }),
    );
  }

  isClusterDeployedServiceMesh = (clusterName: string) => {
    return !!get(this.clustersDeployedStatus, clusterName);
  };

  serviceMeshStatus = (item: KubernetesResource) => {
    return this.k8sUtil.getLabel(item, 'serviceMesh') === 'enabled'
      ? {
          status: 'success',
          text: 'enabled',
        }
      : {
          status: 'stopped',
          text: 'not_enabled',
        };
  };

  private getNamespaceOption(
    entity: NamespaceEntity,
    clusters: ClusterOption[],
  ): NamespaceOption {
    const cluster = clusters.find(c => c.name === entity.cluster);
    return {
      name: entity.namespace.metadata.name,
      display_name: this.k8sUtil.getDisplayName(entity.namespace),
      cluster_name: entity.cluster,
      cluster_display_name: cluster ? cluster.display_name : '',
      create_time: entity.namespace.metadata.creationTimestamp,
      service_mesh: this.serviceMeshStatus(entity.namespace),
    };
  }

  private getNamespacesByCluster(cluster: ClusterOption) {
    return this.projectApi
      .getNamespaces(cluster.project_name, cluster.name)
      .pipe(
        catchError(() => of({ items: [] } as NamespaceResponse)),
        map(namespaces => {
          return namespaces.items.map(namespace => {
            return {
              cluster: cluster.name,
              project: cluster.project_name,
              namespace: namespace,
            } as NamespaceEntity;
          });
        }),
      );
  }

  private getNamespacesByAllClusters(clusters: ClusterOption[]) {
    return combineLatest(
      (clusters || []).map(cluster =>
        this.projectApi.getNamespaces(cluster.project_name, cluster.name).pipe(
          catchError(() => of({ items: [] } as NamespaceResponse)),
          startWith({ items: [] } as NamespaceResponse),
        ),
      ),
    ).pipe(
      map(clusterNamespaces => {
        return clusterNamespaces.reduce(
          (accum, clusterNamespacesItem, currentIndex) => {
            return [
              ...accum,
              ...clusterNamespacesItem.items.map(namespace => {
                return {
                  cluster: clusters[currentIndex].name,
                  project: clusters[currentIndex].project_name,
                  namespace: namespace,
                } as NamespaceEntity;
              }),
            ];
          },
          [],
        );
      }),
    );
  }

  private getApis(cluster: string) {
    return this.navControl.getClusterDeployed(cluster).pipe(
      catchError(() => of(false)),
      map(value => ({
        [cluster]: !!value,
      })),
    );
  }
}
