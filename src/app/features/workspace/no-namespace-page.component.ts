import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'alo-no-namespace-page',
  templateUrl: 'no-namespace-page.component.html',
  styleUrls: ['no-namespace-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NoNamespacePageComponent {
  constructor() {}
}
