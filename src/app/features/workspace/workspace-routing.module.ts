import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NoNamespacePageComponent } from './no-namespace-page.component';
import { UnopenedComponent } from './unopened/unopened.component';
import { WorkspaceComponent } from './workspace.component';
const routes: Routes = [
  {
    path: ':project',
    component: WorkspaceComponent,
    children: [
      { path: '', redirectTo: 'overview', pathMatch: 'full' },
      {
        path: 'overview',
        loadChildren: () =>
          import('../dashboard/dashboard.module').then(M => M.DashboardModule),
      },
      {
        path: 'no-namespace',
        component: NoNamespacePageComponent,
      },
      {
        path: 'clusters/:cluster/namespaces/:namespace/unopened/:deployed',
        component: UnopenedComponent,
      },
      {
        path: 'clusters/:cluster/namespaces/:namespace',
        children: [
          {
            path: 'servicemesh',
            loadChildren: () =>
              import(
                '@asm/features/servicemesh-userview/servicemesh-userview.module'
              ).then(M => M.ServicemeshUserviewModule),
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkspaceRoutingModule {}
