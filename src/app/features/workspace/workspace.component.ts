import { NamespaceIdentity, TranslateService } from '@alauda/common-snippet';
import { NavItemConfig } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import {
  FeatureGateService,
  LatestVisitedService,
  NavControlService,
  TemplateHolderType,
  UiStateService,
} from '@app/services';

import { BehaviorSubject, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
} from 'rxjs/operators';
const baseUrl = '/workspace';
@Component({
  templateUrl: 'workspace.component.html',
  styleUrls: ['workspace.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WorkspaceComponent {
  hide: boolean;
  filter$ = new BehaviorSubject<string>('');
  projectName$ = new BehaviorSubject(
    this.route.snapshot.paramMap.get('project'),
  );

  pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
    TemplateHolderType.PageHeaderContent,
  ).templatePortal$;

  get currentProject() {
    return this.route.snapshot.paramMap.get('project');
  }

  currentNamespace = this.getNavParams().name;
  cluster = this.getNavParams().cluster;

  currentNamespace$ = this.latestVisited.namespace$;

  showLoadingBar$ = this.uiState.showLoadingBar$;

  configs$ = this.navControl.getUserViewNavConfigs().pipe(
    distinctUntilChanged(),
    map(navConfigs => navConfigs.map(config => this.mapConfig(config))),
    publishReplay(1),
    refCount(),
  );

  activatedKey$ = combineLatest([
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map((event: NavigationEnd) => event.url),
      startWith(this.router.url),
      distinctUntilChanged(),
      map(getUrlKey),
    ),
    this.configs$.pipe(map(configs => this.flatConfigs(configs))),
  ]).pipe(
    map(([url, configs]) => {
      const matchedConfig = configs.find(config => url.startsWith(config.key));
      if (matchedConfig) {
        return matchedConfig.key;
      } else {
        return '';
      }
    }),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  filterEnabled = this.featureGate.filterEnabled;

  constructor(
    private readonly uiState: UiStateService,
    private readonly translate: TranslateService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly navControl: NavControlService,
    private readonly latestVisited: LatestVisitedService,
    private readonly featureGate: FeatureGateService,
  ) {
    this.hide = false;
  }

  toggleLanguage() {
    this.translate.toggleLocale();
  }

  handleActivatedNamespacedNavItemChange(
    item: NavItemConfig,
    namespace: NamespaceIdentity,
  ) {
    if (!namespace) {
      this.router.navigate([baseUrl, this.currentProject, 'no-namespace']);
      return;
    }
    const href = item.href;
    if (href.startsWith('http://') || href.startsWith('https://')) {
      window.open(href, '_blank');
    } else {
      this.router.navigate([
        baseUrl,
        this.currentProject,
        'clusters',
        namespace.cluster,
        'namespaces',
        namespace.name,
        ...href.split('/'),
      ]);
    }
  }

  projectSwitchHandle(name: string) {
    this.projectName$.next(name);
    this.latestVisited.setProject(name);
    this.latestVisited.clearNamespace();
    this.currentNamespace = null;
  }

  namespaceSwitchHandle(namespace: NamespaceIdentity) {
    const initNamespace = this.currentNamespace ? this.getNavParams() : null;
    this.latestVisited.setNamespace(namespace || initNamespace);
  }

  private flatConfigs<T extends NavItemConfig>(items: T[]): T[] {
    return items.reduce(
      (prevValue, currValue) => [
        ...prevValue,
        ...(currValue.children
          ? this.flatConfigs(currValue.children)
          : [currValue]),
      ],
      [],
    );
  }

  mapConfig(config: NavItemConfig): NavItemConfig {
    const children =
      config.children && config.children.map(item => this.mapConfig(item));
    const key = config.href;
    return {
      ...config,
      key,
      children,
      label: config.label || this.translate.get(config.name),
      group: config.name,
    };
  }

  getNavParams() {
    const matching = this.latestVisited.routeRegexp.exec(this.router.url);
    if (!matching) {
      return { cluster: '', name: '' };
    }
    const [, , cluster, namespace, ,] = matching;
    return { cluster, name: namespace };
  }
}

// TODO: condition not solid
function getUrlKey(url: string) {
  const key = url.slice(url.indexOf('/', baseUrl.length + 1) + 1);

  if (!key.startsWith('clusters/')) {
    return key;
  }

  return url.slice(
    url.indexOf('/', url.indexOf('/namespaces/') + '/namespaces/'.length) + 1,
  );
}
