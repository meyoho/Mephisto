import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
@Component({
  selector: 'alo-unopened',
  templateUrl: './unopened.component.html',
  styleUrls: ['./unopened.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class UnopenedComponent {
  deployed$ = this.route.paramMap.pipe(
    map(params => {
      if (params.get('deployed') === 'false') {
        return {
          text: 'service_mesh_unopened',
          srcType: 'closed',
        };
      }
      return {
        text: 'not_deployed_nav_not_available',
        srcType: 'unerected',
      };
    }),
  );

  constructor(private route: ActivatedRoute) {}
}
