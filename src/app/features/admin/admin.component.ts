import { Reason, TranslateService } from '@alauda/common-snippet';
import { NavItemConfig } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import {
  FeatureGateService,
  NavControlService,
  TemplateHolderType,
  UiStateService,
} from '@app/services';
import { GlobalPermissionService } from '@app/services/global-permission.service';

import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  takeUntil,
} from 'rxjs/operators';

@Component({
  templateUrl: 'admin.component.html',
  styleUrls: ['admin.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminComponent implements OnInit, OnDestroy {
  onDestroy$ = new Subject<void>();
  filterEnabled = this.featureGate.filterEnabled;
  reason = Reason;
  constructor(
    private readonly uiState: UiStateService,
    private readonly translate: TranslateService,
    private readonly navControl: NavControlService,
    private readonly router: Router,
    private readonly featureGate: FeatureGateService,
    public globalPermission: GlobalPermissionService,
  ) {}

  pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
    TemplateHolderType.PageHeaderContent,
  ).templatePortal$;

  showLoadingBar$ = this.uiState.showLoadingBar$;
  switch$ = new BehaviorSubject(null);

  configs$ = this.navControl.getAdminViewNavConfigs().pipe(
    distinctUntilChanged(),
    map(navConfigs => {
      return navConfigs.map(config => this.mapConfig(config));
    }),
    publishReplay(1),
    refCount(),
  );

  activatedKey$ = combineLatest([
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map((event: NavigationEnd) => event.url),
      startWith(this.router.url),
      distinctUntilChanged(),
      map(url => url.slice(url.indexOf('/', 1) + 1)),
    ),
    this.configs$.pipe(map(configs => this.flatConfigs(configs))),
  ]).pipe(
    map(([url, configs]) => {
      const matchedConfig = configs.find(config => url.includes(config.key));
      if (matchedConfig) {
        return matchedConfig.key;
      } else {
        return '';
      }
    }),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  ngOnInit() {
    this.switch$
      .pipe(
        filter(item => !!item),
        takeUntil(this.onDestroy$),
      )
      .subscribe((item: NavItemConfig) => {
        const href = item.href;
        if (href.startsWith('http://') || href.startsWith('https://')) {
          window.open(href, '_blank');
        }
        this.router.navigate(['/admin', href]);
      });
  }

  private flatConfigs(items: NavItemConfig[]): NavItemConfig[] {
    return items.reduce(
      (prevValue, currValue) => [
        ...prevValue,
        ...(currValue.children
          ? this.flatConfigs(currValue.children)
          : [currValue]),
      ],
      [],
    );
  }

  private mapConfig(config: NavItemConfig): NavItemConfig {
    const children =
      config.children && config.children.map(item => this.mapConfig(item));
    const key = config.href;
    return {
      ...config,
      children,
      key,
      label: config.label || this.translate.get(config.name),
    };
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
