import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from '@app/features/admin/admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      { path: '', redirectTo: 'projects', pathMatch: 'full' },
      {
        path: 'projects',
        loadChildren: () =>
          import('../project/project.module').then(M => M.ProjectModule),
      },
      {
        path: 'service-mesh',
        loadChildren: () =>
          import(
            '@asm/features/servicemesh-admin/service-mesh/service-mesh.module'
          ).then(M => M.ServiceMeshModule),
      },
      {
        path: 'clusters/:cluster',
        children: [
          {
            path: 'servicemesh',
            loadChildren: () =>
              import(
                '@asm/features/servicemesh-admin/servicemesh-admin.module'
              ).then(M => M.ServicemeshAdminModule),
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
