import {
  AuthorizationStateService,
  Locale,
  TranslateService,
} from '@alauda/common-snippet';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalPermissionService } from '@app/services/global-permission.service';

import { get, head } from 'lodash-es';
import {
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
} from 'rxjs/operators';

import { Environments } from 'app/api/envs/types';
import { ENVIRONMENTS } from 'app/services/services.module';

@Component({
  selector: 'alo-global-actions',
  templateUrl: 'global-actions.component.html',
  styleUrls: ['global-actions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('viewState', [
      state(
        'admin',
        style({
          left: '16px',
        }),
      ),
      state(
        'user',
        style({
          left: '0px',
        }),
      ),
      transition(':enter', animate(200)),
    ]),
  ],
})
export class GlobalActionsComponent {
  @Input() hideViewSwitch: boolean;
  get icon() {
    return `basic:switch_to_${
      this.translate.locale === Locale.ZH ? 'english' : 'chinese'
    }`;
  }

  get isAdminView() {
    if (!this.route.snapshot.parent) {
      return false;
    }

    const urlSegment = head(this.route.snapshot.parent.url) || { path: '' };

    return urlSegment.path === 'admin';
  }

  get currentView() {
    return this.isAdminView ? 'admin' : 'user';
  }

  get helpUrl() {
    return `${window.location.protocol}//${window.location.host}/asm-docs`;
  }

  editorOptions = { language: 'yaml' };
  editorConfig = {
    recover: false,
    diffMode: false,
  };

  yaml = '';
  originalYaml = '';

  user$ = this.auth
    .getTokenPayload<{
      email?: string;
      name: string;
      ext: { is_admin: boolean };
    }>()
    .pipe(
      filter(payload => !!payload),
      map(payload => ({
        email: payload.email || '',
        name: payload.name,
        isAdmin: get(payload, 'ext.is_admin'),
      })),
      startWith({ name: '', isAdmin: false }),
      publishReplay(1),
      refCount(),
    );

  constructor(
    @Inject(ENVIRONMENTS) public env: Environments,
    private readonly translate: TranslateService,
    private readonly route: ActivatedRoute,
    public auth: AuthorizationStateService,
    private readonly router: Router,
    public globalPermission: GlobalPermissionService,
  ) {}

  switchView() {
    this.router.navigateByUrl(this.isAdminView ? '/home/projects' : '/admin');
  }
}
