import {
  AuthorizationGuardService,
  LicenseGuardService,
  LicenseProductName,
  LICENSE_PRODUCT_NAME_TOKEN,
} from '@alauda/common-snippet';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { LicenseErrorPageComponent } from './features-shared/license-error-page/license-error-page.component';
import { LicenseErrorPageModule } from './features-shared/license-error-page/license-error-page.module';
import { ProjectNameGuard } from './project-name.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  // List lazy loading modules here:
  {
    path: 'home',
    loadChildren: () =>
      import('./features/home/home.module').then(M => M.HomeModule),
    canActivate: [
      LicenseGuardService,
      AuthorizationGuardService,
      ProjectNameGuard,
    ],
  },
  {
    path: 'admin',
    loadChildren: () =>
      import('./features/admin/admin.module').then(M => M.AdminModule),
    canActivate: [LicenseGuardService, AuthorizationGuardService],
    data: { admin: true },
  },
  {
    path: 'workspace',
    loadChildren: () =>
      import('./features/workspace/workspace.module').then(
        M => M.WorkspaceModule,
      ),
    canActivate: [LicenseGuardService, AuthorizationGuardService],
  },
  {
    path: 'terminal',
    loadChildren: () =>
      import('app/terminal/module').then(M => M.TerminalModule),
    canActivate: [LicenseGuardService, AuthorizationGuardService],
    data: {
      auth: true,
    },
  },
  {
    path: 'license-error',
    component: LicenseErrorPageComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      paramsInheritanceStrategy: 'always',
    }),
    LicenseErrorPageModule,
  ],
  exports: [RouterModule],
  providers: [
    {
      provide: LICENSE_PRODUCT_NAME_TOKEN,
      useValue: LicenseProductName.SERVICE_MESH,
    },
  ],
})
export class AppRoutingModule {}
