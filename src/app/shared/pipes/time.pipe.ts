import { FIELD_NOT_AVAILABLE_PLACEHOLDER } from '@alauda/common-snippet';
import { Pipe, PipeTransform } from '@angular/core';

import dayjs from 'dayjs';

/**
 * Transform a time string to human readable string.
 */
@Pipe({ name: 'aloTime' })
export class TimePipe implements PipeTransform {
  transform(utcStr: string): string {
    if (!utcStr) {
      return FIELD_NOT_AVAILABLE_PLACEHOLDER;
    }
    return dayjs.utc(utcStr).local().format('YYYY-MM-DD HH:mm:ss');
  }
}

@Pipe({ name: 'aloUnix' })
export class UnixPipe implements PipeTransform {
  transform(unixTime: string | number): string {
    if (!unixTime) {
      return '';
    }
    return dayjs.unix(Number(unixTime)).utc().toString();
  }
}
