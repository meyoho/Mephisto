import { K8sUtilService, KubernetesResource } from '@alauda/common-snippet';
import { Pipe, PipeTransform } from '@angular/core';

import { ASM_PREFIX } from './../../constants';
@Pipe({
  name: 'aloDisplayName',
})
export class DisplayNamePipe implements PipeTransform {
  constructor(private k8sUtil: K8sUtilService) {}
  transform(value: KubernetesResource, productPrefix: string = ASM_PREFIX) {
    return this.k8sUtil.getDisplayName(value, productPrefix);
  }
}

@Pipe({
  name: 'aloCreator',
})
export class CreatorPipe implements PipeTransform {
  constructor(private k8sUtil: K8sUtilService) {}
  transform(value: KubernetesResource, productPrefix: string = ASM_PREFIX) {
    return this.k8sUtil.getCreator(value, productPrefix);
  }
}
