import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CreatorPipe, DisplayNamePipe } from './k8s-relates.pipe';
import { MapValuesPipe } from './mapValue.pipe';
import { PurePipe } from './pure.pipe';
import { SIPrefixPipe } from './si-prefix.pipe';
import { TimePipe, UnixPipe } from './time.pipe';

const PIPES = [
  PurePipe,
  TimePipe,
  UnixPipe,
  MapValuesPipe,
  SIPrefixPipe,
  DisplayNamePipe,
  CreatorPipe,
];

@NgModule({
  imports: [CommonModule],
  declarations: PIPES,
  exports: PIPES,
})
export class PipesModule {}
