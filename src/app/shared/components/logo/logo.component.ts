import { Component, Inject } from '@angular/core';

import { Environments } from 'app/api/envs/types';
import { ENVIRONMENTS } from 'app/services/services.module';

@Component({
  selector: 'alo-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
})
export class LogoComponent {
  constructor(@Inject(ENVIRONMENTS) public env: Environments) {}
}
