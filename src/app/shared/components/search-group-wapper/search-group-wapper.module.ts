import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SearchGroupWapperComponent } from './component';

@NgModule({
  imports: [CommonModule],
  declarations: [SearchGroupWapperComponent],
  exports: [SearchGroupWapperComponent],
})
export class SearchGroupWapperModule {}
