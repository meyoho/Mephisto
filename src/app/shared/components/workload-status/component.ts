import { TranslateService } from '@alauda/common-snippet';
import { Status, StatusType } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';

import { debounce } from 'lodash-es';

import { GenericWorkloadStatus, WorkloadStatusEnum } from 'app/typings';
const STATUSES = {
  [WorkloadStatusEnum.running]: StatusType.Success,
  [WorkloadStatusEnum.pending]: StatusType.Primary,
  [WorkloadStatusEnum.stopped]: StatusType.Info,
  [WorkloadStatusEnum.killed]: StatusType.Info,
};

@Component({
  selector: 'alo-workload-status',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WorkloadStatusComponent implements OnChanges {
  constructor(private readonly translate: TranslateService) {}
  @Input()
  workloadStatus: GenericWorkloadStatus;

  @Input()
  inoperable: boolean;

  @Input() isFederated: boolean;

  @Input() isUpdateAllowed: boolean;

  @Output()
  change = new EventEmitter<number>();

  status: Status[];
  tooltip: string;
  desired: number;

  private readonly emitChange = debounce(
    this.change.emit.bind(this.change),
    1000,
  );

  ngOnChanges({ workloadStatus }: SimpleChanges) {
    if (workloadStatus && workloadStatus.currentValue) {
      this.onStatusChange();
    } else {
      this.desired = 0;
      this.tooltip = this.translate.get('running') + ': 0';
      this.status = [
        {
          scale: 1,
          type: StatusType.Info,
        },
      ];
    }
  }

  private onStatusChange() {
    this.desired = this.workloadStatus.desired;
    this.tooltip =
      this.translate.get('running') + ': ' + this.workloadStatus.current;
    if (!this.workloadStatus.desired && !this.workloadStatus.current) {
      this.status = [
        {
          scale: 1,
          type: StatusType.Info,
        },
      ];
      return;
    }
    if (this.workloadStatus.desired > this.workloadStatus.current) {
      this.status = [
        {
          scale: Number.parseFloat(
            (this.workloadStatus.current / this.workloadStatus.desired).toFixed(
              1,
            ),
          ),
          type: StatusType.Success,
        },
        {
          scale: Number.parseFloat(
            (
              (this.workloadStatus.desired - this.workloadStatus.current) /
              this.workloadStatus.desired
            ).toFixed(1),
          ),
          type:
            STATUSES[this.getWorkloadStatus(this.workloadStatus)] ||
            StatusType.Info,
        },
      ];
    } else {
      this.status = [
        {
          scale: 1,
          type: StatusType.Success,
        },
      ];
    }
  }

  decrease() {
    if (this.desired > 0) {
      --this.desired;
      this.emitChange(this.desired);
    }
  }

  increase() {
    ++this.desired;
    this.emitChange(this.desired);
  }

  getWorkloadStatus(status: GenericWorkloadStatus) {
    return status.status.toLowerCase() as WorkloadStatusEnum;
  }
}
