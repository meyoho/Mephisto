import {
  DisabledContainerModule,
  TranslateModule,
} from '@alauda/common-snippet';
import {
  ButtonModule,
  IconModule,
  StatusBarModule,
  TooltipModule,
} from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { WorkloadStatusComponent } from './component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    DisabledContainerModule,
    ButtonModule,
    StatusBarModule,
    TooltipModule,
    IconModule,
  ],
  declarations: [WorkloadStatusComponent],
  exports: [WorkloadStatusComponent],
})
export class WorkloadStatusModule {}
