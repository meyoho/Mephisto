import { TranslateModule } from '@alauda/common-snippet';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MenuTriggerModule } from '@app/shared/components/menu-trigger/menu-trigger.module';

import { BarStatusModule } from './bar-status/bar-status.module';
import { BreadcrumbModule } from './breadcrumb/breadcrumb.module';
import { ChartModule } from './chart/chart.module';
import { CircleChartModule } from './circle-chart/circle.module';
import { ConfirmDeleteModule } from './confirm-delete/confirm-delete.module';
import { LogViewModule } from './log-view';
import { LogoModule } from './logo/logo.module';
import { NoDataModule } from './no-data';
import { NotDeployedModule } from './not-deployed/not-deployed.module';
import { PasswordModule } from './password';
import { RangePickerModule } from './range-picker/range-picker.module';
import { ResourceListFooterModule } from './resource-list-footer/resource-list-footer.module';
import { SearchGroupWapperModule } from './search-group-wapper/search-group-wapper.module';
import { ShellModule } from './shell/shell.module';
import { StatusIconModule } from './status-icon/status-icon.module';
import { TagsLabelModule } from './tags-label/tags-label.module';
import { TooltipListModule } from './tooltip-list/tooltip-list.module';
import { WorkloadStatusModule } from './workload-status/workload-status.module';

const SHARED_MODULES = [
  PasswordModule,
  LogoModule,
  BreadcrumbModule,
  LogViewModule,
  MenuTriggerModule,
  ShellModule,
  StatusIconModule,
  TranslateModule,
  NoDataModule,
  SearchGroupWapperModule,
  NotDeployedModule,
  ResourceListFooterModule,
  ConfirmDeleteModule,
  CircleChartModule,
  TagsLabelModule,
  TooltipListModule,
  ChartModule,
  BarStatusModule,
  RangePickerModule,
  WorkloadStatusModule,
];

@NgModule({
  imports: [CommonModule, ...SHARED_MODULES],
  exports: [...SHARED_MODULES],
})
export class ComponentsModule {}
