import { TranslateModule } from '@alauda/common-snippet';
import {
  ButtonModule,
  DialogModule,
  IconModule,
  InputModule,
} from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ConfirmDeleteComponent } from './confirm-delete.component';

@NgModule({
  imports: [
    ButtonModule,
    CommonModule,
    TranslateModule,
    IconModule,
    TranslateModule,
    DialogModule,
    InputModule,
    FormsModule,
  ],
  declarations: [ConfirmDeleteComponent],
  exports: [ConfirmDeleteComponent],
})
export class ConfirmDeleteModule {}
