import { IconModule, TagModule, TooltipModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PipesModule } from '../../pipes/pipes.module';
import { TagsLabelComponent } from './tags-label.component';

@NgModule({
  imports: [CommonModule, TagModule, TooltipModule, IconModule, PipesModule],
  declarations: [TagsLabelComponent],
  exports: [TagsLabelComponent],
})
export class TagsLabelModule {}
