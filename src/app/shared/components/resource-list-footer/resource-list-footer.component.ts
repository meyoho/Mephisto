import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { ResourceList } from 'app/utils/resource-list';
import { Observable, combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  take,
} from 'rxjs/operators';

@Component({
  selector: 'alo-resource-list-footer',
  templateUrl: 'resource-list-footer.component.html',
  styleUrls: ['resource-list-footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceListFooterComponent implements OnInit {
  @Input()
  list: ResourceList;
  @Input()
  resourceName = '';

  isEmptyList$: Observable<boolean>;

  ngOnInit() {
    this.isEmptyList$ = combineLatest(
      this.list.items$.pipe(map(items => items.length)),
      this.list.hasMore$,
      this.list.loading$,
      this.list.loadingError$,
    ).pipe(
      map(([length, hasMore, loading, loadingError]) => {
        return !(length || hasMore || loading || loadingError);
      }),
      distinctUntilChanged(),
      publishReplay(1),
      refCount(),
    );
  }

  retry() {
    this.list.items$.pipe(take(1)).subscribe(items => {
      if (items.length) {
        this.list.loadMore();
      } else {
        this.list.reload();
      }
    });
  }
}
