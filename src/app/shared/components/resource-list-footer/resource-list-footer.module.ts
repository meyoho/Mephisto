import { TranslateModule } from '@alauda/common-snippet';
import { BackTopModule, ButtonModule, IconModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DirectivesModule } from '../../directives/directives.module';
import { ResourceListFooterComponent } from './resource-list-footer.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    IconModule,
    ButtonModule,
    BackTopModule,
    DirectivesModule,
  ],
  declarations: [ResourceListFooterComponent],
  exports: [ResourceListFooterComponent],
})
export class ResourceListFooterModule {}
