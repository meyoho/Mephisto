import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'alo-tooltip-list',
  templateUrl: './tooltip-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TooltipListComponent {
  @Input() list: string[];
}
