import { TranslateModule } from '@alauda/common-snippet';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TooltipListComponent } from './tooltip-list.component';

@NgModule({
  imports: [CommonModule, TranslateModule],
  declarations: [TooltipListComponent],
  exports: [TooltipListComponent],
})
export class TooltipListModule {}
