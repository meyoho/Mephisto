import { TranslateService } from '@alauda/common-snippet';
import { NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { calculateStep } from '@app/shared/components/chart';
import { dateFormat } from '@app/utils/unit';

import confirmDatePlugin from 'flatpickr/dist/plugins/confirmDate/confirmDate';

import { CalendarData, TIMERANGE } from './range-picker.types';
import { getTimeValueOf, PICKER_TIME_RANGES } from './unit';

@Component({
  selector: 'alo-range-picker',
  templateUrl: './range-picker.component.html',
  styleUrls: ['./range-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RangePickerComponent {
  @Input()
  set defaultRange(ranges: TIMERANGE) {
    this.range = ranges || TIMERANGE['30M'];
  }

  @Input()
  set date(date: CalendarData) {
    if (date) {
      this.calendarData = date;
      return;
    }
    const initTime = PICKER_TIME_RANGES.find(item => item.name === this.range);
    this.calendarData = {
      start: initTime.start(),
      end: initTime.end(),
      step: initTime.step,
    };
    this.broadcast();
  }

  @Output() broadcastTime = new EventEmitter();
  range = TIMERANGE['30M'];
  tiemRanges = PICKER_TIME_RANGES;
  calendarData: CalendarData = {
    start: '',
    end: '',
    step: 1,
  };

  customDateTimeOptions = {
    enableTime: true,
    enableSeconds: true,
    time_24hr: true,
    plugins: [
      confirmDatePlugin({
        confirmIcon: '',
        showAlways: true,
      }),
    ],
  };

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
    private readonly notification: NotificationService,
  ) {}

  changeRangeType($event: string) {
    const filter = PICKER_TIME_RANGES.find(item => item.name === $event);
    if (filter && filter.start) {
      this.calendarData.start = filter.start();
      this.calendarData.end = filter.end();
      this.calendarData.step = filter.step;
    }
    this.broadcast();
    this.cdr.markForCheck();
  }

  startTimeSelect(value: string) {
    this.calendarData.start = value;
    this.range = TIMERANGE.CUSTOM;
    this.broadcast();
    this.cdr.markForCheck();
  }

  endTimeSelect(value: string) {
    this.calendarData.end = value;
    this.range = TIMERANGE.CUSTOM;
    this.broadcast();
    this.cdr.markForCheck();
  }

  broadcast() {
    const start = getTimeValueOf(this.calendarData.start);
    const end = getTimeValueOf(this.calendarData.end);
    if (start >= end) {
      this.notification.warning(this.translate.get('timerange_warning'));
      return;
    }
    const step = calculateStep(+start, +end);
    this.calendarData.step = step;
    this.broadcastTime.next({
      date: this.calendarData,
      dateValueOf: { start, end },
      range: this.range,
    });
  }

  handelDate(date: CalendarData) {
    if (typeof date.start === 'number' && typeof date.end === 'number') {
      return {
        start: dateFormat(date.start),
        end: dateFormat(date.end),
        step: date.step,
      };
    }
    return date;
  }
}
