import dayjs, { OpUnitType } from 'dayjs';
import { PickerTimeRanges } from './range-picker.types';
const FORMAT = 'YYYY-MM-DD HH:mm:ss';
export const getTimeRanges = (value: number, unit: OpUnitType) => {
  return {
    start: () =>
      dayjs()
        .subtract(value, unit)
        .format(FORMAT),
    end: () => dayjs().format(FORMAT),
  };
};

export const PICKER_TIME_RANGES: PickerTimeRanges[] = [
  {
    name: 'last_1_minutes',
    ...getTimeRanges(1, 'minute'),
    step: 1,
  },
  {
    name: 'last_10_minutes',
    ...getTimeRanges(10, 'minute'),
    step: 1,
  },
  {
    name: 'last_30_minutes',
    ...getTimeRanges(30, 'minute'),
    step: 1,
  },
  {
    name: 'last_1_hour',
    ...getTimeRanges(1, 'hour'),
    step: 1,
  },
  {
    name: 'last_3_hours',
    ...getTimeRanges(3, 'hour'),
    step: 1,
  },
  {
    name: 'last_6_hours',
    ...getTimeRanges(6, 'hour'),
    step: 1,
  },
  {
    name: 'last_12_hours',
    ...getTimeRanges(12, 'hour'),
    step: 10,
  },
  {
    name: 'last_1_day',
    ...getTimeRanges(1, 'day'),
    step: 30,
  },
  {
    name: 'last_3_days',
    ...getTimeRanges(3, 'day'),
    step: 30,
  },
  {
    name: 'last_7_days',
    ...getTimeRanges(7, 'day'),
    step: 60,
  },
  {
    name: 'last_30_days',
    ...getTimeRanges(30, 'day'),
    step: 60,
  },
  {
    name: 'custom_tiem',
    step: 60,
  },
];

export function getTimeValueOf(value: string) {
  return ''+dayjs(value).valueOf() / 1000;
}