import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RangePickerComponent } from './range-picker.component';
import { IconModule, InputModule, SelectModule } from '@alauda/ui';
import { TranslateModule } from '@alauda/common-snippet';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from '../../directives/directives.module';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  imports: [
    CommonModule,
    IconModule,
    InputModule,
    SelectModule,
    TranslateModule,
    FormsModule,
    DirectivesModule,
    FlexLayoutModule
  ],
  declarations: [RangePickerComponent],
  exports: [RangePickerComponent],
})
export class RangePickerModule { }
