
export enum  TIMERANGE {
  '1M' = 'last_1_minutes',
  '10M' = 'last_10_minutes',
  '30M' = 'last_30_minutes',
  '1H' = 'last_1_hour',
  '3H' = 'last_3_hours',
  '6H' = 'last_6_hours',
  '12H' = 'last_12_hours',
  '1D' = 'last_1_days',
  '3D' = 'last_3_days',
  '7D' = 'last_7_days',
  '30D' = 'last_30_days',
  'CUSTOM' = 'custom_tiem'
}


export interface PickerTimeRanges {
  name: string;
  start?: () => string;
  end?: () => string;
  step?: number;
}
export interface CalendarData {
  start?: string;
  end?: string;
  step: number;
}

export interface DateBroadcastValue {
  date: CalendarData,
  dateValueOf: {
    start: number;
    end: number;
  }
  range: TIMERANGE
}