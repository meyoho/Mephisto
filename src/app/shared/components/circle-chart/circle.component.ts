import { ObservableInput } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { arc } from 'd3-shape';
import { last } from 'lodash-es';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

interface CircleParam {
  color?: string;
  value: number;
  status?: 'success' | 'warning' | 'error' | 'pending' | 'unknown';
}

@Component({
  selector: 'alo-circle-chart',
  templateUrl: './circle.component.html',
  styleUrls: ['./circle.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CircleChartComponent {
  @Input()
  text: string;
  @Input()
  hint: string;
  @Input()
  size = 120;
  @Input()
  displayTooltip = false;
  @ObservableInput(true)
  private circleParams$: Observable<CircleParam[]>;
  @Input()
  circleParams: CircleParam[];
  tipValue: number;
  tipHidden = false;
  tipLocation = { x: 0, y: 0, value: 0 };
  arcGenerator = arc();

  animatedChart$ = this.circleParams$.pipe(
    delay(0),
    map(params => {
      const availableParams = params.filter(param => !!param.value);
      const total = availableParams.reduce((accum, param) => {
        return accum + param.value;
      }, 0);
      const angles = this.handleAngles(total, availableParams);
      return angles.reduce((prev, curr) => {
        return [
          ...prev,
          {
            color: curr.color,
            status: curr.status,
            value: curr.value,
            path: this.arcGenerator({
              innerRadius: this.size / 2 - 12,
              outerRadius: this.size / 2,
              startAngle: curr.startAngle,
              endAngle: curr.endAngle,
            }),
          },
        ];
      }, []);
    }),
  );

  get translate() {
    return `translate(${this.size / 2},${this.size / 2})`;
  }

  trackByFn(index: number) {
    return index;
  }

  mouseoverEvent(event: any, value: number) {
    if (!this.displayTooltip) {
      return;
    }
    this.tipHidden = false;
    this.tipValue = value;
    this.tipLocation.x = event.x;
    this.tipLocation.y = event.y;
  }

  mouseoutEvent() {
    if (!this.displayTooltip) {
      return;
    }
    this.tipHidden = true;
  }

  private handleAngles(total: number, params: CircleParam[]) {
    return params.reduce(
      (
        prev: Array<{
          color: string;
          status: 'success' | 'warning' | 'error' | 'pending' | 'unknown';
          value: number;
          startAngle: number;
          endAngle: number;
        }>,
        curr,
      ) => {
        return [
          ...prev,
          {
            color: curr.color,
            status: curr.status || '',
            value: curr.value,
            startAngle: prev.length !== 0 ? last(prev).endAngle : 0,
            endAngle:
              prev.length !== 0
                ? last(prev).endAngle + this.processAngle(curr.value / total)
                : this.processAngle(curr.value / total),
          },
        ];
      },
      [],
    );
  }

  private processAngle(percent: number) {
    return 2 * Math.PI * percent;
  }
}
