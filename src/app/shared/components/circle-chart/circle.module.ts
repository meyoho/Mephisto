import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DirectivesModule } from '../../directives/directives.module';
import { CircleChartComponent } from './circle.component';

@NgModule({
  imports: [CommonModule, DirectivesModule],
  declarations: [CircleChartComponent],
  exports: [CircleChartComponent],
})
export class CircleChartModule {}
