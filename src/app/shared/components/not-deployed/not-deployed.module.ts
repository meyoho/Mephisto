import { TranslateModule } from '@alauda/common-snippet';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { NotDeployedComponent } from './not-deployed.component';

@NgModule({
  imports: [CommonModule, TranslateModule],
  declarations: [NotDeployedComponent],
  exports: [NotDeployedComponent],
})
export class NotDeployedModule {}
