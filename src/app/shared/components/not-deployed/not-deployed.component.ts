import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
@Component({
  selector: 'alo-not-deployed',
  templateUrl: './not-deployed.component.html',
  styleUrls: ['./not-deployed.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotDeployedComponent {
  imgSrcType = {
    unerected: 'icons/microservice/unerected.svg',
    closed: 'icons/microservice/resource_closed.svg',
  };
  @Input()
  text: string;
  @Input()
  set srcType(type: 'unerected' | 'closed') {
    if (type) {
      this.imgSrc = this.imgSrcType[type];
    }
  }
  imgSrc = this.imgSrcType.unerected;
  constructor() {}
}
