import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BarStatusComponent } from './bar-status.component';
import { TranslateModule } from '@alauda/common-snippet';
import { StatusBarModule, TooltipModule } from '@alauda/ui';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    StatusBarModule,
    TooltipModule
  ],
  declarations: [BarStatusComponent],
  exports: [BarStatusComponent]
})
export class BarStatusModule { }
