import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { StatusType } from '@alauda/ui';
import {
  HttpCode,
  RequestStat,
} from '@asm/api/service-topology/service-topology-api.types';
@Component({
  selector: 'alo-bar-status',
  templateUrl: './bar-status.component.html',
  styleUrls: ['./bar-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BarStatusComponent {
  @Input()
  title: string;
  @Input()
  get data() {
    return this._data;
  }
  set data(value: RequestStat[]) {
    if(value && value.length > 0) {
      this._data = [...value].sort((a, b) => {
        return Number(b.name.charAt(5)) - Number(a.name.charAt(5));
      });
    } else {
      this._data = []
    }
  }
  _data: RequestStat[] = [];

  get total() {
    return this.data.reduce((prev, cur) => prev + cur.count, 0);
  }

  get errorCount() {
    return this.data
      .filter(
        item =>
          item.name !== HttpCode.HTTP_2XX && item.name !== HttpCode.HTTP_3XX,
      )
      .reduce((prev, cur) => prev + cur.count, 0);
  }

  get successCount() {
    return this.total - this.errorCount;
  }

  get errorRate() {
    return this.total ? this.errorCount / this.total : 0;
  }

  get successRate() {
    return this.total ? this.successCount / this.total : 0;
  }

  get status() {
    return this.data.map(req => {
      return {
        scale: +((req.count / this.total) * 100).toFixed(2),
        ...this.getStatusType(req.name),
      };
    });
  }

  statusMap = {
    [HttpCode.HTTP_2XX]: 'success',
    [HttpCode.HTTP_3XX]: 'notice',
    [HttpCode.HTTP_4XX]: 'warning',
    [HttpCode.HTTP_5XX]: 'error',
  };

  translateMap = {
    [HttpCode.HTTP_2XX]: 'http_2xx',
    [HttpCode.HTTP_3XX]: 'http_3xx',
    [HttpCode.HTTP_4XX]: 'http_4xx',
    [HttpCode.HTTP_5XX]: 'http_5xx',
  };

  getStatusType(code: HttpCode) {
    switch (code) {
      case HttpCode.HTTP_2XX:
        return { type: StatusType.Success };
      case HttpCode.HTTP_3XX:
        return { class: 'notice' };
      case HttpCode.HTTP_4XX:
        return { type: StatusType.Warning };
      case HttpCode.HTTP_5XX:
        return { type: StatusType.Error };
    }
  }


}
