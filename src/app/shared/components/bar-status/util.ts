export enum HttpCode {
  HTTP_2XX = 'http_2xx',
  HTTP_3XX = 'http_3xx',
  HTTP_4XX = 'http_4xx',
  HTTP_5XX = 'http_5xx',
}

export function getRequestStat(requests: { [key: string]: number } = {}) {
  const httpCodes = Object.keys(requests);
  return httpCodes.map((code: HttpCode) => {
    return {
      name: code,
      count: requests[code],
    };
  });
}
