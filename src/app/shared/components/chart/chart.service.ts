import { Injectable } from '@angular/core';

import { scaleLinear } from 'd3-scale';
import { maxBy } from 'lodash-es';
import { Subject } from 'rxjs';

import {
  ChartOptions,
  DataSource,
  Duration,
  SourceValue,
  View,
} from './chart.types';
import { basics, xTickIntervals } from './utils';
@Injectable({
  providedIn: 'root',
})
export class ChartService {
  legendMap: Set<string> = new Set();
  legend$ = new Subject();
  options: ChartOptions[] = [];
  yMax = 0;
  render$ = new Subject<void>();
  disabledTranslate = false;
  catchLegend: string[] = [];
  dataSource: DataSource = {
    data: [],
    total: 0,
    min: 0,
    max: 0,
    avg: null,
  };

  areaSize: View = {
    width: 0,
    height: 0,
  };

  startPoint = { x: 0, y: 0 };
  duration: Duration = {
    period: '1m',
    value: 1,
    step: 1,
  };

  disabledLegend: string[] = [];

  get x() {
    return scaleLinear()
      .domain([0, this.dataSource.total - 1])
      .range([0, this.areaSize.width]);
  }

  get y() {
    return scaleLinear()
      .domain([0, this.dataSource.max])
      .range([this.startPoint.y, basics.xLegendHeight])
      .nice();
  }

  get fields() {
    return this.options.map(item => item.field);
  }

  initLegend(data: ChartOptions[] = []) {
    this.options = data;
    this.legendMap = new Set(data.map(item => item.field));
  }

  initAreaSize(clientWidth = 0, clientHeight = 0) {
    const { top, bottom, left, right } = basics.margin;
    const width = clientWidth - left - right;
    const height = clientHeight - top - bottom - basics.xLegendHeight;
    this.areaSize = { width, height };
  }

  initStartPoint() {
    this.startPoint.y = this.areaSize.height + basics.xLegendHeight;
    this.render$.next();
  }

  source(data: SourceValue[] = [], init = true) {
    this.disabledLegend = [];
    if (init) {
      this.legendMap = new Set(
        this.options.reduce(
          (prev, curr) =>
            this.catchLegend.includes(curr.field)
              ? prev
              : [...prev, curr.field],
          [],
        ),
      );
    }
    const fields = [...this.legendMap] || [];
    const getValue = (value: { [key: string]: any }) => {
      if (fields.length === 0 || !value) {
        return [0];
      }
      return fields.map(key => value[key]);
    };
    const max = maxBy(data, (item: { [key: string]: any }) => {
      const values = getValue(item);
      return Math.max(...values);
    });
    this.options.forEach(item => {
      if (
        !Math.max(...data.map(value => value[item.field])) ||
        data.length === 0
      ) {
        this.disabledLegend.push(item.field);
        if (data && data.length > 0) {
          this.legendMap.delete(item.field);
        }
      }
    });
    const avg = fields.reduce(
      (prev: { [key: string]: number }, key: string) => {
        const value =
          data.reduce((prev, cur) => prev + cur[key], 0) / data.length;
        return {
          ...prev,
          ...{ [key]: +value.toFixed(2) },
        };
      },
      {},
    );
    this.yMax = Math.max(...getValue(max));
    this.dataSource = {
      ...this.dataSource,
      data: data,
      total: data.length,
      max: this.yMax,
      avg,
    };
    this.render$.next();
  }

  getFormat() {
    const formats = {
      m: 'MM-DD HH:mm:ss',
      h: 'MM-DD HH:mm',
      d: 'MM-DD',
    };
    return formats[xTickIntervals[this.duration.period].unit];
  }

  getMode = () => {
    const interval = xTickIntervals[this.duration.period];
    const step = this.duration.step;
    const perUnitDataCount = {
      m: 1 / step,
      h: 60 / step,
      d: (24 * 60) / step,
    };
    return interval.step * perUnitDataCount[interval.unit];
  };

  switchLegend(type: string) {
    if (this.disabledLegend.includes(type)) {
      return;
    }
    const { legendMap, legend$ } = this;
    if (!legendMap.has(type)) {
      legendMap.add(type);
    } else {
      legendMap.delete(type);
    }
    legend$.next(type);
  }
}
