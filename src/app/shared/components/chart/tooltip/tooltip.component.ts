import { TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import { dateFormat } from '@app/utils/unit';

import { get } from 'lodash-es';
import { map, tap } from 'rxjs/operators';

import { basics } from '../utils';
import { ChartService } from './../chart.service';
interface PointerLine {
  x1: number;
  y1: number;
  x2: number;
  y2: number;
}
interface FlowCircle {
  cx: number;
  cy: number;
  name: string;
  stroke: string;
}
interface Tooltip {
  width: number;
  height: number;
  transform: string;
  data: {
    date: string;
    text: Array<{ name: string; value: number; key: string }>;
    error_rate: string;
  };
}
@Component({
  selector: 'g[alo-tooltip]',
  templateUrl: './tooltip.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TooltipComponent {
  @Input() unit: string;
  show = false;
  basics = basics;
  pointerX = 0;
  pointerY = 0;
  pointerLine: PointerLine;
  flowCircle: FlowCircle[];
  tooltip: Tooltip;
  service$ = this.service.render$.pipe(
    map(() => this.service),
    tap(() => {
      this.pointerLine = this.getPointerLine();
      this.flowCircle = this.getFlowCircle();
      this.tooltip = this.getTooltip();
      this.cdr.markForCheck();
    }),
  );

  getPointerLine() {
    return {
      x1: this.pointerX,
      y1: basics.xLegendHeight,
      x2: this.pointerX,
      y2: this.service.startPoint.y,
    };
  }

  getFlowCircle() {
    if (this.service.dataSource.data.length === 0) {
      return;
    }
    const index = Math.min(
      this.service.dataSource.total - 1,
      Math.floor(this.service.x.invert(this.pointerX)),
    );

    const { fields, dataSource, y, options, disabledLegend } = this.service;
    return fields.reduce((prev, curr) => {
      if (disabledLegend.includes(curr)) {
        return prev;
      }
      const data = dataSource.data[index][curr];
      const strokeVal = options.find(item => item.field === curr);
      return [
        ...prev,
        {
          cx: this.pointerX,
          cy: y(data),
          name: curr,
          stroke: strokeVal.stroke.value || strokeVal.stroke.start,
        },
      ];
    }, []);
  }

  getTooltip() {
    if (this.service.dataSource.data.length === 0) {
      return null;
    }
    const ret = {
      width: 151,
      height: 32,
      transform: 'translate(0, 0)',
      data: {
        date: 'unknown',
        text: [{ name: 'inflow', value: 0, key: '' }],
        error_rate: '',
      },
    };
    const {
      fields,
      dataSource,
      x,
      legendMap,
      areaSize,
      options,
      disabledLegend,
    } = this.service;

    const index = Math.min(
      dataSource.total - 1,
      Math.floor(x.invert(this.pointerX)),
    );
    ret.height += legendMap.size * 20;
    ret.data.text = options.reduce((prev, curr) => {
      if (legendMap.has(curr.field) && !disabledLegend.includes(curr.field)) {
        const title = get(
          this.service.options.find(item => item.field === curr.field),
          'title',
          curr.field,
        );
        const strokeVal = options.find(item => item.field === curr.field);
        return [
          ...prev,
          {
            name: curr.field,
            key: title,
            value: (+(dataSource.data[index][curr.field] || '')).toFixed(2),
            fill: strokeVal.stroke.value || strokeVal.stroke.start,
          },
        ];
      }
      return prev;
    }, []);

    const fontSize = 12;
    const maxLength = Math.max(
      ...ret.data.text.map(
        item =>
          item.key.length +
          String(item.value).length +
          String(this.unit).length,
      ),
    );
    const width = (maxLength / 2) * fontSize;
    ret.width = ret.width > width ? ret.width : width;
    if (
      (legendMap.has('request_rate_in') || legendMap.has('request_rate_out')) &&
      legendMap.size > 1 &&
      disabledLegend.length === 0
    ) {
      ret.height += 20;
      const errorrRate =
        dataSource.data[index][fields[1]] / dataSource.data[index][fields[0]];
      const errorValue = errorrRate === Infinity ? 100 : errorrRate;
      ret.data.error_rate = this.translate.get('error_rate_value', {
        errorRate: +(Number.isNaN(errorValue) ? 0 : errorValue * 100).toFixed(
          2,
        ),
      });
    }
    const timestamp = dataSource.data[index].date;
    ret.data.date = dateFormat(timestamp);
    const offsetX = Math.max(
      16,
      Math.min(areaSize.width - 20 - ret.width, this.pointerX + 16),
    );
    const offsetY = Math.max(
      0,
      Math.min(areaSize.height - ret.height, this.pointerY - ret.height / 2),
    );
    ret.transform = `translate(${offsetX} ${offsetY})`;
    return ret;
  }

  constructor(
    public readonly service: ChartService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  onMouseMove(event: any) {
    this.pointerX = event.offsetX - basics.margin.left;
    this.pointerY = event.offsetY;
    this.pointerLine = this.getPointerLine();
    this.flowCircle = this.getFlowCircle();
    this.tooltip = this.getTooltip();
    this.cdr.markForCheck();
  }
}
