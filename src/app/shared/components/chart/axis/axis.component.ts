import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { dateFormat, toFixed } from '@app/utils/unit';

import { range } from 'd3-array';
import { map, publishReplay, refCount } from 'rxjs/operators';

import { basics } from '../utils';
import { ChartService } from './../chart.service';
@Component({
  selector: 'g[alo-axis]',
  templateUrl: './axis.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AxisComponent {
  @Input() isPercent = false;
  yAxis$ = this.service.render$.pipe(
    map(() => this.getYAxis()),
    publishReplay(1),
    refCount(),
  );

  xAxis$ = this.service.render$.pipe(
    map(() => this.getXAxis()),
    publishReplay(1),
    refCount(),
  );

  getYAxis() {
    const { x, y } = this.service.startPoint;
    const { yMax } = this.service;
    const ticks = [...range(0, yMax, yMax / (basics.yTickNumber - 1)), yMax];
    return {
      ticks: ticks.map(value => ({
        value: toFixed(value),
        y: this.service.y(value),
      })),
      path: `M${x} ${y}L${x} ${basics.xLegendHeight}Z`,
    };
  }

  getXAxis() {
    const { data } = this.service.dataSource;
    const { x, y } = this.service.startPoint;
    const ticksAll = data.map((item, i) => {
      return {
        date: dateFormat(item.date, this.service.getFormat()),
        x: this.service.x(i),
        y,
      };
    });
    const ticks = ticksAll.filter((_, i) => i % this.service.getMode() === 0);
    return {
      ticks,
      path: `M${x} ${y}L${this.service.areaSize.width} ${y}Z`,
    };
  }

  constructor(private readonly service: ChartService) {}

  conversionMaximum(value: number) {
    const unit = 'k';
    if (value >= 1000) {
      const num = +(value / 1000).toFixed(2);
      return `${num}${unit}`;
    }
    return value;
  }
}
