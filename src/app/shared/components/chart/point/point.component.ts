import { ChangeDetectionStrategy, Component } from '@angular/core';

import { map } from 'rxjs/operators';

import { ChartService } from '../chart.service';
@Component({
  selector: 'g[alo-point]',
  templateUrl: './point.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PointComponent {
  dots$ = this.service.render$.pipe(
    map(() => {
      return this.getDots();
    }),
  );

  constructor(private readonly service: ChartService) {}

  getDots() {
    const {
      dataSource,
      options,
      getMode,
      startPoint,
      x,
      disabledLegend,
    } = this.service;
    return options.reduce((value, opt) => {
      if (disabledLegend.includes(opt.field)) {
        return value;
      }
      const point = dataSource.data.reduce((prev, curr, index) => {
        const cy = this.service.y(curr[opt.field]);
        return index % getMode() === 0 && startPoint.y !== cy
          ? [
              ...prev,
              {
                cx: x(index),
                cy,
                name: opt.field,
                stroke: opt.stroke.value || opt.stroke.start,
              },
            ]
          : prev;
      }, []);
      return [...value, ...point];
    }, []);
  }
}
