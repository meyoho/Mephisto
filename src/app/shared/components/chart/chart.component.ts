import { ObservableInput } from '@alauda/common-snippet';
import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { responseTimeOptions } from '@asm/features/servicemesh-userview/data-panel/utils';

import { Observable, Subject } from 'rxjs';
import { filter, takeUntil, tap } from 'rxjs/operators';

import { ChartService } from './chart.service';
import { ChartOptions, SourceValue, View } from './chart.types';
import { basics, calculationDuration } from './utils';
@Component({
  selector: 'alo-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  providers: [ChartService],
})
export class ChartComponent implements OnInit, OnDestroy {
  linearData = responseTimeOptions;
  onDestroy$ = new Subject<void>();
  @Input()
  set disabledTranslate(disabled: boolean) {
    this.service.disabledTranslate = !!disabled;
  }

  @Input()
  set disabledLegend(legend: string[]) {
    // pod 数据对比 处理切换指标后，保留当前 switch 的是实例项
    if (legend) {
      this.service.catchLegend = legend;
    }
  }

  @ObservableInput(true)
  private readonly switchLegendValue$: Observable<{ name: string }>;

  @Input() switchLegendValue: { name: string };
  @Input() height: number;
  @Input()
  set time(time: [number, number]) {
    if (time) {
      this.service.duration = calculationDuration(time);
    }
  }

  @Input()
  get options() {
    return this.service.options;
  }

  set options(data: ChartOptions[]) {
    this.service.initLegend(data || []);
  }

  @Input()
  set data(data: SourceValue[]) {
    this.service.source(data || []);
  }

  @ViewChild('view', { static: true })
  viewRef: ElementRef;

  margin = basics.margin;
  view: View = {
    width: 0,
    height: 0,
  };

  get transform() {
    const { left } = this.margin;
    return `translate(${left}, 0)`;
  }

  constructor(public readonly service: ChartService) {}

  ngOnInit() {
    this.switchLegendInit();
    this.service.legend$
      .pipe(
        takeUntil(this.onDestroy$),
        tap(name => {
          if (name) {
            this.service.source(this.service.dataSource.data, false);
          }
        }),
      )
      .subscribe();
  }

  /**
   * 自定义 legend
   * 外部组件传入当前 switch name
   */
  switchLegendInit() {
    this.switchLegendValue$
      .pipe(
        filter(value => !!(value && value.name)),
        takeUntil(this.onDestroy$),
      )
      .subscribe(res => {
        this.service.switchLegend(res.name);
      });
  }

  onResized({ width, height }: any) {
    if (width && height) {
      this.service.initAreaSize(width, height);
      this.service.initStartPoint();
    }
  }

  ngOnDestroy() {
    this.service.legend$.unsubscribe();
    this.service.render$.unsubscribe();
    this.onDestroy$.next();
  }
}
