export interface AxisSeries {
  data: AxisData[];
  avg: number;
  min: number;
  max: number;
}

export interface AxisData {
  timestamp: number;
  value: number;
}

export interface View {
  width: number;
  height: number;
}

export interface ChartOptions {
  field: string;
  title: string;
  stroke: Stroke;
  icon?: string;
  lineType: string;
}

export interface Duration {
  period: string;
  value: number;
  step: number;
}

export interface Stroke {
  start: string;
  stop?: string;
  opacity?: number;
  value?: string;
}

export interface DataSource {
  data: SourceValue[];
  total: number;
  max?: number;
  min?: number;
  avg?: { [key: string]: number };
}

export interface SourceValue {
  date: number;
  [key: string]: number;
}

export interface TimeParams {
  name?: string;
  start: number;
  end: number;
  step: number;
  instances?: string;
  method?: string;
  path?: string;
}
