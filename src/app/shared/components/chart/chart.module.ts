import { TooltipComponent } from './tooltip/tooltip.component';
import { LineComponent } from './line/line.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartComponent } from './chart.component';
import { AxisComponent } from './axis/axis.component';
import { ChartService } from './chart.service';
import { LegendComponent } from './legend/legend.component';
import { PointComponent } from './point/point.component';
import { TranslateModule } from '@alauda/common-snippet';
import { DirectivesModule } from '../../directives/directives.module';
import { IconModule } from '@alauda/ui';
import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
  imports: [
    CommonModule,
    IconModule,
    TranslateModule,
    DirectivesModule,
    PipesModule
  ],
  declarations: [ChartComponent, AxisComponent, LineComponent, LegendComponent, PointComponent, TooltipComponent],
  exports: [ChartComponent, AxisComponent, LineComponent, LegendComponent, PointComponent, TooltipComponent],
  providers: [ChartService]
})
export class ChartModule { }
