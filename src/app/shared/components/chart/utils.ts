import {
  ChartOptions,
  SourceValue,
  TimeParams,
} from '@app/shared/components/chart';
import { responseTimeOptions } from '@asm/features/servicemesh-userview/data-panel/utils';

import dayjs from 'dayjs';
import { get } from 'lodash-es';
export const xTickIntervals: {
  [key: string]: { value: number; unit: 'm' | 'h' | 'd'; step: number };
} = {
  '1m': { value: 1, step: 1, unit: 'm' },
  '5m': { value: 1, step: 1, unit: 'm' },
  '10m': { value: 10, step: 5, unit: 'm' },
  '30m': { value: 30, step: 10, unit: 'm' },
  '1h': { value: 60, step: 30, unit: 'm' },
  '3h': { value: 60 * 3, step: 1, unit: 'h' },
  '6h': { value: 60 * 6, step: 2, unit: 'h' },
  '12h': { value: 60 * 12, step: 3, unit: 'h' },
  '1d': { value: 60 * 24, step: 6, unit: 'h' },
  '3d': { value: 60 * 3 * 24, step: 24, unit: 'h' },
  '7d': { value: 60 * 7 * 24, step: 1, unit: 'd' },
  '30d': { value: 60 * 30 * 25, step: 6, unit: 'd' },
};
export const basics = {
  margin: { top: 0, right: 5, bottom: 20, left: 30 },
  xLegendHeight: 20,
  yTickNumber: 3,
};

export const DEFAULT_COLOR = [
  '#7cb5ec',
  '#f7a35c',
  '#90ee7e',
  '#7798BF',
  '#aaeeee',
  '#ff0066',
  '#eeaaee',
  '#55BF3B',
  '#DF5353',
  '#7798BF',
  '#aaeeee',
];

export function getChartDefaultColor(index: number) {
  const colorIndex = index % (DEFAULT_COLOR ? DEFAULT_COLOR.length : 10);
  return DEFAULT_COLOR[colorIndex];
}

export function handleChartData(
  data: { [key: string]: [number, number] },
  options: ChartOptions[],
  timeParams: TimeParams,
  resourceName?: string,
): SourceValue[] {
  if (!data || !options) {
    return [];
  }
  const response = responseTimeOptions.map(item => item.field);
  const chartValue = handleData(data, options, timeParams);
  const key = Object.keys(chartValue)[0];
  return chartValue[key].map((item: [number, number], index: number) => {
    return options.reduce((prev, curr) => {
      let value = +get(chartValue[curr.field][index], '[1]', 0);
      if (response.includes(resourceName || options[0].field)) {
        value = curr.field === 'avg' ? Math.round(value) : value;
      }
      return {
        ...prev,
        ...{
          date: item[0],
          [curr.field]: value,
        },
      };
    }, {});
  });
}

function handleData(
  data: { [key: string]: any },
  options: ChartOptions[],
  timeParams: TimeParams,
) {
  return options.reduce((prev: { [key: string]: any }, curr) => {
    const value = data[curr.field] || [];
    const key = Object.keys(prev);
    if (value.length > get(prev[key[0]], 'length', 0)) {
      return {
        [curr.field]: fillData(value, timeParams),
        ...prev,
      };
    }
    return {
      ...prev,
      [curr.field]: fillData(value, timeParams),
    };
  }, {});
}

function fillData(res: Array<[number, number]> = [], params: TimeParams) {
  if (res.length === 0) {
    return [];
  }
  const { start, end, step } = params;
  const value = (end - start) / 60;
  const totalPoint = value / step + 1;
  const diff = parseInt('' + (totalPoint - res.length)) || 0;
  let preTime =
    dayjs()
      .subtract(value + step, 'minute')
      .valueOf() / 1000;
  const arr = new Array(diff < 0 ? 0 : diff).fill(0).map(() => {
    const time =
      dayjs(preTime * 1000)
        .add(step, 'minute')
        .valueOf() / 1000;
    preTime = time;
    return [time, 0];
  });
  return [...arr, ...res].sort((a, b) => a[0] - b[0]).splice(0, totalPoint);
}

/**
 * 计算 step
 * @param start
 * @param end
 */
export function calculateStep(start: number, end: number) {
  const minute = 60;
  const day = 60 * 24;
  const diff = (end - start) / minute;
  switch (true) {
    case diff < 12 * minute:
      return 1;
    case diff === 12 * minute:
      return 10;
    case diff <= day * 3:
      return 30;
    default:
      return 60;
  }
}

export function calculationDuration([start, end]: [number, number]) {
  const minute = 60;
  const diff = (end - start) / minute;
  const res = Object.keys(xTickIntervals).reduce(
    (prev, curr) => {
      const data = xTickIntervals[curr].value;
      if (data - diff >= 0 && diff < 5) {
        return {
          value: xTickIntervals['1m'].value,
          name: '1m',
        };
      }
      if (data - diff >= 0 && data - diff <= prev.value - diff) {
        return {
          value: xTickIntervals[curr].value,
          name: curr,
        };
      }
      return prev;
    },
    { value: 60 * 30 * 25, name: '30d' },
  );
  const step = calculateStep(start, end);
  return {
    period: res.name,
    value: res.value,
    step,
  };
}

function parseColor(hexStr: string) {
  return hexStr.length === 4
    ? hexStr
        .slice(1)
        .split('')
        .map(s => 0x11 * parseInt(s, 16))
    : [hexStr.slice(1, 3), hexStr.slice(3, 5), hexStr.slice(5, 7)].map(s =>
        parseInt(s, 16),
      );
}

function normalize(channel: number, gamma: number) {
  return Math.pow(channel / 255, gamma);
}

function pad(s: string) {
  return s.length === 1 ? '0' + s : s;
}

export function gradientColors(
  start: string,
  end = '#fff',
  steps = 3,
  gamma = 2,
) {
  let i;
  let j;
  let ms;
  let me;
  const output = [];
  const so = [];
  gamma = gamma || 1;
  const newStart = parseColor(start).map(item => normalize(item, gamma));
  const newEnd = parseColor(end).map(item => normalize(item, gamma));
  for (i = 0; i < steps; i++) {
    ms = i / (steps - 1);
    me = 1 - ms;
    for (j = 0; j < 3; j++) {
      so[j] = pad(
        Math.round(
          Math.pow(newStart[j] * me + newEnd[j] * ms, 1 / gamma) * 255,
        ).toString(16),
      );
    }
    output.push('#' + so.join(''));
  }
  return output[1];
}

export function optionsBuilder(list: string[]): ChartOptions[] {
  return list.map((item, index) => {
    const color = getChartDefaultColor(index);
    const stop = gradientColors(color);
    return {
      field: item,
      title: item,
      stroke: { start: color, stop },
      lineType: 'area',
    };
  });
}
