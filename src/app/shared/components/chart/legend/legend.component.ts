import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { responseTimeOptions } from '@asm/features/servicemesh-userview/data-panel/utils';

import { map } from 'rxjs/operators';

import { basics } from '../utils';
import { ChartService } from './../chart.service';
interface Legends {
  title: string;
  name: string;
  color: string;
  icon: string;
}
@Component({
  selector: 'alo-legend',
  templateUrl: './legend.component.html',
  styleUrls: ['./legend.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LegendComponent implements OnInit {
  @Input() legend: string[];
  basics = basics;
  disabledLegend$ = this.service.render$.pipe(
    map(() => {
      return this.service.disabledLegend;
    }),
  );

  legends: Legends[];
  getLegends() {
    const reverse = responseTimeOptions
      .map(item => item.field)
      .includes(this.service.options[0].field);
    return this.service.options.reduce((prev, curr) => {
      const values = {
        title: curr.title,
        name: curr.field,
        color: curr.stroke.value || curr.stroke.start,
        icon: curr.icon,
      };
      return reverse ? [values, ...prev] : [...prev, values];
    }, []);
  }

  constructor(private readonly service: ChartService) {}

  ngOnInit() {
    this.legends = this.getLegends();
  }

  switchLegend(type: string) {
    this.service.switchLegend(type);
  }
}
