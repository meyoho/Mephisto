import { ChangeDetectionStrategy, Component } from '@angular/core';

import { area, curveMonotoneX, line } from 'd3-shape';
import { map } from 'rxjs/operators';

import { ChartOptions, SourceValue } from '../chart.types';
import { ChartService } from './../chart.service';
@Component({
  selector: 'g[alo-line]',
  templateUrl: './line.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LineComponent {
  areas$ = this.service.render$.pipe(
    map(() => {
      return this.getAreas();
    }),
  );

  constructor(public readonly service: ChartService) {}
  getAreas() {
    if (this.service.dataSource.data.length > 0) {
      return this.service.options.reduce((prev: any, curr: ChartOptions) => {
        if (this.service.disabledLegend.includes(curr.field)) {
          return prev;
        }
        return [
          ...prev,
          {
            name: curr.field,
            path: this.generatePath(this.service.dataSource.data, curr),
            avgLineY: this.service.y(this.service.dataSource.avg[curr.field]),
            fill: `url(#${curr.field})`,
            stroke: curr.stroke,
          },
        ];
      }, []);
    }
    return [];
  }

  generatePath(data: SourceValue[], option: ChartOptions) {
    const values = data.map(item => {
      return {
        date: item.date,
        value: item[option.field],
      };
    });
    if (option.lineType === 'line') {
      return line<{ date: number; value: number }>()
        .curve(curveMonotoneX)
        .x((_, i) => this.service.x(i))
        .y(d => this.service.y(d.value))(values);
    }
    return area<{ date: number; value: number }>()
      .curve(curveMonotoneX)
      .x((_, i) => this.service.x(i))
      .y0(this.service.startPoint.y)
      .y1(d => this.service.y(d.value))(values);
  }
}
