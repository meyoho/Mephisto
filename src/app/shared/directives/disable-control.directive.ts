import { Directive, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[aloDisableControl]',
})
export class DisableControlDirective {
  // tslint:disable-next-line:no-input-rename
  @Input('aloOnlySelf') aloOnlySelf: boolean;
  @Input() set aloDisableControl(condition: boolean) {
    const action = condition ? 'disable' : 'enable';
    this.ngControl.control[action]({ onlySelf: this.aloOnlySelf });
  }

  constructor(private readonly ngControl: NgControl) {}
}
