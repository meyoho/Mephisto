import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AllNumericValidateDirective } from './all-numeric.directive';
import { AsyncDataDirective } from './async-data.directive';
import { AuiCodeEditorHelperDirective } from './aui-code-editor.directive';
import { CalendarDirective } from './calendar.directive';
import { DisableControlDirective } from './disable-control.directive';
import { PageHeaderContentDirective } from './page-header-content.directive';
import { ResizeDirective } from './resize.directive';
import { VarDirective } from './var.directive';
@NgModule({
  imports: [CommonModule, PortalModule],
  declarations: [
    PageHeaderContentDirective,
    AsyncDataDirective,
    ResizeDirective,
    VarDirective,
    AuiCodeEditorHelperDirective,
    CalendarDirective,
    AllNumericValidateDirective,
    DisableControlDirective,
  ],
  exports: [
    PageHeaderContentDirective,
    AsyncDataDirective,
    ResizeDirective,
    VarDirective,
    AuiCodeEditorHelperDirective,
    CalendarDirective,
    AllNumericValidateDirective,
    DisableControlDirective,
  ],
})
export class DirectivesModule {}
