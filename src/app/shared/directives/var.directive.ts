import { ObservableInput } from '@alauda/common-snippet';
import {
  Directive,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewContainerRef,
  ViewRef,
} from '@angular/core';
import { isObject } from 'lodash-es';
import { Observable, Subject, combineLatest, from, of } from 'rxjs';
import { filter, switchMap, takeUntil } from 'rxjs/operators';

export type VarDirectiveInput<T> =
  | Observable<T>
  | Promise<T>
  | T
  | null
  | undefined;

/**
 * Make it possible to declare temporary variable in template to prevent declaring unnecessary variable in ts code,
 * no need to write `this.observable$.pipe(tap((snapshotValue) => this._snapshot = snapshotValue))` any more
 * AND the variable can be passed to event handler:
 * ```html
 * <div *aloVar="let value from (observable$ | async)">
 *   <button (click)="onClick(value)"></button>
 * </div>
 * ```
 */
@Directive({
  selector: '[aloVar]',
})
export class VarDirective<T> implements OnInit, OnDestroy {
  @Input()
  aloVarFrom?: VarDirectiveInput<T>;

  @Input()
  aloVarNullable: boolean;

  @ObservableInput(true)
  private aloVarFrom$: Observable<VarDirectiveInput<T>>;

  @ObservableInput(true, true)
  private aloVarNullable$: Observable<boolean>;

  private onDestroy$$ = new Subject<void>();

  private viewRef: ViewRef;

  private context: { $implicit?: T } = {};

  constructor(
    private viewContainerRef: ViewContainerRef,
    private templateRef: TemplateRef<any>,
  ) {}

  ngOnInit() {
    combineLatest([this.aloVarFrom$, this.aloVarNullable$])
      .pipe(
        takeUntil(this.onDestroy$$),
        switchMap(([varFrom, nullable]) => {
          const observable = this.normalizeInput(varFrom);
          return nullable ? observable : observable.pipe(filter(_ => !!_));
        }),
      )
      .subscribe(normalized => {
        this.context.$implicit = normalized;
        if (this.viewRef) {
          return this.viewRef.markForCheck();
        }
        this.viewRef = this.viewContainerRef.createEmbeddedView(
          this.templateRef,
          this.context,
        );
      });
  }

  normalizeInput(varFrom: VarDirectiveInput<T>) {
    if (varFrom instanceof Observable) {
      return varFrom;
    }

    if (
      isObject(varFrom) &&
      'then' in varFrom &&
      typeof varFrom.then === 'function'
    ) {
      return from(varFrom);
    }

    return of(varFrom as T);
  }

  ngOnDestroy() {
    this.onDestroy$$.next();
    if (this.viewRef) {
      this.viewRef.destroy();
    }
  }
}
