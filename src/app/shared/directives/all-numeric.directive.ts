import { Directive } from '@angular/core';
import {
  AbstractControl,
  NG_ASYNC_VALIDATORS,
  ValidationErrors,
} from '@angular/forms';

import { of } from 'rxjs';

@Directive({
  selector: '[aloAllNumericValidate]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: AllNumericValidateDirective,
      multi: true,
    },
  ],
})
export class AllNumericValidateDirective {
  validate(ctrl: AbstractControl): ValidationErrors | null {
    if (!ctrl.value) {
      return null;
    }
    const allNumericReg = /^[0-9]+$/;
    if (allNumericReg.test(ctrl.value)) {
      return of({ allNumeric: true });
    } else {
      return of(null);
    }
  }
}
