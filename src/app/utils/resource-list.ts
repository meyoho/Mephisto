import {
  KubernetesResource,
  KubernetesResourceList,
  LabelSelectorRequirement,
  publishRef,
  ResourceListParams,
  StringMap,
} from '@alauda/common-snippet';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

import {
  BehaviorSubject,
  combineLatest,
  EMPTY,
  merge,
  Observable,
  Subject,
} from 'rxjs';
import {
  catchError,
  exhaustMap,
  filter,
  map,
  scan,
  skipWhile,
  startWith,
  switchMap,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import { Status } from 'app/typings';

export class ResourceList<
  R extends KubernetesResource = KubernetesResource,
  P extends ResourceListParams = ResourceListParams
> {
  private loadMoreAction$$ = new Subject<void>();
  private reloadAction$$ = new Subject<number>();
  private hasMore$$ = new BehaviorSubject(false);
  private loading$$ = new BehaviorSubject(false);
  private loadingError$$ = new BehaviorSubject<Status | HttpErrorResponse>(
    null,
  );
  private itemsScanner$$ = new Subject<ItemsScanner<R>>();
  private destroy$$ = new Subject<void>();
  private continueToken = '';

  rawResponse$: Observable<KubernetesResourceList<R>>;
  items$: Observable<Array<R>>;
  reload$: Observable<
    [

        | P
        | {
            labelSelector: string;
            fieldSelector: string;
          },
      number?,
    ]
  >;
  hasMore$ = this.hasMore$$.asObservable().pipe(takeUntil(this.destroy$$));
  loading$ = this.loading$$.asObservable().pipe(takeUntil(this.destroy$$));
  loadingError$ = this.loadingError$$
    .asObservable()
    .pipe(takeUntil(this.destroy$$));

  constructor({ fetcher, activatedRoute }: ConfigWithRoute<R>);
  constructor({ fetcher, fetchParams$ }: ConfigWithParams<R, P>);
  constructor({
    fetcher,
    fetchParams$,
    activatedRoute,
    limit = 20,
  }: ConfigWithRoute<R> & ConfigWithParams<R, P>) {
    const queryParams$ =
      fetchParams$ ||
      activatedRoute.queryParamMap.pipe(
        map(paramMap => ({
          labelSelector: paramMap.get('labelSelector'),
          fieldSelector: paramMap.get('fieldSelector'),
          keyword: paramMap.get('keyword'),
          field: paramMap.get('field'),
        })),
      );

    this.reload$ = combineLatest(
      queryParams$,
      this.reloadAction$$.pipe(startWith(null)),
    ).pipe(tap(() => (this.continueToken = '')));

    this.rawResponse$ = this.reload$.pipe(
      skipWhile(([_, reloadLimit]) => reloadLimit === 0),
      switchMap(([queryParams, reloadLimit]) =>
        this.loadMoreAction$$.pipe(
          withLatestFrom(this.hasMore$),
          filter(([_, hasMore]) => hasMore),
          startWith(null),
          exhaustMap(() => {
            this.loading$$.next(true);
            this.loadingError$$.next(null);
            return fetcher(
              normalizeParams({
                limit: (reloadLimit || limit) + '',
                ...queryParams,
                continue: this.continueToken,
              }),
            ).pipe(
              tap(res => {
                if (reloadLimit) {
                  this.reloadAction$$.next(0);
                }
                this.resolveResponse(res);
              }),
              catchError((err: Status | HttpErrorResponse) => {
                if ('code' in err && err.code === 410) {
                  this.continueToken = err.metadata.continue;
                }
                this.loading$$.next(false);
                this.loadingError$$.next(err);
                return EMPTY;
              }),
            );
          }),
          startWith(null),
        ),
      ),
      takeUntil(this.destroy$$),
      publishRef(),
    );

    this.items$ = merge(
      this.rawResponse$.pipe(
        map(res => (items: R[]) => (res ? items.concat(res.items || []) : [])),
      ),
      this.itemsScanner$$,
    ).pipe(
      scan<ItemsScanner<R>, R[]>((acc, scanner) => scanner(acc), []),
      publishRef(),
    );
  }

  reload(reloadLimit?: number) {
    this.reloadAction$$.next(reloadLimit);
  }

  loadMore() {
    this.loadMoreAction$$.next();
  }

  scanItems(scanner: ItemsScanner<R>) {
    this.itemsScanner$$.next(scanner);
  }

  destroy() {
    this.destroy$$.next();
    this.destroy$$.complete();
  }

  create(resource: R) {
    this.scanItems(items => [resource].concat(items));
  }

  update(resource: R, index?: number) {
    const isNumber = typeof index === 'number';
    this.scanItems(items =>
      items.map((item, i) => {
        if (
          item.metadata.name === resource.metadata.name ||
          (isNumber && index === i)
        ) {
          return resource;
        } else {
          return item;
        }
      }),
    );
  }

  delete({ metadata: { name } }: R) {
    this.scanItems(items => items.filter(item => item.metadata.name !== name));
  }

  replace(res: KubernetesResourceList<R>) {
    this.resolveResponse(res);
    this.scanItems(() => res.items);
  }

  private resolveResponse(res: KubernetesResourceList<R>) {
    this.loading$$.next(false);
    this.loadingError$$.next(null);
    const continueToken = res.metadata.continue;
    this.hasMore$$.next(!!continueToken);
    this.continueToken = continueToken;
  }
}

/**
 * convert selector.matchLabels to string
 * refers to: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#api
 */
export function matchLabelsToString(params: StringMap = {}) {
  return Object.keys(params)
    .filter(key => params[key])
    .map(key => `${key}=${params[key]}`)
    .join(',');
}

/**
 * convert string to selector.matchLabels
 */
export function stringToMatchLabels(str?: string): Record<string, string> {
  const labels: Record<string, string> = {};
  if (str) {
    return str.split(',').reduce((acc, item) => {
      const [key, value] = item.split('=');
      if (key && value) {
        acc[key] = value;
      }
      return acc;
    }, labels);
  }
  return labels;
}

export function matchExpressionsToString(params: LabelSelectorRequirement[]) {
  return params
    .map(param => {
      switch (param.operator.toLowerCase()) {
        case '=':
        case '==':
        case '!=':
          return `${param.key}${param.operator}${param.values[0]}`;
        case 'in':
        case 'notin':
          return `${param.key} ${param.operator} (${param.values.join(',')})`;
        case 'exists':
        case 'doesnotexist':
          return `${param.key} ${param.operator}`;
        default:
          return param.key;
      }
    })
    .join(',');
}

export const normalizeParams = <P>(params: P) =>
  Object.entries(params).reduce(
    (acc, [key, value]) => {
      value = typeof value === 'string' ? value.trim() : value;
      if (value) {
        acc[key as keyof P] = value;
      }
      return acc;
    },
    {} as P,
  );

interface BaseConfig {
  limit?: number;
}

export type ItemsScanner<R> = (items: R[]) => R[];

export interface ConfigWithRoute<R> extends BaseConfig {
  fetcher: (
    params: ResourceListParams,
  ) => Observable<KubernetesResourceList<R>>;
  activatedRoute: ActivatedRoute;
}

export interface ConfigWithParams<R, P> extends BaseConfig {
  fetcher: (params: P) => Observable<KubernetesResourceList<R>>;
  fetchParams$: Observable<P>;
}
