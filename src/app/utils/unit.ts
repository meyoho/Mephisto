import dayjs from 'dayjs';
const FORMAT = 'YYYY-MM-DD HH:mm:ss';
/**
 * 分割字符串中的单位和数字
 * @param value string
 * @returns [数字, 单位] 单位默认s
 */
export const dividedUnitNumber = (value = '') => {
  const string = /[^a-z]+/ig;
  const unit = value.replace(string, '') || 's';
  const number = value.replace(unit, '')
  return [number, unit]
}

export function toFixed(value = 0) {
  return +value.toFixed(2) ? +value.toFixed(2) : +value.toFixed(4);
}

export function dateFormat(timestamp: number, format = FORMAT) {
  return dayjs.unix(timestamp).format(format);
}
