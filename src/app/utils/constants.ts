// tslint:disable: no-duplicate-string
import { createResourceDefinitions } from '@alauda/common-snippet';

const NETWORKING_GROUP = 'networking.istio.io';
const ASM_GROUP = 'asm.alauda.io';

export const RESOURCE_QUOTA_TYPES = [
  { key: 'requests.cpu', name: 'cpu', unit: 'unit_core' },
  { key: 'requests.memory', name: 'memory', unit: 'unit_Gi' },
  { key: 'requests.storage', name: 'storage', unit: 'unit_Gi' },
  { key: 'persistentvolumeclaims', name: 'pvc_num', unit: 'unit_ge' },
  { key: 'pods', name: 'pods_num', unit: 'unit_ge' },
];

export type ResourceQuotaType = typeof RESOURCE_QUOTA_TYPES[number];

const _ = createResourceDefinitions({
  VIRTUALSERVICE: {
    apiGroup: NETWORKING_GROUP,
    apiVersion: 'v1alpha3',
    type: 'virtualservices',
  },
  DESTINATIONRULE: {
    apiGroup: NETWORKING_GROUP,
    apiVersion: 'v1alpha3',
    type: 'destinationrules',
  },
  POLOCY: {
    apiGroup: 'authentication.istio.io',
    apiVersion: 'v1alpha1',
    type: 'policies',
  },
  GATEWAY: {
    apiGroup: NETWORKING_GROUP,
    apiVersion: 'v1alpha3',
    type: 'gateways',
  },
  VIEW: {
    apiGroup: 'auth.alauda.io',
    apiVersion: 'v1',
    type: 'views',
  },
  NAMESPACE: {
    type: 'namespaces',
  },
  CLUSTERCONFIG: {
    apiGroup: ASM_GROUP,
    apiVersion: 'v1beta1',
    type: 'clusterconfigs',
  },
  CLUSTER: {
    apiGroup: 'clusterregistry.k8s.io',
    apiVersion: 'v1alpha1',
    type: 'clusters',
  },
  DEPLOYMENT: {
    apiGroup: 'apps',
    type: 'deployments',
  },
  MICROSERVICE: {
    apiGroup: ASM_GROUP,
    apiVersion: 'v1beta2',
    type: 'microservices',
  },
  SERVICE: {
    apiVersion: 'v1',
    type: 'services',
  },
  LOADBALANCER: {
    apiGroup: ASM_GROUP,
    apiVersion: 'v1beta1',
    type: 'loadbalancers',
  },
  SERVICEMESH: {
    apiGroup: ASM_GROUP,
    apiVersion: 'v1beta1',
    type: 'servicemeshs',
  },
  CUSTOMRESOURCEDEFINITION: {
    apiGroup: 'apiextensions.k8s.io',
    apiVersion: 'v1beta1',
    type: 'customresourcedefinitions',
  },
  OUTLIERDETECTION: {
    apiGroup: ASM_GROUP,
    apiVersion: 'v1alpha1',
    type: 'outlierdetections',
  },
  CONNECTIONPOOL: {
    apiGroup: ASM_GROUP,
    apiVersion: 'v1alpha1',
    type: 'connectionpools',
  },
  WHITELIST: {
    apiGroup: ASM_GROUP,
    apiVersion: 'v1alpha1',
    type: 'whitelists',
  },
  SIDECAR: {
    apiGroup: NETWORKING_GROUP,
    apiVersion: 'v1alpha3',
    type: 'sidecars',
  },
  ERRORCHECK: {
    apiGroup: ASM_GROUP,
    apiVersion: 'v1alpha1',
    type: 'errorchecks',
  },
  CANARY: {
    apiGroup: ASM_GROUP,
    apiVersion: 'v1alpha1',
    type: 'canarydeliveries',
  },
});

export const RESOURCE_DEFINITIONS = _.RESOURCE_DEFINITIONS;
export const RESOURCE_TYPES = _.RESOURCE_TYPES;
export type ResourceType = keyof typeof RESOURCE_TYPES;
