/* eslint-disable unicorn/regex-shorthand */
export const K8S_RESOURCE_NAME_BASE = {
  pattern: /^[\da-z]([\da-z-]*[\da-z])?$/,
  tip: 'regexp_tip_k8s_resource_name_base',
};

export const K8S_CONFIGMAP_SECRET_NAME = {
  pattern: /^[\da-z][\d.a-z-]*$/,
  tip: 'regexp_tip_k8s_config_secret_name',
};

export const K8S_ENV_VARIABLE_NAME = {
  pattern: /^[.A-Z_a-z-][\w.-]*$/,
  tip: 'regexp_tip_k8s_env_variable_name',
};

export const INT_PATTERN = {
  pattern: /^-?\d+$/,
  tip: 'regexp_tip_integer_pattern',
};

export const POSITIVE_INT_PATTERN = {
  pattern: /^[1-9]\d*$/,
  tip: 'regexp_tip_positive_integer_pattern',
};

export const IMAGE_PATH = {
  pattern: /^[\da-z]([\d./:_a-z-]*[\da-z])?$/,
  tip: 'regexp_tip_image_path_rule',
};

export const INT_PATTERN_ZERO = {
  pattern: /^(0|\+?[1-9]\d*)$/,
  tip: 'regexp_tip_positive_integer_pattern_zero',
};

export const HTTP_STATUS_CODE = {
  pattern: /^([2-5]\d\d|599)$/,
  tip: 'regexp_tip_http_status_code_pattern',
};

export const DEPLOY_VERSION = {
  pattern: /^[\dA-Za-z]([\dA-Za-z-]*[\dA-Za-z])?$/,
  tip: 'regexp_tip_deploy_version',
};

export const K8S_SERVICE_NAME = {
  pattern: /^[a-z]([\da-z-]*[\da-z])?$/,
  tip: 'regexp_tip_k8s_service_name',
};

export const PERCENTAGE_RANGE = {
  pattern: /(^100(\.0{1,2})?)$|(^\d{1,2}(\.\d{1,2})?)$/,
  tip: 'regexp_tip_percentage_range_tips',
};

export const URL_ADDRESS_PATTERN = {
  pattern: /^[^ ]+$|^((\w+:)?\/{2})?((\w|%[\dA-f]{2})+(:(\w|%[\dA-f]{2})+)?@)?(\w[\w-]{0,253}\w\.)+\w{1,63}(:\d+)?(\/([\w+.~-]|%[\dA-f]{2})*)*(\?(&?([\w+.~-]|%[\dA-f]{2})=?)*)?(#([\w+.~-]|%[\dA-f]{2})*)?$/,
  tip: 'regexp_tip_ingress_addr',
};

export const IMAGE_ADDRESS = {
  pattern: /^[\da-z][\da-z-]{0,62}(\.[\da-z][\da-z-]{0,62})+(:\d{1,5})?$/,
  tip: 'regexp_tip_image_repository_addr',
};

export const IP_ADDRESS_PATTERN = {
  pattern: /^(?:(?:25[0-5]|2[0-4]\d|[01]?\d{1,2})\.){3}(?:25[0-5]|2[0-4]\d|[01]?\d{1,2})$/,
  tip: 'regexp_tip_ip_address',
};

export const IP_RANGE_PATTERN = {
  pattern: /^\*$|^(?:(?:\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}(?:\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])\/([1-9]|[12]\d|3[0-2])$/,
  tip: 'regexp_tip_ip_range',
};

export const ELASTICSEARCH_USER_PATTERN = {
  pattern: /^((?!:|：).)*$/,
  tip: 'regexp_tip_does_not_support_colons',
};

export const ISOLATION_RATIO = {
  pattern: /^(?:0|[1-9]\d?|100)$/,
  tip: 'regexp_tip_isolation_ratio_tips',
};

export const POSITIVE_NUMBER = {
  pattern: /^\+{0,1}(\d+)$|^\+{0,1}(\d+\.\d+)$/,
  tip: 'regexp_tip_positive_number',
};

export const SUBSET_PATTERN = {
  pattern: /^[\dA-Za-z](?:[\dA-Za-z-]*[\dA-Za-z])?$/,
  tip: 'regexp_tip_subset',
};

export const DOESNOTSUPPORTCHINES_PATTERN = {
  pattern: /^[^ \u4E00-\u9FA5]+$/,
  tip: 'does_not_support_chinese_tip',
};

export const SPACES_NOT_ALLOWED_PATTERN = {
  pattern: /^\S+$/,
  tip: 'spaces_are_not_allowed_tip',
};

export const WORKLOAD_PATH_PATTERN = {
  pattern: /^\/.+/,
};

export const POSITIVE_PERCENTAGE_PATTERN = {
  pattern: /^([1-9]\d?|100)$/,
  tip: 'regexp_positive_percentage_tips',
};
