export type ReducerMap<TResult, TItem> = {
  [p in keyof TResult]: (accum: TResult[p], item: TItem) => TResult[p]
};

export function combineReducers<TResult, TItem>(
  reducers: ReducerMap<TResult, TItem>,
): (accum: TResult, item: TItem) => TResult;
export function combineReducers(reducers: any) {
  const keys = Object.keys(reducers);
  return (accum: any, item: any) =>
    keys.reduce(
      (prev: any, key: any) => ({
        ...prev,
        [key]: reducers[key](prev[key], item),
      }),
      accum || {},
    );
}

// Example:
// const source: Item[] = [
//   { state: 'fail' },
//   { state: 'succ' },
//   { state: 'succ' },
// ];

// interface Item {
//   state: string;
// }

// const countByState = (state: string) => (accum: number = 0, item: Item) =>
//   item.state === state ? accum + 1 : accum;

// const combined = combineReducers({
//   succ: countByState('succ'),
//   fail: countByState('fail'),
//   inverse: (accum: Item[] = [], item: Item) => [item, ...accum],
// });

// console.log(source.reduce(combined, null));
