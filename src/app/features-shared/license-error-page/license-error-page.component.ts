import { Component } from '@angular/core';
@Component({
  template: `
    <acl-license-error>
      <alo-logo></alo-logo>
      <alo-global-actions [hideViewSwitch]="true"></alo-global-actions>
    </acl-license-error>
  `,
})
export class LicenseErrorPageComponent {}
