import { LicenseErrorModule } from '@alauda/common-snippet';
import { PageModule } from '@alauda/ui';
import { NgModule } from '@angular/core';
import { GlobalActionsModule } from '@app/features/shared/global-actions';

import { SharedModule } from 'app/shared/shared.module';
import { LicenseErrorPageComponent } from './license-error-page.component';

@NgModule({
  imports: [SharedModule, LicenseErrorModule, PageModule, GlobalActionsModule],
  declarations: [LicenseErrorPageComponent],
})
export class LicenseErrorPageModule {}
