import { Injectable } from '@angular/core';
import { NavControlService } from '@app/services/nav-control.service';
import { ProjectService } from '@asm/api/project/project.service';

import { get } from 'lodash-es';
import { BehaviorSubject, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
export enum StoreType {
  Deployed = 'deployed',
  EnabledASM = 'enabledASM',
}
@Injectable({
  providedIn: 'root',
})
export class CommonStoreService {
  groups$ = new BehaviorSubject<{ [param: string]: boolean }>({});
  asmLabels = {};
  constructor(
    private readonly navControl: NavControlService,
    private readonly api: ProjectService,
  ) {}

  getDeployed(cluster: string) {
    return this.navControl.getClusterDeployed(cluster).pipe(
      catchError(() => of(false)),
      map(value => ({
        [cluster]: !!value,
      })),
      tap(deployed => {
        const groups = this.getGroupsValue();
        this.groups$.next({ ...groups, ...deployed });
      }),
    );
  }

  deployed(cluster: string) {
    const groups = this.getGroupsValue();
    return get(groups, cluster);
  }

  getGroupsValue() {
    return this.groups$.getValue();
  }

  hasAttribute(name: string, type: string) {
    if (type === StoreType.Deployed) {
      return Object.prototype.hasOwnProperty.call(this.getGroupsValue(), name);
    }
    return Object.prototype.hasOwnProperty.call(this.asmLabels, name);
  }

  getEnabled(cluster: string, name: string) {
    return this.api.getNamespaceDetail(cluster, name).pipe(
      map(namespace => {
        return {
          [name]:
            get(namespace, 'metadata.labels[istio-injection]') === 'enabled',
        };
      }),
      // tap(result => {
      //   this.setASMLabel(result);
      // })
    );
  }
  // TODO: store storage ASMLabel
  // setASMLabel (result: {}) {
  //   this.asmLabels = {...this.asmLabels, ...result }
  // }

  // enabledASM(namespace: string) {
  //   return get(this.asmLabels, namespace)
  // }
}
