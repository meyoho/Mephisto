import { NamespaceIdentity } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { PermissionType, permissionUrl, UrlParams } from '@asm/utils';

import { isEqual } from 'lodash-es';
import pathToRegexp from 'path-to-regexp';
import { merge, Observable, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  take,
  tap,
} from 'rxjs/operators';

const routeRegexp = pathToRegexp(
  '/workspace/:project/clusters/:cluster/namespaces/:namespace/:menu',
  null,
  { end: false },
);

@Injectable({ providedIn: 'root' })
export class LatestVisitedService {
  routeRegexp = routeRegexp;
  constructor(private router: Router) {
    this.project$.subscribe();
    this.namespace$.subscribe();
  }
  private _namespace$ = new Subject<NamespaceIdentity>();
  private _project$ = new Subject<string>();
  private project: string;

  project$ = merge(
    this._project$,
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map((event: NavigationEnd) => routeRegexp.exec(event.url)),
      filter(matching => !!matching),
      map(([, project]) => project),
    ),
  ).pipe(
    tap(project => (this.project = project)),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  namespace$: Observable<any> = merge(
    this._namespace$,
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map((event: NavigationEnd) => routeRegexp.exec(event.url)),
      filter(matching => !!matching),
      map(([, , cluster, name]) => ({
        cluster,
        name,
      })),
    ),
  ).pipe(
    /*tap(namespace => {
      if (!namespace) {
        localStorage.removeItem('cluster');
        localStorage.removeItem('namespace');
        return;
      }

      localStorage.setItem('cluster', namespace.cluster);
      localStorage.setItem('namespace', namespace.name);
    }),*/
    // startWith(this.getVisitedNamespaceFromLocalStorage()),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  setProject(project: string) {
    this._project$.next(project);
    this._namespace$.next(null);
  }

  clearNamespace() {
    this._namespace$.next(null);
  }

  setNamespace(namespace: NamespaceIdentity) {
    const regexp = pathToRegexp('/workspace/:project/:menu', null, {
      end: false,
    });
    const [, project, ,] = regexp.exec(this.router.url);
    this.project$.pipe(take(1)).subscribe(currProject => {
      const params: UrlParams = {
        project: currProject,
        cluster: namespace.cluster,
        namespace: namespace.name,
      };
      return this.router.navigate([
        `${permissionUrl(PermissionType.Servicemesh, params)}`,
      ]);
    });

    if (!namespace) {
      this._namespace$.next(null);
      return this.router.navigate([
        `${permissionUrl(PermissionType.NONamespace, {
          project: this.project || project,
        })}`,
      ]);
    }
  }

  /*
  private getVisitedNamespaceFromLocalStorage() {
    const cluster = localStorage.getItem('cluster');
    const name = localStorage.getItem('namespace');

    if (cluster && name) {
      return {
        cluster,
        name,
      };
    }

    return null;
  }*/
}
