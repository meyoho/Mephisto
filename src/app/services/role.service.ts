import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ASM_SUBPATH } from '@app/constants';

import { map, publishReplay, refCount } from 'rxjs/operators';

@Injectable()
export class RoleService {
  adminDoings$ = this.http
    .get<{ allowed: boolean }>(`${API_GATEWAY}/${ASM_SUBPATH}/api/v1/caniadmin`)
    .pipe(
      map(result => result.allowed),
      publishReplay(1),
      refCount(),
    );

  constructor(private readonly http: HttpClient) {}
}
