import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { take, tap } from 'rxjs/operators';

@Injectable()
export class OAuthSecretValidatorService {
  private secretValidateCode$ = new Subject<string>();

  constructor(private http: HttpClient, private ngZone: NgZone) {
    (window as any).acceptCode = (code: string) => {
      this.ngZone.run(() => {
        this.secretValidateCode$.next(code);
      });
    };
  }

  transportCode(code: string): void {
    if (window.opener && window.opener.acceptCode) {
      window.opener.acceptCode(code);
    }
  }

  validate(url: string): Observable<string> {
    const validateWindow = window.open(url, '_blank');
    return this.secretValidateCode$.pipe(
      take(1),
      tap(() => {
        validateWindow.close();
      }),
    );
  }

  callback(namespace: string, secret: string, codeRepo: string, code: string) {
    return this.http.get(
      `api/v1/callback/oauth/${namespace}/secret/${secret}/codereposervice/${codeRepo}?code=${code}`,
    );
  }
}
