import { K8sApiService } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { NavLoaderService } from '@app/services/nav-loader.service';
import { RESOURCE_TYPES } from '@app/utils';

@Injectable()
export class NavControlService {
  navConfigAdminAddress = 'custom/navconfig-admin.yaml';
  navConfigUserAddress = 'custom/navconfig-user.yaml';

  constructor(
    private readonly navLoader: NavLoaderService,
    private readonly k8sApi: K8sApiService,
  ) {}

  getAdminViewNavConfigs() {
    return this.navLoader
      .loadNavConfig(this.navConfigAdminAddress)
      .pipe(this.navLoader.parseYaml(), this.navLoader.mapToAuiNav());
  }

  getUserViewNavConfigs() {
    return this.navLoader
      .loadNavConfig(this.navConfigUserAddress)
      .pipe(this.navLoader.parseYaml(), this.navLoader.mapToAuiNav());
  }

  getClusterDeployed(cluster: string) {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.CLUSTERCONFIG,
      name: 'asm-cluster-config',
      cluster: cluster,
    });
  }
}
