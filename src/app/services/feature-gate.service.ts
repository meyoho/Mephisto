import {
  createRecursiveFilter,
  FeatureGateService as FeatureGateApi,
} from '@alauda/common-snippet';
import { NavItemConfig } from '@alauda/ui';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class FeatureGateService {
  constructor(private readonly featureGateApi: FeatureGateApi) {}

  filterEnabled = (items: NavItemConfig[], cluster = '') => {
    return this.featureGateApi.filterEnabled(
      items,
      item => item.gate,
      cluster,
      createRecursiveFilter(
        item => item.children,
        (item, children) => ({ ...item, children }),
      ),
    );
  };
}
