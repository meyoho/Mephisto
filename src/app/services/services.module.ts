import {
  AuthorizationInterceptorService,
  CommonLayoutModule,
  TOKEN_BASE_DOMAIN,
  TOKEN_GLOBAL_NAMESPACE,
  TOKEN_RESOURCE_DEFINITIONS,
  TranslateService,
} from '@alauda/common-snippet';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  Inject,
  InjectionToken,
  NgModule,
  Optional,
  SkipSelf,
} from '@angular/core';
import { NavControlService } from '@app/services/nav-control.service';
import { OAuthSecretValidatorService } from '@app/services/oauth-secret-validator.service';
import { RoleService } from '@app/services/role.service';
import { TerminalService } from '@app/services/terminal.service';
import { UiStateService } from '@app/services/ui-state.service';
import { RESOURCE_DEFINITIONS } from '@app/utils';

import { MonacoProviderService } from 'ng-monaco-editor';

import { Environments } from 'app/api/envs/types';
import { globalEnvironments } from 'app/app-global';
import { ApiGatewayInterceptor } from './api-gateway-interceptor.service';
import { GlobalLoadingInterceptor } from './global-loading.interceptor';
import { NAV_CONFIG_LOCAL_STORAGE_KEY } from './nav-loader.service';
import { PermissionService } from './permission.service';
import { ResourceErrorInterceptor } from './resource-error-interceptor';

export const ENVIRONMENTS = new InjectionToken<Environments>('ENVIRONMENTS');
export const TOKEN_ASM_BASE_DOMAIN = new InjectionToken<string>(
  'asm.alauda.io',
);

/**
 * Services in Angular App is static and singleton over the whole system,
 * so we will import them into the root module.
 *
 * This is to replace the Core module, which we believe is somewhat duplicates with the
 * purpose of global service module.
 */
@NgModule({
  imports: [CommonLayoutModule],
  providers: [
    PermissionService,
    MonacoProviderService,
    UiStateService,
    TerminalService,
    OAuthSecretValidatorService,
    NavControlService,
    RoleService,
    {
      provide: ENVIRONMENTS,
      useValue: globalEnvironments,
    },
    {
      provide: TOKEN_BASE_DOMAIN,
      useFactory: (env: Environments) => env.LABEL_BASE_DOMAIN,
      deps: [ENVIRONMENTS],
    },
    {
      provide: TOKEN_GLOBAL_NAMESPACE,
      useFactory: (env: Environments) => env.GLOBAL_NAMESPACE,
      deps: [ENVIRONMENTS],
    },
    {
      provide: TOKEN_ASM_BASE_DOMAIN,
      useFactory: (env: Environments) => `asm.${env.LABEL_BASE_DOMAIN}`,
      deps: [ENVIRONMENTS],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiGatewayInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResourceErrorInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GlobalLoadingInterceptor,
      multi: true,
    },
    {
      provide: TOKEN_RESOURCE_DEFINITIONS,
      useValue: RESOURCE_DEFINITIONS,
    },
    {
      provide: NAV_CONFIG_LOCAL_STORAGE_KEY,
      useValue: 'mephisto',
    },
  ],
})
export class ServicesModule {
  /* Make sure ServicesModule is imported only by one NgModule the RootModule */
  constructor(
    @Inject(ServicesModule)
    @Optional()
    @SkipSelf()
    parentModule: ServicesModule,
    monacoProvider: MonacoProviderService,
    // Inject the following to make sure they are loaded ahead of other components.
    _translate: TranslateService,
  ) {
    if (parentModule) {
      throw new Error(
        'ServicesModule is already loaded. Import only in AppModule.',
      );
    }

    monacoProvider.initMonaco();
  }
}
