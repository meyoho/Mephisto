import {
  K8sPermissionService,
  K8sResourceAction,
} from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { RESOURCE_TYPES } from '@app/utils/constants';

import { map, publishReplay, refCount } from 'rxjs/operators';
const MENU_AUTH_ITEM = ['platformview', 'projectview', 'clusterview'];
@Injectable({
  providedIn: 'root',
})
export class GlobalPermissionService {
  accountMenus$ = this.k8sPermission
    .isAllowed({
      type: RESOURCE_TYPES.VIEW,
      name: MENU_AUTH_ITEM,
      action: K8sResourceAction.GET,
    })
    .pipe(
      map(statuses =>
        MENU_AUTH_ITEM.reduce(
          (prev, curr, index) => ({
            [curr]: statuses[index],
            ...prev,
          }),
          {},
        ),
      ),
      publishReplay(1),
      refCount(),
    );

  showSwitchView$ = this.k8sPermission
    .isAllowed({
      type: RESOURCE_TYPES.VIEW,
      name: ['asm-userview', 'asm-manageview'],
    })
    .pipe(
      map(result => result.every(item => item)),
      publishReplay(1),
      refCount(),
    );

  constructor(private readonly k8sPermission: K8sPermissionService) {}
}
