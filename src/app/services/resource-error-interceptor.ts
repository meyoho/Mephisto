import { NotificationService } from '@alauda/ui';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Status } from 'app/typings';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

const writeActions = ['post', 'put', 'delete', 'patch'];

@Injectable({ providedIn: 'root' })
export class ResourceErrorInterceptor implements HttpInterceptor {
  constructor(private notification: NotificationService) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((e: HttpErrorResponse) => {
        if (
          writeActions.includes(req.method.toLowerCase()) &&
          this.isCommonResourceUrl(req)
        ) {
          this.triggerNotification(e);
        }
        if (this.isK8sErrorStatus(e.error as Status)) {
          return throwError(e.error);
        } else {
          return throwError(e);
        }
      }),
    );
  }

  private isCommonResourceUrl(req: HttpRequest<any>) {
    return !req.url.endsWith('/v1/selfsubjectaccessreviews');
  }

  private triggerNotification(e: HttpErrorResponse) {
    const k8sError = e.error as Status;
    if (this.isK8sErrorStatus(k8sError)) {
      this.notification.error({
        title: k8sError.reason,
        content: k8sError.message,
      });
    } else {
      this.notification.error({
        content: typeof e.error === 'string' ? e.error : e.message,
      });
    }
  }

  private isK8sErrorStatus(e: Status) {
    return e && e.metadata && e.apiVersion;
  }
}
