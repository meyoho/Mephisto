import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ASM_SUBPATH } from '@app/constants';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const RESOURCE_API_GROUP_VERSIONS: Dictionary<{
  group: string;
  version: string;
}> = {
  applications: {
    group: 'app.k8s.io',
    version: 'v1beta1',
  },
  releases: {
    group: 'catalog.alauda.io',
    version: 'v1alpha1',
  },
  secrets: {
    group: '',
    version: 'v1',
  },
  configmaps: {
    group: '',
    version: 'v1',
  },
  persistentvolumeclaims: {
    group: '',
    version: 'v1',
  },
  rolebindings: {
    group: 'rbac.authorization.k8s.io',
    version: 'v1',
  },
  domainbindings: {
    group: 'catalog.alauda.io',
    version: 'v1alpha1',
  },
  projects: {
    group: 'auth.alauda.io',
    version: 'v1alpha1',
  },
  virtualservices: {
    group: 'networking.istio.io',
    version: 'v1alpha3',
  },
  destinationrules: {
    group: 'networking.istio.io',
    version: 'v1alpha3',
  },
  policies: {
    group: 'authentication.istio.io',
    version: 'v1alpha1',
  },
  '*': {
    group: 'devops.alauda.io',
    version: 'v1alpha1',
  },
};

@Injectable()
export class PermissionService {
  constructor(private readonly http: HttpClient) {}

  canI(
    verb: string,
    namespace: string,
    resource: string,
    name = '',
  ): Observable<boolean> {
    return this.http
      .post(`${API_GATEWAY}/${ASM_SUBPATH}/api/v1/cani`, {
        resourceAttributes: {
          namespace: namespace || '*',
          resource,
          verb,
          ...this.getResourceApiGroupVersion(resource),
          name,
        },
      })
      .pipe(map((res: any) => res.allowed));
  }

  private getResourceApiGroupVersion(name: string) {
    return (
      RESOURCE_API_GROUP_VERSIONS[name] || RESOURCE_API_GROUP_VERSIONS['*']
    );
  }
}
