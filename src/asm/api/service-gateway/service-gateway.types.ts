import { KubernetesResource } from '@alauda/common-snippet';

export interface GatewaySpecServers {
  hosts: string[];
  port: {
    name: string;
    number: number;
    protocol: string;
  };
}

export interface GatewaySpec {
  selector?: {
    [key: string]: string;
  };
  servers: GatewaySpecServers[];
}
export interface Gateway extends KubernetesResource {
  spec: GatewaySpec;
}
