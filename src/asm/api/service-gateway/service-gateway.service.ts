import { K8sApiService, KubernetesResourceList } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { RouteParams } from '@app/typings';
import { Service } from '@app/typings/raw-k8s';
import { RESOURCE_TYPES } from '@app/utils';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { PROTOCOLS } from '../service-list';
import { Gateway } from './service-gateway.types';

export const LabelPrefix = 'servicemesh';
@Injectable()
export class ServiceGatewayService {
  constructor(private readonly k8sApi: K8sApiService) {}
  getServiceGatewayList(
    params: RouteParams,
  ): Observable<KubernetesResourceList<Gateway>> {
    return this.k8sApi.getResourceList({
      type: RESOURCE_TYPES.GATEWAY,
      cluster: params.cluster,
      namespace: params.namespace,
      queryParams: {
        ...params,
      },
    });
  }

  getGatewayDetail(params: RouteParams): Observable<Gateway> {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.GATEWAY,
      cluster: params.cluster,
      name: params.name,
      namespace: params.namespace,
    });
  }

  createGateway(cluster: string, payload: Gateway) {
    return this.k8sApi.postResource({
      type: RESOURCE_TYPES.GATEWAY,
      cluster: cluster,
      resource: payload,
    });
  }

  updateGateway(cluster: string, payload: Gateway) {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.GATEWAY,
      cluster: cluster,
      resource: payload,
    });
  }

  deleteGateway(cluster: string, payload: Gateway) {
    return this.k8sApi.deleteResource({
      type: RESOURCE_TYPES.GATEWAY,
      cluster: cluster,
      resource: payload,
    });
  }

  getMicroServicePorts(cluster: string, namespace: string, name: string) {
    return this.k8sApi
      .getResource({
        type: RESOURCE_TYPES.SERVICE,
        cluster: cluster,
        name: name,
        namespace: namespace,
      })
      .pipe(
        map((service: Service) => {
          return service.spec.ports.reduce((accum, port) => {
            const protocol = port.name.split('-')[0];
            const protocolName =
              PROTOCOLS.find(value => value.toLowerCase() === protocol) ||
              port.protocol;
            return ['HTTP', 'HTTP2'].includes(protocolName)
              ? [...accum, port.targetPort]
              : accum;
          }, []);
        }),
      );
  }
}
