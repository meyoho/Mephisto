import {
  API_GATEWAY,
  K8sApiService,
  KubernetesResourceList,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ASM_SUBPATH } from '@app/constants';
import { GenericStatus } from '@app/shared/components/status-icon/status-icon.component';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';

import { get } from 'lodash-es';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  AsmClusterConfigResponse,
  DefaultParams,
  ServiceMesh,
  ServiceMeshCrd,
  ServiceMeshStatus,
  Status,
  StatusType,
} from './service-mesh-api.types';

const ASM_CONFIG_NAME = 'asm-cluster-config';
@Injectable()
export class ServiceMeshApiService {
  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly http: HttpClient,
  ) {}

  getClusterConfig(cluster: string): Observable<AsmClusterConfigResponse> {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.CLUSTERCONFIG,
      cluster: cluster,
      name: ASM_CONFIG_NAME,
    });
  }

  updateClusterConfig(cluster: string, payload: AsmClusterConfigResponse) {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.CLUSTERCONFIG,
      cluster: cluster,
      resource: payload,
    });
  }

  getJaegerUrl(cluster: string) {
    return this.getClusterConfig(cluster).pipe(
      map(data => data.spec.jaegerURL),
    );
  }

  getGrafanaURLUrl(cluster: string) {
    return this.getClusterConfig(cluster).pipe(
      map(data => data.spec.grafanaURL),
    );
  }

  getServiceList(cluster: string, namespace: string) {
    return this.k8sApi.getResourceList({
      type: RESOURCE_TYPES.SERVICE,
      cluster: cluster,
      namespace: namespace,
    });
  }

  getHiddenServiceNames(cluster: string): Observable<string[]> {
    return this.getClusterConfig(cluster).pipe(
      map(res => {
        return res.spec.hiddenServiceNamesInTracing || [];
      }),
    );
  }

  getServiceMeshList(params?: RouteParams) {
    return this.http.get<KubernetesResourceList<ServiceMeshCrd>>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/servicemesh/manage`,
      {
        params,
      },
    );
  }

  getServiceMesh(params: RouteParams) {
    return this.http.get(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/servicemesh/manage/${params.name}`,
    );
  }

  getClusters() {
    return this.k8sApi
      .getGlobalResourceList({
        type: RESOURCE_TYPES.CLUSTER,
        namespaced: true,
      })
      .pipe(map(res => res.items));
  }

  deleteServiceMesh(name: string) {
    return this.http.delete(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/servicemesh/manage/${name}`,
    );
  }

  getDeployStatus(name: string): Observable<ServiceMeshStatus> {
    return this.http.get<ServiceMeshStatus>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/servicemesh/manage/${name}/status`,
    );
  }

  postServiceMesh(payload: ServiceMesh) {
    return this.http.post(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/servicemesh/manage`,
      payload,
    );
  }

  getClusterBaseDeployment(cluster: string) {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.CUSTOMRESOURCEDEFINITION,
      cluster: cluster,
      name: 'applications.app.k8s.io',
    });
  }

  cancelDeploymentServiceMesh(name: string) {
    return this.http.post(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/servicemesh/manage/${name}/cancel`,
      {},
    );
  }

  getDefaultParams(cluster: string) {
    return this.http.get<DefaultParams>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/servicemesh/manage/${cluster}/defaultparams`,
    );
  }
}
export const getStatusValue = (status: ServiceMeshStatus = {}) => {
  return Object.values(status).reduce((prev, curr) => {
    if (curr.status) {
      return [...prev, curr.status];
    }
    return prev;
  }, []);
};

export const isPendingStatus = (statusArray: string[]) => {
  return (
    statusArray.includes('Pending') ||
    (statusArray.includes('Synced') &&
      statusArray.includes('Unknown') &&
      statusArray.every(item => ['Synced', 'Unknown'].includes(item)))
  );
};

const getStatusName = (type: Status) => {
  switch (type) {
    case Status.DELETING:
      return GenericStatus.Deleting;
    case Status.PENDING:
      return GenericStatus.Deploying;
    case Status.FAILED:
      return GenericStatus.Failed;
    case Status.UNKNOWN:
      return GenericStatus.Stopped;
    case Status.SYNCED:
      return GenericStatus.Succeed;
    default:
      break;
  }
};

export const getOldDataStatus = (status: ServiceMeshStatus) => {
  const getStatus = () => {
    const statusArray = getStatusValue(status);
    switch (true) {
      case statusArray.includes('Deleting'):
        return Status.DELETING;
      case statusArray.every(item => item === 'Unknown'):
        return Status.UNKNOWN;
      case statusArray.includes('Failed'):
        return Status.FAILED;
      case isPendingStatus(statusArray):
        return Status.PENDING;
      default:
        return Status.SYNCED;
    }
  };
  return getStatusName(getStatus());
};

export const getServiceMeshStatus = (type: ServiceMeshStatus = {}) => {
  const status = get(type.global, 'status');
  if (!status) {
    // TODO: 兼容老数据，老数据无 global status
    return getOldDataStatus(type);
  }
  switch (true) {
    case status === StatusType.DELETING:
      return GenericStatus.Deleting;
    case status === StatusType.PENDING:
      return GenericStatus.Deploying;
    case status === StatusType.FAILED:
      return GenericStatus.Failed;
    case status === StatusType.UNKNOWN:
      return GenericStatus.Stopped;
    case status === StatusType.SYNCED:
      return GenericStatus.Succeed;
    default:
      return GenericStatus.Failed;
  }
};
