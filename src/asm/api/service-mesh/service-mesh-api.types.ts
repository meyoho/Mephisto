import { KubernetesResource } from '@alauda/common-snippet';
export interface AsmClusterConfigResponse extends KubernetesResource {
  spec: ClusterConfigSpec;
}

export interface ClusterConfigSpec {
  cluster?: string;
  forbiddenSidecarInjectNamespaces?: string[];
  grafanaURL?: string;
  hiddenServiceNamesInTracing?: string[];
  jaegerURL?: string;
  pilot?: {
    tracesampling: number;
  };
  registryAddress: string;
  ingressHost?: string;
  globalIngressHost?: string;
  externalHost?: string;
  clusterNodeIps?: string[];
  traceSampling: number;
  highAvailability: boolean;
  ipranges: {
    mode?: string;
    ranges: string[];
  };
  elasticsearch: {
    password?: string;
    url: string;
    username?: string;
    isDefault: boolean | string;
  };
  prometheusURL?: string;
  ingressScheme?: string;
  ingressgateway?: {
    nodeport: number;
  };
}

export interface ServiceMeshStatus {
  [key: string]: {
    status: string;
    error_message?: string;
  };
}

export interface ServiceMesh extends KubernetesResource {
  spec: ClusterConfigSpec;
}

export interface ServiceMeshCrd {
  serviceMesh: ServiceMesh;
  status?: ServiceMeshStatus;
}

export interface ServiceMeshCrdList extends ServiceMeshCrd {
  status: ServiceMeshStatus;
}

export enum StatusType {
  DELETING = 'Deleting',
  PENDING = 'Pending',
  FAILED = 'Failed',
  UNKNOWN = 'Unknown',
  SYNCED = 'Synced',
}

export enum Status {
  DELETING,
  PENDING,
  FAILED,
  UNKNOWN,
  SYNCED,
}

export interface HealthCheckJob {
  name: string;
  running: boolean;
  type: string;
  error?: string[];
}

export interface HealthCheckResponseBody {
  messageType: string;
  name?: string;
  latestUpdatedTime?: string;
  monitorJobs?: HealthCheckJob[];
  cluster?: string;
  event?: string;
  job?: string;
  status?: {
    message: string;
    phase: string;
  };
}

export interface DefaultParams {
  elasticsearch: {
    url: string;
  };
  prometheus: {
    url: string;
  };
  ipranges: {
    ranges: string[];
  };
  registryAddress: string;
}
