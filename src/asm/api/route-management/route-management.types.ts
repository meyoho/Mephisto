import { KubernetesResource } from '@alauda/common-snippet';

export interface VirtualService extends KubernetesResource {
  spec?: RouterSpec;
}

export interface RouterSpec {
  hosts: [] | any;
  condition?: {
    http?: RouterHttp[];
  };
  gateways?: string[];
  http?: RouterHttp[];
}

export interface RouterHttp {
  match?: RouterHttpMatch[];
  route?: RouterManageRouter[];
  fault?: FaultInjection;
  timeout?: string;
  retries?: HTTPRetry;
  mirror?: Mirror;
  rewrite?: HTTPRewrite;
}

export interface FaultInjection {
  abort?: { percentage: { value: number }; httpStatus: number };
  delay?: { percentage: { value: number }; fixedDelay: string };
}

export interface HTTPRetry {
  attempts: number;
  perTryTimeout: string;
}

export interface HTTPRewrite {
  uri: string;
  authority?: string;
}

export interface RouterHttpMatch {
  key?: string;
  method?: string;
  type?: string;
  value?: string;
  values?: {
    key: string;
    method: string;
    type: string;
    value: string;
  };
}

export interface Mirror {
  host: string;
  subset: string;
  port?: {
    number: number;
  };
}

export interface RouterManageRouter {
  destination: {
    host: string;
    subset?: string;
    port?: {
      number: number;
    };
  };
  weight: number;
  weightIndex?: number;
}

export interface RouterManageList {
  total: number;
  errors: any[];
  services: VirtualService[];
}

export interface ListResponse<T> {
  errors: any[];
  items: T;
  listMeta: {
    totalItems: number;
  };
}

export interface ListParams {
  project?: string;
  namespace?: string;
  pageIndex?: number;
  pageSize?: number;
  searchBy?: string;
  keywords?: string;
  sort?: string;
  direction?: string;
  cluster?: string;
}

export interface ServiceMeshResponse extends KubernetesResource {
  spec: ServiceMeshSpec;
}

interface ServiceMeshSpec {
  host: string;
  subsets: ServiceMeshSubsets[];
}

export interface ServiceMeshSubsets {
  name: string;
  labels: ServiceMeshLabels;
}
interface ServiceMeshLabels {
  app: string;
  version: string;
}

export interface ServiceMeshList {
  host: string;
  version: [{ name: string; value: string }];
}

export interface Services {
  host: string;
  version: [
    {
      value: string;
      name: string;
    },
  ];
}
