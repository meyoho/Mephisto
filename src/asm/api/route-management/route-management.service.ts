import { K8sApiService } from '@alauda/common-snippet';
import { Inject, Injectable } from '@angular/core';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import { MicroServiceStore } from '@asm/api/service-list';

import { get, groupBy, isEmpty, omit } from 'lodash-es';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import {
  RouterHttp,
  RouterHttpMatch,
  RouterManageRouter,
  RouterSpec,
  ServiceMeshResponse,
  Services,
  VirtualService,
} from './route-management.types';

@Injectable()
export class RouteManagementService {
  constructor(
    private readonly k8sApi: K8sApiService,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
    private readonly store: MicroServiceStore,
  ) {}

  getVirtualServiceList(params: RouteParams) {
    return this.k8sApi.getResourceList({
      type: RESOURCE_TYPES.VIRTUALSERVICE,
      cluster: params.cluster,
      namespace: params.namespace,
      queryParams: {
        ...params,
      },
    });
  }

  getDestinationruleList(
    cluster: string,
    namespace: string,
    labelSelector?: string,
  ): Observable<Services[]> {
    return this.k8sApi
      .getResourceList({
        type: RESOURCE_TYPES.DESTINATIONRULE,
        cluster: cluster,
        namespace: namespace,
        ...(labelSelector
          ? {
              queryParams: {
                ...{ labelSelector },
              },
            }
          : {}),
      })
      .pipe(
        catchError(() => of(null)),
        map(res => {
          return get(res, 'items', [])
            .filter((item: ServiceMeshResponse) => {
              return (
                get(
                  item,
                  `metadata.labels["${this.asmBaseDomain}/creator"]`,
                ) === 'asm-controller'
              );
            })
            .map(
              (item: ServiceMeshResponse) =>
                ({
                  host: item.spec.host,
                  version: item.spec.subsets.map(version => ({
                    value: version.name,
                    name: version.labels.version,
                  })),
                } as Services),
            );
        }),
      );
  }

  getDestinationrule(params: RouteParams) {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.DESTINATIONRULE,
      cluster: params.cluster,
      namespace: params.namespace,
      name: params.name,
    });
  }

  createVirtualService(cluster: string, payload: VirtualService) {
    return this.k8sApi.postResource({
      type: RESOURCE_TYPES.VIRTUALSERVICE,
      cluster: cluster,
      resource: this.filterPayload(payload),
    });
  }

  updateVirtualService(cluster: string, payload: VirtualService) {
    return this.k8sApi
      .putResource({
        type: RESOURCE_TYPES.VIRTUALSERVICE,
        cluster: cluster,
        resource: this.filterPayload(payload),
      })
      .pipe(
        map((res: any) => {
          const { spec, ...rest } = res;
          return {
            ...rest,
            spec: this.toModel(spec),
          };
        }),
      );
  }

  getVirtualServiceDetail(params: RouteParams): Observable<VirtualService> {
    return this.k8sApi
      .getResource({
        type: RESOURCE_TYPES.VIRTUALSERVICE,
        cluster: params.cluster,
        name: params.name,
        namespace: params.namespace,
      })
      .pipe(
        map(res => {
          const { spec, ...rest } = res;
          return {
            ...rest,
            spec: this.toModel(spec as RouterSpec),
          } as VirtualService;
        }),
      );
  }

  deleteVirtualService(cluster: string, payload: VirtualService) {
    return this.k8sApi.deleteResource({
      type: RESOURCE_TYPES.VIRTUALSERVICE,
      cluster: cluster,
      resource: payload,
    });
  }

  getGatewayList(cluster: string, namespace: string) {
    return this.k8sApi
      .getResourceList({
        type: RESOURCE_TYPES.GATEWAY,
        cluster: cluster,
        namespace: namespace,
      })
      .pipe(
        map((gateways: any) => {
          return gateways.items.map((item: any) => {
            return item.metadata.name;
          });
        }),
      );
  }

  getServicePort(
    cluster: string,
    namespace: string,
    name: string,
  ): Observable<VirtualService> {
    return this.k8sApi
      .getResource({
        type: RESOURCE_TYPES.SERVICE,
        cluster: cluster,
        name: name,
        namespace: namespace,
      })
      .pipe(
        map((service: any) => {
          return service.spec.ports.map((port: any) => {
            return port.targetPort;
          });
        }),
      );
  }

  toModel(spec: RouterSpec, isStore = false) {
    const { http, ...rest } = spec;
    const conditionMatch = http.filter(item => {
      return !!item.match;
    });
    const https = combinationConditionRule(
      spec,
      conditionMatch,
      isStore ? this.store : null,
    ) as RouterHttp[];

    const oldHttps = conditionMatch.map(item => {
      const { match, route, ...conditionRest } = item;
      return {
        ...conditionRest,
        match: [{ values: handleCondition(match[0]) }],
        route: mapRoute(route),
      };
    });
    // 保存原始处理逻辑，用来减少路由策略的改动
    this.store.originalConditionRule = oldHttps;
    return {
      ...rest,
      condition: {
        http: https,
      },
      http: http.filter(item => {
        return !item.match;
      }),
    };
  }

  filterPayload(payload: VirtualService) {
    const { spec, ...rest } = payload;
    const https = this.splitConditionRuleGroup(
      get(payload, 'spec.condition.http', []),
    );
    const conditionRule = https.reduce((accum, httpItem) => {
      return reduceConditionHttp(accum, httpItem);
    }, []);
    const weightRule = spec.http.map(http => {
      const { route, ...httpRest } = http;
      return {
        ...httpRest,
        route: handleRoute(route),
      };
    });

    return {
      ...rest,
      spec: {
        ...(spec.gateways && spec.gateways.length > 0
          ? omit(spec, 'condition')
          : omit(spec, ['gateways', 'condition'])),
        http: [...conditionRule, ...weightRule],
      },
    };
  }

  splitConditionRuleGroup(https: RouterHttp[]) {
    return https.reduce((prev, httpItem: RouterHttp) => {
      const { match, route, ...res } = httpItem;
      if (!get(match[0], 'values')) {
        return [...prev, httpItem];
      }
      const http = match.reduce((prev, curr) => {
        return [
          ...prev,
          {
            ...res,
            ...omit(curr, 'values'),
            match: get(curr, 'values', []),
            route,
          },
        ];
      }, []);
      return [...prev, ...http];
    }, []);
  }
}

/* eslint-disable sonarjs/cognitive-complexity */
// TODO: Refactor this function to reduce its Cognitive Complexity from 17 to the 15 allowed
function reduceConditionHttp(accum: any[], httpItem: RouterHttp) {
  const { match, route, ...rest } = httpItem;
  const checkedMatch = handleMatchData(match || []);
  return !isEmpty(checkedMatch)
    ? [
        ...accum,
        {
          match: checkedMatch,
          route: handleRoute(route || []),
          ...rest,
        },
      ]
    : accum;
}

function handleMatchData(match: RouterHttpMatch[]) {
  const body = match.reduce((accum: any, matchItem) => {
    const value =
      matchItem.method === 'regex' || matchItem.method === 'exact'
        ? matchItem.value
        : encodeURI(matchItem.value);
    const key = encodeURI(matchItem.key);
    if (!matchItem.type) {
      return accum;
    }
    switch (matchItem.type) {
      case 'port':
        accum[matchItem.type] = Number(matchItem.value);
        break;
      case 'sourceLabels':
        if (!accum[matchItem.type]) {
          accum[matchItem.type] = {};
        }
        Object.assign(accum[matchItem.type], {
          [key]: value,
        });
        break;
      case 'headers':
        if (!accum[matchItem.type]) {
          accum[matchItem.type] = {};
        }
        Object.assign(
          (accum[matchItem.type][key] = {
            [matchItem.method]: value,
          }),
        );
        break;
      default:
        if (!accum[matchItem.type]) {
          accum[matchItem.type] = {};
        }
        Object.assign(
          (accum[matchItem.type] = {
            [matchItem.method]: value,
          }),
        );
    }
    return accum;
  }, {});
  return isEmpty(Object.keys(body)) ? [] : [body];
}

function handleRoute(routes: RouterManageRouter[]) {
  return routes
    .filter(route => !!get(route, 'destination.host'))
    .map(route => {
      const subsetRoute =
        get(route, 'destination.subset', '') === 'all'
          ? {
              ...route,
              destination: omit(route.destination, 'subset'),
            }
          : route;
      return get(subsetRoute, 'destination.port.number')
        ? subsetRoute
        : {
            ...subsetRoute,
            destination: omit(subsetRoute.destination, 'port'),
          };
    });
}

function handleCondition(condition: any) {
  const types = Object.keys(condition);
  return types.reduce((accum: any, type) => {
    switch (type) {
      case 'port': {
        return [
          ...accum,
          {
            type,
            key: '',
            method: '',
            value: condition.port,
          },
        ];
      }
      case 'sourceLabels': {
        const labelKeys = Object.keys(condition[type]);
        const sourceLabels = labelKeys.reduce((labels, labelKey) => {
          const key = decodeURI(labelKey);
          return [
            ...labels,
            {
              type,
              key,
              method: '',
              value: decodeURI(condition[type][labelKey]),
            },
          ];
        }, []);
        return [...accum, ...sourceLabels];
      }
      case 'headers': {
        const headerKeys = Object.keys(condition[type]);
        const headers = headerKeys.reduce((headerList, headerKey) => {
          const key = decodeURI(headerKey);
          return [
            ...headerList,
            {
              type,
              key,
              method: Object.keys(condition[type][headerKey])[0],
              value: decodeURI(
                condition[type][headerKey][
                  Object.keys(condition[type][headerKey])[0]
                ],
              ),
            },
          ];
        }, []);
        return [...accum, ...headers];
      }
      default: {
        const method = Object.keys(condition[type])[0];
        const value = decodeURI(condition[type][method]);
        return [
          ...accum,
          {
            type,
            key: '',
            method,
            value,
          },
        ];
      }
    }
  }, []);
}

/**
 * 生成Workload信息，在微服务关系图、工作负载列表中展示规则条数和流量权重
 */
function generateWorkloadInfo(
  spec: RouterSpec,
  data: { [key: string]: RouterHttp[] },
  store: MicroServiceStore,
) {
  const conditions = Object.keys(data).reduce((prev, curr) => {
    const version = getSubset(curr.split(',')[2]);
    return {
      ...prev,
      [version]: {
        total: data[curr].length,
      },
    };
  }, {});
  const weights = spec.http.reduce((prev, curr) => {
    if (!curr.match) {
      return {
        ...prev,
        ...curr.route.reduce((routePrev, route) => {
          const version = getSubset(route.destination.subset);
          return {
            ...routePrev,
            ...{
              [version]: { weight: route.weight },
            },
          };
        }, {}),
      };
    }
    return prev;
  }, {});
  store.workloadInfo$.next(
    new Map(
      store.deployments.map(item => {
        return [
          item.version,
          {
            count: get(conditions, item.version, { total: 0 }).total,
            weight: get(weights, item.version, { weight: 0 }).weight,
          },
        ];
      }),
    ),
  );
}
/**
 * 组合route相同的条件规则为一组
 * [match: [{ values: [原来的match], ...其他策略 }]]
 * 用来最后处理多个match有不同的策略
 */
export function combinationConditionRule(
  spec: RouterSpec,
  routerHttps: RouterHttp[],
  store?: MicroServiceStore,
) {
  const data: { [key: string]: RouterHttp[] } = groupBy(routerHttps, item => {
    const { route } = item;
    return `${route.length === 1},${route[0].destination.host},${
      route[0].destination.subset
    }`;
  });

  if (store) {
    generateWorkloadInfo(spec, data, store);
  }

  return Object.values(data).map(item => {
    return {
      match: item.reduce((pre, cur) => {
        return [
          ...pre,
          {
            ...omit(cur, ['route', 'match']),
            values: handleCondition(cur.match[0]),
          },
        ];
      }, []),
      route: mapRoute(item[0].route),
    };
  });
}

function mapRoute(route: RouterManageRouter[]) {
  return route.map(conditionRoute =>
    conditionRoute.destination.subset
      ? conditionRoute
      : {
          ...conditionRoute,
          destination: {
            ...conditionRoute.destination,
            subset: 'all',
          },
        },
  );
}

export function getSubset(subset: string, income = false) {
  if (subset) {
    return income ? `asm-subset-${subset}` : subset.replace('asm-subset-', '');
  }
  return '';
}
