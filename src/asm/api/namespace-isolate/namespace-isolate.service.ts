import { K8sApiService } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { RESOURCE_TYPES } from '@app/utils';

import { Observable } from 'rxjs';

import { Sidecar } from './namespace-isolate.types';

@Injectable()
export class NamespaceIsolateApiService {
  constructor(private readonly k8sApi: K8sApiService) {}

  getSideCar(cluster: string, namespace: string): Observable<Sidecar> {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.SIDECAR,
      cluster: cluster,
      namespace: namespace,
      name: namespace,
    });
  }

  update(cluster: string, payload: Sidecar) {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.SIDECAR,
      cluster: cluster,
      resource: payload,
    });
  }
}
