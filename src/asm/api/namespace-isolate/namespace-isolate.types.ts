import { KubernetesResource } from '@alauda/common-snippet';

export interface Sidecar extends KubernetesResource {
  spec: SidecarSpec;
}

export interface SidecarSpec {
  egress: Hosts[];
}

interface Hosts {
  hosts: string[];
}
