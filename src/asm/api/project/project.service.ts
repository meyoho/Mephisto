import {
  API_GATEWAY,
  globalNamespace,
  K8sUtilService,
  KubernetesResource,
  ResourceListParams,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import {
  ClusterResponse,
  NamespaceResponse,
  ProjectListResponse,
  ProjectResponse,
} from './project.types';
@Injectable({
  providedIn: 'root',
})
export class ProjectService {
  constructor(
    private readonly http: HttpClient,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  PROJECT_URL = `${API_GATEWAY}/apis/auth.alauda.io/v1/projects`;
  private projectsStore: ProjectListResponse;
  getProjects(params?: ResourceListParams) {
    return this.http
      .get<ProjectListResponse>(`${API_GATEWAY}/auth/v1/projects`, {
        params: params,
      })
      .pipe(tap(res => (this.projectsStore = res)));
  }

  getProjectsStore() {
    return this.projectsStore ? of(this.projectsStore) : this.getProjects();
  }

  getProjectDetail(name: string): Observable<ProjectResponse> {
    return this.http.get<ProjectResponse>(`${this.PROJECT_URL}/${name}`);
  }

  getNamespaces(
    name: string,
    clusterName: string,
    params?: ResourceListParams,
  ) {
    return this.http.get<NamespaceResponse>(
      `${API_GATEWAY}/auth/v1/projects/${name}/clusters/${clusterName}/namespaces`,
      {
        params: params,
      },
    );
  }

  getNamespaceDetail(cluster: string, namespace: string) {
    return this.http.get<KubernetesResource>(
      `${API_GATEWAY}/kubernetes/${cluster}/api/v1/namespaces/${namespace}`,
    );
  }

  getCluster() {
    return this.http
      .get<ClusterResponse>(
        `${API_GATEWAY}/apis/clusterregistry.k8s.io/v1alpha1/namespaces/${globalNamespace}/clusters`,
      )
      .pipe(
        map((reuslt: ClusterResponse) => ({
          items: reuslt.items.map(item => this.k8sUtil.getName(item)),
        })),
      );
  }

  updateNamespace(name: string, cluster: string, params: KubernetesResource) {
    return this.http.patch(
      `${API_GATEWAY}/kubernetes/${cluster}/api/v1/namespaces/${name}`,
      params,
      {
        headers: {
          'Content-Type': 'application/merge-patch+json',
        },
      },
    );
  }
}
