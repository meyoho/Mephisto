import { KubernetesResource } from '@alauda/common-snippet';
export interface ListResult<T> {
  total: number;
  items: T[];
  errors: any[];
}

export interface ProjectListResponse {
  items: ProjectResponse[];
}

export interface ProjectResponse extends KubernetesResource {
  spec: {
    clusters: Cluster[];
  };
}

export interface Cluster {
  [key: string]: string;
}

export interface ResultResponse {
  name: string;
  currendCluster?: string;
  creationTimestamp: string;
  description?: string;
  displayName: string;
  creator: string;
  serviceMesh?: string;
  clusters?: Array<{ [key: string]: string }>;
  cluster?: string;
  __original: KubernetesResource;
}

export interface NamespaceResponse {
  items: KubernetesResource[];
}

export interface ClusterResponse {
  items: KubernetesResource[];
}
