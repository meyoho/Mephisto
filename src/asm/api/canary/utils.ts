import {
  Deployment,
  GenericWorkloadStatus,
  WorkloadStatusEnum,
} from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';

import { get } from 'lodash-es';

import { RecordStatus } from './canary.types';

export function handleRecordStatus(status: RecordStatus) {
  switch (status) {
    case 'Succeeded':
      return {
        icon: 'success',
        text: 'release_success',
      };
    case 'Failed':
      return {
        icon: 'failed',
        text: 'release_failed',
      };
    default:
      return {
        icon: 'deploying',
        text: 'releasing',
      };
  }
}

export function parseToWorkloadStatus(
  workloadType: string,
  data: Deployment,
): GenericWorkloadStatus {
  return workloadType === RESOURCE_TYPES.DEPLOYMENT
    ? {
        status: parseDeploymentStatus(data),
        desired: get(data, ['spec', 'replicas'], 0),
        current: get(data, ['status', 'availableReplicas'], 0),
      }
    : {
        status: WorkloadStatusEnum.stopped,
        desired: 0,
        current: 0,
      };
}

export function parseDeploymentStatus(res: Deployment) {
  const status = res.status;
  if (
    larger(res.spec.replicas, 0) &&
    equal(status.updatedReplicas, res.spec.replicas) &&
    equal(status.replicas, res.spec.replicas) &&
    equal(status.availableReplicas, res.spec.replicas) &&
    largerEqual(status.observedGeneration, res.metadata.generation)
  ) {
    return WorkloadStatusEnum.running;
  } else if (
    equal(res.spec.replicas, 0) &&
    equal(status.replicas, res.spec.replicas) &&
    equal(status.updatedReplicas, res.spec.replicas) &&
    equal(status.availableReplicas, res.spec.replicas) &&
    largerEqual(status.observedGeneration, res.metadata.generation)
  ) {
    return WorkloadStatusEnum.stopped;
  } else {
    return WorkloadStatusEnum.pending;
  }
}

function equal(x: number | string, y: number | string) {
  return defaultZero(x) === defaultZero(y);
}

function largerEqual(x: number, y: number) {
  return defaultZero(x) >= defaultZero(y);
}

function larger(x: number, y: number) {
  return defaultZero(x) > defaultZero(y);
}

function defaultZero(x: number | string) {
  return x || 0;
}
