import {
  K8sApiResourceService,
  K8sApiService,
  K8sResourceDefinition,
  KubernetesResource,
} from '@alauda/common-snippet';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RouteParams } from '@app/typings';
import { RESOURCE_DEFINITIONS, RESOURCE_TYPES } from '@app/utils';

import { Observable } from 'rxjs';

import { Canary } from './canary.types';

const DELETE_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
  body: {
    kind: 'DeleteOptions',
    apiVersion: 'v1alpha1',
    propagationPolicy: 'Background',
  },
};
@Injectable({
  providedIn: 'root',
})
export class CanaryService {
  constructor(
    private readonly k8sApi: K8sApiService,
    private readonly httpClient: HttpClient,
    private readonly k8sApiResource: K8sApiResourceService,
  ) {}

  getList(params: RouteParams) {
    return this.k8sApi.getResourceList<Canary>({
      type: RESOURCE_TYPES.CANARY,
      cluster: params.cluster,
      namespace: params.namespace,
      queryParams: {
        ...params,
      },
    });
  }

  getCanaryDetail(params: RouteParams): Observable<Canary> {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.CANARY,
      cluster: params.cluster,
      name: params.name,
      namespace: params.namespace,
    });
  }

  watchCanaryDetail(params: RouteParams): Observable<Canary> {
    return this.k8sApi.watchResource({
      type: RESOURCE_TYPES.CANARY,
      cluster: params.cluster,
      namespace: params.namespace,
      name: params.name,
    });
  }

  create(cluster: string, payload: Canary) {
    return this.k8sApi.postResource({
      type: RESOURCE_TYPES.CANARY,
      cluster: cluster,
      resource: payload,
    });
  }

  update(cluster: string, payload: Canary) {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.CANARY,
      cluster: cluster,
      resource: payload,
    });
  }

  // delete(cluster: string, payload: Canary) {
  //   return this.k8sApi.deleteResource({
  //     type: RESOURCE_TYPES.CANARY,
  //     cluster: cluster,
  //     resource: payload,
  //   });
  // }

  delete(cluster: string, payload: Canary) {
    return this.deleteByBackground(
      cluster,
      payload,
      RESOURCE_DEFINITIONS.CANARY,
    );
  }

  getDeployments(params: RouteParams) {
    return this.k8sApi.getResourceList({
      type: RESOURCE_TYPES.DEPLOYMENT,
      namespace: params.namespace,
      cluster: params.cluster,
    });
  }

  getDeployment(cluster: string, namespace: string, name: string) {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.DEPLOYMENT,
      cluster: cluster,
      namespace: namespace,
      name: name,
    });
  }

  watchDeployment(cluster: string, namespace: string, name: string) {
    return this.k8sApi.watchResource({
      type: RESOURCE_TYPES.DEPLOYMENT,
      cluster: cluster,
      namespace: namespace,
      name: name,
    });
  }

  updateDeployment(cluster: string, payload: KubernetesResource) {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.DEPLOYMENT,
      cluster: cluster,
      resource: payload,
    });
  }

  updateReplicas(num: number, cluster: string, payload: KubernetesResource) {
    return this.k8sApi.patchResource({
      type: RESOURCE_TYPES.DEPLOYMENT,
      cluster: cluster,
      resource: payload,
      part: { spec: { replicas: num } },
    });
  }

  deleteByBackground(
    cluster: string,
    payload: Canary,
    definition: K8sResourceDefinition,
  ) {
    return this.httpClient.delete(
      this.k8sApiResource.getApiPath(
        Object.assign(
          {
            cluster: cluster,
            namespace: payload.metadata.namespace,
            name: payload.metadata.name,
          },
          definition,
        ),
      ),
      DELETE_OPTIONS,
    );
  }
}
