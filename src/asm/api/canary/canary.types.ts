import { KubernetesResource } from '@alauda/common-snippet';

export interface Canary extends KubernetesResource {
  spec: {
    targetRef: {
      apiVersion: string;
      kind: string;
      name: string;
    };
    metrics: Metric[];
    analysis: {
      interval: string;
      threshold: number;
      maxWeight: number;
      stepWeight: number;
    };
    records?: Record[];
  };
  status?: {
    conditions: Array<{
      message: string;
    }>;
    phase: string;
  };
}

export interface Metric {
  name: string;
  thresholdRange: {
    min?: number;
    max?: number;
  };
}

export type RecordStatus =
  | 'Initializing'
  | 'Initialized'
  | 'Waiting'
  | 'Progressing'
  | 'Promoting'
  | 'Finalising'
  | 'Succeeded'
  | 'Failed';

export interface Record {
  id: string;
  name?: string;
  namespace?: string;
  startTime?: string;
  endTime?: string;
  currentWeight?: number;
  status?: RecordStatus;
  events?: Event[];
}

export interface Event {
  eventName: string;
  phase: string;
  eventType: string;
  message: string;
  timestamp: string;
  weight: string;
}
