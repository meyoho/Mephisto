import {
  API_GATEWAY,
  getApiPrefixParts,
  K8sApiService,
  K8sResourceAction,
  K8sResourceDefinition,
  KubernetesResource,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { concat, sortBy, uniqBy } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { APIResource, APIResourceList } from 'app/typings';

export interface ResourceManagementListFetchParams {
  definition: K8sResourceDefinition;
}

export interface Category {
  namespaced: APIResource[];
  cluster: APIResource[];
}

@Injectable()
export class ResourceListService {
  private _resourceType: APIResource;
  private readonly resourceType$$ = new BehaviorSubject<APIResource>(null);
  readonly resourceType$ = this.resourceType$$
    .asObservable()
    .pipe(filter(_ => !!_));

  get resourceType() {
    return this._resourceType;
  }

  readonly category$ = new BehaviorSubject<Category>(null);
  readonly resourceData$ = new BehaviorSubject<KubernetesResource>(null);
  readonly navLoading$ = new BehaviorSubject<boolean>(true);

  cluster: string;
  initialized: boolean;

  constructor(
    public k8sApi: K8sApiService,
    private readonly httpClient: HttpClient,
  ) {
    this.reducer = this.reducer.bind(this);
  }

  getResourceTypes(cluster: string) {
    return this.httpClient.get<APIResourceList[]>(
      `${API_GATEWAY}/acp/v1/resources/${cluster}/resourcetypes`,
    );
  }

  getResourceDefinition(resourceType: APIResource) {
    return {
      type: resourceType.name,
      ...getApiPrefixParts(resourceType.groupVersion),
    };
  }

  initService(cluster: string) {
    this.initialized = false;
    this.cluster = cluster;

    if (cluster) {
      this.initResourceList();
    } else {
      this.navLoading$.next(false);
    }
  }

  initResourceList() {
    this.navLoading$.next(true);
    this.getResourceTypes(this.cluster)
      .pipe(
        map(apiResources =>
          this.sortCategory(
            apiResources.reduce<{
              namespaced: APIResource[];
              cluster: APIResource[];
            }>(this.reducer, {
              namespaced: [],
              cluster: [],
            }),
          ),
        ),
      )
      .subscribe(
        category => {
          const resourceType = this._resourceType;
          if (!this.initialized || !(resourceType && resourceType.kind)) {
            this.setResourceType(category.namespaced[0]);
            this.initialized = true;
          }
          this.category$.next(category);
        },
        () => {},
        () => this.navLoading$.next(false),
      );
  }

  private reducer(
    accum: { namespaced: APIResource[]; cluster: APIResource[] },
    curr: APIResourceList,
  ) {
    const categories = (curr.groupVersion.includes('istio.io')
      ? curr.resources
      : []
    ).map(item => ({ ...item, groupVersion: curr.groupVersion }));

    const namespacedKinds = categories.filter(
      ({ namespaced, verbs }) =>
        namespaced && verbs.includes(K8sResourceAction.LIST),
    );
    const clusterKinds = categories.filter(
      ({ namespaced, verbs }) =>
        !namespaced && verbs.includes(K8sResourceAction.LIST),
    );
    return {
      namespaced: uniqBy(concat(accum.namespaced, namespacedKinds), 'kind'),
      cluster: uniqBy(concat(accum.cluster, clusterKinds), 'kind'),
    };
  }

  /**
   * TODO: 资源类别排序，暂用首字母默认排序
   * @param rawCategory
   */
  private sortCategory(rawCategory: {
    namespaced: APIResource[];
    cluster: APIResource[];
  }) {
    return {
      namespaced: sortBy(rawCategory.namespaced, 'kind'),
      cluster: sortBy(rawCategory.cluster, 'kind'),
    };
  }

  setResourceType(resourceType: APIResource): void {
    this._resourceType = resourceType;
    this.resourceType$$.next(resourceType);
  }
}
