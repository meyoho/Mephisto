import { NgModule } from '@angular/core';

import { CanaryService } from './canary';
import { DataPanelService } from './data-panel/data-panel.service';
import { ImageRepositoryApiService } from './image/image-api.service';
import { NamespaceIsolateApiService } from './namespace-isolate/namespace-isolate.service';
import { OverviewService } from './overview/overview.service';
import { ProjectService } from './project/project.service';
import { ResourceListService } from './resource-list/resource-list.service';
import { RouteManagementService } from './route-management/route-management.service';
import { SecurityStrategyService } from './security-strategy/security-strategy.service';
import { ServiceGatewayService } from './service-gateway/service-gateway.service';
import { MicroServiceStore, ServiceListService } from './service-list';
import { ServiceMeshApiService } from './service-mesh/service-mesh-api.service';
import { ServiceTopologyApiService } from './service-topology/service-topology-api.service';
import { TrafficPolicyService } from './traffic-policy/traffic-policy.service';
@NgModule({
  providers: [
    ServiceMeshApiService,
    ServiceTopologyApiService,
    RouteManagementService,
    SecurityStrategyService,
    TrafficPolicyService,
    ProjectService,
    ServiceListService,
    ResourceListService,
    MicroServiceStore,
    ImageRepositoryApiService,
    OverviewService,
    ServiceGatewayService,
    NamespaceIsolateApiService,
    DataPanelService,
    CanaryService,
  ],
})
export class ApiModule {}
