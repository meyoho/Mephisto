import { ChartOptions, SourceValue } from '@app/shared/components/chart';
export enum ChartType {
  'INFLOW' = 'rps_in',
  'OUTFLOW' = 'rps_out',
  'RESPONSETIME' = 'response_time',
}
export interface Workloads {
  name: string;
  pods: Instances[];
  services?: string[];
}
export interface Instances {
  name: string;
  apps?: string[];
}

export interface ChartModel {
  [key: string]: ModelInfo;
}

export interface ModelInfo {
  options: ChartOptions[];
  data: SourceValue[];
  avg?: number;
  avgTitle?: string;
  title: string;
  time?: [number, number];
  unit?: string;
  indicator?: string;
  indicators?: Array<{ [key: string]: string }>;
}

export interface WorkloadPath {
  name: string;
  paths: string[];
}
