import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ASM_SUBPATH } from '@app/constants';
import { RouteParams } from '@app/typings';

import { Instances, Workloads } from './data-panel.types';
@Injectable({
  providedIn: 'root',
})
export class DataPanelService {
  constructor(private readonly http: HttpClient) {}

  getWorkload(params: RouteParams) {
    return this.http.get<Workloads[]>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/namespaces/${params.namespace}/workloads`,
      {
        params: {
          cluster: params.cluster,
          start_time: params.start,
          end_time: params.end,
        },
      },
    );
  }

  getPods(params: RouteParams) {
    return this.http.get<Instances[]>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/namespaces/${params.namespace}/workloads/${params.workload}/pods`,
      {
        params: {
          cluster: params.cluster,
          start_time: params.start,
          end_time: params.end,
          instance: params.instance ? 'only' : '',
        },
      },
    );
  }

  getClients(params: RouteParams) {
    return this.http.get<Instances[]>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/namespaces/${params.namespace}/workloads/${params.workload}/clients`,
      {
        params: {
          cluster: params.cluster,
          start_time: params.start,
          end_time: params.end,
        },
      },
    );
  }

  getClientMetrics = (params: RouteParams) => {
    return this.http.get(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/workloadmetrics/${params.name}/clients`,
      {
        params: {
          cluster: params.cluster,
          namespace: params.namespace,
          start_time: params.start,
          end_time: params.end,
          step: params.step,
          sourceworkloads: params.sourceWorkloads,
        },
      },
    );
  };

  getMetricWorkload = (params: RouteParams) => {
    return this.http.get(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/workloadmetrics`,
      {
        params: {
          cluster: params.cluster,
          namespace: params.namespace,
          workload: params.name,
          start_time: params.start,
          end_time: params.end,
          step: params.step,
        },
      },
    );
  };

  getInstance = (params: RouteParams) => {
    return this.http.get(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/metrics/${params.namespace}/pod/${params.name}`,
      {
        params: {
          cluster: params.cluster,
          start_time: params.start,
          end_time: params.end,
          step: params.step,
        },
      },
    );
  };

  getInstanceList = (params: RouteParams) => {
    const metrics = params.metrics ? { metrics: params.metrics } : {};
    const quantiles = params.quantiles ? { quantiles: params.quantiles } : {};
    const sidecar = params.sidecar ? { sidecar: params.sidecar } : {};
    return this.http.get(`${API_GATEWAY}/${ASM_SUBPATH}/api/v1/podmetrics`, {
      params: {
        namespace: params.namespace,
        cluster: params.cluster,
        start_time: params.start,
        end_time: params.end,
        step: params.step,
        instances: params.instances,
        ...metrics,
        ...quantiles,
        ...sidecar,
      },
    });
  };

  getPath(params: RouteParams) {
    return this.http.get(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/namespaces/${params.namespace}/workloads/${params.workload}/paths`,
      {
        params: {
          cluster: params.cluster,
        },
      },
    );
  }

  setPath(params: RouteParams, payload: string[]) {
    return this.http.put(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/namespaces/${params.namespace}/workloads/${params.name}/paths`,
      { paths: payload },
      {
        params: {
          cluster: params.cluster,
        },
      },
    );
  }

  getApiMetrics = (params: RouteParams) => {
    return this.http.get(`${API_GATEWAY}/${ASM_SUBPATH}/api/v1/apimetrics`, {
      params: {
        namespace: params.namespace,
        workload: params.name,
        cluster: params.cluster,
        method: params.method,
        path: params.path,
        start_time: params.start,
        end_time: params.end,
        step: params.step,
      },
    });
  };

  getMethods(params: RouteParams) {
    return this.http.get(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/namespaces/${params.namespace}/workloads/${params.workload}/methods`,
      {
        params: {
          cluster: params.cluster,
          start_time: params.start,
          end_time: params.end,
        },
      },
    );
  }
}
