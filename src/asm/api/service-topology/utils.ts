import {
  Belong,
  NodeType,
} from '@asm/api/service-topology/service-topology-api.types';

import { groupBy } from 'lodash-es';

import { config, Placement } from './config';
export enum VisStatus {
  Default = 'default',
  Error = 'error',
  Success = 'success',
  Waring = 'warning',
  Disable = 'disable',
}
export interface Point {
  x: number;
  y: number;
}
export interface Size {
  width: number;
  height: number;
}

export interface SourceNode {
  id: string;
  label?: string | string[];
  status?: VisStatus;
  group?: string;
  icon?: string;
  x?: number;
  y?: number;
}

export interface GraphData {
  nodes: SourceNode[];
  lines: SourceLine[];
  groupLines?: Map<string, SourceLine[]>;
}

export enum DisplayState {
  highlight = 'highlight',
  disabled = 'disabled',
  default = 'default',
}

export interface SourceLine {
  source_id: string;
  target_id: string;
  status?: VisStatus;
  label?: string | string[];
  index?: number;
  groupNum?: number;
  num?: number;
}

export interface VisElement {
  id: string;
}
export interface VisNode extends VisElement {
  app?: string;
  label?: string[];
  depth?: number;
  group?: string;
  status: VisStatus;
  position: Point;
  path: string;
  namespace: string;
  node_type: string;
  workload?: string;
  service: string;
  children?: VisNode[];
  leafCount?: number;
  has_istio_sidecar: boolean;
  has_TLS: boolean;
  has_CB: boolean;
  tipData: TipData;
  belong: Belong;
}

export interface TipData {
  rpsIn: number;
  rpsOut: number;
  rpsInErrorRate: number;
}

export interface VisLine extends VisElement {
  status: VisStatus;
  source_id: string;
  target_id: string;
  label: {
    id: string;
    position: Point;
    texts: string[];
    rotate: string;
  };
  d: string;
  start: Point;
  end: Point;
  self?: boolean;
  textX?: number;
  textY?: number;
  marker?: string;
}

export interface NodeGroup extends VisElement {
  name: string;
  position: Point;
  size: Size;
  stroke?: string;
  text: {
    x: number;
    y: number;
    fill: string;
  };
}

export interface VisStatusColor {
  default?: string;
  error?: string;
  success?: string;
  warning?: string;
  disable?: string;
}

export interface VisConfig {
  lineLabelFontSize?: number;
  lineLabelLineHiehgt?: number;
  nodeLabelFontSize?: number;
  nodeLabelLineHeight?: number;
  nodeRadius?: number;
  nodeStrokeWidth?: number;
  nodeHorizontalDis?: number;
  nodeVerticalDis?: number;
  activeNodeStrokeWidth?: number;
  lineStrokeWidth?: number;
  activeLineStrokeWidth?: number;
  markerWidth?: number;
  markerHeight?: number;
  nodeTagWidth?: number;
  arcX?: number;
  arcY?: number;
  arcX2?: number;
  arcY2?: number;
  curve?: number;
  homogeneous?: number;
  lineTextDiff?: number;
  groupFillX?: number;
  groupFillY?: number;
  groupHeightFill?: number;
  nodeStrokeColor?: VisStatusColor;
  nodeFillColor?: VisStatusColor;
  nodeTextColor?: VisStatusColor;
  lineStrokeColor?: VisStatusColor;
  lineTextColor?: VisStatusColor;
  zoomFactor?: number;
  zoomScaleExtent?: [number, number];
  strokeOpacity: [number, number];
  placement: {
    [Placement.TOP_START]: { x: number; y: number };
  };
}

export interface VisTree {
  depth: number;
  root: VisNode;
}

export function getLineKey(line: SourceLine) {
  const lineskey = [line.source_id, line.target_id].sort();
  return `${lineskey[0]}:${lineskey[1]}`;
}

export function getLineGroup(lineGroup: SourceLine[] = []) {
  const groups = groupBy(lineGroup, 'groupNum');
  let upGroup = groups[1] || [];
  let downGroup = groups[-1] || [];
  const maxLength =
    lineGroup.length % 2 === 0
      ? lineGroup.length / 2
      : (lineGroup.length + 1) / 2;
  if (upGroup.length === downGroup.length) {
    const mapNum = (item: SourceLine, index: number) => ({
      ...item,
      num: index + 1,
    });
    upGroup = upGroup.map(mapNum);
    downGroup = downGroup.map(mapNum);
  } else {
    let maxGroup = upGroup.length > downGroup.length ? upGroup : downGroup;
    let minGroup = upGroup.length < downGroup.length ? upGroup : downGroup;
    let len = maxLength;
    minGroup = minGroup.map(item => ({ ...item, num: len-- }));
    maxGroup = maxGroup.map((item, index) => {
      return {
        ...item,
        num: index + 1 <= maxLength ? index + 1 : -1 * (index + 1),
      };
    });
    return [...maxGroup, ...minGroup];
  }
  return [...upGroup, ...downGroup];
}
export function genereatePolygonPath(
  center: Point,
  radius: number,
  edgeNumber = 6,
) {
  const angles = Array.from({ length: edgeNumber }).map((_, i) => {
    return (i + 1) * ((Math.PI * 2) / edgeNumber) - Math.PI / edgeNumber;
  });
  const points = angles.map(angle => {
    return [
      center.x + Math.cos(angle) * radius,
      center.y - Math.sin(angle) * radius,
    ];
  });
  return `M${points.join('L')}Z`;
}

export function calculateLineOffset(deltaY: number, deltaX: number) {
  const offsetAngle = Math.PI / 2 - Math.abs(Math.atan2(deltaY, deltaX));
  const h = config.nodeRadius * Math.cos(Math.PI / 6);
  const markerWidth = config.markerWidth;
  if (Math.abs(offsetAngle) >= Math.PI / 3) {
    return {
      x: h + markerWidth,
      y: (h + (deltaY < 0 ? -1 : 1) * markerWidth) / Math.tan(offsetAngle),
    };
  } else {
    // distance from central point to cross point
    const dis = h / Math.cos(Math.abs(offsetAngle) - Math.PI / 6);
    return {
      x: (dis + markerWidth) * Math.sin(offsetAngle),
      y: (deltaY < 0 ? -1 : 1) * (dis + markerWidth) * Math.cos(offsetAngle),
    };
  }
}

export function getArcHeight(dx: number, dy: number, r: number) {
  if (r === 0) {
    return 0;
  }
  const x = Math.abs(Math.sqrt(Math.pow(dy, 2) + Math.pow(dx, 2)) / 2);
  const h = Math.sqrt(Math.pow(r, 2) - Math.pow(x, 2));
  return r - h;
}

export function isGateway(texts: string[]): boolean {
  return texts.some((text = '') => text.indexOf('ingress') > 0);
}

export function getStatus(error: number, rps: number) {
  switch (true) {
    case error === 0 && rps === 0:
      return VisStatus.Disable;
    case error > 0 && error <= 0.1:
      return VisStatus.Success;
    case error > 0.1 && error <= 20:
      return VisStatus.Waring;
    case error > 20:
      return VisStatus.Error;
    default:
      return VisStatus.Default;
  }
}

export function getTwoway({ x, y }: { x: number; y: number }) {
  const c = {
    x1: x + config.arcX,
    y1: y - config.arcY,
    x2: x - config.arcX2,
    y2: y + config.arcY2,
    mx: x,
    x: x - (config.markerWidth + config.nodeRadius),
    y: y,
  };
  return `M${c.mx} ${c.y}, C${c.x1} ${c.y1}, ${c.x2} ${c.y2}, ${c.x} ${c.y}`;
}

/* eslint-disable sonarjs/cognitive-complexity */
// TODO: Refactor this function to reduce its Cognitive Complexity from 43 to the 15 allowed
export function getArcParameter(
  line: SourceLine,
  offset: { x: number; y: number },
  source: VisNode,
  target: VisNode,
  position: { x: number; y: number },
  groupLines: Map<string, SourceLine[]>,
) {
  const { curve, homogeneous, lineTextDiff, arcY, lineLabelFontSize } = config;
  const result = {
    d: '',
    textY: position.y + lineLabelFontSize / 2 - 2,
    textX: position.x,
  };
  const end = {
    x: target.position.x - offset.x,
    y: target.position.y - offset.y,
  };
  let dr = 0;
  const key = getLineKey(line);
  const {
    position: { x: x1, y: y1 },
  } = source;
  const {
    position: { x: x2, y: y2 },
  } = target;
  const dx = x2 - x1;
  const dy = y2 - y1;
  const groupLine: SourceLine[] = groupLines.get(key);
  const lineArray = getLineGroup(groupLine);
  const existLine = lineArray.find(
    l => line.source_id === l.source_id && line.index === l.index,
  );
  if (line.source_id === line.target_id) {
    result.d = getTwoway({ x: source.position.x, y: source.position.y });
    result.textY = y1 - arcY / 3 - lineLabelFontSize / 2 + lineTextDiff;
  } else if (lineArray.length % 2 !== 0 && existLine.num === 1) {
    if (dx < 0 && x1 > x2 && offset.x > 0) {
      end.x = target.position.x + offset.x;
      end.y = target.position.y + offset.y;
    }
    result.d = `M${x1} ${y1} L${end.x} ${end.y}`;
  } else {
    if (dx < 0 && x1 > x2 && offset.x > 0) {
      end.x = target.position.x + offset.x - 2;
      end.y = target.position.y + offset.y + 4;
    }
    dr =
      (Math.sqrt(dx * dx + dy * dy) * (existLine.num + homogeneous)) /
      (curve * homogeneous);
    if (existLine.num < 0) {
      dr =
        (Math.sqrt(dx * dx + dy * dy) * (-1 * existLine.num + homogeneous)) /
        (curve * homogeneous);
      result.d = `M ${x1} ${y1} A ${dr}, ${dr} 0 0,0 ${end.x} ${end.y}`;
    } else {
      result.d = `M ${x1} ${y1} A ${dr}, ${dr} 0 0,1 ${end.x} ${end.y}`;
    }
  }

  if (dr) {
    const arcHeight = getArcHeight(dx, dy, dr);
    const offsetY =
      (dx / Math.sqrt(Math.pow(dy, 2) + Math.pow(dx, 2))) * arcHeight;
    const offsetX = Math.sqrt(Math.pow(arcHeight, 2) - Math.pow(offsetY, 2));
    result.textX =
      x1 < x2 && y1 > y2 ? position.x - offsetX : position.x + offsetX;
    result.textY = position.y - offsetY;
    const halfLineFontHeight = lineLabelFontSize / 2;
    if ((x1 < x2 && y1 > y2) || (x1 > x2 && y1 > y2)) {
      result.textX = position.x - offsetX;
    } else if ((x1 < x2 && y1 < y2) || (x1 > x2 && y1 < y2)) {
      result.textX = position.x + offsetX;
    }

    if (
      (x1 < x2 && y1 > y2) ||
      (x1 < x2 && y1 < y2) ||
      (x1 < x2 && y1 === y2)
    ) {
      result.textY = result.textY + halfLineFontHeight;
    }

    if (x1 > x2 && y1 > y2) {
      result.textY = result.textY - halfLineFontHeight;
    }
  }
  return result;
}

export function strokeWidth(
  id: string,
  currentIds: Set<string> = new Set(),
  type: string,
) {
  const ids = Array.from(currentIds.values()) || [];
  const activeWidth =
    type === 'node'
      ? config.activeNodeStrokeWidth
      : config.activeLineStrokeWidth;
  const defaultWidth =
    type === 'node' ? config.nodeStrokeWidth : config.lineStrokeWidth;
  return currentIds.size && ids[0] === id ? activeWidth : defaultWidth;
}

export function strokeOpacity(
  has: boolean,
  currentIds: Set<string> = new Set(),
) {
  return !currentIds.size ? 1 : has ? 1 : 0.2;
}

export function getNodeType({
  node_type: nodetType,
  has_istio_sidecar: hasIstioSidecar,
  label,
  workload,
}: VisNode) {
  if (isGateway(label)) {
    return NodeType.IngressGateway;
  }
  if (nodetType === 'service') {
    if (workload === 'unknown') {
      return NodeType.Unknown;
    }
    return NodeType.Service;
  } else if (nodetType === 'workload') {
    if (hasIstioSidecar === false) {
      return NodeType.MissingSidecar;
    }
    return NodeType.Workload;
  }
}

