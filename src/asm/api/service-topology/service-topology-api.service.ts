import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ASM_SUBPATH } from '@app/constants';

import { get, groupBy, isEmpty, sortBy } from 'lodash-es';
import { Observable } from 'rxjs';

import {
  EdgeMetadata,
  NodeMetadata,
  RequestParam,
  ServiceMetricsResponse,
  ServiceNode,
  ServiceTopology,
} from './service-topology-api.types';
import {
  getLineKey,
  getStatus,
  GraphData,
  SourceNode,
  VisStatus,
} from './utils';
@Injectable()
export class ServiceTopologyApiService {
  constructor(private readonly http: HttpClient) {}

  getGraphData(
    namespace: string,
    cluster: string,
    { start_time, end_time, inject_service_nodes }: RequestParam,
  ) {
    return this.http.get<ServiceTopology>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/servicemesh/graphs/${namespace}`,
      {
        params: {
          start_time,
          end_time,
          inject_service_nodes,
          cluster,
        },
      },
    );
  }

  getMetrics(params: RequestParam): Observable<ServiceMetricsResponse> {
    return this.http.get<ServiceMetricsResponse>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/servicemesh/metrics`,
      {
        params: {
          ...params,
        },
      },
    );
  }

  /* eslint-disable sonarjs/cognitive-complexity */
  // TODO: Refactor this function to reduce its Cognitive Complexity from 43 to the 15 allowed
  toSourceData({ nodes, edges }: ServiceTopology): GraphData {
    const groupLines = new Map();
    if (!nodes) {
      return {
        nodes: [],
        lines: [],
        groupLines,
      };
    }
    edges = sortBy(edges, ['source_id', 'target_id']) || [];
    nodes = sortBy(nodes, 'app');

    const lines = edges.map(edge => {
      const key = getLineKey(edge);
      edge.source_id = edge.source_id.toString();
      edge.target_id = edge.target_id.toString();
      let index = 0;
      const tipData = handelTpiValue(edge.metadata, 'lines');
      const status = getStatus(
        tipData.errorRate,
        get(edge, 'metadata.http', 0),
      );
      const groupNum =
        edge.source_id < edge.target_id
          ? 1
          : edge.source_id === edge.target_id
          ? 0
          : -1;
      if (groupLines.has(key)) {
        index = groupLines.get(key).length;
        groupLines.set(key, [
          ...groupLines.get(key),
          { ...edge, groupNum, index },
        ]);
      } else {
        groupLines.set(key, [{ ...edge, groupNum, index }]);
      }
      return {
        source_id: edge.source_id,
        target_id: edge.target_id,
        groupNum,
        index,
        label: ['' + tipData.rps, `${tipData.errorRate}%`],
        status,
      };
    });

    /* eslint-disable @typescript-eslint/camelcase */
    const handledNodes: SourceNode[] = nodes.reduce((prevNode, currNode) => {
      const {
        id,
        app,
        version,
        namespace,
        workload,
        node_type,
        service,
        has_TLS,
        has_CB,
        combination,
        has_istio_sidecar,
        belongs_to,
      } = currNode;
      const microservice = get(belongs_to, 'microservice', null);
      const manualCombination = get(currNode, 'combination');

      const groups = groupBy(
        nodes.filter(
          item => this.microserviceNode(item) || this.unknownWorkloadNode(item),
        ),
        manualCombination && !isEmpty(edges)
          ? manualCombination
          : item => item.belongs_to.microservice,
      );
      const group =
        groups[combination && !isEmpty(edges) ? combination : microservice] ||
        [];
      const requestValues = edges.reduce((prev, edge) => {
        const exist =
          edge.source_id === id.toString() || edge.target_id === id.toString();
        return exist ? [get(edge, 'metadata.http', 0), ...prev] : prev;
      }, []);
      const node = {
        id: id.toString(),
        app,
        label:
          group.length > 0 &&
          (node_type !== 'service' ||
            (node_type === 'service' && workload === 'unknown'))
            ? manualCombination && isEmpty(edges)
              ? [app, version]
              : [version]
            : [node_type === 'service' ? service : app, version],
        group: combination && !isEmpty(edges) ? combination : microservice,
        service,
        namespace,
        workload,
        node_type,
        has_TLS,
        has_CB,
        has_istio_sidecar,
        tipData: handelTpiValue(currNode.metadata, 'node'),
        belong: belongs_to,
        status:
          Math.max(...requestValues) > 0
            ? VisStatus.Success
            : VisStatus.Default,
      };
      return [node, ...prevNode];
    }, []);

    return {
      nodes: handledNodes,
      lines,
      groupLines,
    };
  }

  unknownWorkloadNode(item: ServiceNode) {
    return item.node_type === 'service' && item.workload === 'unknown';
  }

  microserviceNode(item: ServiceNode) {
    return item.node_type !== 'service' && get(item, 'belongs_to.microservice');
  }

  getPartialViewDate(params: {
    namespace: string;
    service?: string;
    workload?: string;
    start_time: string;
    end_time: string;
    inject_service_nodes: string;
    cluster: string;
    selected_namespace?: string;
  }): Observable<ServiceTopology> {
    return this.http.get<ServiceTopology>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/servicemesh/nodegraphs`,
      {
        params,
      },
    );
  }
}

function handelTpiValue(
  metadata: NodeMetadata | EdgeMetadata = {},
  type: string,
) {
  const multiple = 100;
  if (type === 'node') {
    const {
      httpIn = 0,
      httpOut = 0,
      httpIn4xx = 0,
      httpIn5xx = 0,
    } = metadata as NodeMetadata;
    const errorCount = httpIn4xx + httpIn5xx;
    return {
      rpsIn: lessThanValue(httpIn),
      rpsOut: lessThanValue(httpOut),
      rpsInErrorRate: toFixed((errorCount / httpIn) * multiple) || 0,
    };
  }
  const { http = 0, http4xx = 0, http5xx = 0 } = metadata as EdgeMetadata;
  const errorCount = http4xx + http5xx;
  return {
    rps: lessThanValue(http),
    errorRate: toFixed((errorCount / http) * multiple) || 0,
  };
}

function toFixed(value = 0) {
  return +value.toFixed(2);
}

function lessThanValue(value = 0) {
  return value > 0 && value < 0.01 ? '< 0.01' : toFixed(value);
}
