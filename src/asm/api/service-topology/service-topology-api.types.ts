export interface ServiceNode {
  id: string;
  app: string;
  namespace?: string;
  node_type: string;
  service: string;
  workload: string;
  version?: string;
  is_root?: boolean;
  has_istio_sidecar: boolean;
  has_TLS?: boolean;
  has_CB?: boolean;
  combination?: string;
  metadata?: NodeMetadata;
  belongs_to?: Belong;
}

export interface Belong {
  microservice: string;
}

export interface ServiceEdge {
  source_id: string;
  target_id: string;
  request_rate: number;
  error_rate: number;
  metadata: EdgeMetadata;
}

export interface NodeMetadata {
  httpIn?: number;
  httpOut?: number;
  httpIn2xx?: number;
  httpIn3xx?: number;
  httpIn4xx?: number;
  httpIn5xx?: number;
}

export interface EdgeMetadata {
  http: number;
  http2xx?: number;
  http3xx?: number;
  http4xx?: number;
  http5xx?: number;
}

export interface ServiceTopology {
  start_time?: number;
  end_time?: number;
  duration?: number;
  nodes: ServiceNode[];
  edges: ServiceEdge[];
}

export interface RequestParam {
  node_type?: string;
  start_time?: string;
  end_time?: string;
  duration?: string;
  namespace?: string;
  service?: string;
  workload?: string;
  source_namespace?: string;
  source_workload?: string;
  target_namespace?: string;
  target_workload?: string;
  step?: string;
  inject_service_nodes?: string;
  cluster?: string;
}

// [timestamp, value]
export interface NumberPairs {
  [index: number]: [number, number];
}

export interface MetircsResponseBase {
  timestamp: number;
  duration: number;
  type: string;
  namespace: string;
  request_count?: {
    [key: string]: number;
  };
  requests_total_in?: {
    [key: string]: number;
  };
  requests_total_out?: {
    [key: string]: number;
  };
  request_rate_in?: number[][];
  request_rate_out?: number[][];
  request_response_time?: {
    [key: string]: number[][];
  };
  rps_in?: {
    rps: [[number, number]];
    rps_err: [[number, number]];
  };
  rps_out?: {
    rps: [[number, number]];
    rps_err: [[number, number]];
  };
  request_rate?: number[][];
  error_rate?: number[][];
  error_rate_in: number[][];
  error_rate_out: number[][];
  latency_in: number[][];
  latency_out: number[][];
  avg_rps_in?: number;
  avg_rps_out?: number;
  avg_response_time?: number;
}

export interface ServiceMetricsResponse extends MetircsResponseBase {
  service: string;
  workloads: string[];
}

export interface WorkloadMetricsResponse extends MetircsResponseBase {
  workload: string;
  services: string[];
}

export interface ConnectionMetrics {
  timestamp: number;
  duration: number;
  type: string;
  source_namespace: string;
  target_namespace: string;
  source_workload: string;
  target_workload: string;
  request_count: {
    [key: string]: number;
  };
  request_rate: NumberPairs;
  error_rate: NumberPairs;
  latency: NumberPairs;
}

export enum HttpCode {
  HTTP_2XX = 'http_2xx',
  HTTP_3XX = 'http_3xx',
  HTTP_4XX = 'http_4xx',
  HTTP_5XX = 'http_5xx',
}
export interface RequestStat {
  name: HttpCode;
  count: number;
}

export interface ChartDataItem {
  timestamp: number;
  value: number;
}
export interface ChartData {
  inflow?: ChartDataItem[];
  error?: ChartDataItem[];
  outflow?: ChartDataItem[];
  average?: ChartDataItem[];
  tp50?: ChartDataItem[];
  tp95?: ChartDataItem[];
  tp99?: ChartDataItem[];
}

export interface TipInfo {
  type: string;
  width?: number;
  height?: number;
  x: number;
  y: number;
  status?: string;
  data?: string[];
  show: boolean;
}

export interface OperatingState {
  display?: boolean;
  changed?: boolean;
  closed?: boolean;
  injected?: boolean;
}

export enum NodeType {
  IngressGateway = 'node_ingress_gateway',
  Workload = 'node_workload',
  Service = 'node_service',
  MissingSidecar = 'node_missing_sidecar',
  Tls = 'node_tls',
  Unknown = 'node_unknown',
  CircuitBreaker = 'node_circuit_breaker',
}
