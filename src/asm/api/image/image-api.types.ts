export interface ImageRepository {
  name: string;
  namespace: string;
  host: string;
  creationTimestamp: string;
  image: string;
  endpoint: string;
  type: string;
  link: string;
  secretName?: string;
  secretNamespace?: string;
}

export interface ImageTag {
  name: string;
  digest: string;
  createdAt: string;
  size?: string;
  level?: number;
  message?: string;
  author: string;
}
