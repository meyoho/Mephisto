import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { get } from 'lodash-es';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ListResult } from '../project/project.types';
import { ImageRepository, ImageTag } from './image-api.types';

@Injectable({
  providedIn: 'root',
})
export class ImageRepositoryApiService {
  constructor(private readonly http: HttpClient) {}

  getImageRepository(project: string): Observable<ListResult<ImageRepository>> {
    return this.http
      .get(`${API_GATEWAY}/devops/api/v1/imagerepository/${project}`)
      .pipe(map(mapFindRepositoriesResponseToList));
  }

  getImageTags(
    project: string,
    name: string,
  ): Observable<ListResult<ImageTag>> {
    return this.http
      .get(
        `${API_GATEWAY}/devops/api/v1/imagerepository/${project}/${name}/tags`,
      )
      .pipe(
        map((res: any) => ({
          total: (res.tags && res.tags.length) || 0,
          items: (res.tags || []).map(mapDataToRepoTag),
          errors: res.errors,
        })),
      );
  }
}

function mapFindRepositoriesResponseToList(
  res: any,
): ListResult<ImageRepository> {
  return {
    total: get(res, 'listMeta.totalItems', 0),
    items: res.imagerepositories.map(mapResourceToRepository),
    errors: res.errors,
  };
}

function mapResourceToRepository(res: any): ImageRepository {
  const meta = res.objectMeta || res.metadata;

  return {
    name: meta.name,
    namespace: meta.namespace,
    creationTimestamp: meta.creationTimestamp,
    image: get(res, 'spec.image', ''),
    endpoint: get(meta, 'annotations.imageRegistryEndpoint', ''),
    type: get(meta, 'annotations.imageRegistryType', ''),
    link: get(meta, 'annotations.imageRepositoryLink', ''),
    host: get(res, 'objectMeta.annotations.imageRegistryEndpoint', ''),
    secretName: get(meta, 'annotations.secretName', ''),
    secretNamespace: get(meta, 'annotations.secretNamespace', ''),
  };
}

function mapDataToRepoTag(data: any): ImageTag {
  const { created_at, ...rest } = data;
  return {
    ...rest,
    createdAt: created_at,
  };
}
