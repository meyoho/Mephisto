import { Status } from '@alauda/ui';
import { RouteParams } from '@app/typings';
export interface Resources {
  count: number;
  versionCount: number;
  entry: number;
  instance: number;
  healthy: number;
  unhealthy: number;
}

export interface Values {
  name: string;
  value: number;
  msValue?: number[];
}

export interface ResourcesView {
  name: string;
  value: number;
  type: string;
  child?: {
    success: number;
    error: number;
  };
}

export interface MonitorParame extends RouteParams {
  start_time: string;
  end_time: string;
}

export interface MonitorModel {
  type: string;
  name: string;
  data: MonitorStatus[];
}

interface MonitorStatus extends Values {
  status?: Status[];
}

export interface Monitor {
  prs: Values[];
  mortality: Values[];
  ms: Values[];
}

export interface Circuits {
  items: Circuit[];
}

export interface Circuit {
  service: string;
  version: string;
  time: string;
}

export interface Untraffic {
  items: Traffic[];
}
export interface Traffic {
  service: string;
  version: string;
  errorRate: string;
}

export enum MonitorType {
  Rps = 'rps',
  Mortality = 'mortality',
  Ms = 'ms',
}
