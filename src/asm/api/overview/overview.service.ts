import { API_GATEWAY } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ASM_SUBPATH } from '@app/constants';
import { RouteParams } from '@app/typings';

import {
  Circuits,
  Monitor,
  MonitorParame,
  Resources,
  Untraffic,
} from './overview.types';
@Injectable({
  providedIn: 'root',
})
export class OverviewService {
  constructor(private readonly httpClient: HttpClient) {}

  getResources(cluster: string, namespace: string) {
    return this.httpClient.get<Resources>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/overview/resources/${namespace}`,
      {
        params: { cluster },
      },
    );
  }

  getMonitor(params: MonitorParame) {
    const { namespace, cluster, start_time, end_time, step } = params;
    return this.httpClient.get<Monitor>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/overview/monitor/${namespace}`,
      {
        params: {
          cluster,
          start_time,
          end_time,
          step,
        },
      },
    );
  }

  getCircuits(params: RouteParams) {
    const { namespace, cluster, start_time, end_time } = params;
    return this.httpClient.get<Circuits>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/overview/circuits/${namespace}`,
      {
        params: {
          cluster,
          start_time,
          end_time,
        },
      },
    );
  }

  getUntraffic(params: RouteParams) {
    const { namespace, cluster, start_time, end_time } = params;
    return this.httpClient.get<Untraffic>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/overview/untraffic/${namespace}`,
      {
        params: {
          cluster,
          start_time,
          end_time,
        },
      },
    );
  }
}
