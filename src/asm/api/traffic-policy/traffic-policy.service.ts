import { API_GATEWAY, K8sApiService } from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ASM_SUBPATH } from '@app/constants';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import { filterBy, getQuery, pageBy, sortBy } from '@app/utils/query-builder';

import { get } from 'lodash-es';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  DestinationRuleList,
  DestinationRuleResponse,
  ListResponse,
  LoadBalancer,
  LoadBalancerCrd,
  LoadBalancingStrategy,
} from './traffic-policy.types';
const BASE_URL = `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/destinationrule/`;
@Injectable({
  providedIn: 'root',
})
export class TrafficPolicyService {
  constructor(
    private readonly http: HttpClient,
    private readonly k8sApi: K8sApiService,
  ) {}

  getList(params: any): Observable<DestinationRuleList> {
    return this.http
      .get<ListResponse<DestinationRuleResponse[]>>(
        `${BASE_URL}${params.namespace}`,
        {
          params: {
            policyType: '1',
            ...getQuery(
              filterBy('asmHost', params.keywords),
              sortBy(params.sort, params.direction === 'desc'),
              pageBy(params.pageIndex, params.pageSize),
            ),
            cluster: params.cluster,
          },
        },
      )
      .pipe(
        map((list: ListResponse<DestinationRuleResponse[]>) => ({
          total: list.listMeta.totalItems,
          items: list.destinationRules,
          errors: list.errors,
        })),
      );
  }

  getHostDetail(namespace: string, name: string, cluster: string) {
    return this.http.get<DestinationRuleResponse[]>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/destinationruleinfohost/${namespace}/${name}`,
      {
        params: { cluster },
      },
    );
  }

  getService(namespace: string, cluster: string) {
    return this.http
      .get<ListResponse<DestinationRuleResponse[]>>(`${BASE_URL}${namespace}`, {
        params: { cluster },
      })
      .pipe(
        map(
          (list: ListResponse<DestinationRuleResponse[]>) =>
            list.destinationRules,
        ),
      );
  }

  update(namespace: string, payload: DestinationRuleResponse, cluster: string) {
    const name = payload.spec.host;
    return this.http.put(`${BASE_URL}${namespace}/${name}`, payload, {
      params: { cluster },
    });
  }

  getDetail(
    namespace: string,
    name: string,
    cluster: string,
  ): Observable<DestinationRuleResponse> {
    return this.http.get<DestinationRuleResponse>(
      `${BASE_URL}${namespace}/${name}`,
      {
        params: { cluster },
      },
    );
  }

  delete(namespace: string, name: string, cluster: string) {
    return this.http.delete(`${BASE_URL}${namespace}/${name}`, {
      params: { cluster },
    });
  }

  getLoadbalancers(params: RouteParams) {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.LOADBALANCER,
      cluster: params.cluster,
      name: params.name,
      namespace: params.namespace,
    });
  }

  updateLoadbalancer(params: RouteParams, payload: LoadBalancerCrd) {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.LOADBALANCER,
      cluster: params.cluster,
      resource: payload,
    });
  }

  deleteLoadbalancer(params: RouteParams, payload: LoadBalancerCrd) {
    return this.k8sApi.deleteResource({
      type: RESOURCE_TYPES.LOADBALANCER,
      cluster: params.cluster,
      resource: payload,
    });
  }

  createLoadbalancer(params: RouteParams, payload: LoadBalancerCrd) {
    return this.k8sApi.postResource({
      type: RESOURCE_TYPES.LOADBALANCER,
      cluster: params.cluster,
      resource: payload,
    });
  }

  getDetailLoadbalancer(
    params: RouteParams,
    name: string,
  ): Observable<LoadBalancerCrd> {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.LOADBALANCER,
      cluster: params.cluster,
      name: name,
      namespace: params.namespace,
    });
  }
}

export const strategyPure = (item: LoadBalancer) => {
  if (!item) {
    return '-';
  }
  const simple = get(item, 'simple');
  if (simple) {
    const value = LoadBalancingStrategy.find(data => data.value === simple);
    return `${value.name}`;
  }
  return 'session_retention';
};
