import { KubernetesResource } from '@alauda/common-snippet';

export const LoadBalancingStrategy = [
  { name: 'random', value: 'RANDOM' },
  { name: 'round_robin', value: 'ROUND_ROBIN' },
  { name: 'minimum_request_load', value: 'LEAST_CONN' },
  { name: 'session_retention', value: 'CONSISTENTHASH' },
];
export interface ListResponse<T> {
  listMeta: {
    totalItems: number;
  };
  errors: any[];
  destinationRules: T;
}
export interface DestinationRuleList {
  total: number;
  items: DestinationRuleResponse[];
  errors: any[];
}

export interface DestinationRuleResponse extends KubernetesResource {
  spec: DestinationRule;
}

export interface DestinationRule {
  host?: string;
  trafficPolicy?: TrafficPolicy;
}

export interface TrafficPolicy {
  loadBalancer: LoadBalancer;
}

export type LoadBalancerSimple =
  | 'ROUND_ROBIN'
  | 'RANDOM'
  | 'LEAST_CONN'
  | string;

export interface LoadBalancer {
  simple?: LoadBalancerSimple;
  consistentHash?: ConsistentHash;
}

export interface ConsistentHash {
  [key: string]: any;
  key?: string;
  httpCookie?: HttpCookie;
  httpHeaderName?: string;
  useSourceIp?: boolean;
  minimumRingSize?: number;
}

export interface HttpCookie {
  name: string;
  path: string;
  ttl: number | string;
}

export const labelName: { [key: string]: string } = {
  httpHeaderName: 'http header',
  useSourceIp: 'user source ip',
  httpCookie: 'http cookie',
};

export interface LoadBalancerSpec {
  host: string;
  simple?: LoadBalancerSimple;
  consistentHash?: ConsistentHash;
}

export interface LoadBalancerCrd extends KubernetesResource {
  spec: LoadBalancerSpec;
}
