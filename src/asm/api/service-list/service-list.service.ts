import {
  API_GATEWAY,
  K8sApiService,
  K8sUtilService,
  TOKEN_BASE_DOMAIN,
} from '@alauda/common-snippet';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { ASM_SUBPATH } from '@app/constants';
import { Deployment, RouteParams } from '@app/typings';
import { RESOURCE_DEFINITIONS, RESOURCE_TYPES } from '@app/utils';
import { matchLabelsToString } from '@app/utils/resource-list';

import { get } from 'lodash-es';
import { Observable } from 'rxjs';

import {
  ConnectionPool,
  DeploymentItem,
  ErrorCheck,
  MicroService,
  OutlierDetection,
  Service,
  ServiceEntrance,
  Whitelist,
} from './service-list.types';
@Injectable({
  providedIn: 'root',
})
export class ServiceListService {
  constructor(
    private readonly http: HttpClient,
    private readonly k8sApi: K8sApiService,
    private readonly k8sUtil: K8sUtilService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {}

  getList(params: RouteParams) {
    return this.k8sApi.getResourceList({
      type: RESOURCE_TYPES.MICROSERVICE,
      cluster: params.cluster,
      namespace: params.namespace,
      queryParams: {
        ...params,
      },
    });
  }

  create(cluster: string, payload: MicroService) {
    return this.k8sApi.postResource({
      type: RESOURCE_TYPES.MICROSERVICE,
      cluster: cluster,
      resource: payload,
    });
  }

  update(cluster: string, payload: MicroService) {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.MICROSERVICE,
      cluster: cluster,
      resource: payload,
    });
  }

  delete(cluster: string, payload: MicroService) {
    return this.k8sApi.deleteResource({
      type: RESOURCE_TYPES.MICROSERVICE,
      cluster: cluster,
      resource: payload,
    });
  }

  getDeployments(params: RouteParams) {
    return this.k8sApi.getResourceList({
      type: RESOURCE_TYPES.DEPLOYMENT,
      namespace: params.namespace,
      cluster: params.cluster,
    });
  }

  getDetail(cluster: string, namespace: string, name: string) {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.MICROSERVICE,
      cluster: cluster,
      name: name,
      namespace: namespace,
    });
  }

  convertToNode(
    data: { name: string; version?: string },
    microservice: string,
  ) {
    const { name, version } = data;
    const nodeType = version ? 'workload' : 'service';
    return {
      id: `${name}-${nodeType}`,
      app: version ? microservice : name,
      ...(version
        ? {
            version: version,
            combination: 'workload',
          }
        : {}),
      has_istio_sidecar: true,
      node_type: nodeType,
      service: name,
      workload: name,
    };
  }

  convertToEdge(service: string, deploy: string) {
    return {
      source_id: `${service}-service`,
      target_id: `${deploy}-workload`,
      metadata: {
        http: 1,
      },
    };
  }

  getService(cluster: string, namespace: string, name: string) {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.SERVICE,
      cluster: cluster,
      name: name,
      namespace: namespace,
    });
  }

  createService({ cluster, namespace, name }: RouteParams, payload: Service) {
    return this.http.post(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/microservice/${namespace}/${name}/service`,
      payload,
      {
        params: { cluster },
      },
    );
  }

  updateService(
    { cluster, namespace, name }: RouteParams,
    payload: Service,
    servieName: string,
  ) {
    return this.http.put(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/microservice/${namespace}/${name}/service/${servieName}`,
      payload,
      {
        params: { cluster },
      },
    );
  }

  getOutlierDetection(params: RouteParams) {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.OUTLIERDETECTION,
      cluster: params.cluster,
      name: params.name,
      namespace: params.namespace,
    });
  }

  createOutlierDetection(cluster: string, payload: OutlierDetection) {
    return this.k8sApi.postResource({
      type: RESOURCE_TYPES.OUTLIERDETECTION,
      cluster: cluster,
      resource: payload,
    });
  }

  updateOutlierDetection(cluster: string, payload: OutlierDetection) {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.OUTLIERDETECTION,
      cluster: cluster,
      resource: payload,
    });
  }

  deleteOutlierDetection(cluster: string, payload: OutlierDetection) {
    return this.k8sApi.deleteResource({
      type: RESOURCE_TYPES.OUTLIERDETECTION,
      cluster: cluster,
      resource: payload,
    });
  }

  getConnectionPool(params: RouteParams) {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.CONNECTIONPOOL,
      cluster: params.cluster,
      name: params.name,
      namespace: params.namespace,
    });
  }

  createConnectionPool(cluster: string, payload: ConnectionPool) {
    return this.k8sApi.postResource({
      type: RESOURCE_TYPES.CONNECTIONPOOL,
      cluster: cluster,
      resource: payload,
    });
  }

  updateConnectionPool(cluster: string, payload: ConnectionPool) {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.CONNECTIONPOOL,
      cluster: cluster,
      resource: payload,
    });
  }

  deleteConnectionPool(cluster: string, payload: ConnectionPool) {
    return this.k8sApi.deleteResource({
      type: RESOURCE_TYPES.CONNECTIONPOOL,
      cluster: cluster,
      resource: payload,
    });
  }

  getWhitelistDetail({
    cluster,
    namespace,
    service,
    watch = '',
    resourceVersion = '',
  }: RouteParams) {
    const { apiGroup, apiVersion } = RESOURCE_DEFINITIONS.WHITELIST;
    const SUFFIX = watch ? '' : `/${service}`;
    return this.http.get(
      `${API_GATEWAY}/kubernetes/${cluster}/apis/${apiGroup}/${apiVersion}/namespaces/${namespace}/whitelists${SUFFIX}`,
      {
        params: watch
          ? {
              watch,
              resourceVersion,
              fieldSelector: `metadata.name=${service}`,
            }
          : {},
      },
    );
  }

  putWhitelist = (cluster: string, payload: Whitelist) => {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.WHITELIST,
      cluster: cluster,
      resource: payload,
    });
  };

  createWhitelist = (cluster: string, payload: Whitelist) => {
    return this.k8sApi.postResource({
      type: RESOURCE_TYPES.WHITELIST,
      cluster: cluster,
      resource: payload,
    });
  };

  deleteWhitelist = (cluster: string, payload: Whitelist) => {
    return this.k8sApi.deleteResource({
      type: RESOURCE_TYPES.WHITELIST,
      cluster: cluster,
      resource: payload,
    });
  };

  getRelation(
    namespace: string,
    cluster: string,
    service?: string,
  ): Observable<ServiceEntrance[]> {
    const suffix = service ? `/${service}` : '';
    return this.http.get<ServiceEntrance[]>(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/relation/${namespace}${suffix}`,
      {
        params: { cluster },
      },
    );
  }

  createServiceVersion(
    cluster: string,
    namespace: string,
    name: string,
    payload: Deployment,
    version: string,
  ) {
    return this.http.post(
      `${API_GATEWAY}/${ASM_SUBPATH}/api/v1/microservice/${namespace}/${name}/deployment`,
      payload,
      {
        params: { cluster, version },
      },
    );
  }

  // 获取svc下其他关联的名称
  getRelationDeployName(
    relation: ServiceEntrance[],
    deployments: DeploymentItem[],
  ) {
    const deploymentNames = deployments.map(item => item.name);
    const addedDeployment: string[] = [];
    const workloadNames = relation
      .reduce((prev, curr) => {
        const name = this.k8sUtil.getName(curr.deployment);
        if (!deploymentNames.includes(name)) {
          return [...prev, name];
        }
        addedDeployment.push(name);
        return prev;
      }, [])
      .join(',');
    return { workloadNames, addedDeployment };
  }

  errorchecks(params: RouteParams) {
    const label = matchLabelsToString({
      [`servicemesh.${this.baseDomain}/resourcename`]: params.name,
      [`servicemesh.${this.baseDomain}/resourcekind`]: params.resourcekind,
    });
    return this.k8sApi.getResourceList<ErrorCheck>({
      type: RESOURCE_TYPES.ERRORCHECK,
      cluster: params.cluster,
      namespace: params.namespace,
      queryParams: {
        labelSelector: label,
      },
    });
  }
}

export function getDeployments(data: MicroService) {
  return get(data, 'spec.deployments', []) as DeploymentItem[];
}
