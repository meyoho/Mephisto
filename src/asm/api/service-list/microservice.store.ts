import { StringMap } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import {
  RouterHttp,
  RouterManageRouter,
  VirtualService,
} from '@asm/api/route-management';

import { BehaviorSubject, Subject } from 'rxjs';

import {
  DeploymentItem,
  DeploymentRelation,
  RouteActionType,
  TabNumber,
} from './service-list.types';
const tabNumber: TabNumber = {
  serviceEntry: 0,
  strategy: 0,
  routing: 0,
  whitelist: 0,
};
const allowRoute = {
  protocol: true,
  existRoute: true,
};
export const nodeTag = {
  has_TLS: false,
  has_CB: false,
};
@Injectable({
  providedIn: 'root',
})
export class MicroServiceStore {
  disableMicroservice$ = new BehaviorSubject(false);
  currentMicroService$ = new BehaviorSubject('');
  routeRefresh$ = new BehaviorSubject('');
  serviceName$ = new BehaviorSubject('');
  routeTriggerState$ = new Subject<boolean>();
  deployments: DeploymentItem[] = [];
  workloadAbnormal = '';
  weightExistence: RouterManageRouter[] = [];
  allowCreationRoute$ = new BehaviorSubject(allowRoute);
  tabNumberState$ = new BehaviorSubject<TabNumber>(tabNumber);
  routeState$ = new BehaviorSubject<RouteActionType>(RouteActionType.DEFAULT);
  selectedRelation: DeploymentRelation;
  nodeTagState$ = new BehaviorSubject(nodeTag);
  renderGraph$ = new Subject();
  workloadInfo$ = new BehaviorSubject(null);
  virtualService: VirtualService;
  originalConditionRule: RouterHttp[];
  triggerCheckVersion$ = new BehaviorSubject(null);
  gatewayInfo: { name: string; msName: string; port: number } = {
    name: '',
    msName: '',
    port: null,
  };

  get disableMicroservice() {
    return this.disableMicroservice$.getValue();
  }

  get currentMicroService() {
    return this.currentMicroService$.getValue();
  }

  get nodeTagState() {
    return this.nodeTagState$.getValue();
  }

  get tabNumberState() {
    return this.tabNumberState$.getValue();
  }

  get routeState() {
    return this.routeState$.getValue();
  }

  get serviceName() {
    return this.serviceName$.getValue();
  }

  get workloadInfo() {
    return this.workloadInfo$.getValue();
  }

  get routeAllState(): StringMap {
    return Object.values(RouteActionType).reduce(
      (prev, curr) => ({
        ...prev,
        ...{
          [curr]: curr === this.routeState$.getValue(),
        },
      }),
      {},
    );
  }

  get routeCreatePermission() {
    return this.allowCreationRoute$.getValue();
  }

  tabAction(value: number, type: string) {
    const state = this.handleState({ [type]: value });
    this.tabNumberState$.next(state);
  }

  private handleState(value: { [key: string]: number }) {
    return { ...this.tabNumberState, ...value };
  }

  routeAction(type: RouteActionType) {
    this.routeState$.next(type);
  }

  initRouteAction() {
    if (this.routeState !== RouteActionType.DEFAULT) {
      this.routeState$.next(RouteActionType.DEFAULT);
    }
  }

  serviceAction(name: string) {
    this.serviceName$.next(name);
  }

  routeTriggerAction() {
    this.routeTriggerState$.next(true);
  }

  allowCreationRouteAction(value: { [key: string]: boolean }) {
    this.allowCreationRoute$.next({ ...this.routeCreatePermission, ...value });
  }

  setDeployments(deployments: DeploymentItem[]) {
    this.deployments = deployments;
  }

  nodeTagAction(value: { [key: string]: boolean }) {
    this.nodeTagState$.next({ ...this.nodeTagState, ...value });
  }

  initTabNumberState() {
    this.tabNumberState$.next(tabNumber);
  }

  initGatewayState() {
    this.gatewayInfo = {
      name: '',
      msName: '',
      port: null,
    };
  }

  initStore() {
    this.initTabNumberState();
    this.initRouteAction();
    this.workloadInfo$.next(null);
    this.nodeTagAction(nodeTag);
    this.allowCreationRouteAction(allowRoute);
    this.virtualService = null;
    this.originalConditionRule = null;
    this.initGatewayState();
    this.disableMicroservice$.next(false);
    this.currentMicroService$.next('');
  }

  initRouteStore() {
    this.weightExistence = [];
    this.workloadInfo$.next(null);
    this.virtualService = null;
    this.originalConditionRule = null;
  }
}
