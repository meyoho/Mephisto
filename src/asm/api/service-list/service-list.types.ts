import { KubernetesResource, ObjectMeta } from '@alauda/common-snippet';
export const PROTOCOLS = ['TCP', 'HTTP', 'HTTPS', 'HTTP2', 'gRPC'];
export enum StrategyType {
  'SAFETY' = 'safety',
  'LOADBALANCER' = 'loadBalancer',
  'OUTLIERDETECTION' = 'outlierDetection',
  'CONNECTIONPOOL' = 'connectionPool',
}

export enum ConnectionPoolType {
  HTTP = 'http',
  TCP = 'tcp',
}
export interface MicroService extends KubernetesResource {
  spec: {
    deployments: DeploymentItem[];
    services?: ServiceItem[];
  };
}

export interface DeploymentItem {
  name: string;
  services?: string[];
  regservices?: string[];
  version: string;
  disabled?: boolean;
  labelApp?: string;
  labelVersion?: string;
}

export interface GraphDeployments {
  deployments: GraphLabel[];
}

export interface GraphLabel {
  name: string;
  svc: string[];
}

export interface ServiceDeploymentObj {
  deployment: string;
  version: string;
  iscreatebysystem: boolean;
}

export interface ServiceEntrance {
  deployment: DeploymentResponse;
  services: ServiceItem[];
}

export interface ServiceItem {
  name: string;
  iscreatebysystem: boolean;
}

export interface Service extends KubernetesResource {
  spec: ServiceSpec;
}

export interface ServiceSpec {
  ports: ServicePorts[];
}

export interface ServicePorts {
  name?: string;
  protocol: string;
  port: number;
  targetPort: number;
  nodePort?: number;
}

export interface StrategyList {
  type: StrategyType;
  name: string;
  detail?: string;
  originalData: any;
  describe?: ConnectionPoolType;
}

export interface Deployments {
  name: string;
  version: string;
  isDeleted: boolean;
}

export interface OutlierDetection extends KubernetesResource {
  spec: OutlierDetectionSpec;
}

export interface OutlierDetectionSpec {
  host?: string;
  baseEjectionTime: string;
  consecutiveErrors: number;
  interval: string;
  maxEjectionPercent: number;
  intervalUnit?: string;
  baseEjectionTimeUnit?: string;
}

export interface ConnectionPool extends KubernetesResource {
  spec: ConnectionPoolSpec;
}

export interface ConnectionPoolSpec {
  host: string;
  http?: ConnectionPoolHttp;
  tcp?: ConnectionPoolTcp;
}

export interface ConnectionPoolHttp {
  httpMaxPendingRequests: number;
  httpMaxRequests: number;
  maxRequestsPerConnection?: number;
  maxRetries: number;
  maxConnections?: number;
}

export interface ConnectionPoolTcp {
  maxConnections: number;
  connectTimeout?: string;
  connectTimeoutUnit?: string;
}

export interface Whitelist extends KubernetesResource {
  spec: WhitelistSpec;
}

export interface WhitelistSpec {
  destmatch: {
    service: string;
    namespace: string;
  };
  allowlist: Allowlist[];
}

export interface Allowlist {
  app: string;
  version?: string;
}

export const getConnectionPoolTypeName = (type: string) => {
  if (!type) {
    return '';
  }
  return type === 'http' ? '(HTTP/HTTP2)' : '(TCP)';
};

export interface DeploymentRelation {
  deployment: DeploymentItem;
  service: {
    name: string;
    total: number;
  };
}

export interface DeploymentResponse extends KubernetesResource {
  spec: {
    template: {
      metadata: ObjectMeta;
    };
  };
}

export interface ServiceVersion {
  baseWorkload: string;
  version: string;
  images: ServiceVersionImage[];
  initContainerImages?: ServiceVersionImage[];
}

export interface ServiceVersionImage {
  path: string;
  tag: string;
}

export interface TabNumber {
  serviceEntry: number;
  strategy: number;
  routing: number;
  whitelist: number;
}
export enum TabNumberAction {
  SERVICEENTRY = 'serviceEntry',
  STRATEGY = 'strategy',
  ROUTING = 'routing',
  WHITELIST = 'whitelist',
}

export enum RouteActionType {
  DEFAULT = 'Default',
  UPDATE = 'Update',
  CREATE = 'Create',
  NOENTRY = 'NoEntry',
}

export enum ErrorCode {
  'ErrorSvcRelate' = 'ErrorSvcRelateDeploymentButSvcNotInMs',
  'ErrorSvcDeleted' = 'ErrorSvcInMsButDeleted',
  'ErrorDeploymentDeleted' = 'ErrorDeploymentInMsButDeleted',
  'ErrorMicroserviceSvcPortNotValid' = 'ErrorMicroserviceSvcPortNameNotValid',
}

export interface ErrorCheck extends KubernetesResource {
  spec: {
    errlist: ErrorList[];
  };
}

export interface ErrorList {
  errorcode: ErrorCode;
  errordesc: string;
  errorlevel: string;
  relatename: string;
  resourename: string;
}
