import { KubernetesResource } from '@alauda/common-snippet';
export const DEFAULT_MODE = 'PERMISSIVE';
export const STRICT_MODE = 'STRICT';

export interface ListResponse<T> {
  listMeta: {
    totalItems: number;
  };
  errors: any[];
  items: T;
}
export interface PolicyListResponse {
  total: number;
  items: PolicyResponse[];
  errors: any[];
}

export interface PolicyResponse extends KubernetesResource {
  spec: PolicySpec;
}

export interface PolicySpec {
  peers: PolicyMtls[];
  targets: PolicyTargets[];
}

export interface PolicyMtls {
  mtls: {
    mode: string;
  };
}

export interface PolicyTargets {
  name: string;
}

export const PolicyTypeMeta = {
  apiVersion: 'authentication.istio.io/v1alpha1',
  kind: 'Policy',
};

export interface ServiceResponse {
  errors: any[];
  listMeta: { totalItems: number };
  services: string[];
}
