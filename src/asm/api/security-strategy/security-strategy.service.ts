import { K8sApiService } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import { Observable } from 'rxjs';

import { PolicyResponse } from './security-strategy.types';
@Injectable({
  providedIn: 'root',
})
export class SecurityStrategyService {
  constructor(private k8sApi: K8sApiService) {}
  getPolicyList(params: RouteParams) {
    return this.k8sApi.getResourceList({
      type: RESOURCE_TYPES.POLOCY,
      cluster: params.cluster,
      namespace: params.namespace,
      queryParams: {
        ...params,
      },
    });
  }

  create(cluster: string, payload: PolicyResponse) {
    return this.k8sApi.postResource({
      type: RESOURCE_TYPES.POLOCY,
      cluster: cluster,
      resource: payload,
    });
  }

  getDetail(
    namespace: string,
    name: string,
    cluster: string,
  ): Observable<PolicyResponse> {
    return this.k8sApi.getResource({
      type: RESOURCE_TYPES.POLOCY,
      cluster: cluster,
      name: name,
      namespace: namespace,
    });
  }

  update(cluster: string, payload: PolicyResponse) {
    return this.k8sApi.putResource({
      type: RESOURCE_TYPES.POLOCY,
      cluster: cluster,
      resource: payload,
    });
  }

  delete(cluster: string, payload: PolicyResponse) {
    return this.k8sApi.deleteResource({
      type: RESOURCE_TYPES.POLOCY,
      cluster: cluster,
      resource: payload,
    });
  }
}
