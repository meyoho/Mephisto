import { ObservableInput } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ServiceMeshApiService } from '@asm/api/service-mesh/service-mesh-api.service';

import { Observable, of } from 'rxjs';
import {
  catchError,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

@Component({
  selector: 'alo-grafana',
  templateUrl: './grafana.component.html',
  styleUrls: ['./grafana.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GrafanaComponent {
  @ObservableInput(true)
  private readonly cluster$: Observable<string>;

  @Input() cluster: string;

  iframeInitialized = false;
  grafanaUrlGetError: any;
  grafanaUrl$ = this.cluster$.pipe(
    switchMap(cluster =>
      this.api.getGrafanaURLUrl(cluster).pipe(
        catchError(error => {
          this.grafanaUrlGetError = error;
          this.cdr.markForCheck();
          return of('');
        }),
        filter(url => !!url),
        map(url => this.sanitizer.bypassSecurityTrustResourceUrl(url)),
        tap(() => (this.grafanaUrlGetError = null)),
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly sanitizer: DomSanitizer,
    private readonly api: ServiceMeshApiService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  iframeLoaded() {
    this.iframeInitialized = true;
    this.cdr.markForCheck();
  }
}
