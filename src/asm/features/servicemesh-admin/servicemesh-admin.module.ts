import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';

import { ServicemeshAdminRoutingModule } from './servicemesh-admin-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    ServicemeshAdminRoutingModule,
  ],
  exports: [],
  declarations: [],
  providers: [],
})
export class ServicemeshAdminModule {}
