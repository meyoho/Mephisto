import { RouterModule, Routes } from '@angular/router';

import { ServiceMeshCreateComponent } from './create/servicemesh-create.component';
import { ServiceMeshDetailComponent } from './detail/servicemesh-detail.component';
import { ServiceMeshListComponent } from './list/servicemesh-list.component';
import { ServiceMeshUpdateComponent } from './update/servicemesh-update.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: ServiceMeshListComponent,
  },
  {
    path: 'create',
    component: ServiceMeshCreateComponent,
  },
  {
    path: ':name',
    component: ServiceMeshDetailComponent,
  },
  {
    path: 'update/:name',
    component: ServiceMeshUpdateComponent,
  },
];

export const ServiceMeshRoutes = RouterModule.forChild(routes);
