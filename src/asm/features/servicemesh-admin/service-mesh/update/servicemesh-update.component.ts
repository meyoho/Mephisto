import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceMeshApiService } from '@asm/api/service-mesh/service-mesh-api.service';

@Component({
  templateUrl: './servicemesh-update.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMeshUpdateComponent {
  name = this.route.snapshot.paramMap.get('name') || '';

  constructor(
    private route: ActivatedRoute,
    private api: ServiceMeshApiService,
  ) {}

  fetch = () => {
    return this.api.getServiceMesh({ name: this.name });
  };
}
