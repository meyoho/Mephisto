import { AsyncDataLoader } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouteParams } from '@app/typings';
import { ServiceMeshApiService } from '@asm/api/service-mesh/service-mesh-api.service';
import {
  ServiceMesh,
  ServiceMeshCrd,
  ServiceMeshStatus,
} from '@asm/api/service-mesh/service-mesh-api.types';

import { get } from 'lodash-es';
import { map, publishReplay, refCount, tap } from 'rxjs/operators';

@Component({
  templateUrl: './servicemesh-detail.component.html',
  styleUrls: ['./servicemesh-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMeshDetailComponent {
  constructor(
    private readonly route: ActivatedRoute,
    private readonly api: ServiceMeshApiService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  disable = true;
  status: ServiceMeshStatus;
  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get('name') || '',
    })),
    publishReplay(1),
    refCount(),
  );

  fetch = (params: RouteParams) => {
    return this.api.getServiceMesh(params).pipe(
      tap((result: ServiceMeshCrd) => {
        this.status = result.status;
        this.cdr.markForCheck();
      }),
    );
  };

  dataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: this.fetch,
  });

  getCluster = (result: ServiceMesh) => {
    return get(result, 'spec.cluster');
  };

  triggerSuccessDeploy() {
    this.disable = false;
    this.cdr.markForCheck();
  }
}
