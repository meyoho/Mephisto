import {
  K8sPermissionService,
  K8sResourceAction,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GenericStatus } from '@app/shared/components/status-icon/status-icon.component';
import { RESOURCE_TYPES } from '@app/utils';
import {
  getServiceMeshStatus,
  getStatusValue,
  isPendingStatus,
  ServiceMeshApiService,
} from '@asm/api/service-mesh/service-mesh-api.service';
import {
  ServiceMeshCrd,
  ServiceMeshStatus,
  StatusType,
} from '@asm/api/service-mesh/service-mesh-api.types';

import { get, toLower } from 'lodash-es';
import { interval, merge, of, Subject } from 'rxjs';
import {
  catchError,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
  takeWhile,
} from 'rxjs/operators';

import { DeleteServiceMeshDialogComponent } from '../../delete/delete-dialog.component';
@Component({
  selector: 'alo-service-mesh-info',
  templateUrl: './service-mesh-info.component.html',
  styleUrls: ['./service-mesh-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMeshInfoComponent implements OnInit, OnDestroy {
  @Input() status: ServiceMeshStatus;
  @Input() name: string;
  @Input() resource: ServiceMeshCrd;
  @Output() triggerSuccessDeploy = new EventEmitter();
  statusType = [
    GenericStatus.Pending,
    GenericStatus.Unknown,
    GenericStatus.Deploying,
    GenericStatus.Deleting,
  ];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly api: ServiceMeshApiService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {}

  getServiceMeshStatus = getServiceMeshStatus;
  onDestroy$ = new Subject<void>();

  updated$ = this.k8sPermission
    .isAllowed({
      type: RESOURCE_TYPES.SERVICEMESH,
      action: K8sResourceAction.UPDATE,
    })
    .pipe(publishReplay(1), refCount());

  deleted$ = this.k8sPermission
    .isAllowed({
      type: RESOURCE_TYPES.SERVICEMESH,
      action: K8sResourceAction.DELETE,
    })
    .pipe(publishReplay(1), refCount());

  intervalStatus$ = merge(of(null), interval(2000)).pipe(
    switchMap(() => this.api.getDeployStatus(this.name)),
    takeWhile(status => {
      this.status = status;
      this.cdr.markForCheck();
      const state: GenericStatus = getServiceMeshStatus(status);
      if (state === GenericStatus.Succeed) {
        this.triggerSuccessDeploy.emit(true);
      }
      if (this.statusType.includes(state)) {
        return true;
      }
      return false;
    }),
    takeUntil(this.onDestroy$),
    publishReplay(1),
    refCount(),
  );

  ngOnInit(): void {
    this.intervalStatus$.subscribe();
  }

  update() {
    this.router.navigate(['../', 'update', this.name], {
      relativeTo: this.route,
    });
  }

  delete() {
    const dialogRef = this.dialog.open(DeleteServiceMeshDialogComponent, {
      data: {
        name: this.name,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.router.navigate(['../'], {
          relativeTo: this.route,
        });
      }
    });
  }

  existCluster$ = (serviceMesh: ServiceMeshCrd) => {
    return this.api
      .getClusterBaseDeployment(get(serviceMesh, 'serviceMesh.spec.cluster'))
      .pipe(
        catchError(() => of(false)),
        publishReplay(1),
        refCount(),
      );
  };

  clusterConfig = (data: ServiceMeshCrd) => {
    return get(data, 'serviceMesh.spec', {});
  };

  operatingState = (status: ServiceMeshStatus = {}) => {
    const globalStatus = get(status.global, 'status');
    let inDeployment, inDeleing;
    if (globalStatus) {
      inDeployment = globalStatus === StatusType.PENDING;
      inDeleing = globalStatus === StatusType.DELETING;
    } else {
      // TODO: 兼容老数据，老数据无 global status
      const values = Object.values(status);
      inDeployment =
        values.some(value =>
          [GenericStatus.Pending, GenericStatus.Deploying].includes(
            toLower(value.status) as GenericStatus,
          ),
        ) || isPendingStatus(getStatusValue(status));
    }
    if (inDeleing) {
      return {
        status: inDeleing,
        type: 'inDeleing',
        updateTip: 'update_servicemesh_in_deleting_tip',
        deleteTip: 'deleting',
      };
    }
    if (inDeployment) {
      return {
        status: inDeployment,
        type: 'inDeployment',
        updateTip: 'update_servicemesh_in_deploy_tip',
        deleteTip: 'delete_servicemesh_in_deploy_tip',
      };
    }
    return { status: false };
  };

  termination() {
    this.dialog
      .confirm({
        title: this.translate.get(
          this.translate.get('servicemesh_cancel_dialog_title', {
            name: this.name,
          }),
        ),
        content: this.translate.get(
          this.translate.get('servicemesh_cancel_dialog_content'),
        ),
        confirmText: this.translate.get(this.translate.get('termination')),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.api.cancelDeploymentServiceMesh(this.name).subscribe(() => {
          this.message.success(
            this.translate.get('servicemesh_cancel_message_succ', {
              name: this.name,
            }),
          );
        });
      })
      .catch(() => {});
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
