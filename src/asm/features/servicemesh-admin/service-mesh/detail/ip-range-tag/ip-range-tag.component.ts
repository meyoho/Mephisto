import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
@Component({
  selector: 'alo-ip-range-tag',
  templateUrl: './ip-range-tag.component.html',
  styleUrls: ['./ip-range-tag.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IpRangeTagComponent {
  @Input() ipRanges: string[] = [];
  constructor() {}
}
