import {
  ApiGatewayService,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, MessageService, NotificationService } from '@alauda/ui';
import { Location, PlatformLocation } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Directive,
  Inject,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  NgForm,
  NG_ASYNC_VALIDATORS,
  ValidationErrors,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ASM_PREFIX } from '@app/constants';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RESOURCE_DEFINITIONS } from '@app/utils';
import {
  ELASTICSEARCH_USER_PATTERN,
  IMAGE_ADDRESS,
  IP_ADDRESS_PATTERN,
  IP_RANGE_PATTERN,
  K8S_RESOURCE_NAME_BASE,
  PERCENTAGE_RANGE,
  URL_ADDRESS_PATTERN,
} from '@app/utils/patterns';
import { DefaultParams, ServiceMesh, ServiceMeshCrd } from '@asm/api';
import { ServiceMeshApiService } from '@asm/api/service-mesh/service-mesh-api.service';

import { cloneDeep, get, omit, set, toNumber } from 'lodash-es';
import { Observable, of } from 'rxjs';
import {
  catchError,
  map,
  publishReplay,
  refCount,
  startWith,
} from 'rxjs/operators';

@Component({
  selector: 'alo-servicemesh-form',
  templateUrl: './servicemesh-form.component.html',
  styleUrls: ['./servicemesh-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMeshFormComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  submitting = false;
  showPassword = false;
  @Input() data: ServiceMeshCrd;
  urlAddressReg = URL_ADDRESS_PATTERN;
  namePattern = K8S_RESOURCE_NAME_BASE;
  imagePathReg = IMAGE_ADDRESS;
  userPasswordReg = ELASTICSEARCH_USER_PATTERN;
  traceSamplingReg = PERCENTAGE_RANGE;
  default: DefaultParams;
  ingressHost = '';
  serviceMeshModel: ServiceMesh = {
    apiVersion: `${RESOURCE_DEFINITIONS.SERVICEMESH.apiGroup}/${RESOURCE_DEFINITIONS.SERVICEMESH.apiVersion}`,
    kind: 'ServiceMesh',
    metadata: {
      annotations: {
        displayName: '',
      },
      name: '',
    },
    spec: {
      cluster: '',
      registryAddress: '',
      ingressHost: '',
      globalIngressHost: '',
      externalHost: '',
      clusterNodeIps: null,
      traceSampling: 1,
      highAvailability: false,
      ipranges: {
        ranges: null,
      },
      elasticsearch: {
        password: '',
        url: '',
        username: '',
        isDefault: true,
      },
      prometheusURL: '',
      ingressScheme: this.platformLocation.protocol.replace(':', ''),
    },
  };

  existCluster$ = this.api.getServiceMeshList().pipe(
    map((reulst: { items: ServiceMeshCrd[] }) =>
      reulst.items.map(item => get(item, 'serviceMesh.spec.cluster')),
    ),
    startWith([]),
    publishReplay(1),
    refCount(),
  );

  clusters$ = this.api.getClusters();
  constructor(
    private readonly api: ServiceMeshApiService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly dialog: DialogService,
    private readonly platformLocation: PlatformLocation,
    private readonly k8sUtil: K8sUtilService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly location: Location,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
    private readonly notification: NotificationService,
    private readonly apiGateway: ApiGatewayService,
  ) {}

  ngOnInit() {
    if (this.data) {
      const { serviceMesh } = this.data;
      const displayName = this.k8sUtil.getDisplayName(serviceMesh, ASM_PREFIX);
      set(serviceMesh.metadata.annotations, 'displayName', displayName);
      this.serviceMeshModel = cloneDeep(serviceMesh);
      this.getDefaultParams(serviceMesh.spec.cluster);
      this.cdr.markForCheck();
    }
    this.apiGateway
      .getApiAddress()
      .subscribe(apiAddress => (this.ingressHost = apiAddress));
  }

  getDefaultParams(cluster: string) {
    this.api.getDefaultParams(cluster).subscribe(
      result => {
        this.default = result;
        if (!this.data) {
          const { registryAddress, ipranges } = this.serviceMeshModel.spec;
          this.serviceMeshModel.spec.registryAddress =
            registryAddress || result.registryAddress;
          this.serviceMeshModel.spec.ipranges.ranges = get(
            ipranges,
            'ranges[0]',
          )
            ? ipranges.ranges
            : result.ipranges.ranges;
          this.serviceMeshModel.spec.elasticsearch.url =
            result.elasticsearch.url;
          if (!result.prometheus.url) {
            this.form.controls.cluster.setErrors({
              notDeployedPrometheus: true,
            });
          }
        }
        if (!get(this.data, 'serviceMesh.spec.elasticsearch.username')) {
          this.serviceMeshModel.spec.elasticsearch.url =
            result.elasticsearch.url;
        }
        this.serviceMeshModel.spec.prometheusURL = get(
          result,
          'prometheus.url',
          '',
        );
        this.cdr.markForCheck();
      },
      error => {
        this.notification.error(error.message);
      },
    );
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    const payload = this.combineModel();
    if (this.data) {
      this.dialog
        .confirm({
          title: this.translate.get('update_servicemesh_confirm_title', {
            name: this.k8sUtil.getName(payload),
          }),
          content: this.translate.get('update_servicemesh_confirm_body'),
          confirmText: this.translate.get('update'),
          cancelText: this.translate.get('cancel'),
        })
        .then(() => {
          this.requestServicemesh(payload);
        })
        .catch(() => {
          this.submitting = false;
          this.cdr.markForCheck();
        });
    } else {
      this.requestServicemesh(payload);
    }
  }

  requestServicemesh(payload: ServiceMesh) {
    this.api.postServiceMesh(payload).subscribe(
      (result: ServiceMeshCrd) => {
        this.resultProcess(result, this.data ? 'update' : 'create');
      },
      () => {
        this.submitting = false;
        this.cdr.markForCheck();
      },
    );
  }

  resultProcess(result: ServiceMeshCrd, type: string) {
    this.submitting = false;
    const name = this.k8sUtil.getName(result);
    this.message.success({
      content: this.translate.get(`servicemesh_${type}_message_succ`, {
        name,
      }),
    });
    const url = type === 'create' ? '../' : '../../';
    this.router.navigate([url, name], {
      relativeTo: this.route,
    });
    this.cdr.markForCheck();
  }

  cancel() {
    this.location.back();
  }

  combineModel(): ServiceMesh {
    const {
      metadata: {
        annotations: { displayName, ...annotRes },
        ...metaRes
      },
      spec: {
        globalIngressHost,
        traceSampling,
        elasticsearch: { url, password, username, isDefault },
        ...specRes
      },
      ...res
    } = this.serviceMeshModel;
    const esType = isDefault === 'true' || isDefault === true;
    return {
      ...res,
      metadata: {
        ...metaRes,
        annotations: {
          ...omit(annotRes, 'displayName'),
          [`${this.asmBaseDomain}/display-name`]: displayName,
        },
      },
      spec: {
        ...specRes,
        elasticsearch: {
          url,
          isDefault: esType,
          ...(esType ? {} : { password, username }),
        },
        traceSampling: toNumber(traceSampling),
        globalIngressHost: globalIngressHost || `${this.ingressHost}/`,
      },
    };
  }

  setRegistryAddress(address: string) {
    this.serviceMeshModel.spec.registryAddress = address;
  }

  setIpRange(range: string) {
    const ranges = this.serviceMeshModel.spec.ipranges.ranges;
    this.serviceMeshModel.spec.ipranges.ranges = [
      ...new Set([...ranges, range]),
    ];
  }

  changeEsType(type: boolean) {
    this.serviceMeshModel.spec.elasticsearch.url = type
      ? get(this.default, 'elasticsearch.url', '')
      : get(this.data, 'serviceMesh.spec.elasticsearch.url', '');
    this.cdr.markForCheck();
  }
}

@Directive({
  selector: '[aloIpAddressValidate]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: IpAddressValidatorDirective,
      multi: true,
    },
  ],
})
export class IpAddressValidatorDirective {
  validate(ctrl: AbstractControl): ValidationErrors | null {
    if (!ctrl.value) {
      return null;
    }
    if (
      !ctrl.value.every((item: string) => IP_ADDRESS_PATTERN.pattern.test(item))
    ) {
      return of({ ipAddress: true });
    }
    return of(null);
  }
}

@Directive({
  selector: '[aloIpRangeValidate]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: IpRangeValidatorDirective,
      multi: true,
    },
  ],
})
export class IpRangeValidatorDirective {
  validate(ctrl: AbstractControl): ValidationErrors | null {
    if (!ctrl.value) {
      return null;
    }
    if (
      !ctrl.value.every((item: string) => IP_RANGE_PATTERN.pattern.test(item))
    ) {
      return of({ ipRange: true });
    }
    if (ctrl.value.includes('*') && ctrl.value.length > 1) {
      return of({ ipRangeCoexist: true });
    }
    return of(null);
  }
}

@Directive({
  selector: '[aloClusterBaseDeploymentValidate]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: ClusterBaseDeploymentValidatorDirective,
      multi: true,
    },
  ],
})
export class ClusterBaseDeploymentValidatorDirective implements AsyncValidator {
  constructor(private readonly api: ServiceMeshApiService) {}
  validate(ctrl: AbstractControl): Observable<ValidationErrors | null> {
    if (!ctrl.value) {
      return null;
    }

    return this.api.getClusterBaseDeployment(ctrl.value).pipe(
      catchError(() => of({ clusterBaseDeploy: true })),
      map(res => (!get(res, 'clusterBaseDeploy') ? null : res)),
    );
  }
}
