import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';

import { GrafanaComponent } from './../grafana/grafana.component';
import { HealthCheckComponent } from './../health-check/health-check.component';
import { ServiceMeshCreateComponent } from './create/servicemesh-create.component';
import { DeleteServiceMeshDialogComponent } from './delete/delete-dialog.component';
import { IpRangeTagComponent } from './detail/ip-range-tag/ip-range-tag.component';
import { ServiceMeshInfoComponent } from './detail/service-mesh-info/service-mesh-info.component';
import { ServiceMeshDetailComponent } from './detail/servicemesh-detail.component';
import {
  ClusterBaseDeploymentValidatorDirective,
  IpAddressValidatorDirective,
  IpRangeValidatorDirective,
  ServiceMeshFormComponent,
} from './form/servicemesh-form.component';
import { ServiceMeshListComponent } from './list/servicemesh-list.component';
import { ServiceMeshListTableComponent } from './list/table/table.component';
import { ServiceMeshRoutes } from './service-mesh.routing.module';
import { ServiceMeshStatusComponent } from './status/servicemesh-status.component';
import { ServiceMeshUpdateComponent } from './update/servicemesh-update.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceMeshRoutes,
  ],
  declarations: [
    ServiceMeshListComponent,
    ServiceMeshListTableComponent,
    ServiceMeshCreateComponent,
    ServiceMeshDetailComponent,
    ServiceMeshUpdateComponent,
    ServiceMeshFormComponent,
    ServiceMeshStatusComponent,
    DeleteServiceMeshDialogComponent,
    IpRangeTagComponent,
    IpRangeValidatorDirective,
    IpAddressValidatorDirective,
    ClusterBaseDeploymentValidatorDirective,
    ServiceMeshInfoComponent,
    HealthCheckComponent,
    GrafanaComponent,
  ],
  entryComponents: [DeleteServiceMeshDialogComponent],
})
export class ServiceMeshModule {}
