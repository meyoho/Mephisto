import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { ServiceMeshApiService } from '@asm/api/service-mesh/service-mesh-api.service';

@Component({
  templateUrl: './delete-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteServiceMeshDialogComponent {
  context = this;

  constructor(
    public dialogRef: DialogRef<DeleteServiceMeshDialogComponent>,
    private api: ServiceMeshApiService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      name: string;
    },
  ) {}

  deleteServiceMesh() {
    return this.api.deleteServiceMesh(this.dialogData.name);
  }
}
