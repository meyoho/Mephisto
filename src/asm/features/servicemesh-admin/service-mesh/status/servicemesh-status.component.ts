import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { GenericStatus } from '@app/shared/components/status-icon/status-icon.component';
import { getServiceMeshStatus } from '@asm/api/service-mesh/service-mesh-api.service';
import {
  ServiceMeshStatus,
  StatusType,
} from '@asm/api/service-mesh/service-mesh-api.types';

import { get, toLower } from 'lodash-es';
@Component({
  selector: 'alo-servicemesh-status',
  templateUrl: './servicemesh-status.component.html',
  styleUrls: ['./servicemesh-status.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMeshStatusComponent {
  @Input() name: string;
  @Input() status: ServiceMeshStatus;
  @ViewChild('logDialog', { static: true })
  logDialog: TemplateRef<any>;

  constructor(private readonly dialog: DialogService) {}

  getServiceMeshStatus = getServiceMeshStatus;
  get chartList() {
    return Object.keys(this.status || {});
  }

  getStatus = (status: StatusType) => {
    switch (status) {
      case toLower(StatusType.UNKNOWN):
        return GenericStatus.Stopped;
      case toLower(StatusType.SYNCED):
        return GenericStatus.Succeed;
      case toLower(StatusType.PENDING):
        return GenericStatus.Deploying;
      case toLower(StatusType.DELETING):
        return GenericStatus.Deleting;
      case toLower(StatusType.FAILED):
        return GenericStatus.Failed;
      default:
        return GenericStatus.Succeed;
    }
  };

  getStatusText = (status: ServiceMeshStatus) => {
    const state = get(status.global, 'status');
    if (state) {
      return `servicemesh_${toLower(state)}`;
    }
    const value = getServiceMeshStatus(status);
    return this.getStatusTranslate(value);
  };

  getStatusTranslate(status: string) {
    const value = toLower(status);
    const name =
      value === GenericStatus.Deploying
        ? GenericStatus.Pending
        : value === GenericStatus.Succeed
        ? 'synced'
        : value === GenericStatus.Stopped
        ? 'unknown'
        : value;
    return `servicemesh_${name || 'synced'}`;
  }

  openLogDialog() {
    this.dialog.open(this.logDialog, {
      size: DialogSize.Medium,
    });
  }
}
