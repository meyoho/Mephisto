import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
@Component({
  selector: 'alo-service-mesh-list-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMeshListTableComponent {
  @Input()
  data: [];
  columns = ['name', 'cluster', 'status', 'create_by', 'created_at'];
  constructor() {}
}
