import {
  K8sPermissionService,
  K8sResourceAction,
  ResourceListParams,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RESOURCE_TYPES } from '@app/utils';
import { ServiceMeshApiService } from '@asm/api/service-mesh/service-mesh-api.service';

import { isEqual } from 'lodash-es';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

import { ResourceList } from 'app/utils/resource-list';

@Component({
  templateUrl: './servicemesh-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMeshListComponent {
  loading = false;
  list: ResourceList;
  params$ = this.route.queryParamMap.pipe(
    map(queryParams => {
      return {
        keywords: queryParams.get('keywords'),
      };
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishReplay(1),
    refCount(),
  );

  creatable$ = this.k8sPermission
    .isAllowed({
      type: RESOURCE_TYPES.SERVICEMESH,
      action: K8sResourceAction.CREATE,
    })
    .pipe(publishReplay(1), refCount());

  fetchParams$ = this.params$.pipe(
    map(params => ({
      // todo: fuzzy query
      // keyword: params.keywords || '',
      fieldSelector: params.keywords ? `metadata.name=${params.keywords}` : '',
    })),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly api: ServiceMeshApiService,
    private readonly k8sPermission: K8sPermissionService,
  ) {
    this.list = new ResourceList({
      fetchParams$: this.fetchParams$,
      fetcher: this.fetch.bind(this),
    });
  }

  fetch = (params: ResourceListParams) => {
    params.limit = '20';
    return this.api.getServiceMeshList(params);
  };

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords },
      queryParamsHandling: 'merge',
    });
  }
}
