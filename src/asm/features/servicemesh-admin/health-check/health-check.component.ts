import {
  ApiGatewayService,
  AuthorizationStateService,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService, NotificationService } from '@alauda/ui';
import { HttpParams } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ASM_SUBPATH } from '@app/constants';
import {
  HealthCheckJob,
  HealthCheckResponseBody,
} from '@asm/api/service-mesh/service-mesh-api.types';

import { get } from 'lodash-es';
import { splitEvery } from 'ramda';
import { combineLatest } from 'rxjs';

enum CheckType {
  Component = 'componenthealthcheck',
  SidecarInjection = 'sidecarinjection',
  Monitor = 'monitoringservice',
}
const ColumnDisplayNum = 6;
const KeepAliveTime = 30000;

@Component({
  selector: 'alo-health-check',
  templateUrl: './health-check.component.html',
  styleUrls: ['./health-check.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HealthCheckComponent implements OnInit, OnDestroy {
  @Input() cluster: string;
  serviceMeshComponents: HealthCheckJob[] = [];
  sidecarInjectorCases: HealthCheckJob[] = [];
  monitorCases: HealthCheckJob[] = [];

  componentsChecking = false;
  sidecarInjectorChecking = false;
  monitorChecking = false;
  currentComponent: HealthCheckJob;

  private conn: WebSocket;
  private connected = false;
  keepAliveHandle: NodeJS.Timeout;
  componentHealthCheckTime = '';
  sidecarInjectionTime = '';
  monitoringServiceTime = '';

  constructor(
    private readonly apiGateway: ApiGatewayService,
    private readonly auth: AuthorizationStateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly notification: NotificationService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    combineLatest([
      this.apiGateway.getApiAddress(),
      this.auth.getToken(),
    ]).subscribe(args => this.setupConnection(...args));
  }

  ngOnDestroy() {
    this.closeConnection();
  }

  handleChartParams(components: HealthCheckJob[]) {
    if ((components || []).length === 0) {
      return [{ status: 'unknown', value: 1 }];
    }
    const runningNum = components.reduce((prev, curr) => {
      return prev + (curr.running ? 1 : 0);
    }, 0);
    return [
      { status: 'success', value: runningNum },
      { status: 'error', value: components.length - runningNum },
    ];
  }

  getCoreComponents(components: HealthCheckJob[]) {
    return components.filter(component => component.type === 'core');
  }

  getExternalComponents(components: HealthCheckJob[]) {
    return components.filter(
      component => component.type === 'external' || component.type === 'case',
    );
  }

  onErrorShow(component: HealthCheckJob) {
    this.currentComponent = component;
  }

  formatDisplayComponents(components: HealthCheckJob[]) {
    return splitEvery(ColumnDisplayNum, components);
  }

  setupConnection(apiGatewayAddress: string, accessToken: string) {
    this.recovery();
    if (this.conn) {
      this.conn.close();
    }
    this.conn = new WebSocket(
      apiGatewayAddress.replace(/^http/, 'ws') +
        `/${ASM_SUBPATH}/api/v1/healthcheck?${new HttpParams({
          fromObject: {
            token: accessToken,
            cluster: this.cluster,
          },
        })}`,
    );
    this.conn.onopen = this.onConnectionOpen;
    this.conn.onmessage = this.onConnectionMessage;
    this.conn.onclose = this.onConnectionClose;
  }

  onConnectionOpen = () => {
    this.connected = true;
    this.keepAliveHandle = this.keepAlive();
  };

  onConnectionMessage = (evt: MessageEvent) => {
    this.handleHealthCheckData(JSON.parse(evt.data));
  };

  onConnectionClose = () => {
    if (!this.connected) {
      return;
    }
    this.closeConnection();
    this.connected = false;
  };

  refreshCheck(type: CheckType) {
    const refreshBody = {
      event: 'refresh',
      job: type,
    };
    switch (type) {
      case CheckType.Component:
        this.componentsChecking = true;
        break;
      case CheckType.SidecarInjection:
        this.sidecarInjectorChecking = true;
        break;
      case CheckType.Monitor:
        this.monitorChecking = true;
        break;
    }
    if (this.connected) {
      this.conn.send(JSON.stringify(refreshBody));
    }
  }

  private handleHealthCheckData(data: HealthCheckResponseBody) {
    if (data.messageType === 'event') {
      this.componentsChecking = false;
      this.sidecarInjectorChecking = false;
      this.monitorChecking = false;
      this.notification.error({
        title: data.status.phase,
        content: data.status.message,
      });
    } else {
      if (
        this.componentsChecking ||
        this.sidecarInjectorChecking ||
        this.monitorChecking
      ) {
        this.message.success(this.translate.get('refresh_success'));
      }
      switch (data.name) {
        case CheckType.Component:
          this.serviceMeshComponents = data.monitorJobs;
          this.componentHealthCheckTime = data.latestUpdatedTime;
          this.componentsChecking = false;
          break;
        case CheckType.SidecarInjection:
          this.sidecarInjectorCases = data.monitorJobs;
          this.sidecarInjectionTime = data.latestUpdatedTime;
          this.sidecarInjectorChecking = false;
          break;
        case CheckType.Monitor:
          this.monitorCases = data.monitorJobs;
          this.monitoringServiceTime = data.latestUpdatedTime;
          this.monitorChecking = false;
          break;
      }
    }
    this.cdr.markForCheck();
  }

  private closeConnection() {
    if (!this.connected) {
      this.conn.close();
    }
    if (this.keepAliveHandle) {
      clearInterval(this.keepAliveHandle);
    }
  }

  private keepAlive() {
    return setInterval(() => {
      this.conn.send('ping');
    }, KeepAliveTime);
  }

  recovery() {
    this.serviceMeshComponents = [];
    this.sidecarInjectorCases = [];
    this.monitorCases = [];
    this.componentsChecking = false;
    this.sidecarInjectorChecking = false;
    this.monitorChecking = false;
    this.componentHealthCheckTime = '';
    this.sidecarInjectionTime = '';
    this.monitoringServiceTime = '';
    this.cdr.markForCheck();
  }

  handleErrorMessage(errorMessage: string) {
    const errorMessageMap = {
      NoReplicaAvailable: 'no_replica_available',
      PrometheusURLErr: 'prometheus_url_err',
      PrometheusRunningErr: 'prometheus_running_err',
      ElasticsearchURLErr: 'elasticsearch_url_err',
      ElasticsearchRunningErr: 'elasticsearch_running_err',
      ServiceMonitorNotAvailable: 'service_monitor_not_available',
      PrometheusOperatorNotInstall: 'prometheus_operator_not_install',
      NOTINSTALL: 'health_check_component_not_found',
    };
    return get(errorMessageMap, errorMessage, '');
  }
}
