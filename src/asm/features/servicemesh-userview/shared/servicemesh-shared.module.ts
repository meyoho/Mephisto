import { TranslateModule } from '@alauda/common-snippet';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FeatureSharedCommonModule } from '@app/features-shared/common';
import { SharedModule } from '@app/shared';

import {
  GroupGraphComponent,
  LineGraphComponent,
  NodeGraphComponent,
  PartialViewComponent,
  ShapeComponent,
  ToolbarComponent,
  TooltipComponent,
  ZoomableDirective,
} from './commponents';
import {
  ConditionRuleArrayComponent,
  ConditionRuleComponent,
  ConditionRuleFormComponent,
  DelayInjectionDialogComponent,
  FaultInjectionDialogComponent,
  RewriteDialogComponent,
  RouteBaseInfoDetailComponent,
  RouteBaseInfoFormComponent,
  RouteConditionsTableComponent,
  RouteDetailComponent,
  RouteFormComponent,
  RouteFormContainerComponent,
  RoutePolicySelectorComponent,
  RouterCreateComponent,
  RouteRulesComponent,
  RouterUpdateComponent,
  RouteTrafficTableComponent,
  RouteWeightsTableComponent,
  SpacesNotAllowedValidatorDirective,
  TimeoutAndRetryDialogComponent,
  TrafficReplicationDialogComponent,
  WeightRuleComponent,
} from './route-component';
const EXPORTABLE_IMPORTS = [
  PartialViewComponent,
  ToolbarComponent,
  TooltipComponent,
  GroupGraphComponent,
  LineGraphComponent,
  NodeGraphComponent,
  ShapeComponent,
  ZoomableDirective,

  // route
  RouteFormContainerComponent,
  RouteBaseInfoFormComponent,
  RouteFormComponent,
  RouteRulesComponent,
  WeightRuleComponent,
  RouteDetailComponent,
  RouteBaseInfoDetailComponent,
  RouteTrafficTableComponent,
  RouteWeightsTableComponent,
  RouterCreateComponent,
  RouterUpdateComponent,
  ConditionRuleComponent,
  ConditionRuleFormComponent,
  RouteConditionsTableComponent,
  RoutePolicySelectorComponent,
  FaultInjectionDialogComponent,
  DelayInjectionDialogComponent,
  TimeoutAndRetryDialogComponent,
  TrafficReplicationDialogComponent,
  RewriteDialogComponent,
  ConditionRuleArrayComponent,
  SpacesNotAllowedValidatorDirective,
];
@NgModule({
  imports: [
    TranslateModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    FeatureSharedCommonModule,
  ],
  exports: [...EXPORTABLE_IMPORTS],
  declarations: [...EXPORTABLE_IMPORTS],
  entryComponents: [
    FaultInjectionDialogComponent,
    DelayInjectionDialogComponent,
    TimeoutAndRetryDialogComponent,
    TrafficReplicationDialogComponent,
    RewriteDialogComponent,
    RouteDetailComponent,
  ],
  providers: [],
})
export class ServicemeshSharedModule {}
