import { TOKEN_BASE_DOMAIN, TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import {
  RouteManagementService,
  RouterHttp,
  RouterManageRouter,
  VirtualService,
} from '@asm/api/route-management';
import { MicroServiceStore, RouteActionType } from '@asm/api/service-list';

import { first, get, set, toNumber } from 'lodash-es';
import { Subject } from 'rxjs';
import { map, publishReplay, refCount, takeUntil, tap } from 'rxjs/operators';
@Component({
  selector: 'alo-route-form-container',
  templateUrl: './route-form-container.component.html',
  styleUrls: ['./route-form-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteFormContainerComponent implements OnInit, OnDestroy {
  constructor(
    private readonly location: Location,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly activatedRoute: ActivatedRoute,
    private readonly apiService: RouteManagementService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    public store: MicroServiceStore,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  @ViewChild(NgForm, { static: true })
  form: NgForm;

  @Input()
  data: VirtualService;

  routerModel: VirtualService;
  submitting = false;
  namespace = this.activatedRoute.snapshot.params.namespace;
  cluster = this.activatedRoute.snapshot.paramMap.get('cluster') || '';
  onDestroy$ = new Subject<void>();
  params$ = this.route.paramMap.pipe(
    map(params => ({
      cluster: params.get('cluster'),
      namespace: params.get('namespace'),
    })),
    tap(params => {
      this.cluster = params.cluster;
      this.namespace = params.namespace;
    }),
    publishReplay(1),
    refCount(),
  );

  ngOnInit() {
    this.updateEntrance();
    this.store.routeTriggerState$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((type: boolean) => {
        if (type) {
          this.confirm();
        }
      });
  }

  updateEntrance() {
    if (this.data) {
      this.routerModel = this.data;
    }
  }

  confirm() {
    this.form.onSubmit(null);
    const payload: VirtualService = this.form.value.routerForm;
    this.setFormWeightToNumber(payload);
    if (this.form.valid && this.checkWeightRule(payload)) {
      this.submitting = true;
      if (this.store.gatewayInfo.port) {
        payload.spec.http = this.handleGatewayPort(payload.spec.http);
        payload.spec.condition.http = this.handleGatewayPort(
          payload.spec.condition.http,
        );
      }
      if (!this.data) {
        if (this.store.gatewayInfo.name) {
          set(
            payload,
            ['metadata', 'labels', `servicemesh.${this.baseDomain}/resource`],
            'gateway',
          );
        }
        set(
          payload,
          ['metadata', 'labels', `${this.asmBaseDomain}/hostname`],
          first(payload.spec.hosts),
        );
        set(payload, ['metadata', 'namespace'], this.namespace);
        this.apiService.createVirtualService(this.cluster, payload).subscribe(
          res => {
            const name = res.metadata.name;
            this.handleSucc(name, 'create');
          },
          () => {
            this.handleError();
          },
        );
      } else {
        this.apiService.updateVirtualService(this.cluster, payload).subscribe(
          (res: { metadata: { name: string } }) => {
            const name = res.metadata.name;
            this.handleSucc(name, 'update');
          },
          () => {
            this.handleError();
          },
        );
      }
    }
  }

  setFormWeightToNumber(form: VirtualService) {
    if (!form) {
      return;
    }
    const {
      spec: { condition, http },
    } = form;
    const weightToNumber = (route: RouterManageRouter) =>
      (route.weight = toNumber(route.weight));
    condition.http.forEach(https => {
      (https.route || []).forEach(weightToNumber);
    });
    http.forEach(route => {
      route.route = get(route, 'route').reduce((prev, curr) => {
        const weight = toNumber(curr.weight);
        if (weight) {
          return [...prev, { ...curr, weight }];
        }
        return [...prev];
      }, []);
    });
  }

  private handleGatewayPort(https: RouterHttp[]): RouterHttp[] {
    return https.map(httpItem => {
      const { route, ...httpItemRes } = httpItem;
      const gatewayHttpItem = route.reduce((accum, item) => {
        const { destination, ...res } = item;
        const gatewayDestination = {
          ...destination,
          port: { number: this.store.gatewayInfo.port },
        };
        const gatewayHttpItem = { ...res, destination: gatewayDestination };
        return [...accum, gatewayHttpItem];
      }, []);
      return { ...httpItemRes, route: gatewayHttpItem };
    });
  }

  private checkWeightRule(payload: VirtualService) {
    const routes: RouterManageRouter[] = get(payload, 'spec.http[0].route', []);
    const conditions: RouterHttp[] = get(payload, 'spec.condition.http', []);
    const isWeightConditionValid = conditions.every(this.checkConditionWeight);
    const weights: number[] = routes.map(route => route.weight);
    const totalWeight: number = weights.reduce(
      (accum: number, weight: number) => {
        return accum + weight;
      },
      0,
    );
    const isWeightRuleValid: boolean = totalWeight === 100;
    if (!isWeightRuleValid || !isWeightConditionValid) {
      this.message.error(this.translate.get('weight_exceed_max_value_error'));
    }
    return isWeightRuleValid && isWeightConditionValid;
  }

  private checkConditionWeight(condition: RouterHttp) {
    const route: RouterManageRouter[] = get(condition, 'route', []);
    if (!route || !route.length) {
      return true;
    }
    const totalWeight = route.reduce((prev, curr) => {
      return curr.weight + prev;
    }, 0);
    return totalWeight === 100;
  }

  private handleSucc(name: string, action: string) {
    this.submitting = false;
    this.cdr.markForCheck();
    this.message.success(
      this.translate.get(`route_${action}_succ`, {
        name: name,
      }),
    );
    if (this.store.serviceName) {
      this.store.routeAction(RouteActionType.DEFAULT);
      this.store.allowCreationRouteAction({ existRoute: true });
      this.store.routeRefresh$.next('');
      this.cdr.markForCheck();
      return;
    }
    this.switchPage(name, action);
  }

  switchPage(name: string, action: string) {
    switch (action) {
      case 'create':
        this.router.navigate([`../${name}`], {
          relativeTo: this.activatedRoute,
        });
        break;
      case 'update':
        this.router.navigate([`../../${name}`], {
          relativeTo: this.activatedRoute,
        });
    }
  }

  private handleError() {
    this.submitting = false;
    this.cdr.markForCheck();
  }

  cancel() {
    if (this.store.serviceName) {
      this.store.initRouteAction();
      return;
    }
    this.location.back();
  }

  formChange(form: VirtualService) {
    this.routerModel = form;
    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    this.store.initRouteAction();
    this.onDestroy$.next();
  }
}
