import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { AbstractControl, Validators } from '@angular/forms';
import { swap } from '@app/utils/swap';
import { MicroServiceStore } from '@asm/api/service-list';

import { get, isEmpty } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import {
  getSubset,
  RouterHttp,
  RouterManageRouter,
} from 'asm/api/route-management';
@Component({
  selector: 'alo-condition-rule',
  templateUrl: './condition-rule.component.html',
  styleUrls: ['./condition-rule.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConditionRuleComponent extends BaseResourceFormArrayComponent
  implements OnInit {
  @Input()
  services: [] = [];

  @Input()
  namespace: string;

  @Input()
  cluster: string;

  initedConditionRule = false;
  constructor(injector: Injector, public store: MicroServiceStore) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  get canDisplayConditionRule() {
    const conditions = this.form.value || [];
    return (
      !isEmpty(get(conditions[0], 'match', [])) || this.initedConditionRule
    );
  }

  get conditionRuleLen() {
    return this.form.controls.length || 0;
  }

  get existingWorkload() {
    return this.form.value.reduce(
      (prev: string, curr: { route: RouterManageRouter }) => {
        const host = curr.route.destination.host;
        if (host) {
          return [...prev, host];
        }
        return prev;
      },
      [],
    );
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): any {
    return [
      {
        match: [],
        route: {
          destination: {
            host: '',
            subset: '',
          },
          weight: 100,
        },
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  adaptResourceModel(resource: any) {
    if (resource && resource.length) {
      this.initedConditionRule = true;
      return [
        ...resource.map((item: any) => {
          if (Array.isArray(item)) {
            return item.map(data => {
              const { match, route, ...rest } = data;
              return {
                match,
                route: this.handleWorkloadFill(route[0]),
                policy: { ...rest },
              };
            });
          }
          const { match, route, ...rest } = item;
          return {
            match,
            route: this.handleWorkloadFill(route[0]),
            policy: { ...rest },
          };
        }),
      ];
    }
    return resource;
  }

  handleWorkloadFill(route: RouterManageRouter) {
    return this.store.deployments.reduce((prev, curr) => {
      const subset = getSubset(route.destination.subset);
      if (curr.version === subset) {
        return {
          ...prev,
          ...{
            ...route,
            destination: {
              host: curr.name,
              subset,
            },
          },
        };
      }
      return prev;
    }, {});
  }

  adaptFormModel(form: { [key: string]: any }) {
    return [
      ...form.map((item: any) => {
        const { match, route, policy } = item;
        return {
          match,
          route: [
            {
              destination: {
                host: route.destination.host || this.store.serviceName,
                subset:
                  getSubset(route.destination.subset, true) ||
                  route.destination.subset,
              },
              weight: 100,
            },
          ],
          ...policy,
        };
      }),
    ];
  }

  private createNewControl() {
    return this.fb.group({
      match: this.fb.control([]),
      route: this.fb.group({
        destination: this.fb.group({
          host: this.fb.control(''),
          subset: this.fb.control({ value: '', disabled: true }),
        }),
        weight: this.fb.control(''),
      }),
      policy: {},
    });
  }

  addCondition() {
    if (this.initedConditionRule) {
      this.add();
    }
    this.form.controls.forEach(control => {
      control.get('match').setValidators([Validators.required]);
      control.get('match').updateValueAndValidity();
    });
    this.displayConditionRule();
  }

  removeCondition(index: number) {
    if (index === 0 && this.form.value.length === 1) {
      this.remove(index);
      this.add();
      this.initedConditionRule = false;
      this.form.controls.forEach(control => {
        control.get('match').setValidators([]);
        control.get('match').updateValueAndValidity();
      });
    } else {
      this.remove(index);
    }
  }

  displayConditionRule() {
    this.initedConditionRule = true;
  }

  upRecord(index: number) {
    if (index === 0) {
      return;
    }
    this.form.controls = swap(this.form.controls, index, index - 1);
  }

  downRecord(index: number) {
    if (index === this.conditionRuleLen - 1) {
      return;
    }
    this.form.controls = swap(this.form.controls, index, index + 1);
  }

  handleWorkloadChanged(host: string, control: AbstractControl) {
    const deployments = this.store.deployments.find(item => item.name === host);
    control.get('route.destination.subset').setValue(deployments.version);
    control.get('route.destination.subset').disable({ onlySelf: true });
  }

  disableDelete(routHttps: RouterHttp) {
    return routHttps.match.some(item => {
      return Object.keys(item).length > 1;
    });
  }
}
