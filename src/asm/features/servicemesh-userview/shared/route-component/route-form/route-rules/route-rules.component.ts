import { Component, Inject, Injector, Input, OnInit } from '@angular/core';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { matchLabelsToString } from '@app/utils/resource-list';
import {
  getSubset,
  RouteManagementService,
  RouterHttp,
  RouterManageRouter,
  ServiceMeshList,
  Services,
} from '@asm/api/route-management';
import { MicroServiceStore } from '@asm/api/service-list';

import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Component({
  selector: 'alo-route-rules',
  templateUrl: './route-rules.component.html',
  styleUrls: ['./route-rules.component.scss'],
})
export class RouteRulesComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input()
  services: Services[] = [];

  @Input()
  namespace: string;

  @Input()
  cluster: string;

  name = 'RouteRulesComponent';
  deployments: RouterManageRouter[] = [];
  subsets: ServiceMeshList;
  constructor(
    injector: Injector,
    private readonly store: MicroServiceStore,
    private readonly apiService: RouteManagementService,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.getDestinationrule();
    this.backfillRule();
    if (this.store.gatewayInfo.name) {
      this.form.get('gateways').setValue([this.store.gatewayInfo.name]);
    }
  }

  // 通过labelSelector获取 Destinationrule
  // 用来回填工作负载，判断是否存在对应的 Destinationrule 的 verion
  getDestinationrule() {
    const labelSelector = matchLabelsToString({
      [`${this.asmBaseDomain}/hostname`]: this.store.serviceName,
    });
    this.apiService
      .getDestinationruleList(this.cluster, this.namespace, labelSelector)
      .pipe(catchError(() => of({})))
      .subscribe((result: ServiceMeshList[] = []) => {
        this.subsets = get(result, '[0]', {});
        this.cdr.markForCheck();
      });
  }

  backfillRule() {
    if (this.store.deployments) {
      const routes = this.form.get('http.route').value || [];
      const route = this.store.deployments.map((item, index) => {
        const weights =
          this.store.routeState === 'Update'
            ? routes.find(
                (route: RouterManageRouter) =>
                  route.destination.subset.replace('asm-subset-', '') ===
                  item.version,
              )
            : routes[index];
        return {
          destination: {
            host: item.name,
            subset: item.version,
          },
          weight: get(weights, 'weight', 0),
        };
      });
      this.deployments = route;
      this.form.get('http.route').setValue(route);
    }
  }

  createForm() {
    return this.fb.group({
      hosts: this.fb.control([]),
      gateways: this.fb.control([]),
      condition: this.fb.group({
        http: this.fb.control([]),
      }),
      http: this.fb.group({
        route: this.fb.control([]),
      }),
    });
  }

  adaptResourceModel(resource: any) {
    if (resource && resource.http) {
      return {
        ...resource,
        http: {
          ...resource.http[0],
        },
      };
    }
    return { ...resource };
  }

  adaptFormModel(form: { [key: string]: any }) {
    const formModel = { ...form };
    formModel.weights = [];
    const { route, ...rest } = formModel.http;
    const routes = this.setWeight(route);
    routes.forEach((item: RouterManageRouter, index: number) => {
      if (!item.destination.host) {
        route.splice(index, 1);
      } else {
        formModel.weights = [...formModel.weights, ...[item.weight]];
      }
      if (item.destination.subset === 'all') {
        delete item.destination.subset;
      }
    });
    const conditionHttp = get(formModel, 'condition.http', []).map(
      (item: RouterHttp) => ({
        ...item,
        route: this.setWeight(item.route),
      }),
    );
    return {
      ...formModel,
      condition: { http: conditionHttp },
      http: [{ route: routes, ...rest }],
    };
  }

  setWeight(route: RouterManageRouter[]): RouterManageRouter[] {
    if (this.store.deployments.length) {
      return route.map((item, index) => {
        const subset = item.destination.host
          ? this.findSubset(item.destination.host)
          : getSubset(this.deployments[index].destination.subset, true);
        return {
          ...{
            weight: item.weight,
            destination: {
              host: this.store.serviceName,
              subset,
              port: { number: get(index, 'destination.port.number') },
            },
          },
        };
      });
    }
    return route;
  }

  findSubset(host: string) {
    return getSubset(
      get(
        this.store.deployments.find(item => item.name === host),
        'version',
        '',
      ),
      true,
    );
  }

  getDefaultFormModel(): any {
    return {
      hosts: [],
      gateways: [],
      condition: {},
      http: {
        route: [],
      },
    };
  }
}
