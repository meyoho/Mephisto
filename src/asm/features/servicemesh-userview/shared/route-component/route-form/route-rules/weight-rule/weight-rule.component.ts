import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Host,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Optional,
} from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { INT_PATTERN_ZERO } from '@app/utils/patterns';
import {
  RouteManagementService,
  RouterManageRouter,
  ServiceMeshList,
} from '@asm/api/route-management';
import { MicroServiceStore } from '@asm/api/service-list';

import { get, isEqual, toNumber } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  startWith,
  tap,
} from 'rxjs/operators';
@Component({
  selector: 'alo-weight-rule',
  templateUrl: './weight-rule.component.html',
  styleUrls: ['./weight-rule.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeightRuleComponent extends BaseResourceFormArrayComponent
  implements OnInit, OnChanges {
  positiveIntPattern = INT_PATTERN_ZERO;
  weightTotal$: Observable<number>;
  @Input()
  services: [] = [];

  @Input()
  namespace: string;

  @Input()
  cluster: string;

  @Input()
  disaplayRequired = false;

  total = 0;
  portMap: any = {};
  @Input()
  isRule: boolean;

  @Input() subsets: ServiceMeshList;
  @Input() deployments: RouterManageRouter[];
  isWeightRule: boolean;
  constructor(
    injector: Injector,
    private readonly apiService: RouteManagementService,
    public store: MicroServiceStore,
    @Optional()
    @Host()
    private readonly host: ChangeDetectorRef,
  ) {
    super(injector);
  }

  ngOnInit() {
    this.weightTotal$ = this.form.valueChanges.pipe(
      debounceTime(50),
      map((data: RouterManageRouter[]) => {
        return data
          .map((item: RouterManageRouter) => item.weight)
          .reduce((pre, cur) => pre + cur);
      }),
      startWith(0),
      distinctUntilChanged(isEqual),
      tap(total => (this.total = total)),
      publishReplay(1),
      refCount(),
    );
  }

  ngOnChanges() {
    const selectedServiceNameList: string[] = this.form.controls.map(
      control => {
        return this.getHostCtrl(control).value;
      },
    );
    selectedServiceNameList.forEach(name => {
      if (name) {
        this.handlePortMap(name);
      }
    });
  }

  createForm() {
    return this.fb.array([], this.weightExceedValidator);
  }

  getDefaultFormModel(): RouterManageRouter[] {
    return [
      {
        destination: {
          host: '',
          subset: '',
          port: {
            number: null,
          },
        },
        weight: null,
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    this.isWeightRule =
      get(this.host, '_view.parent.component.name', '') ===
      'RouteRulesComponent';
    const defaultValue =
      this.store.serviceName && this.isWeightRule
        ? { value: '', disabled: true }
        : '';
    return this.fb.group({
      destination: this.fb.group({
        host: this.fb.control(defaultValue, [
          Validators.required,
          this.hostConflictValidator,
        ]),
        subset: this.fb.control(defaultValue, [this.versionValidator]),
        port: this.fb.group({
          number: this.fb.control(null),
        }),
      }),
      weight: this.fb.control(null, [
        Validators.required,
        Validators.max(100),
        Validators.min(0),
        Validators.pattern(this.positiveIntPattern.pattern),
        this.workloadAbnormalValidator,
      ]),
    });
  }

  // 判断当前回填的workload是否出现异常，如出现异常值必须为0
  workloadAbnormalValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (control && control.parent && this.deployments) {
      const index = this.form.controls.indexOf(control.parent);
      const subset = get(this.deployments[index], 'destination.subset');
      const pass = get(this.subsets, 'version', []).some(
        item => item.name === subset,
      );

      return pass || toNumber(control.value) === 0
        ? null
        : { workloadAbnormal: true };
    }
  };

  private getPreviousKeys(index: number) {
    return index ? this.formModel.slice(0, index) : this.formModel.slice(1);
  }

  /**
   * @description
   * 验证相同微服务，版本不能一致，并且其中一个版本为 all 情况，不能存在其他版本
   * `previousKeys` 获取除当前item下的所有 itemArray。通过拿当前 control 遍历判断。
   * 在 host 一致下
   * 1. item subset 不能一致
   * 2. item subset不能是 all (父组件会处理subset为all下会删除subset字段，因此判断 subset 为 undefined)
   * 3. 当前 subset 不为 all
   * TODO: 后续可能出现 subset 为 null 情况可否重构简化下
   */
  versionValidator = (control: AbstractControl): { [key: string]: boolean } => {
    if (control && control.parent && control.parent.value.host) {
      const index = this.form.controls.indexOf(control.parent.parent);
      const previousKeys = this.getPreviousKeys(index);
      const conflict = this.isVersionConflict(
        previousKeys,
        control.parent.value,
        control.value,
      );
      return !conflict ? null : { versionConflict: true };
    }
  };

  isVersionConflict(
    formModel: Array<{ [key: string]: any }>,
    control: { [key: string]: any },
    version: string,
  ): boolean {
    return formModel.some((item: RouterManageRouter) => {
      const { host, subset } = item.destination;
      return (
        host === control.host &&
        (subset === version || version === undefined || version === 'all')
      );
    });
  }

  /**
   * @description
   * 验证微服务(host)一致，任意一个subset不能为 all
   * (处理 subset 为 all 和 `versionValidator` 一致 subset === undefined)
   */
  hostConflictValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (
      control &&
      control.value &&
      control.parent &&
      get(control, 'parent.value.host') !== control.value
    ) {
      const index = this.form.controls.indexOf(control.parent.parent);
      const previousKeys = this.getPreviousKeys(index);
      const conflict = previousKeys.find(item => {
        return (
          item.destination.host === control.value &&
          item.destination.subset === undefined
        );
      });
      return !conflict ? null : { hostConflict: true };
    }
  };

  weightExceedValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (control && control.value) {
      return !this.isWeightexceed() ? null : { weightExceed: true };
    }
  };

  isWeightexceed(): boolean {
    if (this.form) {
      const value = this.form.value;
      const weights: number[] = value.map((item: any) => toNumber(item.weight));
      return (
        weights.reduce((weight1: number, weight2: number) => {
          return weight1 + weight2;
        }, 0) > 100
      );
    } else {
      return false;
    }
  }

  // 一开始权重指数需求，暂不删除怕后期再加进来
  // getMathFloor(control: AbstractControl ,value: number, isLaster: Boolean): string {
  //   let weightIndex = Math.floor(value)
  //   const data: RouterManageRouter[] = this.form.value;
  //   const len = data.length;
  //   if(isLaster && len > 1) {
  //     let num = 0;
  //     data.forEach((item, index) => {
  //       let weight;
  //       if (index + 1 !== len) {
  //         weight = Math.floor((item.weight / this.total) * 100);
  //         num += weight;
  //       } else {
  //         weight = 100 - num;
  //         weightIndex = weight
  //       }
  //     })
  //   }
  //   control.get('weightIndex').setValue(weightIndex)
  //   return !value ? '-' : `${weightIndex}%`;
  // }

  getVersion(host: string) {
    if (!host || !this.services) {
      return [];
    }
    const services: ServiceMeshList = this.services.find(
      (service: ServiceMeshList) => service.host === host,
    );
    return services ? [...services.version] : [];
  }

  getPort(host: string) {
    return this.apiService.getServicePort(this.cluster, this.namespace, host);
  }

  handlePortMap(name: string) {
    this.getPort(name).subscribe(res => {
      this.portMap[name] = res;
      this.cdr.markForCheck();
    });
  }

  rowBackgroundColorFn(row: FormControl) {
    return row.invalid && row.touched ? '#fdefef' : '';
  }

  hostChanged(control: AbstractControl) {
    const controlHost = this.getHostCtrl(control);
    const controlSubset = control.get('destination.subset');
    const host = controlHost.value;
    if (host) {
      controlSubset.setValue('');
      controlSubset.setValidators([Validators.required, this.versionValidator]);
      control
        .get('weight')
        .setValidators([
          Validators.required,
          Validators.max(100),
          Validators.pattern(this.positiveIntPattern.pattern),
        ]);
      if (controlHost.hasError('hostConflict')) {
        controlSubset.disable();
        control.get('weight').disable();
      } else {
        controlSubset.enable();
        control.get('weight').enable();
      }
    }
    controlSubset.updateValueAndValidity();
    control.get('weight').updateValueAndValidity();
  }

  getHostCtrl(control: AbstractControl) {
    return control.get('destination.host');
  }
}
