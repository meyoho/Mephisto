import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alo-condition-rule-array',
  templateUrl: './condition-rule-array.component.html',
  styleUrls: ['./condition-rule-array.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConditionRuleArrayComponent extends BaseResourceFormArrayComponent
  implements OnInit {
  @Input() setup: boolean;
  resource: Array<{ [key: string]: any }>;
  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): any {
    return [
      {
        values: [],
      },
    ];
  }

  adaptResourceModel(resource: any) {
    if (resource && resource.length) {
      this.resource = resource;
      return resource;
    }
    return resource;
  }

  adaptFormModel(form: Array<{ [key: string]: any }>) {
    return form.map((item, index) => {
      return {
        ...(this.resource && this.resource[index] ? this.resource[index] : {}),
        ...item,
      };
    });
  }

  private createNewControl() {
    return this.fb.group({
      values: this.fb.control([], [Validators.required]),
    });
  }

  removeControl(index: number) {
    if (this.resource && this.resource[index]) {
      this.resource = this.resource.filter((_, i: number) => index !== i);
    }
    this.remove(index);
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  policyDisable(index: number) {
    if (this.resource && this.resource[index]) {
      return Object.keys(this.resource[index]).length > 1;
    }
    return false;
  }
}
