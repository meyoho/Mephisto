import { TranslateService } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import {
  DOESNOTSUPPORTCHINES_PATTERN,
  SPACES_NOT_ALLOWED_PATTERN,
} from '@app/utils/patterns';
import { MicroServiceStore } from '@asm/api/service-list';

import { isEmpty, uniq } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

interface ConditionRule {
  [key: string]: {
    method: string[];
    key: string;
    value: string;
  };
}
// TODO: authority已废弃，为兼容2.0版本未删除此选项，display为false不显示,在合适时候可将其删除
const conditionRulesMap: ConditionRule = {
  uri: {
    method: ['exact', 'prefix', 'regex'],
    key: 'null',
    value: 'eg:/api/namespace',
  },
  scheme: {
    method: ['exact', 'prefix', 'regex'],
    key: 'null',
    value: 'eg:http, https',
  },
  method: {
    method: ['exact', 'prefix', 'regex'],
    key: 'null',
    value: 'eg:GET, POST, PUT',
  },
  headers: {
    method: ['exact', 'prefix', 'regex'],
    key: 'eg:user',
    value: 'eg:testuser',
  },
  port: { method: [], key: 'null', value: 'eg:8080' },
  authority: {
    method: ['exact', 'prefix', 'regex'],
    key: 'null',
    value: '',
  },
  sourceLabels: {
    method: [],
    key: 'eg:app',
    value: 'eg:ratings',
  },
};

@Component({
  selector: 'alo-condition-rule-form',
  templateUrl: './condition-rule-form.component.html',
  styleUrls: ['./condition-rule-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConditionRuleFormComponent extends BaseResourceFormArrayComponent {
  uriPattern = DOESNOTSUPPORTCHINES_PATTERN;
  spacesNotAllowed = SPACES_NOT_ALLOWED_PATTERN;
  selectedTypes: string[];
  constructor(
    injector: Injector,
    private readonly store: MicroServiceStore,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  conditionRuleTypes = [
    {
      name: 'uri',
      enable: true,
      display: true,
    },
    { name: 'scheme', enable: true, display: true },
    { name: 'method', enable: true, display: true },
    { name: 'headers', enable: true, display: true },
    { name: 'port', enable: true, display: true },
    { name: 'authority', enable: true, display: false },
    {
      name: 'sourceLabels',
      enable: true,
      display: !this.store.gatewayInfo.name,
    },
  ];

  getDefaultFormModel(): any[] {
    return [
      {
        type: '',
        method: null,
        key: null,
        value: null,
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  typeChanged(control: AbstractControl) {
    const type = control.get('type').value;
    if (type) {
      switch (type) {
        case 'port':
          this.setControlRequired(control, ['value']);
          break;
        case 'sourceLabels':
          this.setControlRequired(control, ['key', 'value']);
          break;
        case 'headers':
          this.setControlRequired(control, ['method', 'key', 'value']);
          break;
        default:
          this.setControlRequired(control, ['method', 'value']);
      }
      this.controlRefresh(control, ['method', 'key', 'value']);
    }
  }

  checkSelectedTypes() {
    this.selectedTypes = uniq(
      this.form.controls
        .map(control => control.get('type').value)
        .filter(item => !!item),
    );
    this.conditionRuleTypes.forEach(type => {
      if (type.name !== 'headers' && type.name !== 'sourceLabels') {
        type.enable = !this.selectedTypes.find(
          selectedType => selectedType === type.name,
        );
      }
    });
  }

  getMethods(control: AbstractControl) {
    const type = control.get('type').value;
    if (type) {
      return conditionRulesMap[type].method;
    } else {
      return [];
    }
  }

  getKeyDesc(control: AbstractControl) {
    const type = control.get('type').value;
    if (type) {
      return conditionRulesMap[type].key;
    } else {
      return '';
    }
  }

  isMethodHidden(control: AbstractControl) {
    const type = control.get('type').value;
    if (type) {
      return isEmpty(conditionRulesMap[type].method);
    } else {
      return false;
    }
  }

  getValueDesc(control: AbstractControl) {
    const type = control.get('type').value;
    if (type) {
      return conditionRulesMap[type].value;
    } else {
      return '';
    }
  }

  private setControlRequired(control: AbstractControl, controlNames: string[]) {
    const type = control.get('type').value;
    // 遍历所有，判别是否需要必填，如不必填则删除必填验证
    ['method', 'key', 'value'].forEach(name => {
      const validator =
        controlNames.includes(name) &&
        ['headers', 'sourceLabels'].includes(type) &&
        name === 'key'
          ? [this.sameKeyValidator]
          : [];
      control
        .get(name)
        .setValidators([
          ...(controlNames.includes(name) ? [Validators.required] : []),
          ...(type === 'uri'
            ? [Validators.pattern(this.uriPattern.pattern)]
            : []),
          ...validator,
        ]);
    });
  }

  getValidatorError(control: AbstractControl) {
    const type = control.get('type').value;
    const keyControl = control.get('key');
    const valueControl = control.get('value');
    return [
      {
        name: 'sameKey',
        disabled: keyControl.hasError('sameVersion'),
        tip: this.translate.get('disallow_same_key_tip'),
      },
      {
        name: 'key',
        disabled: keyControl.hasError('pattern'),
        tip: `Key ${this.translate.get(this.spacesNotAllowed.tip)}`,
      },
      {
        name: 'Value',
        disabled: valueControl.hasError('pattern'),
        tip: `Value ${this.translate.get(
          type === 'uri' ? this.uriPattern.tip : this.spacesNotAllowed.tip,
        )}`,
      },
    ].filter(item => item.disabled);
  }

  private controlRefresh(control: AbstractControl, controlNames: string[]) {
    controlNames.forEach(name => {
      control.get(name).setValue(null);
      control.get(name).updateValueAndValidity();
    });
  }

  private createNewControl() {
    return this.fb.group({
      type: this.fb.control('', [Validators.required]),
      method: this.fb.control(''),
      key: this.fb.control('', [
        this.sameKeyValidator,
        Validators.pattern(this.spacesNotAllowed.pattern),
      ]),
      value: this.fb.control(''),
    });
  }

  // headers sourceLabels 验证key不能一致
  private readonly sameKeyValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (control && control.parent) {
      const { type } = control.parent.value;
      if (['headers', 'sourceLabels'].includes(type)) {
        const index = this.form.controls.indexOf(control.parent);
        const previousKeys = this.getPreviousKeys(index);
        const sameKey = previousKeys.some(
          item => item.type === type && item.key === control.value,
        );
        return sameKey ? { sameKey } : null;
      }
      return null;
    }
    return null;
  };

  rowBackgroundColorFn(row: FormControl) {
    return row.invalid && row.touched ? '#fdefef' : '';
  }

  private getPreviousKeys(index: number) {
    return index ? this.formModel.slice(0, index) : this.formModel.slice(1);
  }
}
