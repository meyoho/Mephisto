import { ObjectMeta } from '@alauda/common-snippet';
import { Component, Inject, Injector, Input, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { K8S_RESOURCE_NAME_BASE } from '@app/utils/patterns';
import { MicroServiceStore, RouteActionType } from '@asm/api/service-list';

import { cloneDeep, get, isArray } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alo-route-base-info-form',
  templateUrl: './route-baseInfo-form.component.html',
  styleUrls: ['./route-baseInfo-form.component.scss'],
})
export class RouteBaseInfoFormComponent
  extends BaseResourceFormGroupComponent<any>
  implements OnInit {
  namePattern = K8S_RESOURCE_NAME_BASE;
  namePlaceholder: string;
  isUpdate = false;
  @Input()
  services: [] = [];

  @Input()
  hosts: [] = [];

  constructor(
    injector: Injector,
    public store: MicroServiceStore,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.store.routeAllState[RouteActionType.CREATE]) {
      this.form.get('annotations.hosts').setValue(this.store.serviceName);
      this.form
        .get('name')
        .setValue(this.store.serviceName.replace(/\./g, '-'));
    }
  }

  adaptResourceModel(resource: ObjectMeta = {}) {
    const annotations = resource.annotations;
    if (annotations) {
      annotations.displayName = annotations[`${this.baseDomain}/displayName`];
      annotations.hosts = JSON.parse(
        annotations[`${this.baseDomain}/hosts`] || '[]',
      );
      this.isUpdate = true;
    }
    return { ...resource };
  }

  createForm() {
    return this.fb.group({
      name: this.fb.control('', [Validators.required]),
      annotations: this.fb.group({
        displayName: this.fb.control(''),
        hosts: this.fb.control('', [Validators.required]),
      }),
    });
  }

  getDefaultFormModel(): ObjectMeta {
    return {
      name: '',
      annotations: {
        displayName: '',
        hosts: '',
      },
    };
  }

  adaptFormModel(form: { [key: string]: any }) {
    const cloneForm = cloneDeep(form) || {};
    const displayName = get(cloneForm, 'annotations.displayName');
    cloneForm.annotations[`${this.baseDomain}/displayName`] = displayName;
    delete cloneForm.annotations.displayName;
    const hosts = get(cloneForm, 'annotations.hosts');
    if (hosts) {
      cloneForm.annotations[`${this.baseDomain}/hosts`] = JSON.stringify(
        isArray(hosts) ? hosts : [hosts],
      );
      delete cloneForm.annotations.hosts;
    }
    return cloneForm;
  }
}
