import { Component, Inject, Injector, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import {
  RouteManagementService,
  RouterHttp,
  RouterManageRouter,
  Services,
  VirtualService,
} from '@asm/api/route-management';

import { cloneDeep, get, isEmpty } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'alo-route-form',
  templateUrl: './route-form.component.html',
  styleUrls: ['./route-form.component.scss'],
})
export class RouteFormComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  namespace = this.activatedRoute.snapshot.params.namespace;
  cluster = this.activatedRoute.snapshot.paramMap.get('cluster') || '';
  data: { service: Services[]; host: string[] };
  service: Services[];
  @Input()
  routeModel: VirtualService;

  constructor(
    injector: Injector,
    private readonly apiService: RouteManagementService,
    private readonly activatedRoute: ActivatedRoute,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    super(injector);
    this.getService();
  }

  getService() {
    forkJoin([this.serviceFetch(), this.getRouteList()]).subscribe(
      ([service, hosts]) => {
        this.service = service || [];
        this.setSubset();
        this.data = {
          service: service.filter(item => {
            return !hosts.find(host => host === item.host);
          }),
          host: hosts,
        };
        if (!this.routeModel) {
          this.form.get('spec').setValue({ gateways: [] });
        }
        this.cdr.markForCheck();
      },
    );
  }

  setSubset() {
    const value: VirtualService = this.form.value;
    const {
      spec: { http = [], condition = {}, ...specRes },
      ...resource
    } = value;
    const { route: httpRoutes = [], ...resRoute } =
      http[0] || ({} as RouterHttp);
    const route = httpRoutes.map(this.processSubset);
    const conditionRoute = (get(condition, 'http') || []).map(conditionHttp => {
      if (Array.isArray(conditionHttp)) {
        return conditionHttp.map(item => {
          const { route: routes, ...res } = item;
          return {
            ...res,
            ...{ route: routes.map(this.processSubset) },
          };
        });
      }
      const { route: routes, ...res } = conditionHttp;
      return {
        ...res,
        ...{ route: routes.map(this.processSubset) },
      };
    });
    const result = {
      ...resource,
      spec: {
        ...specRes,
        ...{ http: [{ route, ...resRoute }] },
        ...{ condition: { http: conditionRoute } },
      },
    };
    this.form.setValue(result);
  }

  processSubset = (item: RouterManageRouter) => {
    const existenceHost = (name: string) =>
      this.service.some(service => service.host === name);
    if (!item.destination.subset) {
      item.destination.subset = 'all';
    }
    const { host, subset } = item.destination;
    const existence = existenceHost(host);
    if (
      isEmpty(this.service) ||
      !existence ||
      (existence && subset !== 'all' && !this.getVersion(host, subset))
    ) {
      item.destination.subset = '';
    }
    return item;
  };

  getVersion(host: string, subset: string) {
    const result = this.service.find(service => service.host === host) || {
      version: [],
    };
    return result.version.find(
      (version: { value: string }) => version.value === subset,
    );
  }

  serviceFetch = () => {
    return this.apiService.getDestinationruleList(this.cluster, this.namespace);
  };

  getRouteList(): Observable<string[]> {
    return this.apiService
      .getVirtualServiceList({
        cluster: this.cluster,
        namespace: this.namespace,
      })
      .pipe(
        map(item =>
          item.items.map((data: VirtualService) =>
            JSON.parse(
              get(data, `metadata.annotations["${this.baseDomain}/hosts"]`) ||
                '[]',
            ),
          ),
        ),
        map(item =>
          item.length > 0 ? item.reduce((pre, cur) => pre.concat(cur)) : [],
        ),
      );
  }

  ngOnInit() {
    super.ngOnInit();
  }

  createForm() {
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: this.fb.control({}),
      spec: this.fb.control({}),
    });
  }

  getDefaultFormModel(): any {
    return {
      apiVersion: 'networking.istio.io/v1alpha3',
      kind: 'VirtualService',
      metadata: {},
      spec: {},
    };
  }

  adaptFormModel(form: { [key: string]: any }) {
    const cloneForm = cloneDeep(form) || {};
    if (!cloneForm.metadata.annotations) {
      cloneForm.metadata.annotations = {};
    }
    const hosts = JSON.parse(
      cloneForm.metadata.annotations[`${this.baseDomain}/hosts`] || '[]',
    );
    const disabled = JSON.parse(
      cloneForm.metadata.annotations[`${this.baseDomain}/disabled`] || false,
    );
    cloneForm.spec.hosts = disabled
      ? hosts.map((item: string) => (item = `X-ASM-DISABLED-${item}`))
      : hosts;
    delete cloneForm.spec.weights;
    return { ...cloneForm };
  }
}
