import { AsyncDataLoader } from '@alauda/common-snippet';
import { Component, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RouteParams } from '@app/typings';
import { matchLabelsToString } from '@app/utils/resource-list';
import {
  RouteManagementService,
  VirtualService,
} from '@asm/api/route-management';

import { omit } from 'lodash-es';
import { map, publishReplay, refCount } from 'rxjs/operators';

import { MicroServiceStore } from 'asm/api/service-list';
@Component({
  selector: 'alo-router-update',
  templateUrl: './router-update.component.html',
})
export class RouterUpdateComponent {
  constructor(
    private readonly route: ActivatedRoute,
    private readonly apiService: RouteManagementService,
    private readonly store: MicroServiceStore,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: this.store.serviceName || params.get('name'),
      namespace: params.get('namespace') || '',
      cluster: params.get('cluster') || '',
    })),
    publishReplay(1),
    refCount(),
  );

  fetch = (params: RouteParams) => {
    const labelSelector = matchLabelsToString({
      [`${this.asmBaseDomain}/hostname`]: params.name,
    });
    return this.apiService
      .getVirtualServiceList({
        ...omit(params, ['name', 'isHost']),
        labelSelector,
      })
      .pipe(
        map(item => {
          const { spec, ...rest } = item.items[0] as VirtualService;
          return {
            ...rest,
            spec: this.apiService.toModel(spec),
          };
        }),
      );
  };

  dataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: this.fetch,
  });
}
