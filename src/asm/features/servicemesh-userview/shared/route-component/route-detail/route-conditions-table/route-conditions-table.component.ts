import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import {
  RouterHttp,
  Services,
  VirtualService,
} from '@asm/api/route-management';
import { MicroServiceStore } from '@asm/api/service-list';
@Component({
  selector: 'alo-route-conditions-table',
  templateUrl: './route-conditions-table.component.html',
  styleUrls: ['./route-conditions-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteConditionsTableComponent {
  @Input()
  data: RouterHttp;

  @Input()
  routeModel: VirtualService;

  @Input()
  allowedUpdate: boolean;

  @Input()
  namespace: string;

  @Input()
  services: Services[];

  @Input()
  priorityIndex: number;

  @Input()
  cluster: string;

  @Output()
  updated = new EventEmitter<void>();

  show = false;
  get columns() {
    return ['type', 'method', 'key', 'value'];
  }

  constructor(private readonly store: MicroServiceStore) {}

  getIndex = (index: number) => {
    return this.priorityIndex + index;
  };

  refresh() {
    this.updated.emit();
  }

  trackByFn(index: number) {
    return index;
  }

  handleRouteModel = () => {
    const {
      spec: { ...resSpec },
      ...res
    } = this.routeModel;
    return {
      ...res,
      spec: {
        ...resSpec,
        condition: {
          http: this.store.originalConditionRule,
        },
      },
    };
  };
}
