import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { RouterManageRouter } from '@asm/api/route-management';

import { get } from 'lodash-es';

@Component({
  selector: 'alo-route-weights-table',
  templateUrl: './route-weights-table.component.html',
  styleUrls: ['./route-weights-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteWeightsTableComponent {
  @Input()
  data: RouterManageRouter[];

  @Input()
  isWorkload: boolean;

  get columns() {
    return ['service', 'version', 'weights'];
  }

  getPort(rowData: RouterManageRouter) {
    return get(rowData, 'destination.port.number');
  }
}
