import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
} from '@angular/core';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { MicroServiceStore } from '@asm/api/service-list';
import { ObjectMeta, TranslateService } from '@alauda/common-snippet';

import { get } from 'lodash-es';
@Component({
  selector: 'alo-route-base-info-detail',
  templateUrl: './route-baseInfo-detail.component.html',
  styleUrls: ['./route-baseInfo-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteBaseInfoDetailComponent {
  @Input()
  data: ObjectMeta;

  @Input()
  state: boolean;

  @Input()
  effective: boolean;

  @Input() permissions: { update: boolean; delete: boolean };
  @Output() actions = new EventEmitter();

  get dialogText() {
    return !this.state
      ? {
          title: 'router_enable_confirm_title',
          buttonTitle: 'enable',
          content: 'router_enable_confirm_body',
          button: 'disable',
          icon: 'basic:stop_medium_s',
          type: 'primary',
        }
      : {
          title: 'router_disable_confirm_title',
          buttonTitle: 'disable',
          content: 'router_disable_confirm_body',
          button: 'enable',
          icon: 'basic:stop_medium_s',
          type: 'primary',
        };
  }

  constructor(
    private readonly translate: TranslateService,
    public store: MicroServiceStore,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly baseDomain: string,
  ) {}

  get hosts() {
    const hosts = JSON.parse(
      get(this.data, `annotations["${this.baseDomain}/hosts"]`, '[]'),
    ).join(', ');
    return hosts.length > 0 ? hosts : '-';
  }

  get creator() {
    return get(this.data, `annotations["${this.baseDomain}/creator"]`) || '-';
  }

  get displayName() {
    return (
      get(this.data, `annotations["${this.baseDomain}/displayName"]`) || '-'
    );
  }

  getStatus() {
    return this.state
      ? this.translate.get('enabled')
      : this.translate.get('not_enabled');
  }

  triggerAction(action: string) {
    this.actions.emit(action);
  }
}
