import { TOKEN_BASE_DOMAIN, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Directive,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  NG_ASYNC_VALIDATORS,
  NgForm,
  ValidationErrors,
} from '@angular/forms';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import {
  DOESNOTSUPPORTCHINES_PATTERN,
  SPACES_NOT_ALLOWED_PATTERN,
} from '@app/utils/patterns';
import {
  HTTPRewrite,
  RouteManagementService,
  Services,
  VirtualService,
} from '@asm/api/route-management';

import { get, isNumber, set } from 'lodash-es';
import { forkJoin, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: './rewrite-dialog.component.html',
  styleUrls: ['./rewrite-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RewriteDialogComponent implements OnInit {
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  params: HTTPRewrite = {
    authority: '',
    uri: '',
  };

  submitting = false;
  services: Services[] = [];
  uriPattern = DOESNOTSUPPORTCHINES_PATTERN;
  spacesNotAllowed = SPACES_NOT_ALLOWED_PATTERN;
  constructor(
    private readonly dialogRef: DialogRef<RewriteDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      namespace: string;
      routeModel: VirtualService;
      index: number;
      isCreate: boolean;
      cluster: string;
      message: string;
    },
    private readonly apiService: RouteManagementService,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  ngOnInit() {
    if (this.data && !this.data.isCreate) {
      const http = this.isConditionRule
        ? this.data.routeModel.spec.condition.http[this.data.index]
        : this.data.routeModel.spec.http[0];
      this.params.authority = http.rewrite.authority || '';
      this.params.uri = decodeURI(http.rewrite.uri);
    }
    this.getServices();
  }

  get isConditionRule() {
    return isNumber(this.data.index);
  }

  save() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      this.submitting = true;
      this.apiService
        .updateVirtualService(this.data.cluster, this.handleBody())
        .subscribe(
          () => {
            this.message.success(this.translate.get(this.data.message));
            this.dialogRef.close(true);
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  private getServices() {
    forkJoin([this.serviceFetch(), this.getRouteList()]).subscribe(
      ([service, hosts]) => {
        this.services = service.filter(item => {
          return !hosts.find(host => host === item.host);
        });
        this.cdr.markForCheck();
      },
    );
  }

  serviceFetch() {
    const labelSelector = `servicemesh.${this.baseDomain}/resource!=gateway`;
    return this.apiService.getDestinationruleList(
      this.data.cluster,
      this.data.namespace,
      labelSelector,
    );
  }

  getRouteList(): Observable<string[]> {
    return this.apiService
      .getVirtualServiceList({
        cluster: this.data.cluster,
        namespace: this.data.namespace,
      })
      .pipe(
        map(item =>
          item.items.map((data: VirtualService) =>
            JSON.parse(
              get(
                data,
                `metadata.annotations["${this.asmBaseDomain}/hosts"]`,
              ) || '[]',
            ),
          ),
        ),
        map(item =>
          item.length > 0 ? item.reduce((pre, cur) => pre.concat(cur)) : [],
        ),
      );
  }

  private handleBody() {
    const { spec, ...rest } = this.data.routeModel;
    const { condition, http, ...specRest } = spec;
    const { authority, uri: uriValue } = this.params;
    const uri = encodeURI(uriValue);
    if (this.isConditionRule) {
      set(
        condition.http[this.data.index],
        'rewrite',
        authority
          ? {
              authority,
              uri,
            }
          : {
              uri,
            },
      );
    } else {
      set(
        http[0],
        'rewrite',
        authority
          ? {
              authority,
              uri,
            }
          : {
              uri,
            },
      );
    }
    return { ...rest, spec: { condition, http, ...specRest } };
  }
}

@Directive({
  selector: '[aloSpacesNotAllowed]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: SpacesNotAllowedValidatorDirective,
      multi: true,
    },
  ],
})
export class SpacesNotAllowedValidatorDirective {
  pattern = SPACES_NOT_ALLOWED_PATTERN;
  validate(ctrl: AbstractControl): ValidationErrors | null {
    if (!ctrl.value) {
      return of(null);
    }
    if (!this.pattern.pattern.test(ctrl.value)) {
      return of({ spacesNotAllowed: true });
    }
    return of(null);
  }
}
