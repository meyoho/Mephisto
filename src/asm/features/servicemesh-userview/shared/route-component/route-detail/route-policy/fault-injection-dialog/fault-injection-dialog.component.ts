import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { HTTP_STATUS_CODE, PERCENTAGE_RANGE } from '@app/utils/patterns';
import {
  FaultInjection,
  RouteManagementService,
  VirtualService,
} from '@asm/api/route-management';

import { isNumber, set, toNumber } from 'lodash-es';

@Component({
  templateUrl: './fault-injection-dialog.component.html',
  styleUrls: ['./fault-injection-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FaultInjectionDialogComponent implements OnInit {
  httpStatusCodePattern = HTTP_STATUS_CODE;
  percentagePattern = PERCENTAGE_RANGE;
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  injectionParams: FaultInjection = {
    abort: {
      percentage: {
        value: null,
      },
      httpStatus: null,
    },
  };

  submitting = false;

  constructor(
    private readonly dialogRef: DialogRef<FaultInjectionDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      namespace: string;
      routeModel: VirtualService;
      index: number;
      isCreate: boolean;
      cluster: string;
      message: string;
    },
    private readonly apiService: RouteManagementService,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    if (this.data && !this.data.isCreate) {
      const http = this.isConditionRule
        ? this.data.routeModel.spec.condition.http[this.data.index]
        : this.data.routeModel.spec.http[0];
      this.injectionParams.abort.httpStatus = http.fault.abort.httpStatus;
      this.injectionParams.abort.percentage.value =
        http.fault.abort.percentage.value;
    }
  }

  get isConditionRule() {
    return isNumber(this.data.index);
  }

  save() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      this.submitting = true;
      this.apiService
        .updateVirtualService(this.data.cluster, this.handleFaultBody())
        .subscribe(
          () => {
            this.message.success(this.translate.get(this.data.message));
            this.dialogRef.close(true);
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  private handleFaultBody() {
    const { spec, ...rest } = this.data.routeModel;
    const { condition, http, ...specRest } = spec;
    if (this.isConditionRule) {
      const { percentage, httpStatus } = this.injectionParams.abort;
      set(condition.http[this.data.index], 'fault.abort', {
        percentage: {
          value: toNumber(percentage.value),
        },
        httpStatus: toNumber(httpStatus),
      });
    } else {
      const { percentage, httpStatus } = this.injectionParams.abort;
      set(http[0], 'fault.abort', {
        percentage: {
          value: toNumber(percentage.value),
        },
        httpStatus: toNumber(httpStatus),
      });
    }
    return { ...rest, spec: { condition, http, ...specRest } };
  }
}
