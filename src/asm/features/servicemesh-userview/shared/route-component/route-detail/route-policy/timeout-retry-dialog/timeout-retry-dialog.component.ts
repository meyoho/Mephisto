import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { POSITIVE_INT_PATTERN } from '@app/utils/patterns';
import {
  RouteManagementService,
  VirtualService,
} from '@asm/api/route-management';

import { isNumber, omit, set, toNumber } from 'lodash-es';

@Component({
  templateUrl: './timeout-retry-dialog.component.html',
  styleUrls: ['./timeout-retry-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimeoutAndRetryDialogComponent implements OnInit {
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  params: {
    timeout: number;
    retries: { attempts: number; perTryTimeout: number };
  } = {
    timeout: null,
    retries: {
      attempts: null,
      perTryTimeout: null,
    },
  };

  submitting = false;
  positiveIntPattern = POSITIVE_INT_PATTERN;

  constructor(
    private readonly dialogRef: DialogRef<TimeoutAndRetryDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      namespace: string;
      routeModel: VirtualService;
      index: number;
      isCreate: boolean;
      cluster: string;
      message: string;
    },
    private readonly apiService: RouteManagementService,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    if (this.data && !this.data.isCreate) {
      const http = this.isConditionRule
        ? this.data.routeModel.spec.condition.http[this.data.index]
        : this.data.routeModel.spec.http[0];
      this.params.timeout = parseInt(http.timeout.slice(0, -2), 10);
      if (http.retries) {
        this.params.retries.attempts = http.retries.attempts;
        this.params.retries.perTryTimeout = parseInt(
          http.retries.perTryTimeout.slice(0, -2),
          10,
        );
      }
    }
  }

  get isConditionRule() {
    return isNumber(this.data.index);
  }

  save() {
    this.form.onSubmit(null);
    if (this.form.valid && this.checkTimeoutValid()) {
      this.submitting = true;
      this.apiService
        .updateVirtualService(this.data.cluster, this.handleFormBody())
        .subscribe(
          () => {
            this.message.success(this.translate.get(this.data.message));
            this.dialogRef.close(true);
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  private handleFormBody() {
    const { spec, ...rest } = this.data.routeModel;
    const { condition, http, ...specRest } = spec;
    this.params.retries.attempts =
      toNumber(this.params.retries.attempts) || null;
    if (this.isConditionRule) {
      set(
        condition.http[this.data.index],
        'timeout',
        `${this.params.timeout}ms`,
      );
      if (this.params.retries.perTryTimeout && this.params.retries.attempts) {
        set(condition.http[this.data.index], 'retries', {
          attempts: this.params.retries.attempts,
          perTryTimeout: `${this.params.retries.perTryTimeout}ms`,
        });
      } else {
        condition.http[this.data.index] = omit(
          condition.http[this.data.index],
          'retries',
        );
      }
    } else {
      if (this.params.timeout) {
        set(http[0], 'timeout', `${this.params.timeout}ms`);
      }
      if (this.params.retries.perTryTimeout && this.params.retries.attempts) {
        set(http[0], 'retries', {
          attempts: this.params.retries.attempts,
          perTryTimeout: `${this.params.retries.perTryTimeout}ms`,
        });
      } else {
        http[0] = omit(http[0], 'retries');
      }
    }
    return { ...rest, spec: { condition, http, ...specRest } };
  }

  private checkTimeoutValid() {
    const { perTryTimeout, attempts } = this.params.retries;
    if (
      this.params.retries.perTryTimeout &&
      this.params.retries.attempts &&
      this.params.timeout <= toNumber(perTryTimeout) * (toNumber(attempts) + 1)
    ) {
      this.message.error(this.translate.get('timeout_tips'));
      return false;
    } else {
      return true;
    }
  }
}
