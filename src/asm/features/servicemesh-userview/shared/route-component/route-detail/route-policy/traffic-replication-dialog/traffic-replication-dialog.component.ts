import { TOKEN_BASE_DOMAIN, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { POSITIVE_INT_PATTERN } from '@app/utils/patterns';
import {
  Mirror,
  RouteManagementService,
  ServiceMeshList,
  VirtualService,
} from '@asm/api/route-management';

import { get, isNumber, omit, set, toNumber } from 'lodash-es';
import { tap } from 'rxjs/operators';

@Component({
  templateUrl: './traffic-replication-dialog.component.html',
  styleUrls: ['./traffic-replication-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrafficReplicationDialogComponent implements OnInit {
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  positiveIntPattern = POSITIVE_INT_PATTERN;
  replicationParams: Mirror = {
    host: '',
    subset: '',
    port: {
      number: null,
    },
  };

  services: ServiceMeshList[] = [];
  submitting = false;
  constructor(
    private readonly dialogRef: DialogRef<TrafficReplicationDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      namespace: string;
      routeModel: VirtualService;
      index: number;
      isCreate: boolean;
      cluster: string;
      message: string;
    },
    private readonly apiService: RouteManagementService,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {}

  serviceFetch = () => {
    const labelSelector = `servicemesh.${this.baseDomain}/resource!=gateway`;
    return this.apiService
      .getDestinationruleList(
        this.data.cluster,
        this.data.namespace,
        labelSelector,
      )
      .pipe(tap(result => (this.services = result)));
  };

  ngOnInit() {
    if (this.data && !this.data.isCreate) {
      const http = this.isConditionRule
        ? this.data.routeModel.spec.condition.http[this.data.index]
        : this.data.routeModel.spec.http[0];
      this.replicationParams.host = http.mirror.host;
      this.replicationParams.port.number = get(http, 'mirror.port.number');
      this.replicationParams.subset = http.mirror.subset;
    }
  }

  get isConditionRule() {
    return isNumber(this.data.index);
  }

  save() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      this.submitting = true;
      this.apiService
        .updateVirtualService(this.data.cluster, this.handleReplicationBody())
        .subscribe(
          () => {
            this.message.success(this.translate.get(this.data.message));
            this.dialogRef.close(true);
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  getVersion = (host: string) => {
    if (!host || !this.services) {
      return [];
    }
    const services: ServiceMeshList = this.services.find(
      (service: ServiceMeshList) => service.host === host,
    );
    return services ? [...services.version] : [];
  };

  hostChanged() {
    this.replicationParams.subset = '';
  }

  handleReplicationBody() {
    const { spec, ...rest } = this.data.routeModel;
    const { condition, http, ...specRest } = spec;
    this.replicationParams.port.number =
      toNumber(this.replicationParams.port.number) || null;
    const replicationParams = this.replicationParams.port.number
      ? this.replicationParams
      : omit(this.replicationParams, 'port');
    if (this.isConditionRule) {
      set(condition.http[this.data.index], 'mirror', replicationParams);
    } else {
      set(http[0], 'mirror', replicationParams);
    }
    return { ...rest, spec: { condition, http, ...specRest } };
  }
}
