import { TranslateService } from '@alauda/common-snippet';
import { DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { VirtualService } from '@asm/api/route-management';

import { get, isNumber } from 'lodash-es';

import { DelayInjectionDialogComponent } from '../delay-injection-dialog/delay-injection-dialog.component';
import { FaultInjectionDialogComponent } from '../fault-injection-dialog/fault-injection-dialog.component';
import { RewriteDialogComponent } from '../rewrite-dialog/rewrite-dialog.component';
import { TimeoutAndRetryDialogComponent } from '../timeout-retry-dialog/timeout-retry-dialog.component';
import { TrafficReplicationDialogComponent } from '../traffic-replication-dialog/traffic-replication-dialog.component';

@Component({
  selector: 'alo-route-policy-selector',
  templateUrl: './route-policy-selector.component.html',
  styleUrls: ['./route-policy-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RoutePolicySelectorComponent {
  @Input()
  routeModel: VirtualService;

  @Input()
  index: number;

  @Input()
  namespace: string;

  @Input()
  cluster: string;

  @Input()
  allowedUpdate: boolean;

  @Output()
  updated = new EventEmitter<void>();

  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
  ) {}

  get isConditionRule() {
    return isNumber(this.index);
  }

  get targetHttp() {
    return this.isConditionRule
      ? this.routeModel.spec.condition.http[this.index]
      : this.routeModel.spec.http[0];
  }

  get isAbortExisted() {
    return !!get(this.targetHttp, 'fault.abort');
  }

  get isDelayExisted() {
    return !!get(this.targetHttp, 'fault.delay');
  }

  get isTimeoutAndRetryExisted() {
    return !!(
      get(this.targetHttp, 'timeout') || get(this.targetHttp, 'retries')
    );
  }

  get isRewriteExisted() {
    return !!get(this.targetHttp, 'rewrite');
  }

  get isReplicationExisted() {
    const http = this.isConditionRule
      ? this.routeModel.spec.condition.http[this.index]
      : this.routeModel.spec.http[0];
    return !!get(http, 'mirror');
  }

  get conflictTips() {
    if (this.isTimeoutAndRetryExisted) {
      return this.translate.get('policy_conflict', {
        type: this.translate.get('timeout_and_retry'),
      });
    } else {
      if (this.isAbortExisted && this.isDelayExisted) {
        return this.translate.get('policy_conflict', {
          type: `${this.translate.get('abort_injection')}、${this.translate.get(
            'delay_response',
          )}`,
        });
      } else {
        if (this.isAbortExisted) {
          return this.translate.get('policy_conflict', {
            type: this.translate.get('abort_injection'),
          });
        } else if (this.isReplicationExisted) {
          return this.translate.get('policy_conflict', {
            type: this.translate.get('traffic_replication'),
          });
        } else {
          return this.translate.get('policy_conflict', {
            type: this.translate.get('delay_response'),
          });
        }
      }
    }
  }

  get trafficReplicationConflictTips() {
    if (this.isReplicationExisted && this.isTimeoutAndRetryExisted) {
      return this.translate.get('policy_conflict', {
        type: `${this.translate.get(
          'traffic_replication',
        )}、${this.translate.get('timeout_and_retry')}`,
      });
    }
    if (this.isAbortExisted) {
      return this.translate.get('policy_conflict', {
        type: this.translate.get('abort_injection'),
      });
    }
  }

  openCreatePolicyDialog(type: string) {
    this.openCreateDialog(this.getDialogComponent(type));
  }

  private getDialogComponent(type: string) {
    switch (type) {
      case 'fault':
        return FaultInjectionDialogComponent;
      case 'delay':
        return DelayInjectionDialogComponent;
      case 'retry':
        return TimeoutAndRetryDialogComponent;
      case 'rewrite':
        return RewriteDialogComponent;
      case 'replication':
        return TrafficReplicationDialogComponent;
    }
  }

  private openCreateDialog(dialogComponent: any) {
    this.dialog
      .open(dialogComponent, {
        data: {
          namespace: this.namespace,
          routeModel: this.routeModel,
          index: this.index,
          isCreate: true,
          cluster: this.cluster,
          message: 'policy_created_message_success',
        },
      })
      .afterClosed()
      .subscribe((result: boolean) => {
        if (result) {
          this.updated.emit();
        }
      });
  }
}
