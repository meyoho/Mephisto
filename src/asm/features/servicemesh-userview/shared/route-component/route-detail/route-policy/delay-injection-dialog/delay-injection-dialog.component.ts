import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { PERCENTAGE_RANGE, POSITIVE_INT_PATTERN } from '@app/utils/patterns';
import {
  RouteManagementService,
  VirtualService,
} from '@asm/api/route-management';

import { isNumber, set, toNumber } from 'lodash-es';

@Component({
  templateUrl: './delay-injection-dialog.component.html',
  styleUrls: ['./delay-injection-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DelayInjectionDialogComponent implements OnInit {
  positiveIntPattern = POSITIVE_INT_PATTERN;
  percentagePattern = PERCENTAGE_RANGE;
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  injectionParams: any = {
    delay: {
      percentage: {
        value: null,
      },
      fixedDelay: null,
    },
  };

  submitting = false;

  constructor(
    private readonly dialogRef: DialogRef<DelayInjectionDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      namespace: string;
      routeModel: VirtualService;
      index: number;
      isCreate: boolean;
      cluster: string;
      message: string;
    },
    private readonly apiService: RouteManagementService,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly translate: TranslateService,
  ) {}

  ngOnInit() {
    if (this.data && !this.data.isCreate) {
      const http = this.isConditionRule
        ? this.data.routeModel.spec.condition.http[this.data.index]
        : this.data.routeModel.spec.http[0];
      this.injectionParams.delay.fixedDelay = parseInt(
        http.fault.delay.fixedDelay.slice(0, -2),
        10,
      );
      this.injectionParams.delay.percentage.value =
        http.fault.delay.percentage.value;
    }
  }

  get isConditionRule() {
    return isNumber(this.data.index);
  }

  save() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      this.submitting = true;
      this.apiService
        .updateVirtualService(this.data.cluster, this.handleDelayBody())
        .subscribe(
          () => {
            this.message.success(this.translate.get(this.data.message));
            this.dialogRef.close(true);
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
    }
  }

  private handleDelayBody() {
    const { spec, ...rest } = this.data.routeModel;
    const { condition, http, ...specRest } = spec;
    if (this.isConditionRule) {
      const { percentage, fixedDelay } = this.injectionParams.delay;
      set(condition.http[this.data.index], 'fault.delay', {
        percentage: {
          value: toNumber(percentage.value),
        },
        fixedDelay: `${fixedDelay}ms`,
      });
    } else {
      const { percentage, fixedDelay } = this.injectionParams.delay;
      set(http[0], 'fault.delay', {
        percentage: {
          value: toNumber(percentage.value),
        },
        fixedDelay: `${fixedDelay}ms`,
      });
    }
    return { ...rest, spec: { condition, http, ...specRest } };
  }
}
