import { TranslateService } from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import {
  Mirror,
  RouteManagementService,
  RouterHttp,
  Services,
  VirtualService,
} from '@asm/api/route-management';

import { get, isNumber, omit } from 'lodash-es';

import { DelayInjectionDialogComponent } from '../route-policy/delay-injection-dialog/delay-injection-dialog.component';
import { FaultInjectionDialogComponent } from '../route-policy/fault-injection-dialog/fault-injection-dialog.component';
import { RewriteDialogComponent } from '../route-policy/rewrite-dialog/rewrite-dialog.component';
import { TimeoutAndRetryDialogComponent } from '../route-policy/timeout-retry-dialog/timeout-retry-dialog.component';
import { TrafficReplicationDialogComponent } from '../route-policy/traffic-replication-dialog/traffic-replication-dialog.component';

enum RoutePolicy {
  Abort = 'abort_injection',
  Delay = 'delay_response',
  TimeoutAndRetry = 'timeout_and_retry',
  Rewrite = 'rewrite',
  Replication = 'traffic_replication',
}

@Component({
  selector: 'alo-route-traffic-table',
  templateUrl: './route-traffic-table.component.html',
  styleUrls: ['./route-traffic-table.component.scss'],
})
export class RouteTrafficTableComponent implements OnChanges {
  @Input()
  index: number;

  @Input()
  routeModel: VirtualService;

  @Input()
  namespace: string;

  @Input()
  cluster: string;

  @Output()
  updated = new EventEmitter<void>();

  @Input()
  services: Services[] = [];

  @Input()
  allowedUpdate: boolean;

  data: Array<{ type: string; desc: string[] }>;
  types = [
    RoutePolicy.Abort,
    RoutePolicy.Delay,
    RoutePolicy.TimeoutAndRetry,
    RoutePolicy.Rewrite,
    RoutePolicy.Replication,
  ];

  columns = ['type', 'desc', 'actions'];
  constructor(
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly apiService: RouteManagementService,
    private readonly message: MessageService,
  ) {}

  ngOnChanges() {
    if (this.routeModel) {
      this.data = this.handleTableData();
    }
  }

  get isConditionRule() {
    return isNumber(this.index);
  }

  update(item: { type: string; desc: string }) {
    switch (item.type) {
      case RoutePolicy.Abort:
        this.openUpdateDialog(FaultInjectionDialogComponent);
        break;
      case RoutePolicy.Delay:
        this.openUpdateDialog(DelayInjectionDialogComponent);
        break;
      case RoutePolicy.TimeoutAndRetry:
        this.openUpdateDialog(TimeoutAndRetryDialogComponent);
        break;
      case RoutePolicy.Rewrite:
        this.openUpdateDialog(RewriteDialogComponent);
        break;
      case RoutePolicy.Replication:
        this.openUpdateDialog(TrafficReplicationDialogComponent);
        break;
    }
  }

  delete(item: { type: RoutePolicy; desc: string }) {
    this.dialog
      .confirm({
        title: this.translate.get('delete_confirm', {
          type: this.translate.get(this.getPolicyTypeName(item.type)),
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          this.apiService
            .updateVirtualService(
              this.cluster,
              this.handleDeleteData(
                this.routeModel,
                item.type,
                this.isConditionRule,
                this.index,
              ),
            )
            .subscribe(resolve, () => {
              reject();
            });
        },
      })
      .then(() => {
        this.message.success(
          this.translate.get('policy_deleted_message_success'),
        );
        this.updated.emit();
      })
      .catch();
  }

  private getPolicyTypeName(type: RoutePolicy) {
    switch (type) {
      case RoutePolicy.Abort:
        return 'abort_injection_policy';
      case RoutePolicy.Delay:
        return 'delay_response_policy';
      case RoutePolicy.TimeoutAndRetry:
        return 'timeout_and_retry_policy';
      case RoutePolicy.Rewrite:
        return 'rewrite_policy';
      case RoutePolicy.Replication:
        return 'traffic_replication_policy';
    }
  }

  private openUpdateDialog(dialogComponent: any) {
    this.dialog
      .open(dialogComponent, {
        data: {
          namespace: this.namespace,
          routeModel: this.routeModel,
          index: this.index,
          isCreate: false,
          cluster: this.cluster,
          message: 'policy_updated_message_success',
        },
      })
      .afterClosed()
      .subscribe((result: boolean) => {
        if (result) {
          this.updated.emit();
        }
      });
  }

  /* eslint-disable sonarjs/cognitive-complexity */
  // TODO: Refactor this function to reduce its Cognitive Complexity from 35 to the 15 allowed
  private handleTableData() {
    const http = this.isConditionRule
      ? this.routeModel.spec.condition.http[this.index]
      : this.routeModel.spec.http[0];
    return this.types.reduce((accum, item) => {
      switch (item) {
        case RoutePolicy.Abort:
          if (http.fault && http.fault.abort) {
            return [
              ...accum,
              {
                type: RoutePolicy.Abort,
                desc: [
                  {
                    key: 'http_status',
                    value: http.fault.abort.httpStatus,
                  },
                  {
                    key: 'fault_percentage',
                    value: `${http.fault.abort.percentage.value}%`,
                  },
                ],
              },
            ];
          } else {
            return accum;
          }
        case RoutePolicy.Delay:
          if (http.fault && http.fault.delay) {
            return [
              ...accum,
              {
                type: RoutePolicy.Delay,
                desc: [
                  {
                    key: 'fixed_delay',
                    value: http.fault.delay.fixedDelay,
                  },
                  {
                    key: 'delay_percentage',
                    value: `${http.fault.delay.percentage.value}%`,
                  },
                ],
              },
            ];
          } else {
            return accum;
          }
        case RoutePolicy.TimeoutAndRetry:
          if (http.timeout || http.retries) {
            return [
              ...accum,
              {
                type: RoutePolicy.TimeoutAndRetry,
                desc: http.retries
                  ? [
                      {
                        key: 'timeout',
                        value: http.timeout,
                      },
                      {
                        key: 'attempts',
                        value: http.retries.attempts,
                      },
                      {
                        key: 'per_try_timeout',
                        value: http.retries.perTryTimeout,
                      },
                    ]
                  : [
                      {
                        key: 'timeout',
                        value: http.timeout,
                      },
                    ],
              },
            ];
          } else {
            return accum;
          }
        case RoutePolicy.Rewrite:
          if (http.rewrite) {
            const value = decodeURI(http.rewrite.uri);
            return [
              ...accum,
              {
                type: RoutePolicy.Rewrite,
                desc: http.rewrite.authority
                  ? [
                      {
                        key: 'rewrite_host',
                        value: http.rewrite.authority,
                      },
                      {
                        key: 'rewrite_uri',
                        value,
                      },
                    ]
                  : [
                      {
                        key: 'rewrite_uri',
                        value,
                      },
                    ],
              },
            ];
          } else {
            return accum;
          }
        case RoutePolicy.Replication:
          if (http.mirror) {
            return [
              ...accum,
              {
                type: RoutePolicy.Replication,
                desc: [
                  {
                    key: 'target_service',
                    value: http.mirror.host,
                  },
                  {
                    key: 'target_service_version',
                    value: this.getMirrorSubset(http.mirror),
                  },
                  {
                    key: 'target_service_port',
                    value: get(http, 'mirror.port.number') || '-',
                  },
                ],
              },
            ];
          } else {
            return accum;
          }
        default:
          return accum;
      }
    }, []);
  }

  getMirrorSubset(mirror: Mirror) {
    const { host, subset } = mirror;
    const hosts = this.services.find(item => item.host === host);
    if (!hosts) {
      return '-';
    }
    const version = hosts.version.find(item => item.value === subset);
    return get(version, 'name') || '-';
  }

  private handleDeleteData(
    data: VirtualService,
    type: RoutePolicy,
    isConditionRule: boolean,
    index: number,
  ) {
    const {
      spec: { condition, http, ...specRest },
      ...rest
    } = data;
    const targetHttp = isConditionRule ? condition.http[index] : http[0];
    let parsedHttp: RouterHttp;
    switch (type) {
      case RoutePolicy.Abort:
        parsedHttp = targetHttp.fault.delay
          ? omit(targetHttp, 'fault.abort')
          : omit(targetHttp, 'fault');
        break;
      case RoutePolicy.Delay:
        parsedHttp = targetHttp.fault.abort
          ? omit(targetHttp, 'fault.delay')
          : omit(targetHttp, 'fault');
        break;
      case RoutePolicy.TimeoutAndRetry:
        parsedHttp = omit(targetHttp, ['timeout', 'retries']);
        break;
      case RoutePolicy.Rewrite:
        parsedHttp = omit(targetHttp, 'rewrite');
        break;
      case RoutePolicy.Replication:
        parsedHttp = omit(targetHttp, 'mirror');
        break;
    }
    isConditionRule
      ? (condition.http[index] = parsedHttp)
      : (http[0] = parsedHttp);
    return { spec: { condition, http, ...specRest }, ...rest };
  }
}
