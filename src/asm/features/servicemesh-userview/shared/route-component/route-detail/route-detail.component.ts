import {
  AsyncDataLoader,
  K8sPermissionService,
  K8sResourceAction,
  TranslateService,
} from '@alauda/common-snippet';
import { ConfirmType, DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import { matchLabelsToString } from '@app/utils/resource-list';
import {
  RouteManagementService,
  RouterHttp,
  RouterManageRouter,
  Services,
  VirtualService,
} from '@asm/api/route-management';
import {
  MicroServiceStore,
  RouteActionType,
  TabNumberAction,
} from '@asm/api/service-list';

import { get, omit } from 'lodash-es';
import { combineLatest, of, Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';
@Component({
  selector: 'alo-route-detail',
  templateUrl: './route-detail.component.html',
  styleUrls: ['./route-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteDetailComponent implements OnInit, OnDestroy {
  @Output() triggerUpdate = new EventEmitter();
  conditions: RouterHttp[];
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly apiService: RouteManagementService,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sPermission: K8sPermissionService,
    private store: MicroServiceStore,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly baseDomain: string,
  ) {}

  get dialogText() {
    return !this.enabled
      ? {
          title: 'router_enable_confirm_title',
          buttonTitle: 'enable',
          content: 'router_enable_confirm_body',
          button: 'disable',
          icon: 'basic:stop_medium_s',
          type: 'primary',
        }
      : {
          title: 'router_disable_confirm_title',
          buttonTitle: 'disable',
          content: 'router_disable_confirm_body',
          button: 'enable',
          icon: 'basic:stop_medium_s',
          type: 'primary',
        };
  }

  enabled = true;
  name: string;
  namespace = this.route.snapshot.params.namespace;
  cluster = this.route.snapshot.paramMap.get('cluster') || '';
  routeModel: VirtualService;
  serviceMesh: Services[];
  effective = true;
  refresh$ = this.store.routeRefresh$;
  onDestroy$ = new Subject<void>();

  params$ = combineLatest([
    this.route.paramMap,
    this.store.serviceName$,
    this.store.routeRefresh$,
  ]).pipe(
    map(([params, serviceName]) => ({
      name: serviceName || params.get('name'),
      namespace: params.get('namespace') || '',
      project: params.get('project') || '',
      cluster: params.get('cluster') || '',
      ...(serviceName ? { isHost: true } : {}),
    })),
    tap(params => {
      this.name = params.name;
      this.namespace = params.namespace;
      this.cluster = params.cluster;
      this.cdr.markForCheck();
    }),
    publishReplay(1),
    refCount(),
  );

  fetch = (params: RouteParams) => {
    if (!params.isHost) {
      return of(null);
    }
    const resultProcess = (res: VirtualService) => {
      if (res) {
        this.store.tabAction(1, TabNumberAction.ROUTING);
      }
      this.routeModel = res;
      this.store.virtualService = res;
      this.enabled = this.getState(res);
      this.cdr.markForCheck();
    };
    this.store.tabAction(0, TabNumberAction.ROUTING);
    const labelSelector = matchLabelsToString({
      [`${this.baseDomain}/hostname`]: params.name,
    });
    return this.apiService
      .getVirtualServiceList({
        ...omit(params, ['name', 'isHost']),
        labelSelector,
      })
      .pipe(
        tap(() => {
          this.store.initRouteStore();
        }),
        map(item => {
          this.store.allowCreationRouteAction({
            existRoute: !!item.items.length,
          });
          const { spec, ...rest } = item.items[0] as VirtualService;
          return {
            ...rest,
            spec: this.apiService.toModel(spec, true),
          };
        }),
        tap(res => resultProcess(res)),
      );
  };

  dataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: this.fetch,
  });

  permissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.VIRTUALSERVICE,
        action: [K8sResourceAction.UPDATE, K8sResourceAction.DELETE],
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  ngOnInit() {
    this.store.triggerCheckVersion$
      .pipe(
        switchMap(() =>
          this.apiService.getDestinationruleList(this.cluster, this.namespace),
        ),
        takeUntil(this.onDestroy$),
      )
      .subscribe(result => {
        this.serviceMesh = result || [];
        this.cdr.markForCheck();
      });
  }

  trackByFn(index: number) {
    return index;
  }

  update() {
    if (this.store.serviceName) {
      this.store.routeAction(RouteActionType.UPDATE);
      this.triggerUpdate.emit();
      return;
    }
    this.router.navigate(['../', 'update', this.name], {
      relativeTo: this.route,
    });
  }

  delete() {
    if (!this.enabled) {
      this.dialog
        .confirm({
          title: this.translate.get('router_delete_confirm_title', {
            name: this.name,
          }),
          confirmType: ConfirmType.Danger,
          content: this.translate.get('router_delete_confirm_body'),
          confirmText: this.translate.get('delete'),
          cancelText: this.translate.get('cancel'),
        })
        .then(() => {
          this.apiService
            .deleteVirtualService(this.cluster, this.routeModel)
            .subscribe(
              () => {
                this.message.success(
                  this.translate.get('route_deleted_message_success', {
                    name: this.name,
                  }),
                );
                this.store.initRouteStore();
                this.store.routeRefresh$.next('');
              }
            );
        })
        .catch(() => false);
    } else {
      this.dialog.confirm({
        title: this.translate.get('router_enable_delete_confirm', {
          name: this.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelButton: false,
      });
    }
  }

  getState(data: VirtualService) {
    const state =
      get(data, `metadata.annotations["${this.baseDomain}/disabled"]`) || false;
    return !JSON.parse(state);
  }

  enableFn() {
    this.dialog
      .confirm({
        title: this.translate.get(this.dialogText.title, {
          name: this.name,
        }),
        content: this.translate.get(this.dialogText.content),
        confirmText: this.translate.get(this.dialogText.buttonTitle),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.apiService
          .updateVirtualService(this.cluster, this.handleData())
          .subscribe(
            result => {
              this.routeModel = result;
              this.store.virtualService = result;
              let massage = 'success_enabled';
              if (this.enabled) {
                massage = 'success_disabled';
              }
              this.message.success(this.translate.get(massage));
              this.enabled = !this.enabled;
              this.cdr.markForCheck();
            }
          );
      })
      .catch(() => false);
  }

  handleData() {
    const {
      spec: { hosts, ...specRes },
      metadata: { annotations, ...metadataRes },
      ...rest
    } = this.routeModel;
    const hostList = this.enabled
      ? hosts.map((item: string) => (item = `X-ASM-DISABLED-${item}`))
      : JSON.parse(annotations[`${this.baseDomain}/hosts`]);
    const disabled = this.enabled
      ? { ...annotations, [`${this.baseDomain}/disabled`]: 'true' }
      : omit(annotations, `${this.baseDomain}/disabled`);
    return {
      ...rest,
      metadata: {
        ...metadataRes,
        annotations: {
          ...disabled,
        },
      },
      spec: {
        ...specRes,
        hosts: hostList,
      },
    };
  }

  getConditions = (data: VirtualService) => {
    const http: RouterHttp[] = get(data, 'spec.condition.http', []);
    this.conditions = http.map(item => {
      return {
        match: item.match,
        route: this.handleWeights(item.route),
      };
    });
    return this.conditions.length > 0 ? this.conditions : null;
  };

  getWeights = (data: VirtualService) => {
    const routers: RouterManageRouter[] = get(data, 'spec.http[0].route', []);
    const weights = this.handleWeights(routers, true);
    this.store.weightExistence = weights;
    return weights;
  };

  handleWeights(routers: RouterManageRouter[], type = false) {
    const weights = routers.map(item => {
      const { subset = '', host } = item.destination;
      const displaySubset = this.getVersion(item.destination.host, subset).name;
      let hostName: string;
      if (type) {
        const deployments = this.store.deployments.find(
          item => item.version === subset.replace('asm-subset-', ''),
        );
        hostName = get(deployments, 'name', '-');
      }
      return {
        destination: {
          host: type ? hostName : host,
          subset: displaySubset,
        },
        weight: !item.weight && routers.length === 1 ? 100 : item.weight,
      };
    });
    this.effective = weights.reduce(
      (accum, item) => accum && !!item.destination.subset,
      true,
    );
    return weights;
  }

  getVersion(host: string, version: string) {
    const versions = (this.serviceMesh || []).find(item => item.host === host);
    if (versions) {
      const subset = versions.version.find(item => item.value === version);
      return !version || version === 'all'
        ? subset || { name: 'all' }
        : subset || { name: null };
    }
    return { name: null };
  }

  refresh() {
    this.dataLoader.reload();
  }

  triggerAction(action: string) {
    switch (action) {
      case 'switch':
        this.enableFn();
        break;
      case 'update':
        this.update();
        break;
      case 'delete':
        this.delete();
        break;
      default:
        break;
    }
  }

  getIndex = (index: number) => {
    if (index - 1 < 0) {
      return 0;
    }
    return this.conditions[index - 1].match.length;
  };

  existingStrategy(routHttps: RouterHttp) {
    return routHttps.match.some(item => {
      return Object.keys(item).length > 1;
    });
  }

  getWorkloadName = (route: RouterManageRouter) => {
    const workload = this.store.deployments.find(
      item => item.version === route.destination.subset,
    );
    return get(workload, 'name', '-');
  };

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
