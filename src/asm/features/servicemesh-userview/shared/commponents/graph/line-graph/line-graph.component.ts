import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { config } from '@asm/api/service-topology/config';
import {
  VisLine,
  strokeOpacity,
  strokeWidth,
} from '@asm/api/service-topology/utils';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

import { StatusService } from '../../../../service-topology/status.service';

@Component({
  selector: 'g[alo-line-graph]',
  templateUrl: './line-graph.component.html',
  styleUrls: ['./line-graph.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LineGraphComponent {
  @Input() color: string;
  @Input()
  lineMap: Map<string, VisLine> = new Map();
  @Input()
  currentIds: Set<string> = new Set();
  @Input()
  scale: number;

  @Output()
  clickEvent = new EventEmitter();
  @Output()
  mouseMoveEvent = new EventEmitter();
  @Output()
  mouseoverEvent = new EventEmitter();
  @Output()
  mouseoutEvent = new EventEmitter();
  config = config;

  get nodeStatus() {
    return this.currentIds;
  }

  opacityed$ = this.status.statusSub$.pipe(
    map(params => params.display),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  constructor(private status: StatusService) {}

  getStrokeWidth(id: string) {
    return strokeWidth(id, this.currentIds, 'line');
  }

  getStrokeOpacity(has: boolean) {
    return strokeOpacity(has, this.currentIds);
  }

  getBgAttr({ label }: VisLine) {
    const result = {
      x: 0,
      y: 0,
    };
    result.x = label.position.x - this.config.arcX - 5;
    result.y =
      label.position.y -
      this.config.arcY / 3 -
      this.config.lineLabelFontSize / 2;
    return result;
  }
}
