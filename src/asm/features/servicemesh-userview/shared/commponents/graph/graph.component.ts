import { TranslateService } from '@alauda/common-snippet';
import { ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { config, Placement } from '@asm/api/service-topology/config';
import {
  calculateLineOffset,
  genereatePolygonPath,
  getArcParameter,
  NodeGroup,
  Size,
  SourceLine,
  SourceNode,
  VisLine,
  VisNode,
  VisStatus,
} from '@asm/api/service-topology/utils';

import { ZoomBehavior, zoomTransform } from 'd3-zoom';
import { graphlib, layout } from 'dagre';
import { groupBy, sortBy } from 'lodash-es';
import { Subject } from 'rxjs';

import { ZoomableService } from './zoomable.service';
export interface GraphParams {
  ranksep?: number;
  nodesep?: number;
  placement?: Placement;
}
export class GraphComponent implements OnDestroy {
  groupMap: Map<string, NodeGroup> = new Map();
  nodeMap: Map<string, VisNode> = new Map();
  lineMap: Map<string, VisLine> = new Map();
  groupLines: Map<string, SourceLine[]>;
  lines: SourceLine[];
  graph: any;
  grabState = false;
  zoomBehavior: ZoomBehavior<Element, {}>;
  minMaxValue = { minX: 0, minY: 0, maxX: 0, maxY: 0 };

  onDestroy$ = new Subject<void>();
  config = config;
  size: Size;
  @ViewChild('graph', { static: true })
  graphElement: ElementRef;

  graphParams: GraphParams;
  get markerColorNames() {
    return Object.keys(this.config.lineStrokeColor);
  }

  get marker() {
    return {
      viewBox: `0 0 ${this.config.markerWidth} ${this.config.markerHeight}`,
      refY: this.config.markerHeight / 2,
      height: this.config.markerHeight,
      width: this.config.markerWidth,
      path: `M 0 0 L ${this.config.markerHeight} ${this.config.markerWidth /
        2} L 0 ${this.config.markerWidth} Z`,
    };
  }

  get graphScale() {
    if (!this.zoomService) {
      return 1;
    }
    const node = this.zoomService.svg ? this.zoomService.svg.node() : [];
    const { k = 1 } = zoomTransform(node);
    this.grabState = true;
    return k;
  }

  renderGraph(
    nodes: SourceNode[],
    lines: SourceLine[],
    groupLines?: Map<string, SourceLine[]>,
    params: GraphParams = {},
  ) {
    this.reset();
    this.graphParams = params;
    this.groupLines = groupLines || new Map();
    const g = new graphlib.Graph({ compound: true }).setGraph({
      rankdir: 'LR',
      ranksep: params.ranksep || this.config.nodeHorizontalDis,
      nodesep: params.nodesep || this.config.nodeVerticalDis,
    });
    g.setDefaultEdgeLabel(() => ({}));
    const groups = groupBy(nodes, 'group');
    nodes.forEach(item => {
      g.setNode(item.id, item);
      if (groups[item.group].length > 1 && item.group) {
        g.setParent(item.id, item.group);
      }
    });
    lines.forEach(item => {
      g.setEdge(item.source_id, item.target_id);
    });
    layout(g);
    const gNodes = g.nodes().reduce((prev, curr) => {
      const node = g.node(curr) ? [g.node(curr)] : [];
      if (g.node(curr)) {
        this.setDataMap(node[0]);
      }
      return [...node, ...prev];
    }, []);
    lines.forEach(item => {
      this.setLineMap(item);
    });
    this.lines = lines;
    this.setGroupMap(groupBy(gNodes, 'group'));
    this.adaption();
  }

  existEdge(id: string) {
    return this.lines.some(
      item => item.source_id === id || item.target_id === id,
    );
  }

  setDataMap(node: any) {
    const {
      app,
      id,
      group,
      label = [],
      x,
      y,
      status,
      namespace,
      workload,
      node_type,
      service,
      has_istio_sidecar,
      has_TLS,
      has_CB,
      tipData,
      belong,
    } = node;
    const { minX, minY, maxX, maxY } = this.minMaxValue;
    this.minMaxValue = {
      minX: Math.min(minX, x),
      minY: Math.min(minY, y),
      maxX: Math.max(maxX, x),
      maxY: Math.max(maxY, y),
    };
    this.nodeMap.set(id, {
      app,
      id,
      group,
      label,
      namespace,
      workload,
      service,
      node_type,
      path: genereatePolygonPath({ x, y }, this.config.nodeRadius),
      position: { x, y },
      status,
      has_istio_sidecar,
      has_TLS,
      has_CB,
      tipData,
      belong,
    });
  }

  setGroupMap(groups: { [key: string]: SourceNode[] }) {
    delete groups.null;
    Object.keys(groups).forEach(service => {
      const nodesX = sortBy(groups[service], ['x']);
      const nodesY = sortBy(groups[service], ['y']);
      const nodeXDis = Math.abs(nodesX[nodesX.length - 1].x - nodesX[0].x);
      const nodeYDis = nodesY[nodesY.length - 1].y - nodesY[0].y;
      const {
        lineStrokeColor,
        nodeRadius,
        groupFillX,
        groupFillY,
        lineTextColor,
        groupHeightFill,
      } = this.config;
      if (nodesX.length > 0) {
        const stroke = nodesX.some(item => item.status === 'error')
          ? lineStrokeColor.error
          : nodesX.some(item => item.status === 'warning')
          ? lineStrokeColor.warning
          : lineStrokeColor.success;
        const id = `group-${nodesX.map(n => n.id).join('_')}`;
        const x =
          Math.min(...nodesX.map(item => item.x)) - nodeRadius - groupFillX;
        const y =
          Math.min(...nodesY.map(item => item.y)) - nodeRadius - groupFillY;
        this.groupMap.set(id, {
          id,
          name: nodesX[0].group,
          stroke,
          position: {
            x: x,
            y: y,
          },
          size: {
            width: nodeXDis + (nodeRadius + groupFillX) * 2,
            height: nodeYDis + (nodeRadius + groupHeightFill) * 2,
          },
          text: {
            x: x + nodeXDis / 2 + (nodeRadius + groupFillX),
            y: y,
            fill: lineTextColor.default,
          },
        });
      }
    });
  }

  setLineMap(line: SourceLine) {
    const source = this.getNode(line.source_id);
    const target = this.getNode(line.target_id);
    // caculate offset
    const offset = calculateLineOffset(
      target.position.y - source.position.y,
      target.position.x - source.position.x,
    );
    const status = line.status || VisStatus.Default;
    const start = {
      x: source.position.x,
      y: source.position.y,
    };
    const end = {
      x: target.position.x - offset.x,
      y: target.position.y - offset.y,
    };

    // label
    const texts = Array.isArray(line.label) ? line.label : [line.label];
    const angle = Math.atan2(end.y - start.y, end.x - start.x);
    const position = {
      x: (start.x + end.x) / 2,
      y: (start.y + end.y) / 2,
    };
    const label = {
      id: `text-${line.source_id}_${line.target_id}`,
      position,
      texts,
      rotate: `rotate(${(180 * angle) / Math.PI}, ${position.x} ${position.y})`,
    };
    const id = `${line.source_id}_${line.target_id}`;
    const { d, textY, textX } = getArcParameter(
      line,
      offset,
      source,
      target,
      position,
      this.groupLines,
    );
    this.lineMap.set(id, {
      ...line,
      id: `${line.source_id}_${line.target_id}`,
      status,
      marker: `url(#${status})`,
      start,
      end,
      d,
      textY,
      textX,
      label,
      self: line.source_id === line.target_id,
    });
  }

  getNode(id: string) {
    return this.nodeMap.get(id);
  }

  onResized({ width, height }: any) {
    this.size = { width, height };
  }

  constructor(
    public translate: TranslateService,
    public zoomService: ZoomableService,
  ) {}

  zoomIn() {
    this.zoomService.zoom(1);
  }

  zoomReset() {
    const { translateX, translateY } = this.zoomService.getTranslateXY(
      this.graphParams.placement,
      this.size,
      this.minMaxValue,
      1,
    );
    this.zoomService.interpolateZoom([translateX, translateY], 1);
  }

  zoomOut() {
    this.zoomService.zoom(-1);
  }

  adaption() {
    const {
      clientWidth: width,
      clientHeight: height,
    } = this.graphElement.nativeElement;
    this.size = { width, height };
    const { translateX, translateY, factor } = this.zoomService.getTranslateXY(
      this.graphParams.placement,
      this.size,
      this.minMaxValue,
    );
    this.interpolateZoom([translateX, translateY], factor);
  }

  interpolateZoom(translate: [number, number], scale: number) {
    this.zoomService.interpolateZoom(translate, scale);
  }

  getLineId(e: any) {
    const tag = e.srcElement.tagName;
    if (tag === 'tspan') {
      const textId = e.srcElement.parentElement.id;
      return textId.split('-')[1];
    }
    return e.srcElement.id;
  }

  getLinkInfo(id: string, line: VisLine): string[] {
    const [sourceId, targetId] = id.split('_');
    const source = this.getNode(sourceId);
    const target = this.getNode(targetId);
    const sourceVersion =
      source.label.length > 1 ? source.label[1] : source.label[0];
    const targetVersion =
      target.label.length > 1 ? target.label[1] : target.label[0];
    return [
      `${source.label.length > 1 ? source.label[0] : source.group}${
        sourceVersion ? ':' + sourceVersion : sourceVersion
      }`,
      `${target.label.length > 1 ? target.label[0] : target.group}${
        targetVersion ? ':' + targetVersion : targetVersion
      }`,
      `RPS：${line.label.texts[0]}`,
      `${this.translate.get('error_rate')}：${line.label.texts[1]}`,
    ];
  }

  reset() {
    this.nodeMap = new Map();
    this.lineMap = new Map();
    this.groupMap = new Map();
    this.minMaxValue = { minX: 0, minY: 0, maxX: 0, maxY: 0 };
  }

  getNodeTipInfo(node: VisNode) {
    const { node_type: nodeType, tipData } = node;
    const tipText =
      nodeType === 'service'
        ? [
            `RPS：${tipData.rpsIn}`,
            this.translate.get('error_rate_value', {
              errorRate: tipData.rpsInErrorRate,
            }),
          ]
        : [
            this.translate.get('flow_in', { value: tipData.rpsIn }),
            this.translate.get('incoming_error_rate', {
              value: tipData.rpsInErrorRate,
            }),
            this.translate.get('flow_out', {
              value: tipData.rpsOut,
            }),
          ];
    const serviceLabel =
      node.node_type === 'service' ? '' : `:${node.label[0]}`;
    const nodeLabel = node.label[1] ? `:${node.label[1]}` : serviceLabel;
    return [
      `${node.node_type === 'service' ? node.service : node.app}
      ${node.app.includes('ingressgateway') ? '' : nodeLabel}`,
      ...tipText,
    ];
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
