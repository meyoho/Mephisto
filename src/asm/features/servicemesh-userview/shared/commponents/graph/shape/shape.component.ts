import { Component } from '@angular/core';
import { config, shapePath } from '@asm/api/service-topology/config';
import { NodeType } from '@asm/api/service-topology/service-topology-api.types';
@Component({
  selector: 'alo-shape',
  templateUrl: './shape.component.html',
  styleUrls: ['./shape.component.scss'],
})
export class ShapeComponent {
  config = config;
  shapes = [
    { name: NodeType.IngressGateway, path: shapePath[NodeType.IngressGateway] },
    { name: NodeType.Workload, path: shapePath[NodeType.Workload] },
    { name: NodeType.Service, path: shapePath[NodeType.Service] },
    { name: NodeType.MissingSidecar, path: shapePath[NodeType.MissingSidecar] },
    { name: NodeType.Tls, path: shapePath[NodeType.Tls] },
    { name: NodeType.CircuitBreaker, path: shapePath[NodeType.CircuitBreaker] },
    { name: NodeType.Unknown, path: shapePath[NodeType.Unknown] },
  ];

  opacity = [0.2, 1];
  get markerColorNames() {
    return Object.keys(this.config.lineStrokeColor);
  }

  get marker() {
    return {
      viewBox: `0 0 ${this.config.markerWidth} ${this.config.markerHeight}`,
      refY: this.config.markerHeight / 2,
      height: this.config.markerHeight,
      width: this.config.markerWidth,
      path: `M 0 0 L ${this.config.markerHeight} ${this.config.markerWidth /
        2} L 0 ${this.config.markerWidth} Z`,
    };
  }
}
