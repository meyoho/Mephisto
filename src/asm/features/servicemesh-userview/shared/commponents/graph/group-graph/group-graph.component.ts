import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { strokeOpacity } from '@asm/api/service-topology/utils';
import { NodeGroup } from '@asm/api/service-topology/utils';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';
import { StatusService } from '@asm/features/servicemesh-userview/service-topology/status.service';
@Component({
  selector: 'g[alo-group-graph]',
  templateUrl: './group-graph.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GroupGraphComponent {
  @Input()
  groupMap: Map<string, NodeGroup> = new Map();
  @Input()
  currentIds: Set<string> = new Set();

  get nodeStatus() {
    return this.currentIds;
  }
  opacityed$ = this.status.statusSub$.pipe(
    map(params => params.display),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  constructor(private status: StatusService) {}

  getStrokeOpacity(has: boolean) {
    return strokeOpacity(has, this.currentIds);
  }
}
