import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { config } from '@asm/api/service-topology/config';
import { NodeType } from '@asm/api/service-topology/service-topology-api.types';
import {
  getNodeType,
  isGateway,
  strokeOpacity,
  strokeWidth,
  VisNode,
} from '@asm/api/service-topology/utils';

import { get } from 'lodash-es';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

import { StatusService } from '../../../../service-topology/status.service';
@Component({
  selector: 'g[alo-node-graph]',
  templateUrl: './node-graph.component.html',
  styleUrls: ['./node-graph.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeGraphComponent {
  @Input()
  nodeMap: Map<string, VisNode> = new Map();

  @Input()
  currentIds: Set<string> = new Set();

  @Output()
  clickEvent = new EventEmitter();

  @Output()
  mouseMoveEvent = new EventEmitter();

  @Output()
  mouseoverEvent = new EventEmitter();

  @Output()
  mouseoutEvent = new EventEmitter();

  @Input()
  scale: number;

  config = config;
  isGateway = isGateway;
  getNodeType = getNodeType;
  get nodeStatus() {
    return this.currentIds;
  }

  opacityed$ = this.status.statusSub$.pipe(
    map(params => params.display),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  constructor(private readonly status: StatusService) {}

  getStrokeWidth(id: string) {
    return strokeWidth(id, this.currentIds, 'node');
  }

  getStrokeOpacity(has: boolean) {
    return strokeOpacity(has, this.currentIds);
  }

  nodeTagConfig = (node: VisNode) => {
    const { node_type: nodeType, position, label } = node;
    const offsetY =
      nodeType === 'service' || (nodeType === 'workload' && label.length === 1)
        ? 0
        : config.nodeLabelLineHeight;
    return [node.has_TLS, node.has_CB].reduce((prev, curr, index) => {
      if (curr) {
        return [
          ...prev,
          {
            type: index === 0 ? NodeType.Tls : NodeType.CircuitBreaker,
            y:
              position.y -
              (label.length + 1) * config.nodeLabelLineHeight -
              2 +
              offsetY,
            width: config.nodeTagWidth,
          },
        ];
      }
      return prev;
    }, []);
  };

  getTextOffsetX = (text: any, node: VisNode, index: number) => {
    const { nodeTagWidth } = config;
    if (node.node_type !== 'workload') {
      return (
        node.position.x +
        (get(text.getBBox(), 'width', 0) / 2 + (index ? nodeTagWidth : 0))
      );
    }
    const { has_CB: hasCB, has_TLS: hasTLS, position } = node;
    const offsetX = hasCB && hasTLS ? nodeTagWidth : nodeTagWidth / 2;
    const value = index ? nodeTagWidth : 0;
    const tspan = get(text, `children[${text.children.length - 1}]`);
    let tspanWidth = 0;
    if (tspan) {
      tspanWidth = tspan.getComputedTextLength();
    }
    return position.x - tspanWidth / 2 - offsetX + tspanWidth + value;
  };

  tspanOffsetX = (node: VisNode, index: number) => {
    const {
      has_CB: hasCB,
      has_TLS: hasTLS,
      node_type: nodeType,
      label,
      position,
    } = node;
    if (
      nodeType === 'workload' &&
      (index || label.length === 1) &&
      (hasCB || hasTLS)
    ) {
      const offsetX =
        hasCB && hasTLS ? config.nodeTagWidth : config.nodeTagWidth / 2;
      return position.x - offsetX;
    }
    return position.x;
  };
}
