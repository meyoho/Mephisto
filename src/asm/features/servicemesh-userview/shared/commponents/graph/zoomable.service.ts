import { ElementRef, Injectable } from '@angular/core';
import { Placement, config } from '@asm/api/service-topology/config';
import { Size } from '@asm/api/service-topology/utils';
import { select } from 'd3-selection';
import { transition } from 'd3-transition';
import { ZoomBehavior, zoom, zoomIdentity, zoomTransform } from 'd3-zoom';
@Injectable({
  providedIn: 'root',
})
export class ZoomableService {
  svg: any;
  container: any;
  zoomBehavior: ZoomBehavior<Element, {}>;
  graphRect: SVGRect;

  constructor() {}

  zoomableElement(
    svgElement: ElementRef['nativeElement'],
    containerElement: ElementRef['nativeElement'],
  ) {
    this.svg = select(svgElement);
    this.container = select(containerElement);
    this.zoomBehavior = zoom().scaleExtent(config.zoomScaleExtent);
  }

  setGraphRect(rect: SVGRect) {
    this.graphRect = rect;
  }

  interpolateZoom(translate: [number, number], scale: number) {
    const t = transition()
      .duration(200)
      .ease();
    return this.svg
      .transition(t)
      .call(
        this.zoomBehavior.transform,
        zoomIdentity.translate(translate[0], translate[1]).scale(scale),
      );
  }

  zoom(dir: number) {
    const { x, y, k } = this.getTargetTransform(dir, {
      width: this.graphRect.width,
      height: this.graphRect.height,
    });
    this.interpolateZoom([x, y], k);
  }

  getTargetTransform(dir: number, size: Size) {
    const { x, y, k } = zoomTransform(this.svg.node());
    const newScale = this.getNewScale(k, dir);
    return {
      x: x + (size.width * (k - newScale)) / 2,
      y: y + (size.height * (k - newScale)) / 2,
      k: newScale,
    };
  }

  getNewScale(k: number, dir: number) {
    let newScale = k * (1 + config.zoomFactor * dir);
    const scaleExtent = config.zoomScaleExtent;
    newScale = Math.max(newScale, scaleExtent[0]);
    newScale = Math.min(newScale, scaleExtent[1]);
    return newScale;
  }

  getTranslateXY(
    placement: string = Placement.AUTO,
    grapSize: { width: number; height: number },
    minMaxValue: { minX: number; minY: number; maxX: number; maxY: number },
    size?: number,
  ) {
    const { k } = zoomTransform(this.svg.node());
    const padding = 20 + config.nodeLabelLineHeight * 2 * k * 2;
    const { width, height } = grapSize;
    const { minX, minY, maxX, maxY } = minMaxValue;
    const offset = {
      width: maxX - minX,
      height: maxY - minY,
      x: minX,
      y: minY,
    };
    let factor = 1,
      translateX = 0,
      translateY = 0;
    if (!size) {
      if (offset.width > width || offset.height > height) {
        if (offset.width / width > offset.height / height) {
          factor = (width - padding) / offset.width;
        } else {
          factor = (height - padding) / offset.height;
        }
      }
    } else {
      factor = size;
    }

    switch (placement) {
      case Placement.AUTO:
        translateX = width / 2 - minX * factor - (offset.width / 2) * factor;
        translateY = height / 2 - minY * factor - (offset.height / 2) * factor;
        break;
      case Placement.TOP_START:
        translateX = 40;
        translateY = 10;
        break;
      default:
        break;
    }

    return {
      translateX,
      translateY,
      factor,
    };
  }
}
