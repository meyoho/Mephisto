import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { config } from '@asm/api/service-topology/config';
@Component({
  selector: 'alo-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarComponent {
  zoomInStatus = false;
  zoomOutStatus = false;
  scale = 0;
  @Input()
  viewSize = '100%';
  @Input()
  set graphScale(scale: number) {
    this.zoomInStatus = scale === config.zoomScaleExtent[1];
    this.zoomOutStatus = scale === config.zoomScaleExtent[0];
    this.scale = scale;
  }

  @Output()
  resetEvent = new EventEmitter();
  @Output()
  zoomInEvent = new EventEmitter();
  @Output()
  zooOutEvent = new EventEmitter();
  @Output()
  adaptionEvent = new EventEmitter();

  constructor() {}

  zoomReset() {
    this.resetEvent.emit();
  }
  zoomIn() {
    this.zoomInEvent.emit();
  }
  zoomOut() {
    this.zooOutEvent.emit();
  }
  adaption() {
    this.adaptionEvent.emit();
  }
}
