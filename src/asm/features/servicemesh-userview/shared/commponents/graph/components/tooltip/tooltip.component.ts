import { Component, Input } from '@angular/core';
import { config } from '@asm/api/service-topology/config';
import { TipInfo } from '@asm/api/service-topology/service-topology-api.types';
@Component({
  selector: 'alo-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss'],
})
export class TooltipComponent {
  @Input()
  tipInfo: TipInfo;
  config = config;
  @Input()
  type: string;
}
