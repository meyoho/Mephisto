import { TranslateService } from '@alauda/common-snippet';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceTopologyApiService } from '@asm/api/service-topology/service-topology-api.service';
import {
  OperatingState,
  TipInfo,
} from '@asm/api/service-topology/service-topology-api.types';
import { isGateway, VisLine, VisNode } from '@asm/api/service-topology/utils';

import { isEmpty } from 'lodash-es';
import {
  distinctUntilChanged,
  publishReplay,
  refCount,
  takeUntil,
} from 'rxjs/operators';

import { StatusService } from '../../../../../service-topology/status.service';
import { GraphComponent } from '../../graph.component';
import { ZoomableService } from '../../zoomable.service';
@Component({
  selector: 'alo-partial-view',
  templateUrl: './partial-view.component.html',
  styleUrls: ['./partial-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ZoomableService],
})
export class PartialViewComponent extends GraphComponent
  implements OnInit, AfterViewInit {
  cluster = this.route.snapshot.paramMap.get('cluster') || '';
  namespace = this.route.snapshot.paramMap.get('namespace') || '';
  isGateway = isGateway;
  selectedNode: VisNode;
  @Input()
  set node(node: VisNode) {
    this.selectedNode = node;
    this.selected = { id: node.id, type: 'node' };
  }

  @Output()
  closeEvent = new EventEmitter();

  @Output()
  nodeClickEvent = new EventEmitter();

  lineTooltip: TipInfo = {
    type: 'line',
    width: 145,
    height: 88,
    show: false,
    x: 0,
    y: 0,
  };

  nodeTooltip: TipInfo = {
    type: 'node',
    width: 145,
    height: 115,
    x: 0,
    y: 0,
    show: false,
  };

  currentIds = new Set<string>();
  selected: { id: string; type: string; ev?: MouseEvent };
  injected = false;
  get title() {
    const label = this.selectedNode.label;
    return label.length > 1
      ? `${label[0]} ${label[1]}`
      : `${this.selectedNode.group} ${label[0]}`;
  }

  getTitle(node: VisNode) {
    return node.label.length > 1
      ? `${node.label[0]}${node.label[1] ? ':' + node.label[1] : node.label[1]}`
      : `${node.group}${node.label[0] ? ':' + node.label[0] : node.label[0]}`;
  }

  constructor(
    private readonly cdr: ChangeDetectorRef,
    translate: TranslateService,
    zoomService: ZoomableService,
    private readonly api: ServiceTopologyApiService,
    private readonly status: StatusService,
    private readonly route: ActivatedRoute,
  ) {
    super(translate, zoomService);
  }

  ngOnInit() {
    this.status.statusSub$
      .pipe(
        takeUntil(this.onDestroy$),
        distinctUntilChanged(isEmpty),
        publishReplay(1),
        refCount(),
      )
      .subscribe(({ closed, changed, display, injected }: OperatingState) => {
        if (
          ['true', 'false'].includes(String(injected)) &&
          injected !== this.injected
        ) {
          this.injected = injected || false;
          this.render();
        }
        if (changed && display) {
          this.render();
        }
        if (closed) {
          this.clearIds();
          this.cdr.markForCheck();
        }
      });
  }

  ngAfterViewInit() {
    this.render();
  }

  zoomEnd() {
    this.grabState = false;
  }

  render() {
    this.clearIds();
    this.currentIds.add(this.selectedNode.id);
    const {
      namespace,
      workload,
      node_type: nodeType,
      service,
    } = this.selectedNode;
    const [startTime, endTime] = this.status.time;
    const params = {
      namespace,
      ...(nodeType === 'service' ? { service } : { workload }),
      start_time: startTime,
      end_time: endTime,
      inject_service_nodes: String(this.injected),
      cluster: this.cluster,
      ...(this.namespace !== namespace
        ? {
            selected_namespace: this.namespace,
          }
        : {}),
    };
    this.api.getPartialViewDate(params).subscribe(result => {
      const data = this.api.toSourceData(result);
      const { nodes, lines, groupLines } = data;
      this.renderGraph(nodes, lines, groupLines);
      this.status.setParams({ sourceData: result });
      this.setCurrentId(this.selected.id, this.selected.type, this.selected.ev);
    });
    this.status.setMapStatus({ changed: false });
  }

  lineClick(e: MouseEvent) {
    const lineId = this.getLineId(e);
    this.setCurrentId(lineId, 'line', e);
  }

  lineMouseMove([e, line]: [MouseEvent, VisLine]) {
    this.lineTooltip.x = Math.min(
      this.size.width - this.lineTooltip.width,
      e.offsetX + 8,
    );
    this.lineTooltip.y = Math.min(
      this.size.height - this.lineTooltip.height,
      e.offsetY + 16,
    );
    const id = this.getLineId(e);
    const value = this.getLinkInfo(id, line);
    this.lineTooltip.status = line.status;
    this.lineTooltip.data = value;
  }

  nodeClick({ detail, srcElement }: any) {
    const { id } = srcElement;
    const { node_type: nodeType } = this.getNode(id);
    if (detail === 1) {
      // TODO: 禁用单击：解决单击的节点展示不是当前节点局部图，右侧信息和局部图数据不统一
      // this.setCurrentId(id, 'node');
    } else if (detail === 2 && nodeType !== 'service') {
      this.setCurrentId(id, 'node');
      this.selectedNode = this.getNode(id);
      this.status.setMapStatus({ changed: true });
    }
  }

  setCurrentId(id: string, type: string, ev?: MouseEvent) {
    this.clearIds();
    this.currentIds.add(id);
    let status = '';
    if (type === 'node') {
      status = this.getNode(id).status;
    }
    this.nodeClickEvent.emit({ type, id, ev, status });
    this.selected = { id, type, ev };
  }

  nodeMouseMove(e: MouseEvent) {
    this.nodeTooltip.x = Math.min(
      this.size.width - this.nodeTooltip.width,
      e.offsetX + 8,
    );
    this.nodeTooltip.y = Math.min(
      this.size.height - this.nodeTooltip.height,
      e.offsetY + 16,
    );
  }

  nodeMouseover(id: string) {
    this.nodeTooltip.show = true;
    const node = this.getNode(id);
    this.nodeTooltip.data = this.getNodeTipInfo(node);
  }

  close() {
    this.status.setMapStatus({ display: false });
    this.closeEvent.emit(this.selected || {});
  }

  getStrokeWidth(id: string) {
    return this.currentIds.has(id)
      ? this.config.activeNodeStrokeWidth
      : this.config.nodeStrokeWidth;
  }

  clearIds() {
    this.currentIds = new Set();
  }
}
