import {
  AfterViewChecked,
  ChangeDetectorRef,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Placement, config } from '@asm/api/service-topology/config';
import { event } from 'd3-selection';

import { ZoomableService } from './zoomable.service';

@Directive({
  selector: '[aloZoomableOf]',
})
export class ZoomableDirective implements OnInit, AfterViewChecked {
  @Input('aloZoomableOf')
  zoomableOf: ElementRef['nativeElement'];
  // tslint:disable-next-line:no-input-rename
  @Input('aloZoomUnmovable')
  zoomUnmovable: boolean;
  // tslint:disable-next-line:no-input-rename
  @Input('aloNotZoom')
  aloNotZoom: boolean;

  @Output()
  zoomEnd = new EventEmitter();
  constructor(
    private zoomService: ZoomableService,
    private _element: ElementRef,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.zoomService.zoomableElement(
      this.zoomableOf,
      this._element.nativeElement,
    );
    if (!this.zoomUnmovable) {
      this.applyZoomBehavior();
    } else {
      const { container } = this.zoomService;
      const { placement } = config;
      const { TOP_START } = Placement;
      container.attr(
        'transform',
        `translate(
          ${placement[TOP_START].x},
          ${placement[TOP_START].y}
        ) scale(1)`,
      );
    }
  }

  ngAfterViewChecked(): void {
    this.zoomService.setGraphRect(this._element.nativeElement.getBBox());
  }

  applyZoomBehavior() {
    const { svg, zoomBehavior, container } = this.zoomService;
    svg
      .call(
        zoomBehavior.on('zoom', () => {
          if (this.aloNotZoom) {
            const { x, y } = event.transform;
            container.attr('transform', `translate(${x},${y}) scale(1)`);
          } else {
            container.attr('transform', event.transform);
          }
          this.cdr.markForCheck();
        }),
        zoomBehavior.on('end', () => {
          this.zoomEnd.emit();
        }),
      )
      .on('dblclick.zoom', null);
  }
}
