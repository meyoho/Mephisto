import {
  K8sApiService,
  KubernetesResource,
  publishRef,
  TOKEN_BASE_DOMAIN,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResourceType } from '@app/utils';
import {
  Category,
  ResourceListService,
} from '@asm/api/resource-list/resource-list.service';

import { safeDump } from 'js-yaml';
import {
  debounce,
  find,
  get,
  includes,
  isEqual,
  memoize,
  omitBy,
} from 'lodash-es';
import { combineLatest, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { APIResource } from 'app/typings';
import { viewActions, yamlReadOptions } from 'app/utils/code-editor-config';
import { ResourceList } from 'app/utils/resource-list';

export interface ColumnDef {
  name: string;
  label?: string;
}

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
  },
  {
    name: 'label',
    label: 'label',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
  },
];

const EXPANDED_MAP = {
  namespaced: false,
  cluster: false,
};

@Component({
  selector: 'alo-resource-management-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceListComponent implements OnInit, OnDestroy {
  private readonly onDestroy$$ = new Subject<void>();

  columnDefs = ALL_COLUMN_DEFS;

  keyword$ = this.route.queryParamMap.pipe(
    map(params => params.get('keyword')),
  );

  expandedMap = EXPANDED_MAP;

  activePath = '';
  yamlInputValue = '';

  category: Category = {
    namespaced: [],
    cluster: [],
  };

  editorActions = viewActions;
  editorOptions = yamlReadOptions;

  get resourceType() {
    return this.resourceListService.resourceType;
  }

  params$ = this.route.paramMap.pipe(
    map(params => {
      return {
        cluster: params.get('cluster') || '',
        namespace: params.get('namespace') || '',
      };
    }),
    tap(({ cluster }) => this.resourceListService.initService(cluster)),
    publishReplay(1),
    refCount(),
  );

  navLoading$ = this.resourceListService.navLoading$;

  initialized: boolean;
  filterName: string;

  getFilteredCategory = memoize(
    (category: Category, filterName: string) =>
      filterName
        ? {
            namespaced: category.namespaced.filter(item =>
              item.kind.toLowerCase().includes(filterName.toLowerCase()),
            ),
            cluster: category.cluster.filter(item =>
              item.kind.toLowerCase().includes(filterName.toLowerCase()),
            ),
          }
        : category,
    (category: Category, filterName: string) =>
      JSON.stringify(
        category.namespaced
          .concat(category.cluster)
          .map(({ kind }) => kind)
          .concat(filterName),
      ),
  );

  get filteredCategory() {
    return this.getFilteredCategory(this.category, this.filterName);
  }

  fetchParams$ = combineLatest([
    this.params$,
    this.resourceListService.resourceType$,
    this.keyword$,
  ]).pipe(
    distinctUntilChanged(isEqual),
    map(([params, resourceType, keyword]) => ({
      cluster: params.cluster,
      namespace: params.namespace,
      resourceType,
      keyword,
    })),
    publishRef(),
  );

  list = new ResourceList({
    fetchParams$: this.fetchParams$,
    fetcher: this.fetchResourceList.bind(this),
  });

  constructor(
    private readonly cdr: ChangeDetectorRef,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly dialog: DialogService,
    private readonly k8sApi: K8sApiService<ResourceType>,
    private readonly resourceListService: ResourceListService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {}

  fetchResourceList({ cluster, namespace, resourceType, ...queryParams }: any) {
    return this.k8sApi.getResourceList({
      definition: this.resourceListService.getResourceDefinition(resourceType),
      cluster,
      ...(resourceType.namespaced ? { namespace: namespace } : {}),
      queryParams,
    });
  }

  ngOnInit() {
    this.resourceListService.category$
      .pipe(
        takeUntil(this.onDestroy$$),
        filter(_ => !!_),
      )
      .subscribe(category => {
        this.category = category;
        this.initNav();
      });
  }

  ngOnDestroy() {
    this.onDestroy$$.next();
  }

  get columns() {
    return this.columnDefs.map(colDef => colDef.name);
  }

  initNav() {
    if (!this.resourceType) {
      return;
    }
    if (!this.resourceType.kind) {
      this.expandedMap.namespaced = true;
      this.activePath = 'namespaced';
    } else {
      const isNamespacedKind = !!find(this.category.namespaced, [
        'kind',
        this.resourceType.kind,
      ]);
      this.expandedMap = {
        namespaced: isNamespacedKind,
        cluster: !isNamespacedKind,
      };
      this.activePath = isNamespacedKind ? 'namespaced' : 'cluster';
    }
  }

  getLabelValue(resource: KubernetesResource) {
    return omitBy(get(resource, 'metadata.labels', {}), (_v, k) =>
      includes(k, `.${this.baseDomain}`),
    );
  }

  viewResource(resource: KubernetesResource, templateRef: TemplateRef<any>) {
    this.yamlInputValue = safeDump(resource, {
      lineWidth: 9999,
      sortKeys: true,
    });
    this.dialog.open(templateRef, {
      size: DialogSize.Large,
      data: {
        name: resource.metadata.name,
      },
    });
  }

  /* Nav list */
  toggleExpand(path: keyof typeof EXPANDED_MAP) {
    this.expandedMap[path] = !this.expandedMap[path];
    this.activePath = path;
  }

  onResourceTypeChange(resourceType: APIResource) {
    this.resourceListService.setResourceType(resourceType);
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        keyword: '',
      },
      queryParamsHandling: 'merge',
    });
  }

  isPathActive(path: string) {
    return this.activePath.startsWith(path);
  }

  isPathExpanded(path: keyof typeof EXPANDED_MAP) {
    return this.expandedMap[path];
  }

  navSearch(queryString: string) {
    this.filterName = queryString.trim();
    this.expandedMap.namespaced = true;
    this.expandedMap.cluster = true;
    this.cdr.markForCheck();
  }

  onChangeHandler = debounce(function(
    this: ResourceListComponent,
    queryString: string,
  ) {
    this.navSearch(queryString);
  },
  100);

  trackByFn(index: number, item: KubernetesResource) {
    return get(item, 'metadata.uid', index);
  }

  onSearch(keyword: string) {
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {
        keyword,
      },
      queryParamsHandling: 'merge',
    });
  }
}
