import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from 'app/shared/shared.module';
import { ResourceListPageComponent } from './list-page.component';
import { ResourceListComponent } from './list/list.component';
import { ResourceManageRoutingModule } from './routing.module';

@NgModule({
  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ResourceManageRoutingModule,
  ],
  declarations: [ResourceListComponent, ResourceListPageComponent],
})
export class ResourceManageModule {}
