import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { CommonStoreService } from '@app/services/common-store.service';
import { PermissionType, permissionUrl } from '@asm/utils';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserGuardService implements CanActivate {
  constructor(
    private commonStore: CommonStoreService,
    private router: Router,
  ) {}
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const { project, cluster, namespace } = route.params;
    const deployedUrl = permissionUrl(PermissionType.Notdeployed, {
      project,
      cluster,
      namespace,
    });
    const unopenedUrl = permissionUrl(PermissionType.Unopened, {
      project,
      cluster,
      namespace,
    });
    return forkJoin(
      this.commonStore.getDeployed(cluster),
      this.commonStore.getEnabled(cluster, namespace),
    ).pipe(
      map(([deployed, unopened]) => {
        if (!deployed[cluster]) {
          this.router.navigate([deployedUrl]);
          return false;
        }
        if (!unopened[namespace]) {
          this.router.navigate([unopenedUrl]);
          return false;
        }
        return true;
      }),
    );
    // TODO: store storage deployed
    // if(!this.commonStore.hasAttribute(cluster,  StoreType.Deployed)){
    //   return this.commonStore.getDeployed(cluster).toPromise().then(result => {
    //     if(result[cluster]){
    //       return true
    //     }
    //     return this.router.navigate([url]);
    //   })
    // }else {
    //   if(!this.commonStore.deployed(cluster)){
    //     return this.router.navigate([url]);
    //   }
    //   return true
    // }
  }
}
