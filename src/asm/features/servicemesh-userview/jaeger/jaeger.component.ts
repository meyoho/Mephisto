import { TranslateService } from '@alauda/common-snippet';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { ServiceMeshApiService } from '@asm/api/service-mesh/service-mesh-api.service';

import { combineLatest, fromEvent, of, Subject } from 'rxjs';
import {
  catchError,
  filter,
  map,
  // mapTo,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

@Component({
  templateUrl: './jaeger.component.html',
  styleUrls: ['./jaeger.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JaegerComponent implements OnDestroy, AfterViewInit {
  @ViewChild('jaegerUI')
  iframe: ElementRef<HTMLIFrameElement>;

  destroy$ = new Subject<void>();
  iframeInitialized = false;
  postTimer: any;
  cluster = this.route.snapshot.paramMap.get('cluster') || '';

  params$ = this.route.queryParamMap.pipe(
    map(queryParams => ({
      selectedService: queryParams.get('selectedService') || '',
      workloadService: queryParams.get('workloadService') || '',
      lookback: queryParams.get('lookback') || '',
      start: queryParams.get('start') || '',
      end: queryParams.get('end') || '',
    })),
    publishReplay(1),
    refCount(),
  );

  namespace$ = this.route.paramMap.pipe(
    takeUntil(this.destroy$),
    map(params => params.get('namespace') || ''),
    publishReplay(1),
    refCount(),
  );

  service$ = this.namespace$.pipe(
    switchMap(params => this.api.getServiceList(this.cluster, params)),
    map(res => res.items || []),
    publishReplay(1),
    refCount(),
  );

  jaegerUrl$ = this.api.getJaegerUrl(this.cluster).pipe(
    catchError(_ => {
      return of('');
    }),
    startWith(''),
    // mapTo('http://localhost:3000'),
    map(url => this.sanitizer.bypassSecurityTrustResourceUrl(url)),
  );

  hiddenServiceNames$ = this.api.getHiddenServiceNames(this.cluster);

  message$ = combineLatest(
    this.params$,
    this.translate.locale$,
    this.service$,
    this.namespace$,
    this.hiddenServiceNames$,
  ).pipe(
    takeUntil(this.destroy$),
    map(([params, lang, services, namespace, hiddenServices]) => {
      return {
        source: 'diablo',
        ...params,
        ...{
          services: services.filter(service => {
            return !hiddenServices.find(item => item === service.metadata.name);
          }),
        },
        lang,
        namespace,
      };
    }),
    publishReplay(1),
    refCount(),
  );

  jaegerInitialized$ = fromEvent(window, 'message').pipe(
    takeUntil(this.destroy$),
    map((event: MessageEvent) => event.data),
    filter(data => data === 'jaegerInitialized'),
    tap(() => {
      this.iframeInitialized = true;
      this.cdr.markForCheck();
    }),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly sanitizer: DomSanitizer,
    private readonly api: ServiceMeshApiService,
    private readonly route: ActivatedRoute,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngAfterViewInit() {
    combineLatest(this.jaegerInitialized$, this.message$)
      .pipe(takeUntil(this.destroy$))
      .subscribe(([, msg]) => {
        this.iframe.nativeElement.contentWindow.postMessage(msg, '*');
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
  }
}
