import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FeatureSharedCommonModule } from '@app/features-shared/common';
import { SharedModule } from '@app/shared';

import { AllowlistTableComponent } from './whitelist-dialog/allowlist-table/allowlist-table.component';
import { WhitelistContainerComponent } from './whitelist-dialog/whitelist-container/whitelist-container.component';
import { WhitelistDialogComponent } from './whitelist-dialog/whitelist-dialog.component';
import { WhitelistComponent } from './whitelist.component';
const MODULES = [
  WhitelistComponent,
  WhitelistContainerComponent,
  WhitelistDialogComponent,
  AllowlistTableComponent,
];
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    FeatureSharedCommonModule,
  ],
  declarations: [...MODULES],
  exports: [...MODULES],
  entryComponents: [WhitelistDialogComponent],
})
export class WhitelistModule {}
