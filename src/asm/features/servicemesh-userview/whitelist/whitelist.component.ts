import {
  COMMON_WRITABLE_ACTIONS,
  K8sPermissionService,
  ObservableInput,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils/constants';
import {
  Allowlist,
  MicroServiceStore,
  ServiceListService,
  TabNumberAction,
  Whitelist,
} from '@asm/api/service-list';

import { get } from 'lodash-es';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import {
  catchError,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
  timeout,
} from 'rxjs/operators';

import { WhitelistDialogComponent } from './whitelist-dialog/whitelist-dialog.component';
@Component({
  selector: 'alo-whitelist',
  templateUrl: './whitelist.component.html',
  styleUrls: ['./whitelist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhitelistComponent {
  columns = ['microservice', 'version', 'action'];
  @ObservableInput(true)
  private readonly name$: Observable<string>;

  @Input() name: string;
  params: RouteParams;
  refresh$ = new Subject<void>();
  whitelist: Whitelist;
  params$ = combineLatest([this.route.paramMap, this.name$]).pipe(
    map(([params, name]) => ({
      namespace: params.get('namespace'),
      cluster: params.get('cluster'),
      name: params.get('name'),
      service: name,
    })),
    tap(params => (this.params = params)),
    publishReplay(1),
    refCount(),
  );

  permissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.WHITELIST,
        action: COMMON_WRITABLE_ACTIONS,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly api: ServiceListService,
    private readonly dialog: DialogService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly store: MicroServiceStore,
  ) {}

  fetch = (params: RouteParams) => {
    if (!params || !get(params, 'service')) {
      return of([]);
    }
    return this.api.getWhitelistDetail(params).pipe(
      catchError(() => of(null)),
      tap((result: Whitelist) => {
        this.whitelist = result;
      }),
      map(result => get(result, 'spec.allowlist', [])),
      tap(result => {
        this.store.tabAction(result.length, TabNumberAction.WHITELIST);
      }),
    );
  };

  openWhitelistDialog() {
    const dialogRef = this.dialog.open(WhitelistDialogComponent, {
      data: {
        params: this.params,
        whitelist: this.whitelist,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.refresh$.next();
      }
    });
  }

  deleteWhitelist(result: Allowlist) {
    const messageName = `${result.app}:${result.version || 'all'}`;
    this.dialog
      .confirm({
        title: this.translate.get('delete_whitelist_title', {
          name: messageName,
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        const {
          spec: { allowlist, ...resSpec },
          ...res
        } = this.whitelist;
        const curretnAllowlist = allowlist.filter(
          item => item.app !== result.app || item.version !== result.version,
        );
        const payload = {
          ...res,
          spec: {
            ...resSpec,
            allowlist: curretnAllowlist,
          },
        };
        const delCallback = (result: Whitelist) => {
          this.whitelist = result;
          this.message.success(
            this.translate.get('whitelist_delete_successfully', {
              name: messageName,
            }),
          );
          this.refresh$.next();
        };
        if (!curretnAllowlist.length) {
          this.deleteWhitelistCrd(payload, delCallback);
          return;
        }

        this.api
          .putWhitelist(this.params.cluster, payload)
          .subscribe(result => {
            delCallback(result);
          });
      })
      .catch(() => {});
  }

  // 通过 watch: true， watch crd 资源，等待资源被修改后返回
  // 解决: controller 删除 crd 异步有延迟。
  deleteWhitelistCrd(payload: Whitelist, cb: any) {
    this.api.deleteWhitelist(this.params.cluster, payload).subscribe(() => {
      this.api
        .getWhitelistDetail(this.params)
        .pipe(catchError(() => of(null)))
        .subscribe(result => {
          if (result) {
            const resourceVersion = get(result, 'metadata.resourceVersion');
            this.api
              .getWhitelistDetail({
                ...this.params,
                watch: true,
                resourceVersion,
              })
              .pipe(
                timeout(3000),
                catchError(() => of(null)),
              )
              .subscribe(() => {
                cb();
              });
            return;
          }
          cb();
        });
    });
  }
}
