import { TranslateService } from '@alauda/common-snippet';
import { DialogRef, DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RouteParams } from '@app/typings';
import { RESOURCE_DEFINITIONS } from '@app/utils';
import { ServiceListService } from '@asm/api/service-list/service-list.service';
import { Allowlist, Whitelist } from '@asm/api/service-list/service-list.types';

import { get } from 'lodash-es';
@Component({
  selector: 'alo-whitelist-dialog',
  templateUrl: './whitelist-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhitelistDialogComponent {
  @ViewChild('form')
  form: NgForm;

  submitting = false;
  model: Whitelist = {
    apiVersion: `${RESOURCE_DEFINITIONS.WHITELIST.apiGroup}/${RESOURCE_DEFINITIONS.WHITELIST.apiVersion}`,
    kind: 'WhiteList',
    metadata: {
      name: this.dialogData.params.service,
      namespace: this.dialogData.params.namespace,
      labels: { [`${this.asmBaseDomain}/msname`]: this.dialogData.params.name },
    },
    spec: {
      destmatch: {
        service: this.dialogData.params.service,
        namespace: this.dialogData.params.namespace,
      },
      allowlist: [],
    },
  };

  get allowlist() {
    return this.getAllowlist(this.dialogData.whitelist);
  }

  getAllowlist(data: Whitelist): Allowlist[] {
    return get(data, 'spec.allowlist', []);
  }

  constructor(
    private readonly dialogRef: DialogRef<WhitelistDialogComponent>,
    @Inject(DIALOG_DATA)
    public dialogData: {
      params: RouteParams;
      whitelist: Whitelist;
    },
    private readonly api: ServiceListService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  addWhitelist() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    const { whitelistForm } = this.form.value;
    const request =
      this.allowlist.length > 0
        ? this.api.putWhitelist
        : this.api.createWhitelist;
    request(
      this.dialogData.params.cluster,
      this.transformData(whitelistForm),
    ).subscribe(
      () => {
        this.responseProcessor();
        this.dialogRef.close(true);
        const name = this.getAllowlist(whitelistForm)
          .map(item => `${item.app}:${item.version || 'all'}`)
          .join(',');
        this.message.success(
          this.translate.get('whitelist_create_successfully', { name }),
        );
      },
      () => {
        this.responseProcessor();
      },
    );
  }

  responseProcessor() {
    this.submitting = false;
    this.cdr.markForCheck();
  }

  transformData(whitelistForm: Whitelist) {
    if (this.allowlist.length > 0) {
      const {
        spec: { allowlist, ...resSpec },
        ...res
      } = this.dialogData.whitelist;
      const currentAllowlist = this.getAllowlist(whitelistForm);
      return {
        ...res,
        spec: {
          ...resSpec,
          allowlist: [...allowlist, ...currentAllowlist],
        },
      };
    }
    return whitelistForm;
  }
}
