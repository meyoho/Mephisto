import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RouteParams } from '@app/typings';
import { RESOURCE_DEFINITIONS } from '@app/utils';
import { Allowlist, Whitelist } from '@asm/api/service-list/service-list.types';

import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
@Component({
  selector: 'alo-whitelist-container',
  templateUrl: './whitelist-container.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WhitelistContainerComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input() params: RouteParams;
  @Input() allowlist: Allowlist[];
  @Input() submitted: boolean;
  createForm() {
    return this.fb.group({
      spec: this.fb.group({
        allowlist: this.fb.control([]),
      }),
    });
  }

  getDefaultFormModel(): any {
    return {
      apiVersion: `${RESOURCE_DEFINITIONS.WHITELIST.apiGroup}/${RESOURCE_DEFINITIONS.WHITELIST.apiVersion}`,
      kind: 'WhiteList',
      metadata: {
        labels: { [`${this.asmBaseDomain}/msname`]: '' },
      },
      spec: {
        destmatch: {
          service: '',
          namespace: '',
        },
        allowlist: [],
      },
    };
  }

  constructor(
    public injector: Injector,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {
    super(injector);
  }

  adaptFormModel(resource: Whitelist) {
    const list = get(resource, 'spec.allowlist', []);
    if (list && list.length) {
      const {
        spec: { allowlist, ...resSpec },
        ...res
      } = resource;
      return {
        ...res,
        spec: {
          ...resSpec,
          allowlist: allowlist.map(item => ({
            app: item.app,
            ...(item.version === 'all'
              ? {}
              : {
                  version: item.version,
                }),
          })),
        },
      };
    }
  }

  adaptResourceModel(form: Whitelist) {
    const list = get(form, 'spec.allowlist', []);
    if (list && list.length) {
      const {
        spec: { allowlist, ...resSpec },
        ...res
      } = form;
      return {
        ...res,
        spec: {
          ...resSpec,
          allowlist: allowlist.map(item => ({
            app: item.app,
            version: item.version ? item.version : 'all',
          })),
        },
      };
    }
    return { ...form };
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
