import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { RouteParams } from '@app/typings';
import {
  Allowlist,
  DeploymentItem,
  getDeployments,
  MicroService,
  ServiceListService,
} from '@asm/api/service-list';

import { get } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { map } from 'rxjs/operators';
@Component({
  selector: 'alo-allowlist-table',
  templateUrl: './allowlist-table.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AllowlistTableComponent extends BaseResourceFormArrayComponent
  implements OnInit {
  @Input() params: RouteParams;
  @Input() allowlist: Allowlist[];
  @Input() submitted: boolean;
  microserviceMap: Map<
    string,
    {
      deployments: DeploymentItem[];
    }
  > = new Map();

  constructor(
    injector: Injector,
    private readonly api: ServiceListService,
    private readonly k8sUtil: K8sUtilService,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  get allowlistMap(): Map<string, string[]> {
    const list = this.allowlist.reduce((prev, curr) => {
      const existiApp = prev.find(item => item.app === curr.app);
      return [
        ...prev,
        {
          app: curr.app,
          versions: [curr.version || 'all', ...get(existiApp, 'versions', [])],
        },
      ];
    }, []);
    return new Map(list.map(item => [item.app, item.versions]));
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel() {
    return [
      {
        app: '',
        version: '',
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      app: this.fb.control('', [
        Validators.required,
        this.microserviceConflictValidator,
      ]),
      version: this.fb.control('', [
        Validators.required,
        this.versionConflictValidator,
      ]),
    });
  }

  microserviceConflictValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (control && control.parent) {
      const index = this.form.controls.indexOf(control.parent);
      const previousKeys = this.getPreviousKeys(index) as Allowlist[];
      const current: string = control.value;
      const microserviceConflict = previousKeys.some(item => {
        return item.app === current && item.version === 'all';
      });
      return microserviceConflict ? { microserviceConflict } : null;
    }
  };

  private getPreviousKeys(index: number) {
    return index ? this.formModel.slice(0, index) : this.formModel.slice(1);
  }

  versionConflictValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (control && control.parent) {
      const index = this.form.controls.indexOf(control.parent);
      const previousKeys = this.getPreviousKeys(index) as Allowlist[];
      const current: string = control.value;
      const parent: Allowlist = control.parent.value;
      const versionConflict = previousKeys.some(item => {
        return (
          item.app === parent.app &&
          ([item.version, current].includes('all') || item.version === current)
        );
      });
      // 版本冲突 = 已添加白名单 && 当前版本为all
      if (this.allowlistMap.get(parent.app) && current === 'all') {
        return { versionConflict: true };
      }
      return versionConflict ? { versionConflict } : null;
    }
  };

  rowBackgroundColorFn(row: FormControl) {
    return row.invalid && row.touched ? '#fdefef' : '';
  }

  ngOnInit() {
    this.getMicroservices();
  }

  getMicroservices() {
    this.api
      .getList(this.params)
      .pipe(
        map(
          (result: { items: MicroService[] }) =>
            new Map(
              result.items.map(item => [
                this.k8sUtil.getName(item),
                {
                  deployments: getDeployments(item),
                },
              ]),
            ),
        ),
      )
      .subscribe(result => {
        this.microserviceMap = result;
        this.cdr.markForCheck();
      });
  }

  transitionArray = (map: Map<string, Record<string, any>>) => {
    return Array.from(map.keys());
  };

  allowListExisted = (value: string) => {
    const version = this.allowlistMap.get(value);
    return version ? version.includes('all') : version;
  };

  appChanged(control: AbstractControl) {
    if (control.get('app')) {
      control.get('version').setValue('');
      if (control.get('app').getError('microserviceConflict')) {
        control.get('version').disable();
      } else {
        control.get('version').enable();
      }
    }
  }

  tableVerificationMessage(control: AbstractControl) {
    const app = control.get('app');
    const version = control.get('version');
    const getTranslate = (name: string, params?: any): string =>
      this.translate.get(name, params);
    return [
      {
        disabled: (app.touched || this.submitted) && app.hasError('required'),
        tip: getTranslate('not_null_tip', {
          name: getTranslate('nav_microservice'),
        }),
      },
      {
        disabled:
          (version.touched || this.submitted) && version.hasError('required'),
        tip: getTranslate('not_null_tip', { name: getTranslate('version') }),
      },
      {
        disabled: app.touched && app.hasError('microserviceConflict'),
        tip: getTranslate('microservice_conflict_tip'),
      },
      {
        disabled: version.touched && version.hasError('versionConflict'),
        tip: getTranslate('version_conflict_tip'),
      },
    ].filter(item => item.disabled);
  }
}
