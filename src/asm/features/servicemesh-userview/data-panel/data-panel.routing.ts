import { RouterModule, Routes } from '@angular/router';

import { DataPanelComponent } from './data-panel.component';
const routes: Routes = [
  {
    path: '',
    component: DataPanelComponent,
  },
];
export const DataPanelRoutes = RouterModule.forChild(routes);
