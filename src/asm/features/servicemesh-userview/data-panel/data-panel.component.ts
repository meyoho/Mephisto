import { DialogService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { getRequestStat } from '@app/shared/components/bar-status/util';
import {
  calculateStep,
  getChartDefaultColor,
  handleChartData,
  TimeParams,
} from '@app/shared/components/chart';
import {
  CalendarData,
  DateBroadcastValue,
  getTimeValueOf,
} from '@app/shared/components/range-picker';
import { RouteParams } from '@app/typings';
import { toFixed } from '@app/utils/unit';
import { DataPanelService } from '@asm/api/data-panel/data-panel.service';
import {
  ChartModel,
  ChartType,
  Instances,
  Workloads,
} from '@asm/api/data-panel/data-panel.types';
import {
  MetircsResponseBase,
  RequestStat,
} from '@asm/api/service-topology/service-topology-api.types';

import { get } from 'lodash-es';
import { BehaviorSubject, combineLatest, of, Subject } from 'rxjs';
import {
  catchError,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { RequestPathDialogComponent } from './request-path-dialog/request-path-dialog.component';
import { inflowOptions, outflowOptions, responseTimeOptions } from './utils';
const DEFFAULTPATH = '/*';
@Component({
  selector: 'alo-data-panel',
  templateUrl: './data-panel.component.html',
  styleUrls: ['./data-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataPanelComponent implements OnInit, OnDestroy {
  tabSelectedIndex = 0;
  getChartDefaultColor = getChartDefaultColor;
  labelName = '';
  chartModel: ChartModel = {
    response_time: {
      options: responseTimeOptions,
      data: [],
      avg: 0,
      avgTitle: 'average_response_time',
      title: 'response_time',
      unit: 'ms',
    },
    rps_in: {
      options: inflowOptions,
      data: [],
      avg: 0,
      avgTitle: 'average_incoming_RPS',
      title: 'incoming_rps',
      unit: 'rps_unit',
    },
    rps_out: {
      options: outflowOptions,
      data: [],
      avg: 0,
      avgTitle: 'average_outgoing_RPS',
      title: 'outgoing_rps',
      unit: 'rps_unit',
    },
  };

  apiChartModel: ChartModel = {
    response_time: {
      options: responseTimeOptions,
      data: [],
      avg: 0,
      avgTitle: 'average_response_time',
      title: 'response_time',
      unit: 'ms',
    },
    rps_in: {
      options: inflowOptions,
      data: [],
      avg: 0,
      avgTitle: 'average_rps',
      title: 'rps',
      unit: 'rps_unit',
    },
  };

  calendarData: CalendarData;
  range: string;
  search$ = new Subject<void>();
  time: [number, number];
  onDestroy$ = new Subject<void>();
  triggerSelect$ = new Subject();
  workloads: Workloads[] = [];
  method: string;
  paths: string[] = [];
  path: string = DEFFAULTPATH;
  defaultVal = true;
  model = {
    workload: '',
    instance: '',
  };

  timeParams: TimeParams = {
    name: '',
    start: 0,
    end: 0,
    step: 0,
  };

  inRequestState: RequestStat[];
  outRequestState: RequestStat[];
  params: RouteParams;
  refreshPath$ = new BehaviorSubject(null);
  fetchMethod$ = new BehaviorSubject(null);
  refreshWorkload$ = new BehaviorSubject(null);
  switchWorkload$ = new Subject();
  defaultPod: string;
  params$ = this.route.paramMap.pipe(
    map(params => {
      return {
        project: params.get('project'),
        namespace: params.get('namespace'),
        cluster: params.get('cluster'),
        workload: params.get('workload'),
        start: params.get('start'),
        end: params.get('end'),
        step: params.get('step'),
        range: params.get('range'),
      };
    }),
    tap(params => {
      this.params = params;
    }),
    publishReplay(1),
    refCount(),
  );

  workload$ = combineLatest([this.params$, this.refreshWorkload$]).pipe(
    filter(() => !!this.time),
    switchMap(([params]) => {
      return this.dataPanelService.getWorkload({
        ...params,
        start: this.time[0],
        end: this.time[1],
      });
    }),
    tap(res => {
      this.workloads = res;
      if (this.params.workload && this.defaultVal) {
        this.model.workload = this.params.workload;
        this.calendarData.start = this.params.start;
        this.calendarData.end = this.params.end;
        this.calendarData.step = +this.params.step;
        this.range = this.params.range;
        this.time = [
          +getTimeValueOf(this.params.start),
          +getTimeValueOf(this.params.end),
        ];
        this.triggerSelect();
        this.defaultVal = false;
      } else {
        this.setWorkload(res);
        this.switchWorkload$.next(this.model.workload);
      }
      this.cdr.markForCheck();
    }),
    publishReplay(1),
    refCount(),
  );

  instances$ = this.switchWorkload$.pipe(
    switchMap(workload =>
      this.dataPanelService
        .getPods({
          ...this.params,
          start: this.time[0],
          end: this.time[1],
          workload,
          instance: this.tabSelectedIndex === 1,
        })
        .pipe(catchError(() => of([]))),
    ),
    tap(res => {
      this.setInstanceVal(res);
      this.search();
    }),
    publishReplay(1),
    refCount(),
  );

  clients$ = this.switchWorkload$.pipe(
    switchMap(workload =>
      this.dataPanelService
        .getClients({
          ...this.params,
          start: this.time[0],
          end: this.time[1],
          workload,
        })
        .pipe(catchError(() => of([]))),
    ),
    publishReplay(1),
    refCount(),
  );

  paths$ = combineLatest([this.workload$, this.refreshPath$]).pipe(
    switchMap(() => {
      return this.dataPanelService
        .getPath({
          ...this.params,
          workload: this.model.workload,
        })
        .pipe(catchError(() => of({})));
    }),
    map(result => get(result, 'paths', [])),
    tap((result: string[]) => {
      const path = result.includes(this.path) ? this.path : DEFFAULTPATH;
      this.path = path;
      this.paths = result;
      this.cdr.markForCheck();
    }),
    publishReplay(1),
    refCount(),
  );

  methods$ = combineLatest([
    this.workload$,
    this.paths$,
    this.fetchMethod$,
  ]).pipe(
    switchMap(() => {
      return this.dataPanelService
        .getMethods({
          ...this.params,
          workload: this.model.workload,
          start: this.time[0],
          end: this.time[1],
        })
        .pipe(
          map((res: string[]) => {
            return res.sort();
          }),
          catchError(() => of([])),
          tap((res: string[]) => {
            const method = res.includes(this.method) ? this.method : res[0];
            this.method = method;
            this.search();
          }),
        );
    }),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly dataPanelService: DataPanelService,
    private readonly cdr: ChangeDetectorRef,
    private readonly notification: NotificationService,
    private readonly dialogService: DialogService,
  ) {}

  allowRequest() {
    switch (this.tabSelectedIndex) {
      case 0:
        return this.model.workload;
      case 1:
        return this.model.instance;
      case 2:
        return this.path && this.method;
      default:
        return true;
    }
  }

  ngOnInit() {
    combineLatest([this.params$, this.search$])
      .pipe(
        takeUntil(this.onDestroy$),
        filter(() => {
          if (this.allowRequest()) {
            return true;
          }
          this.initData();
          return false;
        }),
        switchMap(([params]) => {
          const step = calculateStep(this.time[0], this.time[1]);
          this.timeParams = {
            ...params,
            name:
              this.tabSelectedIndex === 1
                ? this.model.instance
                : this.model.workload,
            start: this.time[0],
            end: this.time[1],
            step,
            method: this.method,
            instances: this.model.instance,
            path: this.path,
          };
          this.calendarData.step = step;
          this.cdr.markForCheck();
          const requestUrl = this.getRequestUrl();
          return requestUrl({
            ...this.timeParams,
            step: step * 60,
          }).pipe(
            catchError(error => {
              if (error && error.message) {
                this.notification.error(error.message);
              }
              return of(null);
            }),
          );
        }),
      )
      .subscribe((res: MetircsResponseBase) => {
        const data =
          this.tabSelectedIndex === 1 ? get(res, this.model.instance) : res;
        if (data) {
          this.inRequestState = getRequestStat(data.requests_total_in);
          if (this.tabSelectedIndex !== 2) {
            this.outRequestState = getRequestStat(data.requests_total_out);
          }
          this.setChartData(data);
        } else {
          this.initData();
        }
        this.cdr.markForCheck();
      });
  }

  getRequestUrl() {
    switch (this.tabSelectedIndex) {
      case 0:
        return this.dataPanelService.getMetricWorkload;
      case 1:
        return this.dataPanelService.getInstanceList;
      case 2:
        return this.dataPanelService.getApiMetrics;
      default:
        break;
    }
  }

  setChartData(res: MetircsResponseBase) {
    Object.keys(this.viewModel).forEach((key: ChartType) => {
      const value = this.viewModel[key];
      const data = get(res, key, {});
      if (value.options) {
        value.data = handleChartData(data, value.options, this.timeParams);
      }
      this.setAvg(key, res, this.viewModel);
    });
    this.cdr.markForCheck();
  }

  setAvg(key: ChartType, res: MetircsResponseBase, model: ChartModel) {
    switch (key) {
      case ChartType.INFLOW:
        model[key].avg = toFixed(res.avg_rps_in);
        break;
      case ChartType.OUTFLOW:
        model[key].avg = toFixed(res.avg_rps_out);
        break;
      case ChartType.RESPONSETIME:
        model[key].avg = toFixed(res.avg_response_time || 0);
        break;
      default:
        break;
    }
  }

  initData() {
    Object.keys(this.viewModel).forEach(item => {
      if (get(this.viewModel[item], 'options')) {
        this.viewModel[item].data = [];
        this.viewModel[item].avg = 0;
      }
    });
    this.inRequestState = [];
    this.outRequestState = [];
    this.cdr.markForCheck();
  }

  search() {
    this.search$.next();
  }

  triggerSelect() {
    if (this.tabSelectedIndex === 2) {
      this.refreshPath$.next(null);
    }
    this.switchWorkload$.next(this.model.workload);
    this.defaultPod = '';
  }

  tabSelected() {
    if (this.tabSelectedIndex === 2) {
      return;
    }
    this.switchWorkload$.next(this.model.workload);
    this.cdr.markForCheck();
  }

  get viewModel() {
    return this.tabSelectedIndex === 2 ? this.apiChartModel : this.chartModel;
  }

  get chartModelKey() {
    return Object.keys(this.viewModel);
  }

  broadcastTime(event: DateBroadcastValue) {
    this.calendarData = event.date;
    this.time = [event.dateValueOf.start, event.dateValueOf.end];
    this.refreshWorkload$.next(null);
    this.cdr.markForCheck();
  }

  openRequestPathDialog() {
    const dialogRef = this.dialogService.open(RequestPathDialogComponent, {
      data: {
        paths: this.paths,
        params: this.params,
        name: this.model.workload,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (Array.isArray(result)) {
        this.refreshPath$.next(null);
      }
    });
  }

  skipPodTab(pod: string) {
    if (pod) {
      this.defaultPod = pod;
      this.tabSelectedIndex = 1;
    }
  }

  setWorkload(workloads: Workloads[]) {
    const existing = workloads.some(item => item.name === this.model.workload);
    const workload = existing ? this.model.workload : get(workloads[0], 'name');
    this.model.workload = workload;
  }

  setInstanceVal(instance: Instances[]) {
    if (
      this.defaultPod &&
      instance.some(item => item.name === this.defaultPod)
    ) {
      this.model.instance = this.defaultPod;
      return;
    }
    if (instance.some(item => this.model.instance === item.name)) {
      return;
    }
    this.model.instance = get(instance[0], 'name');
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
