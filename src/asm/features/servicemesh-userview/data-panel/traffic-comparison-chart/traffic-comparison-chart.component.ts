import { ObservableInput, TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { Router } from '@angular/router';
import {
  getChartDefaultColor,
  handleChartData,
  optionsBuilder,
} from '@app/shared/components/chart';
import { RouteParams } from '@app/typings';
import { DataPanelService } from '@asm/api/data-panel/data-panel.service';
import {
  ChartModel,
  Instances,
  Workloads,
} from '@asm/api/data-panel/data-panel.types';
import { MetircsResponseBase } from '@asm/api/service-topology/service-topology-api.types';

import { get } from 'lodash-es';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import {
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';
@Component({
  selector: 'alo-traffic-comparison-chart',
  templateUrl: './traffic-comparison-chart.component.html',
  styleUrls: ['./traffic-comparison-chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrafficComparisonChartComponent implements OnDestroy {
  @Input() tabSelectedIndex: number;
  @Input() params: RouteParams;
  @Input() step: number;
  @ObservableInput(true)
  private readonly instances$: Observable<Instances[]>;

  @Input() instances: Instances[];

  @Input() sidecar = false;

  @Input() workload = '';

  @Input() workloads: Workloads[];

  @Output() skipPodTab = new EventEmitter();

  @ObservableInput(true)
  private readonly time$: Observable<[[number, number]]>;

  @Input() time: [number, number];
  search$ = new BehaviorSubject(null);
  onDestroy$ = new Subject<void>();
  _instances: string[] = [];
  legendValueMap: Set<string> = new Set();
  legendValue: { name: string };
  disabledLegend: string[];
  getChartDefaultColor = getChartDefaultColor;
  remake = false;
  comparisonModel: ChartModel = {
    response_time: {
      options: [],
      data: [],
      title: 'response_time',
      unit: 'ms',
      indicator: 'avg',
      time: [0, 0],
      indicators: [
        { name: 'average', value: 'avg' },
        { name: 'tp_50', value: '0.5' },
        { name: 'tp_95', value: '0.95' },
        { name: 'tp_99', value: '0.99' },
      ],
    },
    rps_in: {
      options: [],
      data: [],
      title: 'incoming_rps',
      unit: this.translate.get('rps_unit'),
      indicator: 'rps',
      time: [0, 0],
      indicators: [
        { name: 'rps', value: 'rps' },
        { name: 'rps_error', value: 'rps_err' },
      ],
    },
    rps_err_ratio: {
      options: [],
      data: [],
      title: 'incoming_rps_err_ratio',
      indicator: 'rps_err_ratio',
      time: [0, 0],
      indicators: [{ name: 'error_rate', value: 'rps_err_ratio' }],
    },
  };

  pods$ = this.instances$.pipe(
    filter(instances => {
      return !!instances;
    }),
    map(res => {
      const instances = res.map(item => {
        return item.name;
      });
      this._instances = instances;
      this.modelKey.forEach(key => {
        this.comparisonModel[key].options = optionsBuilder(instances);
      });
      this.cdr.markForCheck();
      return res;
    }),
  );

  chartData$ = combineLatest([
    this.time$.pipe(tap(() => (this.remake = true))),
    this.pods$,
    this.search$,
  ]).pipe(
    filter(([time]) => !!time && this._instances.length > 0),
    switchMap(() => {
      return this.sidecar
        ? this.api.getClientMetrics({
            ...this.params,
            name: this.workload,
            start: this.time[0],
            end: this.time[1],
            step: this.step * 60,
            sourceWorkloads: this._instances.join(','),
          })
        : this.api.getInstanceList({
            ...this.params,
            start: this.time[0],
            end: this.time[1],
            step: this.step * 60,
            quantiles: this.comparisonModel.response_time.indicator,
            instances: this._instances.join(','),
            metrics: ['response_time', 'rps_in'].join(','),
            ...(this.sidecar ? { sidecar: true } : {}),
          });
    }),
    map((result: { [key: string]: MetircsResponseBase }) => {
      const indicators =
        this.search$.getValue() && !this.remake
          ? [this.search$.getValue()]
          : this.modelKey;
      const res = this.handleChartData(indicators, result);
      this.comparisonModel = {
        ...this.comparisonModel,
        ...res,
      };
      return this.comparisonModel;
    }),
    takeUntil(this.onDestroy$),
    startWith(this.comparisonModel),
    publishReplay(1),
    refCount(),
  );

  get modelKey() {
    return Object.keys(this.comparisonModel);
  }

  constructor(
    private readonly translate: TranslateService,
    private readonly api: DataPanelService,
    private readonly cdr: ChangeDetectorRef,
    private readonly router: Router,
  ) {}

  handleChartData(
    indicators: string[],
    result: { [key: string]: MetircsResponseBase },
  ) {
    return indicators.reduce((prev, key) => {
      const options = this.comparisonModel[key].options;
      if (options.length > 0) {
        const value = options.reduce((prev, curr) => {
          const workloadMetricData = get(
            get(result, curr.field),
            `${key === 'rps_err_ratio' ? 'rps_in' : key}[${
              this.comparisonModel[key].indicator
            }]`,
          );
          const chartData = {
            [curr.field]:
              key === 'rps_err_ratio'
                ? workloadMetricData.map((item: number[]) => [
                    item[0],
                    item[1] * 100,
                  ])
                : workloadMetricData,
          };
          return { ...prev, ...chartData };
        }, {});
        const params = {
          start: this.time[0],
          end: this.time[1],
          step: this.step,
        };
        const data = handleChartData(
          value,
          this.comparisonModel[key].options,
          params,
          this.comparisonModel[key].indicator,
        );
        const model = {
          ...this.comparisonModel[key],
          data,
          time: this.time,
        };
        this.comparisonModel[key] = model;
        return {
          ...prev,
          [key]: model,
        };
      }
      return prev;
    }, {});
  }

  switchLegend(name: string) {
    this.legendValue = Object.assign({}, { name });
    if (this.legendValueMap.has(name)) {
      this.legendValueMap.delete(name);
      this.disabledLegend = [...this.legendValueMap];
      return;
    }
    this.legendValueMap.add(name);
    this.disabledLegend = [...this.legendValueMap];
  }

  switchIndicator(indicator: string) {
    this.remake = false;
    if (indicator.includes('rps')) {
      this.comparisonModel.rps_in.indicator = indicator;
      this.search$.next('rps_in');
      return;
    }
    this.comparisonModel.response_time.indicator = indicator;
    this.search$.next('response_time');
  }

  podDetail(instance: Instances) {
    if (this.sidecar) {
      const selectedWorkload = this.workloads.filter(
        item => item.name === this.workload,
      )[0];
      const app = (instance.apps || []).length > 0 ? instance.apps[0] : '';
      this.router.navigate(
        [
          '/workspace',
          this.params.project,
          'clusters',
          this.params.cluster,
          'namespaces',
          this.params.namespace,
          'servicemesh',
          'jaeger',
        ],
        {
          queryParams: {
            selectedService: app || instance.name,
            start: this.time[0],
            end: this.time[1],
            lookback: 'custom',
            ...(selectedWorkload.services?.length > 0
              ? { workloadService: selectedWorkload.services[0] }
              : {}),
          },
        },
      );
    } else {
      this.skipPodTab.next(instance.name);
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
