import { DialogRef, DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { RouteParams } from '@app/typings';
import { DataPanelService } from '@asm/api/data-panel/data-panel.service';

import { get } from 'lodash-es';
interface Path {
  paths: Array<{ path: string }>;
}
@Component({
  selector: 'alo-request-path-dialog',
  templateUrl: './request-path-dialog.component.html',
  styleUrls: ['./request-path-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RequestPathDialogComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  submitting = false;

  pathModel: Path = {
    paths: [],
  };

  constructor(
    private readonly dialogRef: DialogRef<RequestPathDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      paths: string[];
      params: RouteParams;
      name: string;
    },
    private readonly api: DataPanelService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    if (this.data) {
      this.pathModel.paths = this.data.paths.map(item => ({ path: item }));
    }
  }

  addPath() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    const { pathForm } = this.form.value;
    const value = get(pathForm, 'paths', []).map(
      (item: { path: string }) => item.path,
    );
    const { params, name } = this.data;
    this.api.setPath({ ...params, name }, value).subscribe(
      () => {
        this.submitting = false;
        this.cdr.markForCheck();
        this.dialogRef.close(value);
      },
      () => {
        this.submitting = false;
        this.cdr.markForCheck();
      },
    );
  }
}
