import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
// 如果只使用 arrayfrom 不套一层form group 会导致submitt 无法响应子组件
@Component({
  selector: 'alo-path-form-group',
  template: `
    <form
      auiForm
      [formGroup]="form"
      class="routing-rule"
      auiFormLabelWidth="150px"
      [auiFormEmptyAddon]="true"
    >
      <alo-path-form
        [formControl]="form.get('paths')"
        [submitted]="submitted"
      ></alo-path-form>
    </form>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PathFormGroupComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input() submitted: boolean;
  createForm() {
    return this.fb.group({
      paths: this.fb.control(''),
    });
  }

  getDefaultFormModel() {
    return {
      path: this.fb.control([]),
    };
  }

  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
