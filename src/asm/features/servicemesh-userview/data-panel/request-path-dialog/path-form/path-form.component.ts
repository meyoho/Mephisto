import { Component, Injector, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { WORKLOAD_PATH_PATTERN } from '@app/utils/patterns';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
@Component({
  selector: 'alo-path-form',
  templateUrl: './path-form.component.html',
})
export class PathFormComponent extends BaseResourceFormArrayComponent
  implements OnInit {
  @Input() submitted: boolean;
  pathPattern = WORKLOAD_PATH_PATTERN;
  constructor(public injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  rowBackgroundColorFn(row: FormControl) {
    return row.invalid && row.touched ? '#fdefef' : '';
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel() {
    return [
      {
        path: '',
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      path: this.fb.control('', [
        Validators.required,
        Validators.pattern(this.pathPattern.pattern),
        this.pathConflictValidator,
      ]),
    });
  }

  private getPreviousKeys(index: number) {
    return index ? this.formModel.slice(0, index) : this.formModel.slice(1);
  }

  pathConflictValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (control && control.parent) {
      const index = this.form.controls.indexOf(control.parent);
      const previousKeys = this.getPreviousKeys(index);
      const conflict = previousKeys.some(item => item.path === control.value);
      return conflict ? { pathConflict: true } : null;
    }
    return null;
  };
}
