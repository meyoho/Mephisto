import { ChartOptions } from '@app/shared/components/chart';
const CIRCUIT = 'basic:circuit';
export const inflowOptions: ChartOptions[] = [
  {
    field: 'rps',
    title: 'rps',
    stroke: { start: '#9d8ce1', stop: '#F9F6FD' },
    lineType: 'area',
  },
  {
    field: 'rps_err',
    title: 'rps_error',
    icon: CIRCUIT,
    stroke: { start: '#eb6262', stop: '#fff', opacity: 0 },
    lineType: 'line',
  },
];

export const flowOptions: ChartOptions[] = [
  {
    field: 'rps',
    title: 'rps',
    stroke: { start: '#009CE3', stop: '#E5F5FC' },
    lineType: 'area',
  },
  {
    field: 'rps_err',
    title: 'rps_error',
    icon: CIRCUIT,
    stroke: { start: '#eb6262', stop: '#fff', opacity: 0 },
    lineType: 'line',
  },
];

export const outflowOptions: ChartOptions[] = [
  {
    field: 'rps',
    title: 'rps',
    stroke: { start: '#009CE3', stop: '#E5F5FC' },
    lineType: 'area',
  },
  {
    field: 'rps_err',
    title: 'rps_error',
    icon: CIRCUIT,
    stroke: { start: '#eb6262', stop: '#fff', opacity: 0 },
    lineType: 'line',
  },
];

export const responseTimeOptions: ChartOptions[] = [
  {
    field: '0.99',
    title: 'tp_99',
    stroke: { value: '#6e91a2', start: '#DFE7EA', stop: '#E6ECEF' },
    lineType: 'area',
  },
  {
    field: '0.95',
    title: 'tp_95',
    stroke: { value: '#7da41b', start: '#E4ECD1', stop: '#E8EED5' },
    lineType: 'area',
  },
  {
    field: '0.5',
    title: 'tp_50',
    stroke: { value: '#158f75', start: '#D0E8E3', stop: '#D9ECE8' },
    lineType: 'area',
  },
  {
    field: 'avg',
    title: 'average',
    stroke: { value: '#c68946', start: '#F3E7DA', stop: '#F5EBDF' },
    lineType: 'area',
  },
];
