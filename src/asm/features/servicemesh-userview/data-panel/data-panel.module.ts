import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FeatureSharedCommonModule } from '@app/features-shared/common';
import { SharedModule } from '@app/shared';

import { DataPanelComponent } from './data-panel.component';
import { DataPanelRoutes } from './data-panel.routing';
import { PathFormGroupComponent } from './request-path-dialog/path-form/path-form-group.component';
import { PathFormComponent } from './request-path-dialog/path-form/path-form.component';
import { RequestPathDialogComponent } from './request-path-dialog/request-path-dialog.component';
import { TrafficComparisonChartComponent } from './traffic-comparison-chart/traffic-comparison-chart.component';

@NgModule({
  imports: [
    CommonModule,
    DataPanelRoutes,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    FeatureSharedCommonModule,
  ],
  declarations: [
    DataPanelComponent,
    TrafficComparisonChartComponent,
    RequestPathDialogComponent,
    PathFormComponent,
    PathFormGroupComponent,
  ],
  entryComponents: [RequestPathDialogComponent],
})
export class DataPanelModule {}
