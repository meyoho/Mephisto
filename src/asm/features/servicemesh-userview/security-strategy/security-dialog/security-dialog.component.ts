import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { matchLabelsToString } from '@app/utils/resource-list';
import { RouteManagementService } from '@asm/api/route-management';
import { SecurityStrategyService } from '@asm/api/security-strategy/security-strategy.service';
import {
  DEFAULT_MODE,
  PolicyResponse,
  PolicyTypeMeta,
  STRICT_MODE,
} from '@asm/api/security-strategy/security-strategy.types';

import { get, set } from 'lodash-es';
import { of } from 'rxjs';
import { publishReplay, refCount, switchMap } from 'rxjs/operators';
interface PolicyModel {
  name: string;
  mode: string;
}
@Component({
  selector: 'alo-security-dialog',
  templateUrl: './security-dialog.component.html',
  styleUrls: ['./security-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecurityDialogComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  model: PolicyResponse = this.defaultModel;
  viewModel: PolicyModel = {
    name: '',
    mode: DEFAULT_MODE,
  };

  loading = false;
  get dialogText() {
    return !this.data.isUpdate
      ? {
          title: 'create_safety_rules',
          confirm: 'create',
        }
      : {
          title: 'update_safety_rules',
          confirm: 'update',
        };
  }

  get defaultModel() {
    return {
      apiVersion: PolicyTypeMeta.apiVersion,
      kind: PolicyTypeMeta.kind,
      metadata: {
        name: '',
        namespace: this.data.namespace,
      },
      spec: {
        peers: [
          {
            mtls: {
              mode: DEFAULT_MODE,
            },
          },
        ],
        targets: [
          {
            name: '',
          },
        ],
      },
    };
  }

  services$ = of(this.data.namespace).pipe(
    switchMap(namespace =>
      this.routeApi.getDestinationruleList(this.data.cluster, namespace),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly dialogRef: DialogRef<SecurityDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      isUpdate: boolean;
      hideName: boolean;
      name: string;
      project: string;
      namespace: string;
      services: string;
      cluster: string;
    },
    private readonly apiService: SecurityStrategyService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly routeApi: RouteManagementService,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  ngOnInit() {
    this.getDetail();
  }

  getDetail() {
    const { namespace, isUpdate, cluster, name } = this.data;
    this.viewModel.name = name;
    if (isUpdate) {
      this.loading = true;
      this.apiService
        .getPolicyList({
          cluster,
          namespace,
          labelSelector: matchLabelsToString({
            [`${this.asmBaseDomain}/hostname`]: name,
          }),
        })
        .subscribe(result => {
          const data = result.items[0] as PolicyResponse;
          this.viewModel.mode =
            get(data, 'spec.peers[0].mtls.mode') || STRICT_MODE;
          this.model = data;
          this.loading = false;
          this.cdr.markForCheck();
        });
    } else {
      this.model = this.defaultModel;
    }
  }

  save() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    const { cluster } = this.data;
    this.setModel(this.viewModel);
    if (!this.data.isUpdate) {
      this.apiService.create(cluster, this.model).subscribe(
        result => {
          this.resultProcessor(result, 'created');
        },
        () => {
          this.errorProcessor();
        },
      );
    } else {
      this.apiService.update(cluster, this.model).subscribe(
        result => {
          this.resultProcessor(result, 'updated');
        },
        () => {
          this.errorProcessor();
        },
      );
    }
  }

  resultProcessor(result: PolicyResponse, type: string) {
    this.loading = false;
    this.message.success({
      content: this.translate.get(`security_${type}_message_success`, {
        name: get(result, 'spec.targets[0].name'),
      }),
    });
    this.dialogRef.close(true);
    this.cdr.markForCheck();
  }

  errorProcessor() {
    this.loading = false;
    this.cdr.markForCheck();
  }

  setModel({ name, mode }: PolicyModel) {
    set(this.model, 'spec.targets[0].name', name);
    set(this.model, 'metadata.name', `${name}`);
    set(this.model, 'metadata.labels', {
      [`${this.asmBaseDomain}/hostname`]: name,
    });
    set(this.model, 'spec.peers[0].mtls.mode', mode);
  }
}
