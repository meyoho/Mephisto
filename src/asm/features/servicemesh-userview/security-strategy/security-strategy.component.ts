import {
  COMMON_WRITABLE_ACTIONS,
  K8sPermissionService,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import { SecurityStrategyService } from '@asm/api/security-strategy/security-strategy.service';
import {
  PolicyMtls,
  PolicyResponse,
  PolicySpec,
} from '@asm/api/security-strategy/security-strategy.types';

import { get, isEqual } from 'lodash-es';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import { matchLabelsToString, ResourceList } from 'app/utils/resource-list';
import { securityPolicyRuleProcess } from './../../../utils/common';
import { SecurityDialogComponent } from './security-dialog/security-dialog.component';
@Component({
  selector: 'alo-security-strategy',
  templateUrl: './security-strategy.component.html',
  styleUrls: ['./security-strategy.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecurityStrategyComponent {
  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly apiService: SecurityStrategyService,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sPermission: K8sPermissionService,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {
    this.list = new ResourceList({
      fetchParams$: this.fetchParams$,
      fetcher: this.fetch.bind(this),
      limit: 20,
    });
  }

  columns = ['name', 'rule', 'create_by', 'creationTimestamp', 'action'];
  project: string;
  isUpdate = false;
  notificationVisible = true;
  services: string[] = [];
  cluster: string;
  namespace: string;
  list: ResourceList;
  params$ = combineLatest([this.route.paramMap, this.route.queryParamMap]).pipe(
    map(([params, queryParams]) => {
      return {
        keywords: queryParams.get('keywords') || '',
        namespace: params.get('namespace'),
        cluster: params.get('cluster') || '',
      };
    }),
    tap(params => {
      this.cluster = params.cluster;
      this.namespace = params.namespace;
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishReplay(1),
    refCount(),
  );

  permissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.POLOCY,
        action: COMMON_WRITABLE_ACTIONS,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  fetchParams$ = this.params$.pipe(
    map(params => ({
      namespace: params.namespace,
      cluster: params.cluster,
      labelSelector: matchLabelsToString({
        [`${this.asmBaseDomain}/hostname`]: params.keywords,
      }),
    })),
  );

  getService(data: PolicySpec) {
    const target = data.targets.map(item => get(item, 'name')) || ['-'];
    return target[0];
  }

  fetch = (params: RouteParams) => {
    return this.apiService.getPolicyList(params).pipe(
      tap(result => {
        this.services = result.items.map((item: PolicyResponse) =>
          this.getService(item.spec),
        );
      }),
    );
  };

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords, page: 1 },
      queryParamsHandling: 'merge',
    });
  }

  create() {
    this.isUpdate = false;
    const dialogRef = this.dialog.open(SecurityDialogComponent, {
      data: {
        namespace: this.namespace,
        isUpdate: this.isUpdate,
        services: this.services,
        cluster: this.cluster,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.reload(result);
    });
  }

  update(data: PolicyResponse) {
    this.isUpdate = true;
    const dialogRef = this.dialog.open(SecurityDialogComponent, {
      data: {
        namespace: this.namespace,
        isUpdate: this.isUpdate,
        name: this.getService(data.spec),
        cluster: this.cluster,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      this.reload(result);
    });
  }

  reload(result: boolean) {
    if (result) {
      this.list.reload();
    }
  }

  delete(data: PolicyResponse) {
    const service = this.getService(data.spec);
    this.dialog
      .confirm({
        title: this.translate.get('safety_rules_delete_confirm_title', {
          name: service,
        }),
        content: this.translate.get('safety_rules_delete_confirm_body'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.apiService.delete(this.cluster, data).subscribe(
          () => {
            this.message.success({
              content: this.translate.get('security_deleted_message_success', {
                name: service,
              }),
            });
            this.list.reload();
          },
          () => {},
        );
      })
      .catch(() => {});
  }

  getRuleText(data: PolicyMtls[] = []) {
    return securityPolicyRuleProcess(data);
  }

  dispalyNotification() {
    this.notificationVisible = true;
    this.cdr.markForCheck();
  }
}
