import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OverviewService } from '@asm/api/overview/overview.service';
import { ResourcesView } from '@asm/api/overview/overview.types';

import { get } from 'lodash-es';
import { Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
} from 'rxjs/operators';
@Component({
  selector: 'alo-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OverviewComponent implements OnInit, OnDestroy {
  onDestroy$ = new Subject<void>();
  resourceView: ResourcesView[] = [
    { name: 'nav_microservice', type: 'count', value: 0 },
    {
      name: 'service_version',
      type: 'versionCount',
      value: 0,
    },
    {
      name: 'service_entrance',
      type: 'entry',
      value: 0,
    },
    {
      name: 'instance',
      value: 0,
      type: 'instance',
      child: {
        success: 0,
        error: 0,
      },
    },
  ];

  params$ = this.route.paramMap.pipe(
    map(params => {
      return {
        namespace: params.get('namespace'),
        cluster: params.get('cluster'),
      };
    }),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly api: OverviewService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.getResource();
  }

  getResource() {
    this.params$
      .pipe(
        takeUntil(this.onDestroy$),
        switchMap(params =>
          this.api.getResources(params.cluster, params.namespace),
        ),
      )
      .subscribe(result => {
        this.resourceView.forEach(item => {
          item.value = get(result, item.type, 0);
          if (item.type === 'instance') {
            item.child.success = get(result, 'healthy', 0);
            item.child.error = get(result, 'unhealthy', 0);
          }
        });
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
