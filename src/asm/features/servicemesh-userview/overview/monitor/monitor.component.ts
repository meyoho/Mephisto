import { NotificationService, StatusType } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  CalendarData,
  DateBroadcastValue,
  getTimeValueOf,
  PICKER_TIME_RANGES,
} from '@app/shared/components/range-picker';
import { toFixed } from '@app/utils/unit';
import { OverviewService } from '@asm/api/overview/overview.service';
import {
  Monitor,
  MonitorModel,
  MonitorType,
  Values,
} from '@asm/api/overview/overview.types';

import { get } from 'lodash-es';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';
import {
  catchError,
  map,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';
const COUNT = 5;
@Component({
  selector: 'alo-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MonitorComponent implements OnInit, OnDestroy {
  monitorType = MonitorType;
  status = [{ scale: 1, type: StatusType.Info }];
  defaultData = this.getDefaultData();
  onDestroy$ = new Subject<void>();
  search$ = new BehaviorSubject<void>(null);
  defaultModel = [
    {
      type: this.monitorType.Rps,
      name: 'call_dosage',
      data: this.defaultData,
    },
    {
      type: this.monitorType.Mortality,
      name: 'call_failure_rate',
      data: this.defaultData,
    },
    {
      type: this.monitorType.Ms,
      name: 'average_response_time',
      data: this.defaultData,
    },
  ];

  monitorModel: MonitorModel[] = this.defaultModel;
  calendarData: CalendarData;

  params$ = this.route.paramMap.pipe(
    map(params => {
      return {
        namespace: params.get('namespace'),
        cluster: params.get('cluster'),
      };
    }),
    publishReplay(1),
    refCount(),
  );

  customDateTimeOptions = {
    enableTime: true,
    enableSeconds: true,
    time_24hr: true,
  };

  constructor(
    private readonly route: ActivatedRoute,
    private readonly api: OverviewService,
    private readonly cdr: ChangeDetectorRef,
    private readonly notification: NotificationService,
  ) {}

  ngOnInit() {
    // TODO: 当前组件作为子组件时，broadcastTime 在 onInit 之后触发
    // 导致 date 无值，先给默认值，后续如何解决
    if (!this.calendarData) {
      const date = PICKER_TIME_RANGES[2];
      this.calendarData = {
        start: date.start(),
        end: date.end(),
        step: date.step,
      };
    }
    combineLatest([this.params$, this.search$])
      .pipe(
        takeUntil(this.onDestroy$),
        switchMap(([params]) => {
          const timeParams = {
            start_time: getTimeValueOf(this.calendarData.start),
            end_time: getTimeValueOf(this.calendarData.end),
            step: this.calendarData.step * 60,
          };
          return this.api.getMonitor({ ...params, ...timeParams }).pipe(
            catchError(rej => {
              this.notification.error(rej.message);
              return null;
            }),
            tap((result: Monitor) => {
              this.handlePayload(result);
            }),
          );
        }),
      )
      .subscribe();
  }

  handlePayload(result: Monitor) {
    this.monitorModel.forEach(monitor => {
      const values: Values[] = get(result, monitor.type, []) || [];
      const fillValues = [...values, ...this.fillArray(COUNT - values.length)];
      const maxVal = Math.max(
        ...fillValues.map(val => {
          const itemValue = get(val, 'value', 0);
          return toFixed(itemValue);
        }),
      );

      monitor.data = this.fillMonitorModel(fillValues, maxVal);
    });
    this.cdr.markForCheck();
  }

  fillMonitorModel(fillValues: Values[], maxVal: number) {
    return fillValues.map(item => {
      const { value: itemValue = 0 } = item;
      const value = toFixed(itemValue);
      return {
        ...item,
        value,
        status: [
          {
            scale: value,
            type: value ? StatusType.Primary : StatusType.Info,
          },
          ...(value === 1
            ? []
            : [
                {
                  scale: Math.max(1, maxVal) - value,
                  type: StatusType.Info,
                },
              ]),
        ],
      };
    });
  }

  getDefaultData() {
    return this.fillArray();
  }

  fillArray(count = COUNT) {
    return new Array(count).fill(null).map(() => ({
      name: '',
      value: 0,
      msValue: [0, 0],
      status: this.status,
    }));
  }

  search() {
    this.search$.next();
  }

  broadcastTime(event: DateBroadcastValue) {
    this.calendarData = event.date;
    this.search();
    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
