import { RouterModule, Routes } from '@angular/router';

import { OverviewComponent } from './overview.component';
const routes: Routes = [
  {
    path: '',
    component: OverviewComponent,
  },
];
export const OverviewRoutes = RouterModule.forChild(routes);
