import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  getTimeRanges,
  getTimeValueOf,
} from '@app/shared/components/range-picker';
import { RouteParams } from '@app/typings';
import { OverviewService } from '@asm/api/overview/overview.service';
import { getSubset } from '@asm/api/route-management';

import { map, publishReplay, refCount } from 'rxjs/operators';
@Component({
  selector: 'alo-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecordComponent {
  circuitBreakerColumns = ['service_version', 'circuit_breaker_time'];
  trafficColumns = ['service_version', 'error_rate'];
  defaultTime = getTimeRanges(10, 'minute');
  getSubset = getSubset;
  params$ = this.route.paramMap.pipe(
    map(params => {
      return {
        namespace: params.get('namespace'),
        cluster: params.get('cluster'),
        start_time: getTimeValueOf(this.defaultTime.start()),
        end_time: getTimeValueOf(this.defaultTime.end()),
      };
    }),
    publishReplay(1),
    refCount(),
  );

  fetchCircuitBreaker = (params: RouteParams) => {
    return this.api.getCircuits(params);
  };

  fetchTraffic = (params: RouteParams) => this.api.getUntraffic(params);

  constructor(
    private readonly route: ActivatedRoute,
    private readonly api: OverviewService,
  ) {}
}
