import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';

import { MonitorComponent } from './monitor/monitor.component';
import { OverviewComponent } from './overview.component';
import { OverviewRoutes } from './overview.routing';
import { RecordComponent } from './record/record.component';
@NgModule({
  imports: [CommonModule, SharedModule, OverviewRoutes, FormsModule],
  declarations: [OverviewComponent, MonitorComponent, RecordComponent],
})
export class OverviewModule {}
