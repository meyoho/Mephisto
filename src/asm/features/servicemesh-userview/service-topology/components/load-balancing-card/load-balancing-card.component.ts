import {
  COMMON_WRITABLE_ACTIONS,
  K8sPermissionService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE_TYPES } from '@app/utils';
import { TrafficPolicyService } from '@asm/api/traffic-policy/traffic-policy.service';
import {
  DestinationRule,
  DestinationRuleResponse,
} from '@asm/api/traffic-policy/traffic-policy.types';
import { DeleteLoadBalancingDialogComponent } from '@asm/features/servicemesh-userview/load-balancing/dialog/delete-load-balancing-dialog/delete-load-balancing-dialog.component';
import { LoadBalancingDialogComponent } from '@asm/features/servicemesh-userview/load-balancing/dialog/load-balancing-dialog/load-balancing-dialog.component';

import { get } from 'lodash-es';
import { Subject } from 'rxjs';
import { map, publishReplay, refCount, switchMap, tap } from 'rxjs/operators';
@Component({
  selector: 'alo-load-balancing-card',
  templateUrl: './load-balancing-card.component.html',
  styleUrls: ['./load-balancing-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancingCardComponent implements OnChanges {
  nodata = false;
  @Input()
  params: {
    name: string;
    sidecar: boolean;
  };

  refresh$ = new Subject<void>();
  loadBalancer: DestinationRuleResponse[];
  auth = {
    create: false,
    update: false,
    delete: false,
  };

  creatable = false;
  baseParams: {
    project: string;
    cluster: string;
    namespace: string;
  };

  params$ = this.route.paramMap.pipe(
    map(params => ({
      project: params.get('project'),
      cluster: params.get('cluster'),
      namespace: params.get('namespace'),
    })),
    tap(params => {
      this.baseParams = params;
    }),
    publishReplay(1),
    refCount(),
  );

  permissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.DESTINATIONRULE,
        action: COMMON_WRITABLE_ACTIONS,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    tap(result => {
      this.auth = result;
    }),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly api: TrafficPolicyService,
    private readonly dialog: DialogService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sPermission: K8sPermissionService,
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.params.currentValue) {
      this.nodata = false;
      this.refresh$.next();
      this.cdr.markForCheck();
    }
  }

  fetch = (params: { namespace: string; cluster: string }) => {
    const { name } = this.params;
    return this.api.getHostDetail(params.namespace, name, params.cluster).pipe(
      tap(result => {
        const existence = (item: DestinationRuleResponse) =>
          !get(item, 'spec.trafficPolicy.loadBalancer');
        if (result.every(existence)) {
          this.nodata = true;
          this.creatable = false;
        }
        if (result.some(existence) && result.length > 1) {
          this.creatable = true;
        }
        this.loadBalancer = result;
        this.cdr.markForCheck();
      }),
    );
  };

  loadBalancerPure = (item: DestinationRuleResponse) => {
    return get(item, 'spec.trafficPolicy.loadBalancer');
  };

  create() {
    if (!this.auth.create) {
      return;
    }
    const { namespace, cluster, project } = this.baseParams;
    const dialogRef = this.dialog.open(LoadBalancingDialogComponent, {
      data: {
        isUpdate: false,
        host: get(this.loadBalancer, 'spec.host'),
        name: get(this.loadBalancer, 'metadata.name'),
        project: project,
        namespace: namespace,
        services: this.loadBalancer,
        cluster: cluster,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.nodata = false;
        this.creatable = false;
        this.refresh$.next();
        this.cdr.markForCheck();
      }
    });
  }

  update(item: DestinationRule) {
    if (!this.auth.update) {
      return;
    }
    const { namespace, cluster } = this.baseParams;
    const dialogRef = this.dialog.open(LoadBalancingDialogComponent, {
      data: {
        isUpdate: true,
        namespace: namespace,
        host: get(item, 'spec.host'),
        name: get(item, 'metadata.name'),
        cluster: cluster,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh(result);
    });
  }

  refresh(result: boolean) {
    if (result) {
      this.refresh$.next();
    }
  }

  delete(item: DestinationRule) {
    if (!this.auth.delete) {
      return;
    }
    const { namespace, cluster } = this.baseParams;
    const dialogRef = this.dialog.open(DeleteLoadBalancingDialogComponent, {
      size: DialogSize.Small,
      data: {
        host: get(item, 'spec.host'),
        name: get(item, 'metadata.name'),
        namespace: namespace,
        cluster: cluster,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      this.refresh(result);
    });
  }
}
