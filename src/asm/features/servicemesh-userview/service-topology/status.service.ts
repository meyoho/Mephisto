import { Injectable } from '@angular/core';

import { BehaviorSubject, Subject } from 'rxjs';

import {
  OperatingState,
  ServiceTopology,
} from '../../../api/service-topology/service-topology-api.types';
interface Params {
  sourceData: ServiceTopology;
}
@Injectable({
  providedIn: 'root',
})
export class StatusService {
  status = {
    display: false,
    changed: false,
    closed: true,
    injectedService: true,
  };

  params: Params = {
    sourceData: null,
  };

  time: string[] = [];
  statusSub$ = new BehaviorSubject<OperatingState>(this.status);
  paramsSub$ = new BehaviorSubject<any>(this.params);
  actionClear$ = new Subject();

  setMapStatus(params: OperatingState) {
    this.status = { ...this.status, ...params };
    this.statusSub$.next(this.status);
  }

  setParams(params: Params) {
    this.params = { ...this.params, ...params };
    this.paramsSub$.next(this.params);
  }

  setTime(time: string[]) {
    this.time = time;
  }
}
