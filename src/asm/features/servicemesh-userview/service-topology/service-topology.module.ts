import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FeatureSharedCommonModule } from '@app/features-shared/common';
import { SharedModule } from '@app/shared';

import { LoadBalancingModule } from '../load-balancing/load-balancing.module';
import { ServicemeshSharedModule } from './../shared/servicemesh-shared.module';
import { LoadBalancingCardComponent } from './components/load-balancing-card/load-balancing-card.component';
import { GraphLegendComponent } from './graph-legend/graph-legend.component';
import { ServiceTopologyComponent } from './service-topology.component';
import { ServiceTopologyRoutes } from './service-topology.routing';
import { TopologyGraphComponent } from './topology-graph/topology-graph.component';
@NgModule({
  imports: [
    CommonModule,
    LoadBalancingModule,
    ServiceTopologyRoutes,
    RouterModule,
    SharedModule,
    FeatureSharedCommonModule,
    FormsModule,
    ReactiveFormsModule,
    ServicemeshSharedModule,
  ],
  declarations: [
    ServiceTopologyComponent,
    TopologyGraphComponent,
    LoadBalancingCardComponent,
    GraphLegendComponent,
  ],
  exports: [LoadBalancingCardComponent],
})
export class ServiceTopologyModule {}
