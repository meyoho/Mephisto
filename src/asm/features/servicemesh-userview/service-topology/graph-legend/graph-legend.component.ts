import { Component } from '@angular/core';
const ICON_PREFIX = 'basic';
@Component({
  selector: 'alo-graph-legend',
  templateUrl: './graph-legend.component.html',
  styleUrls: ['./graph-legend.component.scss'],
})
export class GraphLegendComponent {
  nodeTypes = [
    // 暂不展示 gateway 节点
    // { name: 'Ingress Gateway', icon: `${ICON_PREFIX}:node_ingress_gateway` },
    { name: 'Workload', icon: `${ICON_PREFIX}:node_workload` },
    { name: 'Missing Sidecar', icon: `${ICON_PREFIX}:node_missing_sidecar` },
    // { name: 'Service', icon: `${ICON_PREFIX}:node_service` },
    { name: 'Unknown', icon: `${ICON_PREFIX}:node_workload_unknown` },
  ];

  nodeColors = [
    {
      name: 'calling',
      icon: `${ICON_PREFIX}:node_ingress_gateway`,
      color: '#009ce3',
    },
    {
      name: 'legend_line_free',
      icon: `${ICON_PREFIX}:node_ingress_gateway`,
      color: '#cccccc',
    },
  ];

  nodeTag = [
    {
      name: 'safety',
      icon: `${ICON_PREFIX}:node_safe`,
      color: '#009ce3',
    },
    {
      name: 'outlier_detection',
      icon: `${ICON_PREFIX}:circuit_breaker`,
      color: '#009ce3',
    },
  ];

  lineColors = [
    { name: 'legend_line_20', icon: `${ICON_PREFIX}:arrow`, color: '#eb6262' },
    {
      name: 'legend_line_01_20',
      icon: `${ICON_PREFIX}:arrow`,
      color: '#f8ac58',
    },
    { name: 'legend_line_01', icon: `${ICON_PREFIX}:arrow`, color: '#1bb393' },
    {
      name: 'legend_line_free',
      icon: `${ICON_PREFIX}:arrow`,
      color: '#cccccc',
    },
  ];
}
