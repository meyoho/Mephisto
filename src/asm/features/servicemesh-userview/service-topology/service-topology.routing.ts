import { RouterModule, Routes } from '@angular/router';

import { ServiceTopologyComponent } from './service-topology.component';

const routes: Routes = [
  {
    path: '',
    component: ServiceTopologyComponent,
  },
];

export const ServiceTopologyRoutes = RouterModule.forChild(routes);
