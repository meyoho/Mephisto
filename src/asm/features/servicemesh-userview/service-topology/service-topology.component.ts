import { StringMap } from '@alauda/common-snippet';
import { animate, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { getRequestStat } from '@app/shared/components/bar-status/util';
import {
  calculateStep,
  handleChartData,
  TimeParams,
} from '@app/shared/components/chart';
import {
  CalendarData,
  DateBroadcastValue,
  getTimeValueOf,
  TIMERANGE,
} from '@app/shared/components/range-picker';
import { config } from '@asm/api/service-topology/config';
import { ServiceTopologyApiService } from '@asm/api/service-topology/service-topology-api.service';
import {
  Belong,
  RequestParam,
  RequestStat,
  ServiceMetricsResponse,
  ServiceTopology,
} from '@asm/api/service-topology/service-topology-api.types';
import {
  getNodeType,
  GraphData,
  VisStatus,
} from '@asm/api/service-topology/utils';

import { get, omit } from 'lodash-es';
import { BehaviorSubject, combineLatest, of, Subject } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import {
  flowOptions,
  inflowOptions,
  outflowOptions,
  responseTimeOptions,
} from '../data-panel/utils';
import { ZoomableService } from '../shared/commponents/graph/zoomable.service';
import { StatusService } from './status.service';
@Component({
  templateUrl: './service-topology.component.html',
  styleUrls: ['./service-topology.component.scss'],
  animations: [
    trigger('drawer', [
      transition(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' })),
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' })),
      ]),
    ]),
  ],
  providers: [StatusService, ZoomableService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceTopologyComponent implements OnInit, OnDestroy {
  selectedDuration = TIMERANGE['30M'];
  type: 'node' | 'line' = 'node';
  tabSelectedIndex = 0;
  sourceData: ServiceTopology;
  responses: RequestStat[];
  service = '';
  serviceJaeger = '';
  version: string[] = [];
  workloads: string[] = [];
  noData = false;
  nodeState: VisStatus;
  config = config;
  checkedService = false;
  nodeType: string;
  lastParams: RequestParam;
  nodeTipInfo: string[] = [];
  onDestroy$ = new Subject<void>();
  getNodeType = getNodeType;
  project = '';
  cluster = this.route.snapshot.paramMap.get('cluster') || '';
  namespace: string = this.route.snapshot.params.namespace;
  graphValue: GraphData;
  cluster$ = this.route.paramMap.pipe(
    map(params => {
      this.project = params.get('project');
      this.namespace = params.get('namespace');
      this.cluster = params.get('cluster');
      return {
        cluster: params.get('cluster') || '',
      };
    }),
    publishReplay(1),
    refCount(),
  );

  calendarData: CalendarData;
  timeParams: TimeParams = {
    start: 0,
    end: 0,
    step: 0,
  };

  loadBalancerParams = {
    name: '',
    sidecar: false,
  };

  currentNode: {
    node_type: string;
    has_TLS: boolean;
    has_istio_sidecar: boolean;
    label: string[];
    workload: string;
    belong: Belong;
  };

  chartModel: { [key: string]: any } = {
    rps: {
      options: flowOptions,
      data: [],
    },
    rps_in: {
      options: inflowOptions,
      data: [],
    },
    rps_out: {
      options: outflowOptions,
      data: [],
    },
    response_time: {
      options: responseTimeOptions,
      data: [],
    },
  };

  metricsParams$ = new BehaviorSubject<RequestParam>({});

  graphParams$ = new BehaviorSubject<RequestParam>({});
  closed: boolean;
  closed$ = this.status.statusSub$.pipe(
    takeUntil(this.onDestroy$),
    map(params => params.closed),
    tap(closed => {
      this.closed = closed;
    }),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  display$ = this.status.statusSub$.pipe(
    takeUntil(this.onDestroy$),
    map(params => params.display),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  graphData$ = combineLatest([
    this.cluster$,
    this.graphParams$,
    this.display$,
  ]).pipe(
    filter(([, _, display]) => !display),
    switchMap(([cluster, graphParams]) => {
      return this.api
        .getGraphData(this.namespace, this.cluster, {
          ...cluster,
          ...graphParams,
          start_time: getTimeValueOf(this.calendarData.start),
          end_time: getTimeValueOf(this.calendarData.end),
          inject_service_nodes: String(this.checkedService),
        })
        .pipe(catchError(() => of({ nodes: [], lines: [] })));
    }),
    tap((source: ServiceTopology) => {
      this.setTime();
      this.sourceData = source;
      this.noData = !source.edges || !source.edges.length;
    }),
    map(source => {
      return this.api.toSourceData(source);
    }),
    tap(result => {
      this.graphValue = result;
    }),
    startWith({ nodes: [], lines: [] }),
    publishReplay(1),
    refCount(),
  );

  metrics$ = combineLatest([
    this.cluster$,
    this.metricsParams$,
    this.graphParams$,
  ]).pipe(
    map(([cluster, metricsParams]) => {
      return {
        ...cluster,
        ...metricsParams,
        start_time: getTimeValueOf(this.calendarData.start),
        end_time: getTimeValueOf(this.calendarData.end),
      };
    }),
    tap(params => {
      this.lastParams = params;
    }),
    switchMap(params => {
      const { node_type: nodeType } = params;
      if (
        this.type === 'node' &&
        (nodeType === 'service' ? !params.service : !params.workload)
      ) {
        return of({} as ServiceMetricsResponse);
      }
      if (
        this.type === 'node' &&
        nodeType === 'service' &&
        params.workload === 'unknown'
      ) {
        params.workload = 'unknown';
      }
      return this.api
        .getMetrics(omit(params, 'node_type'))
        .pipe(catchError(() => of({} as ServiceMetricsResponse)));
    }),
    publishReplay(1),
    refCount(),
  );

  inRequestStat$ = this.metrics$.pipe(
    map(res =>
      res && res.requests_total_in
        ? getRequestStat(res.requests_total_in)
        : null,
    ),
    publishReplay(1),
    refCount(),
  );

  outRequestStat$ = this.metrics$.pipe(
    map(res =>
      res &&
      res.requests_total_out &&
      this.type === 'node' &&
      this.nodeType !== 'service'
        ? getRequestStat(res.requests_total_out)
        : null,
    ),
    publishReplay(1),
    refCount(),
  );

  metricsChart$ = this.metrics$.pipe(
    map(res => {
      return Object.keys(this.chartModel).reduce((prev, key) => {
        const value = this.chartModel[key];
        const name = key === 'rps' ? 'rps_in' : key;
        const data = get(res, name, {});
        const time = [
          +getTimeValueOf(this.calendarData.start),
          +getTimeValueOf(this.calendarData.end),
        ];
        const step = calculateStep(time[0], time[1]);
        return {
          ...prev,
          [key]: {
            ...this.chartModel[key],
            data: handleChartData(data, value.options, {
              ...this.timeParams,
              step,
            }),
            time: [this.timeParams.start, this.timeParams.end],
          },
        };
      }, {});
    }),
    startWith(this.chartModel),
    publishReplay(1),
    refCount(),
  );

  get step() {
    return (this.calendarData.step * 60).toString();
  }

  constructor(
    private readonly api: ServiceTopologyApiService,
    private readonly route: ActivatedRoute,
    private readonly status: StatusService,
    private readonly router: Router,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.status.paramsSub$
      .pipe(
        takeUntil(this.onDestroy$),
        map(item => item.sourceData),
        distinctUntilChanged(),
      )
      .subscribe(result => {
        this.sourceData = result;
      });
  }

  refresh() {
    if (this.closed) {
      this.status.actionClear$.next(true);
    }
    const metricsParams = this.metricsParams$.getValue();
    this.metricsParams$.next({
      ...metricsParams,
      step: this.step,
    });
    this.setTime();
    this.graphParams$.next({});
    this.status.setMapStatus({ changed: true });
  }

  setTime() {
    this.status.setTime([
      getTimeValueOf(this.calendarData.start),
      getTimeValueOf(this.calendarData.end),
    ]);
  }

  private getNode(id: string) {
    return this.sourceData.nodes.find(node => node.id.toString() === id);
  }

  /* eslint-disable sonarjs/cognitive-complexity */
  // TODO: Refactor this function to reduce its Cognitive Complexity from 18 to the 15 allowed
  select({ type, id, status }: any) {
    this.status.setMapStatus({ closed: false });
    let params: RequestParam = {};
    this.type = type;
    let selectedNamespace = true;
    if (type === 'node') {
      const node = this.getNode(id);
      const {
        workload,
        service,
        namespace,
        node_type: nodeType,
        app,
        has_istio_sidecar,
        has_TLS,
        belongs_to,
      } = node;
      selectedNamespace = namespace === this.namespace;
      this.currentNode = {
        node_type: nodeType,
        has_TLS,
        has_istio_sidecar,
        label: [app, service],
        workload,
        belong: belongs_to,
      };
      this.loadBalancerParams = {
        name: service,
        sidecar: has_istio_sidecar,
      };
      this.nodeType = nodeType;
      this.nodeState = status || this.getNodeState(id);
      const isService = nodeType === 'service';
      const value = isService
        ? {
            service,
            ...(workload === 'unknown' ? { workload } : {}),
          }
        : { workload };
      params = {
        namespace,
        ...value,
        node_type: nodeType,
      };
      this.service = isService ? service : app;
      this.version = [node.version];
      this.workloads = [];
    } else if (type === 'line') {
      const [sourceId, targetId] = id.split('_');
      const source = this.getNode(sourceId);
      const target = this.getNode(targetId);
      this.nodeState = this.getNodeState(
        source.id.toString(),
        target.id.toString(),
      );
      const sourceParams =
        source.node_type === 'service'
          ? {
              source_service: source.service,
              ...(source.workload === 'unknown'
                ? { source_workload: source.workload }
                : {}),
            }
          : { source_workload: source.workload };
      const targetParams =
        target.node_type === 'service'
          ? {
              target_service: target.service,
              ...(target.workload === 'unknown'
                ? { target_workload: target.workload }
                : {}),
            }
          : { target_workload: target.workload };

      let sourceParamsData: StringMap = {
        source_namespace: source.namespace,
      };
      if (source.node_type === 'service' && target.node_type === 'workload') {
        sourceParamsData = {
          target_service: source.service,
        };
      }
      params = {
        ...sourceParams,
        ...targetParams,
        ...sourceParamsData,
        target_namespace: target.namespace,
      };
      this.workloads = [
        source.node_type === 'service' ? source.service : source.app,
        target.node_type === 'service' ? target.service : target.app,
      ];
      this.service = '';
      this.version = [source.version, target.version];
    }
    this.metricsParams$.next({
      ...params,
      ...(selectedNamespace ? {} : { selected_namespace: this.namespace }),
      step: this.step,
    });
    this.cdr.markForCheck();
  }

  getNodeState(id: string, targetId?: string) {
    if (!targetId) {
      return get(this.getGraphNode(id), 'status', VisStatus.Default);
    }
    return get(this.getGraphLines(id, targetId), 'status', VisStatus.Default);
  }

  getGraphNode(id: string) {
    return this.graphValue.nodes.find(item => item.id === id);
  }

  getGraphLines(sourceId: string, targetId: string) {
    return this.graphValue.lines.find(
      item => item.source_id === sourceId && item.target_id === targetId,
    );
  }

  referencedLinesStatus(id: string) {
    return this.sourceData.edges.reduce((prev, edge) => {
      const exist =
        edge.source_id === id.toString() || edge.target_id === id.toString();
      return exist ? [edge.request_rate, ...prev] : prev;
    }, []);
  }

  closedFn() {
    this.status.setMapStatus({ closed: true });
  }

  setStatus(id: string) {
    this.nodeState = this.getNodeState(id);
    this.cdr.markForCheck();
  }

  checkboxed(injected: boolean) {
    if (this.closed) {
      this.status.actionClear$.next(true);
    }
    if (!injected && this.nodeType === 'service') {
      this.status.setMapStatus({ closed: true });
    }
    this.status.setMapStatus({ injected });
    this.graphParams$.next({});
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  getChartType = (type: string) => {
    if (type === 'line' || (type === 'node' && this.nodeType === 'service')) {
      return true;
    }
    return false;
  };

  setLineParams({
    workloads = [],
    version = [],
  }: {
    workloads: string[];
    version: string[];
  }) {
    this.workloads = workloads;
    this.version = version;
    this.cdr.markForCheck();
  }

  jumpMicroservice() {
    const {
      belong: { microservice },
    } = this.currentNode;
    if (microservice) {
      this.router.navigate([this.getJumpUrl('service-list'), microservice]);
    }
  }

  jumpTraffic(workload: string) {
    if (workload) {
      this.router.navigate([
        this.getJumpUrl('data-panel'),
        {
          workload: `${workload}`,
          ...this.calendarData,
          range: this.selectedDuration,
        },
      ]);
    }
  }

  getJumpUrl(name: string) {
    return `/workspace/${this.project}/clusters/${this.cluster}/namespaces/${this.namespace}/servicemesh/${name}`;
  }

  broadcastTime(event: DateBroadcastValue) {
    this.calendarData = Object.assign({}, event.date);
    this.timeParams = {
      step: event.date.step,
      ...event.dateValueOf,
    };
    this.selectedDuration = event.range;
    this.refresh();
  }
}
