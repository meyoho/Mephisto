import { TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { TipInfo } from '@asm/api/service-topology/service-topology-api.types';
import { GraphData, VisLine, VisNode } from '@asm/api/service-topology/utils';

import { Subject } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  takeUntil,
} from 'rxjs/operators';

import { GraphComponent } from '../../shared/commponents/graph/graph.component';
import { ZoomableService } from '../../shared/commponents/graph/zoomable.service';
import { StatusService } from '../status.service';

interface SelectedPoint {
  id: string;
  type: string;
  status?: string;
  ev: MouseEvent;
}
@Component({
  selector: 'alo-topology-graph',
  templateUrl: './topology-graph.component.html',
  styleUrls: ['./topology-graph.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopologyGraphComponent extends GraphComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input()
  set data({ nodes, lines, groupLines }: GraphData) {
    if (nodes.length > 0) {
      this.renderGraph(nodes, lines, groupLines);
      this.render$.next();
    } else {
      this.nodeMap.clear();
      this.lineMap.clear();
      this.groupMap.clear();
    }
  }

  get nodeStatus() {
    return this.currentIds;
  }

  constructor(
    translate: TranslateService,
    zoomService: ZoomableService,
    private readonly status: StatusService,
  ) {
    super(translate, zoomService);
  }

  render$ = new Subject();
  @Input()
  injectService: boolean;

  currentIds: Set<string> = new Set();
  currentId: string;
  nodeSelected = false;
  lineSelected = false;
  curretNode: VisNode;
  lineInfo: TipInfo = {
    type: 'line',
    width: 145,
    height: 88,
    show: false,
    x: 0,
    y: 0,
  };

  nodeInfo: TipInfo = {
    type: 'node',
    width: 145,
    height: 115,
    x: 0,
    y: 0,
    show: false,
  };

  displayLeged = false;

  @Output()
  select = new EventEmitter<{
    type: string;
    id: string;
    position?: [number, number]; // node position in tree
  }>();

  @Input()
  refreshed: EventEmitter<string>;

  @Output()
  setStatus = new EventEmitter();

  @Output()
  setLineParams = new EventEmitter();

  partialViewDisplay$ = this.status.statusSub$.pipe(
    takeUntil(this.onDestroy$),
    map(params => params.display),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  fixedState$ = this.render$.pipe(
    takeUntil(this.onDestroy$),
    publishReplay(1),
    refCount(),
  );

  @HostListener('window:click', ['$event'])
  onClicked() {
    this.displayLeged = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      changes.injectService &&
      ['true', 'false'].includes(String(changes.injectService.currentValue)) &&
      !changes.injectService.currentValue
    ) {
      this.currentId = '';
    }
  }

  ngOnInit() {
    this.listenClear();
    this.status.statusSub$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(params => params.closed),
      )
      .subscribe(_ => {
        this.lineSelected = false;
        this.nodeSelected = false;
        this.clearIds();
      });

    this.fixedState$.subscribe(_ => {
      if (this.currentId) {
        if (this.currentId.includes('_')) {
          const [sourceId, targetId] = this.currentId.split('_');
          this.currentIds = this.getSelectedLinkedNodeId(
            `${sourceId}_${targetId}`,
          );
          const source = this.getNode(sourceId);
          const target = this.getNode(targetId);
          const { length: sourceLen } = source.label;
          const { length: targetLen } = target.label;
          this.setLineParams.next({
            workloads: [
              source.node_type === 'service' ? source.service : source.app,
              target.node_type === 'service' ? target.service : target.app,
            ],
            version: [
              source.label[sourceLen - 1] || '',
              target.label[targetLen - 1] || '',
            ],
          });
        } else {
          this.currentIds = this.getSelectedLinkedNodeId(this.currentId);
        }
      }
    });
  }

  listenClear() {
    this.status.actionClear$.pipe(takeUntil(this.onDestroy$)).subscribe(res => {
      if (res) {
        this.currentId = '';
        this.clearIds();
      }
    });
  }

  getEdgeLineId(source: string, target: string) {
    if (source === target) {
      return target;
    }
    const linesList = this.lines.reduce((prev, curr) => {
      let lines: string[] = [];
      if (curr.source_id === source) {
        lines = [curr.target_id];
      } else if (curr.target_id === target) {
        lines = [curr.source_id];
      }
      return [...lines, ...prev];
    }, []);
    return (
      linesList.find((value, index, iteratee) =>
        iteratee.includes(value, index + 1),
      ) || target
    );
  }

  nodeClick(event: any) {
    const { detail } = event;
    const id = event.srcElement.id;
    this.currentId = id;
    const { node_type: nodeType } = this.getNode(id);
    if (detail === 1) {
      this.nodeSelected = true;
      this.lineSelected = false;
      this.currentIds = this.getSelectedLinkedNodeId(id);
      this.select.emit({ type: 'node', id: id });
    } else if (detail === 2 && this.existEdge(id) && nodeType !== 'service') {
      this.status.setMapStatus({ display: true });
      this.curretNode = this.getNode(id);
      this.select.emit({ type: 'node', id: id });
    }
  }

  nodeMouseMove(e: MouseEvent) {
    this.nodeInfo.x = Math.min(
      this.size.width - this.nodeInfo.width,
      e.offsetX + 8,
    );
    this.nodeInfo.y = Math.min(
      this.size.height - this.nodeInfo.height,
      e.offsetY + 16,
    );
  }

  nodeMouseover(id: string) {
    this.nodeInfo.show = true;
    this.nodeInfo.data = this.getNodeTipInfo(this.getNode(id));
    if (!this.nodeSelected && !this.lineSelected) {
      this.currentIds = this.getSelectedLinkedNodeId(id);
    }
  }

  nodeMouseout() {
    this.nodeInfo.show = false;
    if (!this.nodeSelected && !this.lineSelected) {
      this.clearIds();
    }
  }

  getSelectedLinkedNodeId(id: string) {
    const lineIds = [...this.lineMap.keys()];
    const ids = lineIds.reduce((prev, curr) => {
      let result: string[] = [];
      if (curr.includes(id)) {
        result = [...curr.split('_'), curr];
      }
      return [...result, ...prev];
    }, []);
    ids.unshift(id);
    return new Set(ids);
  }

  lineClick(e: MouseEvent) {
    this.currentIds = this.getSelectedLinkedlineId(e);
    this.lineSelected = true;
    this.nodeSelected = false;
    const lineId = this.getLineId(e);
    this.currentId = lineId;
    this.select.emit({ type: 'line', id: lineId });
  }

  lineMouseover(e: MouseEvent) {
    this.lineInfo.show = true;
    if (!this.lineSelected && !this.nodeSelected) {
      this.currentIds = this.getSelectedLinkedlineId(e);
    }
  }

  lineMouseout() {
    this.lineInfo.show = false;
    if (!this.lineSelected && !this.nodeSelected) {
      this.clearIds();
    }
  }

  getSelectedLinkedlineId(e: MouseEvent) {
    const lineId = this.getLineId(e);
    const linkedNodeIds = lineId.split('_');
    return new Set([lineId, ...linkedNodeIds]);
  }

  lineMouseMove([e, line]: [MouseEvent, VisLine]) {
    this.lineInfo.x = Math.min(
      this.size.width - this.lineInfo.width,
      e.offsetX + 8,
    );
    this.lineInfo.y = Math.min(
      this.size.height - this.lineInfo.height,
      e.offsetY + 16,
    );
    const id = this.getLineId(e);
    const value = this.getLinkInfo(id, line);
    this.lineInfo.status = line.status;
    this.lineInfo.data = value;
  }

  partialViewClick(data: { type: string; id: string }) {
    this.select.emit(data);
  }

  closePartialView({ id, type, ev }: SelectedPoint) {
    if (id) {
      if (type === 'node') {
        this.nodeSelected = true;
        this.currentIds = this.getSelectedLinkedNodeId(id);
        this.currentId = id;
        this.setStatus.emit(id);
      } else {
        this.lineSelected = true;
        this.currentId = this.getLineId(ev);
        this.currentIds = this.getSelectedLinkedlineId(ev);
      }
    }
  }

  clearIds() {
    this.currentIds = new Set();
  }

  zoomEnd() {
    this.grabState = false;
  }

  clickedLegend(e: Event) {
    e.stopPropagation();
    this.displayLeged = !this.displayLeged;
  }
}
