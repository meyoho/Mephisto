import {
  K8sPermissionService,
  K8sResourceAction,
  ObservableInput,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import {
  getDeployments,
  MicroService,
  MicroServiceStore,
  nodeTag,
  ServiceListService,
  TabNumberAction,
} from '@asm/api/service-list';
import {
  PROTOCOLS,
  Service,
  ServicePorts,
} from '@asm/api/service-list/service-list.types';

import { get } from 'lodash-es';
import { combineLatest, Observable, of, Subject } from 'rxjs';
import { map, publishReplay, refCount, switchMap, tap } from 'rxjs/operators';

import { ServiceDialogComponent } from '../service-list/service-list-detail/service-dialog/service-dialog.component';

@Component({
  selector: 'alo-protocol-table',
  templateUrl: './protocol-table.component.html',
  styleUrls: ['./protocol-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProtocolTableComponent implements OnInit {
  @ObservableInput(true)
  private readonly name$: Observable<string>;

  @Input() name: string;
  @Input() update$: Subject<boolean>;
  @Input() module: MicroService;
  @Input() service: { [key: string]: any };
  @Output() refresh = new EventEmitter();
  refresh$ = new Subject();
  columns = ['protocol', 'service_port', 'container_port'];
  routeParams: RouteParams;
  params$ = combineLatest([this.route.paramMap, this.name$]).pipe(
    map(([params, name]) => ({
      namespace: params.get('namespace') || '',
      cluster: params.get('cluster') || '',
      name,
    })),
    tap(routeParams => {
      this.routeParams = routeParams;
    }),
    publishReplay(1),
    refCount(),
  );

  servicePermissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.SERVICE,
        action: [K8sResourceAction.DELETE, K8sResourceAction.UPDATE],
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly api: ServiceListService,
    private readonly route: ActivatedRoute,
    private readonly store: MicroServiceStore,
    private readonly k8sPermission: K8sPermissionService,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
  ) {}

  ngOnInit(): void {
    this.update$.subscribe(() => {
      this.refresh$.next();
    });
  }

  fetch = (params: RouteParams) => {
    if (!params || !get(params, 'name')) {
      return of([]);
    }
    return this.api
      .getService(params.cluster, params.namespace, params.name)
      .pipe(
        map((result: Service) => {
          const res = get(result, 'spec.ports', []);
          if (res.length) {
            const allow = res.some((item: ServicePorts) =>
              ['HTTP', 'HTTP2'].includes(this.getNameProtocol(item)),
            );
            // 判断是否协议中存在http/2，只有http/2才能创建路由
            this.store.allowCreationRouteAction({ protocol: allow });
          }
          return res;
        }),
      );
  };

  getNameProtocol = (data: ServicePorts) => {
    const protocol = data.name.split('-')[0];
    return (
      PROTOCOLS.find(value => value.toLowerCase() === protocol) || data.protocol
    );
  };

  openServiceDialog() {
    const dialogRef = this.dialog.open(ServiceDialogComponent, {
      size: DialogSize.Big,
      data: {
        routeParams: this.routeParams,
        update: true,
        deployments: getDeployments(this.module),
        name: this.service.name,
      },
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.refresh$.next();
      }
    });
  }

  openDeleteDialog() {
    const { name } = this.service;
    const confirmCongif = {
      title: 'delete_service_entry_confirm_title',
      body: 'delete_service_entry_confirm_body',
    };
    const payload = this.deleteModelProcessing(name);
    this.dialog
      .confirm({
        title: this.translate.get(confirmCongif.title, { name }),
        content: this.translate.get(confirmCongif.body),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.api.update(this.routeParams.cluster, payload).subscribe(() => {
          this.message.success(
            this.translate.get('service_entry_delete_successfully', { name }),
          );
          this.store.tabAction(0, TabNumberAction.ROUTING);
          this.store.initTabNumberState();
          this.store.nodeTagAction(nodeTag);
          this.refresh.emit();
        });
      })
      .catch(() => {});
  }

  deleteModelProcessing(name: string) {
    const {
      spec: { services, ...specRes },
      ...res
    } = this.module;
    const specServices = services.filter(item => item.name !== name);
    return { ...res, spec: { ...specRes, services: specServices } };
  }
}
