import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';

import { ProtocolTableComponent } from './protocol-table.component';
@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [ProtocolTableComponent],
  exports: [ProtocolTableComponent],
})
export class ProtocolModule {}
