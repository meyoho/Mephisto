import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared';

import { JaegerComponent } from './jaeger/jaeger.component';
import { LoadBalancingModule } from './load-balancing/load-balancing.module';
import { SecurityDialogComponent } from './security-strategy/security-dialog/security-dialog.component';
import { SecurityStrategyComponent } from './security-strategy/security-strategy.component';
import { ServicemeshUserviewRoutingModule } from './servicemesh-userview-routing.module';
import { TrafficPolicyComponent } from './traffic-policy/traffic-policy.component';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    ServicemeshUserviewRoutingModule,
    LoadBalancingModule,
  ],
  exports: [],
  declarations: [
    JaegerComponent,
    SecurityStrategyComponent,
    TrafficPolicyComponent,
    SecurityDialogComponent,
  ],
  providers: [],
  entryComponents: [SecurityDialogComponent],
})
export class ServicemeshUserviewModule {}
