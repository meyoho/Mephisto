import { K8sUtilService, TOKEN_BASE_DOMAIN } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
} from '@angular/core';
import { Gateway, LabelPrefix } from '@asm/api/service-gateway';

import { get } from 'lodash-es';

@Component({
  selector: 'alo-service-gateway-basic-info',
  templateUrl: './basic-info.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceGatewayBasicInfoComponent {
  @Input()
  resource: Gateway;

  @Input()
  nodePort: number;

  @Output()
  viewMicroservice = new EventEmitter<string>();

  @Input()
  microServiceStatus: 'success' | 'pending' | 'error' = 'pending';

  @Input()
  serviceName = '';

  constructor(
    private readonly k8sUtil: K8sUtilService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {}

  get host() {
    return this.k8sUtil.getLabel(this.resource, 'gatewayhost', LabelPrefix);
  }

  get protocol() {
    return get(
      this.resource,
      `spec.servers[0].port.protocol`,
      '',
    ).toLocaleLowerCase();
  }

  get entry() {
    return `${this.protocol}://${this.host}:${this.nodePort}`;
  }

  get microService() {
    return get(
      this.resource,
      ['metadata', 'labels', `servicemesh.${this.baseDomain}/msname`],
      '',
    );
  }

  get microServicePort() {
    return get(
      this.resource,
      ['metadata', 'labels', `servicemesh.${this.baseDomain}/msport`],
      '',
    );
  }
}
