import {
  K8sPermissionService,
  K8sResourceAction,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RESOURCE_TYPES } from '@app/utils';
import { MicroServiceStore, RouteActionType } from '@asm/api/service-list';

import {
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
} from 'rxjs/operators';

@Component({
  selector: 'alo-service-gateway-advanced-info',
  styleUrls: ['./advanced-info.component.scss'],
  templateUrl: './advanced-info.component.html',
})
export class ServiceGatewayAdvancedInfoComponent implements OnDestroy, OnInit {
  @Input() gatewayHost: string;
  @Input()
  serviceName = '';

  routeDialogRef: DialogRef;
  routeActionType = RouteActionType;
  hiddenItem = ['outlier_detection', 'safety'];

  params$ = this.route.paramMap.pipe(
    map(params => ({
      cluster: params.get('cluster'),
      namespace: params.get('namespace'),
    })),
    publishReplay(1),
    refCount(),
  );

  creatableRoute$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.VIRTUALSERVICE,
        action: K8sResourceAction.CREATE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    public store: MicroServiceStore,
    private readonly route: ActivatedRoute,
    private readonly k8sPermission: K8sPermissionService,
    private readonly dialog: DialogService,
  ) {}

  ngOnInit() {
    this.initSubscribe();
    this.store.initRouteAction();
  }

  ngOnDestroy() {
    this.store.initTabNumberState();
  }

  initSubscribe() {
    this.store.routeState$
      .pipe(filter(item => item === RouteActionType.DEFAULT))
      .subscribe(() => {
        if (this.routeDialogRef) {
          this.routeDialogRef.close();
        }
      });
  }

  routeDialogConfirm() {
    this.store.routeTriggerAction();
  }

  triggerCreateRoute(templateRef: TemplateRef<any>) {
    this.store.routeAction(RouteActionType.CREATE);
    this.routeDialogRef = this.dialog.open(templateRef, {
      size: DialogSize.Large,
    });
  }

  triggerUpdateRoute(templateRef: TemplateRef<any>) {
    this.routeDialogRef = this.dialog.open(templateRef, {
      size: DialogSize.Large,
    });
  }
}
