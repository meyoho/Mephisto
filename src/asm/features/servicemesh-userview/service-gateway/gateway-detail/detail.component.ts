import {
  AsyncDataLoader,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  ObservableInput,
  TranslateService,
} from '@alauda/common-snippet';
import { ConfirmType, DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import {
  Gateway,
  LabelPrefix,
  ServiceGatewayService,
} from '@asm/api/service-gateway';
import {
  MicroServiceStore,
  PROTOCOLS,
  Service,
  ServiceListService,
  ServicePorts,
} from '@asm/api/service-list';
import { ServiceMeshApiService } from '@asm/api/service-mesh/service-mesh-api.service';

import { get } from 'lodash-es';
import { combineLatest, Observable } from 'rxjs';
import { map, publishReplay, refCount, switchMap, tap } from 'rxjs/operators';
@Component({
  templateUrl: './detail.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceGatewayDetailComponent implements OnDestroy {
  @ObservableInput(true)
  private readonly gatewayName$: Observable<string>;

  @Input() gatewayName: string;
  @Output() setTabTotal = new EventEmitter();
  @Output() emitNoRoute = new EventEmitter();
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly apiService: ServiceGatewayService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sPermission: K8sPermissionService,
    private readonly serviceListApi: ServiceListService,
    private readonly serviceMeshApiService: ServiceMeshApiService,
    private readonly k8sUtil: K8sUtilService,
    public store: MicroServiceStore,
    private readonly message: MessageService,
  ) {}

  enabled = true;
  project: string;
  name: string;
  namespace: string;
  cluster: string;
  gatewayInit = false;
  microServiceStatus: 'success' | 'pending' | 'error' = 'pending';
  serviceName = '';
  params$ = combineLatest([this.route.paramMap, this.gatewayName$]).pipe(
    map(([params, routerName]) => ({
      name: routerName || params.get('name'),
      namespace: params.get('namespace') || '',
      project: params.get('project') || '',
      cluster: params.get('cluster') || '',
      ...(routerName ? { isHost: true } : {}),
    })),
    tap(params => {
      this.name = params.name;
      this.namespace = params.namespace;
      this.cluster = params.cluster;
      this.project = params.project;
      this.cdr.markForCheck();
    }),
    publishReplay(1),
    refCount(),
  );

  istioIngressGateway$ = this.params$.pipe(
    switchMap(params =>
      this.serviceMeshApiService.getClusterConfig(params.cluster).pipe(
        map(res => {
          return get(res, 'spec.ingressgateway.nodeport', null);
        }),
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  fetch = (params: RouteParams) => {
    return this.apiService.getGatewayDetail(params).pipe(
      tap(gateway => {
        this.initGatewayStore(gateway);
        this.serviceListApi
          .getDetail(
            params.cluster,
            params.namespace,
            this.k8sUtil.getLabel(gateway, 'msname', LabelPrefix),
          )
          .subscribe(
            result => {
              const deployments = get(result, 'spec.deployments', []);
              this.store.setDeployments(deployments);
              this.initGatewayStore(gateway);
              this.microServiceStatus = 'success';
              const serviceName = get(result, 'spec.services[0].name', '');
              this.serviceName = serviceName;
              if (serviceName) {
                this.getServiceEntry(params, serviceName);
              }
            },
            () => {
              this.microServiceStatus = 'error';
            },
          );
        return gateway;
      }),
    );
  };

  getServiceEntry(params: RouteParams, name: string) {
    this.serviceListApi
      .getService(params.cluster, params.namespace, name)
      .pipe(
        map((result: Service) => {
          const res = get(result, 'spec.ports', []);
          if (res.length) {
            const allow = res.some((item: ServicePorts) =>
              ['HTTP', 'HTTP2'].includes(this.getNameProtocol(item)),
            );
            // 判断是否协议中存在http/2，只有http/2才能创建路由
            this.store.allowCreationRouteAction({ protocol: allow });
          }
          return res;
        }),
      )
      .subscribe();
  }

  getNameProtocol = (data: ServicePorts) => {
    const protocol = data.name.split('-')[0];
    return (
      PROTOCOLS.find(value => value.toLowerCase() === protocol) || data.protocol
    );
  };

  dataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: this.fetch,
  });

  updated$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.GATEWAY,
        action: K8sResourceAction.UPDATE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  deleted$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.GATEWAY,
        action: K8sResourceAction.DELETE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  viewMicroservice(name: string) {
    this.router.navigate([
      '/workspace',
      this.project,
      'clusters',
      this.cluster,
      'namespaces',
      this.namespace,
      'servicemesh',
      'service-list',
      name,
    ]);
  }

  update() {
    this.router.navigate(['../', 'update', this.name], {
      relativeTo: this.route,
    });
  }

  delete(resource: Gateway) {
    this.dialog
      .confirm({
        title: this.translate.get('gateway_delete_confirm_title', {
          name: this.name,
        }),
        confirmType: ConfirmType.Danger,
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.apiService.deleteGateway(this.cluster, resource).subscribe(
          () => {
            this.message.success(
              this.translate.get('gateway_deleted_message_success', {
                name: this.name,
              }),
            );
            this.router.navigate(['../'], {
              relativeTo: this.route,
            });
          },
          () => {},
        );
      })
      .catch(() => {});
  }

  refresh() {
    this.dataLoader.reload();
  }

  ngOnDestroy() {
    this.store.initStore();
  }

  private initGatewayStore(gateway: Gateway) {
    this.store.serviceAction(
      this.k8sUtil.getLabel(gateway, 'gatewayhost', LabelPrefix),
    );
    this.store.gatewayInfo.name = this.k8sUtil.getName(gateway);
    this.store.gatewayInfo.port = parseInt(
      this.k8sUtil.getLabel(gateway, 'msport', LabelPrefix),
    );
    this.store.gatewayInfo.msName = this.k8sUtil.getLabel(
      gateway,
      'msname',
      LabelPrefix,
    );
    this.gatewayInit = true;
    this.cdr.markForCheck();
  }
}
