import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FeatureSharedCommonModule } from '@app/features-shared/common';
import { SharedModule } from '@app/shared';
import { ServiceListModule } from '@asm/features/servicemesh-userview/service-list/service-list.module';

import {
  GatewayHostValidatorDirective,
  ServiceGatewayAdvancedInfoComponent,
  ServiceGatewayBasicInfoComponent,
  ServiceGatewayCreateComponent,
  ServiceGatewayDetailComponent,
  ServiceGatewayFormComponent,
  ServiceGatewayListComponent,
  ServiceGatewayUpdateComponent,
} from '../service-gateway';
import { ServicemeshSharedModule } from '../shared/servicemesh-shared.module';
import { RouteModule } from './gateway.routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FeatureSharedCommonModule,
    SharedModule,
    RouterModule,
    RouteModule,
    ReactiveFormsModule,
    ServiceListModule,
    ServicemeshSharedModule,
  ],
  declarations: [
    ServiceGatewayListComponent,
    ServiceGatewayDetailComponent,
    ServiceGatewayBasicInfoComponent,
    ServiceGatewayCreateComponent,
    ServiceGatewayFormComponent,
    ServiceGatewayUpdateComponent,
    ServiceGatewayAdvancedInfoComponent,
    GatewayHostValidatorDirective,
  ],
  exports: [],
  entryComponents: [],
})
export class ServiceGatewayModule {}
