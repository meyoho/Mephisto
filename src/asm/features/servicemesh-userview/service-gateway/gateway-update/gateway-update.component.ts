import { AsyncDataLoader } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceGatewayService } from '@asm/api/service-gateway';

import { map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  selector: 'alo-gateway-update',
  templateUrl: './gateway-update.component.html',
})
export class ServiceGatewayUpdateComponent {
  constructor(
    private readonly route: ActivatedRoute,
    private readonly apiService: ServiceGatewayService,
  ) {}

  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get('name') || '',
      namespace: params.get('namespace') || '',
      cluster: params.get('cluster') || '',
    })),
    publishReplay(1),
    refCount(),
  );

  fetch = (params: { namespace: string; name: string; cluster: string }) =>
    this.apiService.getGatewayDetail(params);

  dataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: this.fetch,
  });
}
