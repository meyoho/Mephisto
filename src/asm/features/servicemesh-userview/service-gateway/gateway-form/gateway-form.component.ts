import {
  K8sUtilService,
  TOKEN_BASE_DOMAIN,
  TranslateService,
} from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectorRef,
  Component,
  Directive,
  Inject,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  NG_ASYNC_VALIDATORS,
  NgForm,
  ValidationErrors,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RESOURCE_DEFINITIONS } from '@app/utils';
import { K8S_RESOURCE_NAME_BASE } from '@app/utils/patterns';
import {
  Gateway,
  LabelPrefix,
  ServiceGatewayService,
} from '@asm/api/service-gateway';
import { MicroService, ServiceListService } from '@asm/api/service-list';
import { ServiceMeshApiService } from '@asm/api/service-mesh/service-mesh-api.service';

import { get, isEqual } from 'lodash-es';
import { of } from 'rxjs';
import {
  catchError,
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

@Component({
  selector: 'alo-gateway-form',
  styleUrls: ['./gateway-form.component.scss'],
  templateUrl: './gateway-form.component.html',
})
export class ServiceGatewayFormComponent implements OnInit {
  constructor(
    private readonly location: Location,
    private readonly microApiService: ServiceListService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly gatewayApiService: ServiceGatewayService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    private readonly serviceMeshApiService: ServiceMeshApiService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {}

  @Input() data: Gateway;
  @ViewChild('form', { static: true })
  form: NgForm;

  namePattern = K8S_RESOURCE_NAME_BASE;
  cluster: string;
  namespace: string;
  submitting = false;
  existedHost: string[] = [];
  microServicePorts: number[] = [];
  nodePort: number;

  params$ = this.route.paramMap.pipe(
    map(params => {
      return {
        namespace: params.get('namespace'),
        cluster: params.get('cluster'),
      };
    }),
    tap(params => {
      this.cluster = params.cluster;
      this.namespace = params.namespace;
      this.resource.metadata.namespace = params.namespace;
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  microservices$ = this.params$.pipe(
    switchMap(params => this.microApiService.getList(params)),
    map(res => res.items),
    startWith([]),
    publishReplay(1),
    refCount(),
  );

  serviceGateways$ = this.params$.pipe(
    switchMap(params => this.gatewayApiService.getServiceGatewayList(params)),
    publishReplay(1),
    refCount(),
  );

  istioIngressGateway$ = this.params$.pipe(
    switchMap(params =>
      this.serviceMeshApiService.getClusterConfig(params.cluster).pipe(
        map(res => {
          return get(res, 'spec.ingressgateway.nodeport', null);
        }),
      ),
    ),
  );

  gatewayFormData = {
    microservice: {
      name: '',
      port: '',
    },
    name: '',
    displayName: '',
    gatewayEntryHost: '',
  };

  resource: Gateway = {
    apiVersion: `${RESOURCE_DEFINITIONS.GATEWAY.apiGroup}/${RESOURCE_DEFINITIONS.GATEWAY.apiVersion}`,
    kind: 'Gateway',
    metadata: {
      name: '',
      namespace: '',
      labels: {},
      annotations: {},
    },
    spec: {
      selector: {
        istio: 'ingressgateway',
      },
      servers: [
        { hosts: [], port: { name: 'http', number: 80, protocol: 'HTTP' } },
      ],
    },
  };

  ngOnInit() {
    if (this.data) {
      this.initUpdateResource();
    }
    this.istioIngressGateway$.subscribe(res => {
      this.nodePort = res;
    });
    this.serviceGateways$.subscribe(res => {
      this.existedHost = res.items.map(item =>
        get(item, `spec.servers[0].hosts[0]`, ''),
      );
    });
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    if (this.data) {
      this.updateResource();
    } else {
      this.createResource();
    }
  }

  cancel() {
    this.location.back();
  }

  getServicePorts(microService: MicroService) {
    const name = get(microService, 'spec.services[0].name');
    this.gatewayApiService
      .getMicroServicePorts(this.cluster, this.namespace, name)
      .pipe(catchError(() => of(null)))
      .subscribe(ports => {
        this.microServicePorts = ports || [];
      });
  }

  handleMicroserviceChanged(name: string) {
    this.microservices$.subscribe(res => {
      const microservice = res.find(
        item => this.k8sUtil.getName(item) === name,
      );
      if (microservice) {
        this.gatewayFormData.microservice.port = '';
        this.getServicePorts(microservice);
      }
    });
  }

  private createResource() {
    this.handleCreateFormData();
    this.gatewayApiService.createGateway(this.cluster, this.resource).subscribe(
      res => {
        this.handleSuccessed(res.metadata.name);
        this.router.navigate([`../${res.metadata.name}`], {
          relativeTo: this.route,
        });
      },
      () => {
        this.handleError();
      },
    );
  }

  private updateResource() {
    this.handleUpdateFormData();
    this.gatewayApiService.updateGateway(this.cluster, this.data).subscribe(
      res => {
        this.handleSuccessed(res.metadata.name, false);
        this.router.navigate([`../../${res.metadata.name}`], {
          relativeTo: this.route,
        });
      },
      () => {
        this.handleError();
      },
    );
  }

  private initUpdateResource() {
    const name = this.k8sUtil.getLabel(this.data, 'msname', LabelPrefix);
    this.gatewayFormData = {
      name: this.k8sUtil.getName(this.data),
      displayName: this.k8sUtil.getDisplayName(this.data),
      microservice: {
        name: this.k8sUtil.getLabel(this.data, 'msname', LabelPrefix),
        port: this.k8sUtil.getLabel(this.data, 'msport', LabelPrefix),
      },
      gatewayEntryHost: get(this.data, 'spec.servers[0].hosts[0]'),
    };
    this.params$
      .pipe(
        switchMap(params =>
          this.microApiService.getDetail(
            params.cluster,
            params.namespace,
            name,
          ),
        ),
      )
      .subscribe((res: MicroService) => {
        this.getServicePorts(res);
      });
  }

  private handleCreateFormData() {
    this.resource.metadata.name = this.gatewayFormData.name;
    this.resource.metadata.annotations = {
      [`${this.baseDomain}/display-name`]:
        this.gatewayFormData.displayName || '',
    };
    this.resource.metadata.labels = {
      [`${LabelPrefix}.${this.baseDomain}/msname`]: this.gatewayFormData
        .microservice.name,
      [`${LabelPrefix}.${this.baseDomain}/msport`]: this.gatewayFormData.microservice.port.toString(),
      [`${LabelPrefix}.${this.baseDomain}/gatewayhost`]: this.gatewayFormData
        .gatewayEntryHost,
      [`${LabelPrefix}.${this.baseDomain}/mscreator`]: 'servicemesh',
      [`${LabelPrefix}.${this.baseDomain}/resource`]: 'gateway',
    };
    this.resource.spec.servers[0].hosts = [
      `${this.gatewayFormData.gatewayEntryHost}`,
    ];
  }

  private handleUpdateFormData() {
    this.data.metadata.annotations[
      `${this.baseDomain}/display-name`
    ] = this.gatewayFormData.displayName;
    this.data.metadata.labels[
      `${LabelPrefix}.${this.baseDomain}/msport`
    ] = this.gatewayFormData.microservice.port.toString();
    this.data.metadata.labels[
      `${LabelPrefix}.${this.baseDomain}/gatewayhost`
    ] = this.gatewayFormData.gatewayEntryHost;
    this.data.spec.servers[0].hosts = [
      `${this.gatewayFormData.gatewayEntryHost}`,
    ];
  }

  private handleSuccessed(name: string, isCreate = true) {
    this.cdr.markForCheck();
    const message = isCreate
      ? 'gateway_created_message_success'
      : 'gateway_updated_message_success';
    this.message.success(this.translate.get(message, { name }));
  }

  private handleError() {
    this.submitting = false;
    this.cdr.markForCheck();
  }
}

@Directive({
  selector: '[aloGatewayHostValidate]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: GatewayHostValidatorDirective,
      multi: true,
    },
  ],
})
export class GatewayHostValidatorDirective {
  @Input('aloGatewayHostValidate') hosts: string[];
  validate(ctrl: AbstractControl): ValidationErrors | null {
    if (!ctrl.value) {
      return null;
    }
    if (this.hosts.includes(ctrl.value)) {
      return of({ hostExisted: true });
    }
    return of(null);
  }
}
