import {
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  TOKEN_BASE_DOMAIN,
  TranslateService,
} from '@alauda/common-snippet';
import { ConfirmType, DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RESOURCE_TYPES } from '@app/utils';
import {
  Gateway,
  LabelPrefix,
  ServiceGatewayService,
} from '@asm/api/service-gateway';
import { ServiceMeshApiService } from '@asm/api/service-mesh/service-mesh-api.service';

import { get, isEqual } from 'lodash-es';
import { combineLatest, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import { ResourceListParams } from 'app/typings';
import { matchLabelsToString, ResourceList } from 'app/utils/resource-list';
@Component({
  templateUrl: './gateway-list.component.html',
  styleUrls: ['./gateway-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceGatewayListComponent {
  columns = [
    'name',
    'gateway_entry',
    'microservice',
    'creator',
    'creationTimestamp',
    'action',
  ];

  loading = false;
  list: ResourceList;
  refresh$ = new Subject<void>();
  notificationVisible = true;
  project: string;
  cluster: string;
  namespace: string;

  params$ = combineLatest([this.route.paramMap, this.route.queryParamMap]).pipe(
    map(([params, queryParams]) => {
      this.loading = true;
      return {
        keywords: queryParams.get('keywords') || '',
        namespace: params.get('namespace'),
        project: params.get('project'),
        cluster: params.get('cluster') || '',
      };
    }),
    tap(params => {
      this.project = params.project;
      this.cluster = params.cluster;
      this.namespace = params.namespace;
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishReplay(1),
    refCount(),
  );

  creatable$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.GATEWAY,
        action: K8sResourceAction.CREATE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  deleted$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.GATEWAY,
        action: K8sResourceAction.DELETE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  fetchParams$ = this.params$.pipe(
    map(params => ({
      namespace: params.namespace,
      cluster: params.cluster,
      keyword: params.keywords || '',
      labelSelector: matchLabelsToString({
        [`servicemesh.${this.baseDomain}/mscreator`]: 'servicemesh',
      }),
    })),
  );

  istioIngressGateway$ = this.params$.pipe(
    switchMap(params =>
      this.serviceMeshApiService.getClusterConfig(params.cluster).pipe(
        map(res => {
          return get(res, 'spec.ingressgateway.nodeport', null);
        }),
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly apiService: ServiceGatewayService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sPermission: K8sPermissionService,
    private readonly serviceMeshApiService: ServiceMeshApiService,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    this.list = new ResourceList({
      fetchParams$: this.fetchParams$,
      fetcher: this.fetch.bind(this),
    });
  }

  fetch = (params: ResourceListParams) => {
    params.limit = '20';
    return this.apiService.getServiceGatewayList(params);
  };

  getHost(data: Gateway) {
    return this.k8sUtil.getLabel(data, 'gatewayhost', LabelPrefix);
  }

  getProtocol(data: Gateway) {
    return get(data, `spec.servers[0].port.protocol`, '').toLocaleLowerCase();
  }

  getServiceEntry = (data: Gateway) => {
    return `${this.getProtocol(data)}://${this.getHost(data)}`;
  };

  getMicroService(data: Gateway) {
    return get(
      data,
      ['metadata', 'labels', `servicemesh.${this.baseDomain}/msname`],
      '',
    );
  }

  createGateway() {
    this.router.navigate(['./', 'create'], {
      relativeTo: this.route,
    });
  }

  viewMicroservice(name: string) {
    this.router.navigate([
      '/workspace',
      this.project,
      'clusters',
      this.cluster,
      'namespaces',
      this.namespace,
      'servicemesh',
      'service-list',
      name,
    ]);
  }

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords, page: 1 },
      queryParamsHandling: 'merge',
    });
  }

  showTip() {
    this.notificationVisible = true;
    this.cdr.markForCheck();
  }

  delete(resource: Gateway) {
    this.dialog
      .confirm({
        title: this.translate.get('gateway_delete_confirm_title', {
          name: this.k8sUtil.getName(resource),
        }),
        confirmType: ConfirmType.Danger,
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.apiService.deleteGateway(this.cluster, resource).subscribe(() => {
          this.message.success(
            this.translate.get('gateway_deleted_message_success', {
              name: this.k8sUtil.getName(resource),
            }),
          );
          this.list.reload();
        });
      })
      .catch(() => null);
  }
}
