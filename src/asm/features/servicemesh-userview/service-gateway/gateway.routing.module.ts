import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
  ServiceGatewayCreateComponent,
  ServiceGatewayDetailComponent,
  ServiceGatewayListComponent,
  ServiceGatewayUpdateComponent,
} from '../service-gateway';

const routes: Routes = [
  {
    path: '',
    component: ServiceGatewayListComponent,
  },
  {
    path: 'create',
    component: ServiceGatewayCreateComponent,
  },
  {
    path: ':name',
    component: ServiceGatewayDetailComponent,
  },
  {
    path: 'update/:name',
    component: ServiceGatewayUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RouteModule {}
