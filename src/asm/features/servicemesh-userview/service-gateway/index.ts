export * from './gateway-list/gateway-list.component';
export * from './gateway-detail/detail.component';
export * from './gateway-detail/basic-info/basic-info.component';
export * from './gateway-create/gateway-create.component';
export * from './gateway-form/gateway-form.component';
export * from './gateway-update/gateway-update.component';
export * from './gateway-detail/advanced-info/advanced-info.component';
