import {
  K8sPermissionService,
  K8sResourceAction,
  ResourceListParams,
} from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RESOURCE_TYPES } from '@app/utils';
import { ServiceListService } from '@asm/api/service-list/service-list.service';

import { isEqual } from 'lodash-es';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import { ResourceList } from 'app/utils/resource-list';
@Component({
  selector: 'alo-service-list',
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceListComponent {
  loading = false;
  list: ResourceList;
  cluster: string;
  params$ = combineLatest([this.route.paramMap, this.route.queryParamMap]).pipe(
    map(([params, queryParams]) => {
      return {
        keywords: queryParams.get('keywords'),
        namespace: params.get('namespace'),
        cluster: params.get('cluster'),
      };
    }),
    tap(params => {
      this.cluster = params.cluster;
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishReplay(1),
    refCount(),
  );

  creatable$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.MICROSERVICE,
        action: K8sResourceAction.CREATE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  deleted$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.MICROSERVICE,
        action: K8sResourceAction.DELETE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  fetchParams$ = this.params$.pipe(
    map(params => ({
      namespace: params.namespace,
      cluster: params.cluster,
      keyword: params.keywords || '',
    })),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly api: ServiceListService,
    private readonly k8sPermission: K8sPermissionService,
  ) {
    this.list = new ResourceList({
      fetchParams$: this.fetchParams$,
      fetcher: this.fetch.bind(this),
    });
  }

  fetch = (params: ResourceListParams) => {
    params.limit = '20';
    return this.api.getList(params);
  };

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords },
      queryParamsHandling: 'merge',
    });
  }
}
