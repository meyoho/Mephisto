import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'alo-deploy-tags',
  templateUrl: './deploy-tags.component.html',
  styleUrls: ['./deploy-tags.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeployTagsComponent {
  @Input() deployments: { [key: string]: string }[];
  constructor() {}
}
