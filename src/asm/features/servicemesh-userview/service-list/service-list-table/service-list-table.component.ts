import {
  baseDomain,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import { ConfirmType, DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MicroService, ServiceListService } from '@asm/api/service-list';
@Component({
  selector: 'alo-service-list-table',
  templateUrl: './service-list-table.component.html',
  styleUrls: ['./service-list-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceListTableComponent {
  @Input()
  data: [];

  @Input()
  deleted: boolean;

  @Input()
  cluster: string;

  @Output()
  reload = new EventEmitter();

  prefix = baseDomain;
  columns = ['name', 'workload', 'create_by', 'created_at', 'action'];

  constructor(
    private readonly api: ServiceListService,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  delete(resource: MicroService) {
    this.dialog
      .confirm({
        title: this.translate.get('microservice_delete_confirm_title', {
          name: this.k8sUtil.getName(resource),
        }),
        confirmType: ConfirmType.Danger,
        content: this.translate.get('microservice_delete_confirm_body'),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.api.delete(this.cluster, resource).subscribe(() => {
          this.reload.emit();
        });
      })
      .catch(() => false);
  }
}
