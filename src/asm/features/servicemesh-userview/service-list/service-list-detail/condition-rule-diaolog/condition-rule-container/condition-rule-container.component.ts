import { Component, Injector, Input, OnInit } from '@angular/core';
import { getSubset } from '@asm/api/route-management';
import { MicroServiceStore } from '@asm/api/service-list';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
@Component({
  selector: 'alo-condition-rule-container',
  templateUrl: './condition-rule-container.component.html',
  styleUrls: ['./condition-rule-container.component.scss'],
})
export class ConditionRuleContainerComponent
  extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input() routeModel: any;
  @Input() index: number;
  @Input() deployment: { name: string; version: string };
  constructor(injector: Injector, public readonly store: MicroServiceStore) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  conditionModel: any;

  createForm() {
    return this.fb.group({
      match: this.fb.control([]),
      route: this.fb.group({
        destination: this.fb.group({
          host: this.fb.control(''),
          subset: this.fb.control(''),
        }),
        weight: this.fb.control(''),
      }),
    });
  }

  adaptFormModel(form: { [key: string]: any }) {
    const { route, ...res } = form;
    return {
      ...res,
      route: Array.isArray(route) ? route : [route],
    };
  }

  getDefaultFormModel(): any {
    return {
      match: [],
      route: {
        destination: {
          host: this.store.serviceName || this.deployment.name,
          subset: getSubset(this.deployment.version, true),
        },
        weight: 100,
      },
    };
  }
}
