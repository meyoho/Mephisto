import { TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import {
  getSubset,
  RouteManagementService,
  RouterHttp,
  RouterManageRouter,
} from '@asm/api/route-management';
import { MicroServiceStore } from '@asm/api/service-list';

import { get } from 'lodash-es';
@Component({
  selector: 'alo-condition-rule-diaolog',
  templateUrl: './condition-rule-diaolog.component.html',
  styleUrls: ['./condition-rule-diaolog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConditionRuleDiaologComponent implements OnInit {
  @ViewChild(NgForm, { static: true })
  form: NgForm;

  conditionModel: RouterHttp;
  index: number;
  constructor(
    private readonly dialogRef: DialogRef<ConditionRuleDiaologComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      cluster: string;
      deployment: { name: string; version: string };
    },
    private readonly store: MicroServiceStore,
    private readonly apiService: RouteManagementService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.findCurrentCondition();
  }

  formChange(form: RouterHttp) {
    this.conditionModel = form;
    this.cdr.markForCheck();
  }

  findCurrentCondition() {
    const conditions: RouterHttp[] = get(
      this.store.virtualService,
      'spec.condition.http',
      [],
    );
    this.index = conditions.length;
    const condition = conditions.find((item, index) => {
      const every = item.route.every(
        (route: RouterManageRouter) =>
          getSubset(route.destination.subset) === this.data.deployment.version,
      );
      if (every) {
        this.index = index;
      }
      return every;
    });
    this.conditionModel = condition;
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      // 修改完version后，如果添加条件规则需要重新触发更新destinationrule，
      // 避免添加条件规则后，destinationrule 没有更新
      this.store.triggerCheckVersion$.next(null);
      const payload = this.handlePayload();
      this.apiService
        .updateVirtualService(this.data.cluster, payload)
        .subscribe(() => {
          this.message.success(
            this.translate.get('condition_rule_set_successfully'),
          );
          this.dialogRef.close(true);
        });
    }
  }

  handlePayload() {
    const { conditionForm } = this.form.value;
    const {
      spec: {
        condition: { http, ...conditionRes },
        ...sepcRes
      },
      ...res
    } = this.store.virtualService;
    const https =
      this.index === http.length
        ? [...http, conditionForm]
        : http.map((item, i) => {
            if (i === this.index) {
              return conditionForm;
            }
            return Array.isArray(item) ? item[0] : item;
          });

    return {
      ...res,
      spec: {
        ...sepcRes,
        condition: {
          ...conditionRes,
          http: https,
        },
      },
    };
  }
}
