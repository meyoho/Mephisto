import { TranslateService } from '@alauda/common-snippet';
import { Component, Injector, Input } from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { POSITIVE_INT_PATTERN } from '@app/utils/patterns';
import {
  PROTOCOLS,
  ServicePorts,
} from '@asm/api/service-list/service-list.types';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
@Component({
  selector: 'alo-service-ports',
  templateUrl: './service-ports.component.html',
  styleUrls: ['./service-ports.component.scss'],
})
export class ServicePortsComponent extends BaseResourceFormArrayComponent {
  @Input() submitted: boolean;
  protocols = PROTOCOLS;
  positiveIntPattern = POSITIVE_INT_PATTERN;
  constructor(
    injector: Injector,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): ServicePorts[] {
    return [
      {
        protocol: '',
        port: null,
        targetPort: null,
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      protocol: this.fb.control('', [Validators.required]),
      port: this.fb.control('', [
        Validators.required,
        this.samePortValidator,
        Validators.pattern(this.positiveIntPattern.pattern),
      ]),
      targetPort: this.fb.control('', [
        Validators.required,
        this.sameTargetPortValidator,
        Validators.pattern(this.positiveIntPattern.pattern),
      ]),
    });
  }

  samePortValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (!control || !control.value || control.hasError('pattern')) {
      return;
    }
    const parentValue = control.parent.value;
    const samePort = this.form.controls.some(
      form =>
        parentValue.port !== String(form.value.port) &&
        String(form.value.port) === control.value,
    );
    return !samePort ? null : { samePort: true };
  };

  sameTargetPortValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (!control || !control.value || control.hasError('pattern')) {
      return;
    }
    const parentValue = control.parent.value;
    const sameTargetPort = this.form.controls.some(
      form =>
        parentValue.targetPort !== String(form.value.targetPort) &&
        String(form.value.targetPort) === control.value,
    );
    return !sameTargetPort ? null : { sameTargetPort: true };
  };

  rowBackgroundColorFn(row: FormControl) {
    return row.invalid && row.touched ? '#fdefef' : '';
  }

  getRowValidate(control: AbstractControl) {
    const port = control.get('port');
    const targetPort = control.get('targetPort');
    const protocol = control.get('protocol');
    const validateMap: { [key: string]: boolean } = {
      protocolRequired:
        (protocol.touched || this.submitted) && protocol.hasError('required'),
      portPatter: port.hasError('pattern'),
      samePort: port.hasError('samePort'),
      targetPortPatter: targetPort.hasError('pattern'),
      sameTargetPort: targetPort.hasError('sameTargetPort'),
      portRequired:
        (port.touched || this.submitted) && port.hasError('required'),
      targetPortRequired:
        (targetPort.touched || this.submitted) &&
        targetPort.hasError('required'),
    };
    return Object.keys(validateMap).reduce((prev, curr) => {
      if (validateMap[curr]) {
        return [...prev, curr];
      }
      return [...prev];
    }, []);
  }

  getTranslate = (name: string) => this.translate.get(name);

  getErrorTip = (value: string) => {
    switch (value) {
      case 'protocolRequired':
        return this.translate.get('not_null_tip', {
          name: this.getTranslate('protocol'),
        });
      case 'portPatter':
        return this.getTranslate('port_IntPattern_tip');
      case 'targetPortPatter':
        return this.getTranslate('target_port_IntPattern_tip');
      case 'samePort':
        return this.getTranslate('port_exists_tip');
      case 'sameTargetPort':
        return this.getTranslate('target_port_exists_tip');
      case 'portRequired':
        return this.getTranslate('port_required_tip');
      case 'targetPortRequired':
        return this.getTranslate('target_port_required_tip');
      default:
        break;
    }
  };
}
