import { Component, Inject, Injector, Input, OnInit } from '@angular/core';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RouteParams } from '@app/typings';
import { RESOURCE_DEFINITIONS } from '@app/utils';
import { K8S_SERVICE_NAME } from '@app/utils/patterns';
import {
  DeploymentItem,
  PROTOCOLS,
  Service,
  ServiceSpec,
} from '@asm/api/service-list/service-list.types';

import { cloneDeep, get, set, toNumber } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
@Component({
  selector: 'alo-service-form',
  templateUrl: './service-form.component.html',
  styleUrls: ['./service-form.component.scss'],
})
export class ServiceFormComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  resource = RESOURCE_DEFINITIONS.SERVICE;
  servicePattern = K8S_SERVICE_NAME;
  @Input() submitted: boolean;
  @Input() paramsData: {
    routeParams: RouteParams;
    update: boolean;
    deployments: DeploymentItem[];
    name: string;
  };

  constructor(
    injector: Injector,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {
    super(injector);
  }

  get deployments(): string[] {
    return this.paramsData.deployments.map(item => item.name);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  createForm() {
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: this.fb.group({
        namespace: this.fb.control(''),
        name: this.fb.control(''),
        annotations: this.fb.group({
          deployments: this.fb.control([]),
        }),
      }),
      spec: this.fb.group({
        ports: this.fb.control([]),
      }),
    });
  }

  getDefaultFormModel(): Service {
    return {
      apiVersion: this.resource.apiVersion,
      kind: 'Service',
      metadata: {
        name: '',
        namespace: this.paramsData.routeParams.namespace,
        annotations: {
          deployments: null,
        },
      },
      spec: {
        ports: [
          {
            protocol: '',
            port: null,
            targetPort: null,
          },
        ],
      },
    };
  }

  adaptResourceModel(resource: Service) {
    if (!resource) {
      return {};
    }
    const {
      spec: { ports = [], ...resSpec },
      ...res
    } = resource;
    const portList = ports.map(item => {
      const protocol = item.name.split('-')[0];
      return {
        ...item,
        protocol: PROTOCOLS.find(value => value.toLowerCase() === protocol),
      };
    });
    return {
      ...res,
      spec: { ...resSpec, ports: portList },
    };
  }

  adaptFormModel(form: { [key: string]: any }) {
    const cloneForm = cloneDeep(form) || {};
    const metadata = get(cloneForm, 'metadata', {});
    const spec = get(cloneForm, 'spec') as ServiceSpec;
    const { annotations } = metadata;

    set(spec, 'selector', {
      [`${this.asmBaseDomain}/msselector`]: this.paramsData.routeParams.name,
    });
    if (annotations) {
      annotations[
        `${this.asmBaseDomain}/msdeployments`
      ] = this.deployments.join(',');
      cloneForm.metadata.annotations = annotations;
    }
    spec.ports = spec.ports.map(item => ({
      ...item,
      protocol: 'TCP',
      name: `${item.protocol.toLowerCase()}-${item.port}-${item.targetPort}`,
      port: toNumber(item.port),
      targetPort: toNumber(item.targetPort),
    }));
    return cloneForm;
  }
}
