import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import { DialogRef, DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { RouteParams } from '@app/typings';
import { ServiceListService } from '@asm/api/service-list/service-list.service';
import {
  DeploymentItem,
  Service,
} from '@asm/api/service-list/service-list.types';

@Component({
  selector: 'alo-service-dialog',
  templateUrl: './service-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceDialogComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;

  serviceModel: Service;
  submitting = false;
  constructor(
    private readonly dialogRef: DialogRef<ServiceDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      routeParams: RouteParams;
      update: boolean;
      deployments: DeploymentItem[];
      name: string;
    },
    private readonly message: MessageService,
    private readonly api: ServiceListService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    if (this.data.name && this.data.update) {
      this.getService();
    }
  }

  getService() {
    const { cluster, namespace } = this.data.routeParams;
    this.api
      .getService(cluster, namespace, this.data.name)
      .subscribe((result: Service) => {
        this.serviceModel = result;
        this.cdr.markForCheck();
      });
  }

  confirm() {
    this.form.onSubmit(null);
    const { serviceForm } = this.form.value;
    if (this.form.valid) {
      this.submitting = true;
      if (!this.data.update) {
        this.api.createService(this.data.routeParams, serviceForm).subscribe(
          (result: Service) => {
            this.handleSucc(this.k8sUtil.getName(result), 'created');
            this.dialogRef.close(true);
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
      } else {
        this.api
          .updateService(this.data.routeParams, serviceForm, this.data.name)
          .subscribe(
            (result: Service) => {
              this.handleSucc(this.k8sUtil.getName(result), 'update');
              this.dialogRef.close(true);
            },
            () => {
              this.submitting = false;
              this.cdr.markForCheck();
            },
          );
      }
    }
  }

  private handleSucc(name: string, action: string) {
    this.submitting = false;
    this.cdr.markForCheck();
    this.message.success(
      this.translate.get(`service_entry_${action}_successfully`, {
        name: name,
      }),
    );
  }
}
