import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { RouteParams } from '@app/typings';
import { DeploymentItem, MicroService } from '@asm/api/service-list';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alo-service-version-form',
  templateUrl: './service-version-form.component.html',
  styleUrls: ['./service-version-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceVersionFormComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input() fillDeployments: DeploymentItem[];
  @Input() submitted: boolean;
  constructor(
    injector: Injector,
    @Inject(DIALOG_DATA)
    public data: {
      routeParams: RouteParams;
      add: boolean;
      detail: MicroService;
    },
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  createForm() {
    return this.fb.group({
      deployments: this.fb.control([]),
    });
  }

  getDefaultFormModel(): any {
    return {
      deployments: [],
    };
  }
}
