import { K8sApiService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import { SUBSET_PATTERN } from '@app/utils/patterns';
import { DeploymentItem, ServiceVersion } from '@asm/api/service-list';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
@Component({
  selector: 'alo-create-service-version-container',
  templateUrl: './create-container.component.html',
  styleUrls: ['./create-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateServiceVersionContainerComponent
  extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input() params: RouteParams;
  @Input() baseWorkloads: DeploymentItem[];
  @Input() tagMap: { [key: string]: string[] };
  @Input() submitted: boolean;
  @Input() displayInitContainerImages = false;
  @Output() baseWorkloadChanged = new EventEmitter();
  namePattern = SUBSET_PATTERN;
  createForm() {
    return this.fb.group({
      baseWorkload: this.fb.control(''),
      version: this.fb.control('', [this.sameVersionValidator]),
      images: this.fb.control([]),
      initContainerImages: this.fb.control([]),
    });
  }

  getDefaultFormModel(): ServiceVersion {
    return {
      baseWorkload: '',
      version: '',
      images: [],
      initContainerImages: [],
    };
  }

  constructor(
    public injector: Injector,
    private readonly k8sApi: K8sApiService,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  displayBaseWorkload = (baseWorkload: DeploymentItem) => {
    return `${baseWorkload.name} (${baseWorkload.version})`;
  };

  handleBaseWorkloadChanged(control: AbstractControl) {
    if (control.value) {
      this.k8sApi
        .getResource({
          type: RESOURCE_TYPES.DEPLOYMENT,
          name: control.value,
          namespace: this.params.namespace,
          cluster: this.params.cluster,
        })
        .subscribe(res => {
          this.baseWorkloadChanged.next(res);
        });
    }
  }

  sameVersionValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (!control.value) {
      return null;
    }
    const sameVersion = this.baseWorkloads.some(item => {
      return item.version === control.value;
    });
    return sameVersion ? { sameVersion: true } : null;
  };
}
