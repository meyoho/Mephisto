import { TranslateService } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { RouteParams } from '@app/typings';
import { DeploymentItem } from '@asm/api/service-list';

import { get } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

@Component({
  selector: 'alo-images-table',
  templateUrl: './images-table.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImagesTableComponent extends BaseResourceFormArrayComponent
  implements OnInit {
  @Input() params: RouteParams;
  @Input() submitted: boolean;
  @Input() tagMap: { [key: string]: string[] };
  microserviceMap: Map<
    string,
    {
      deployments: DeploymentItem[];
    }
  > = new Map();

  constructor(
    injector: Injector,
    private readonly translate: TranslateService,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel() {
    return [
      {
        path: '',
        tag: '',
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  handleTags(control: AbstractControl): string[] {
    return get(this.tagMap, [control.get('path').value], []);
  }

  private createNewControl() {
    return this.fb.group({
      path: this.fb.control('', [Validators.required]),
      tag: this.fb.control('', [Validators.required]),
    });
  }

  rowBackgroundColorFn(row: FormControl) {
    return row.invalid && row.touched ? '#fdefef' : '';
  }

  tableVerificationMessage(control: AbstractControl) {
    const tag = control.get('tag');
    const getTranslate = (name: string, params?: any): string =>
      this.translate.get(name, params);
    return [
      {
        disabled: (tag.touched || this.submitted) && tag.hasError('required'),
        tip: getTranslate('not_null_tip', { name: getTranslate('version') }),
      },
    ].filter(item => item.disabled);
  }
}
