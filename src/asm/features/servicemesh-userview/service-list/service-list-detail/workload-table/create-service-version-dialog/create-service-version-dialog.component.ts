import { TranslateService } from '@alauda/common-snippet';
import { DialogRef, DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { Deployment, RouteParams } from '@app/typings';
import { ImageRepositoryApiService } from '@asm/api/image/image-api.service';
import {
  DeploymentItem,
  getDeployments,
  MicroService,
  ServiceListService,
  ServiceVersion,
} from '@asm/api/service-list';

import { get, omit } from 'lodash-es';

@Component({
  templateUrl: './create-service-version-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateServiceVersionDialogComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;

  submitting = false;
  displayInitContainerImages = false;
  model: ServiceVersion = {
    baseWorkload: '',
    version: '',
    images: [],
    initContainerImages: [],
  };

  currentDeployment: Deployment;
  imageRepository: Array<{ name: string; image: string }> = [];
  tagMap: { [key: string]: string[] } = {};

  get baseWorkloads(): DeploymentItem[] {
    return getDeployments(this.data.detail);
  }

  constructor(
    private readonly dialogRef: DialogRef<CreateServiceVersionDialogComponent>,
    private readonly message: MessageService,
    private readonly api: ServiceListService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly imageApi: ImageRepositoryApiService,
    @Inject(DIALOG_DATA)
    public data: {
      routeParams: RouteParams;
      detail: MicroService;
    },
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  ngOnInit() {
    this.imageApi
      .getImageRepository(this.data.routeParams.project)
      .subscribe(res => {
        this.imageRepository = res.items.map(item => ({
          name: item.name,
          image: item.image,
        }));
      });
  }

  baseWorkloadChanged(deployment: Deployment) {
    this.currentDeployment = deployment;
    const images = deployment.spec.template.spec.containers.map(
      container => container.image,
    );
    const initContainerImages = (
      deployment.spec.template.spec.initContainers || []
    ).map(container => container.image);

    this.displayInitContainerImages = initContainerImages.length > 0;
    images.concat(initContainerImages).forEach(image => {
      const index = this.imageRepository.findIndex(
        item => item.image === image,
      );
      if (index >= 0) {
        this.imageApi
          .getImageTags(
            this.data.routeParams.project,
            this.imageRepository[index].name,
          )
          .subscribe(res => {
            this.tagMap[this.imageRepository[index].name] = (
              res.items || []
            ).map(tag => tag.name);
          });
      }
    });

    this.form.controls.createServiceVersionForm.setValue({
      ...this.model,
      images: images.map(image => this.splitImageAddress(image)),
      initContainerImages: initContainerImages.map(initContainerImage =>
        this.splitImageAddress(initContainerImage),
      ),
    });
    this.cdr.markForCheck();
  }

  create() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    this.api
      .createServiceVersion(
        this.data.routeParams.cluster,
        this.data.routeParams.namespace,
        this.data.routeParams.name,
        this.handlePayload(this.currentDeployment, this.model),
        this.model.version,
      )
      .subscribe(
        () => {
          this.message.success(
            this.translate.get('workload_add_successfully', {
              name: name,
            }),
          );
          this.dialogRef.close(true);
        },
        () => {
          this.submitting = false;
          this.cdr.markForCheck();
        },
      );
  }

  handlePayload(deployment: Deployment, model: ServiceVersion): Deployment {
    const { template, selector, ...specRest } = deployment.spec;
    const { spec, metadata, ...templateRest } = template;
    const { containers, initContainers, ...tempSpecRest } = spec;
    const version = get(deployment, 'spec.selector.matchLabels.version');
    let deploySelector = selector;
    if (version) {
      deploySelector = omit(selector, 'matchLabels.version');
      deploySelector.matchLabels[`${this.asmBaseDomain}.version`] =
        model.version;
      metadata.labels[`${this.asmBaseDomain}.version`] = model.version;
      metadata.labels.version = model.version;
    }

    return {
      ...deployment,
      spec: {
        selector: {
          ...deploySelector,
        },
        ...specRest,
        template: {
          metadata: {
            ...metadata,
          },
          ...templateRest,
          spec: {
            ...tempSpecRest,
            ...((initContainers || []).length > 0
              ? {
                  initContainers: initContainers.map((container, index) => ({
                    ...container,
                    image: `${model.initContainerImages[index].path}:${model.initContainerImages[index].tag}`,
                  })),
                }
              : {}),
            containers: containers.map((container, index) => ({
              ...container,
              image: `${model.images[index].path}:${model.images[index].tag}`,
            })),
          },
        },
      },
    };
  }

  private splitImageAddress(imageAddress: string) {
    const splitImage = imageAddress.split(':');
    const tag = splitImage.pop();
    const path = splitImage.join(':');
    return { path, tag };
  }
}
