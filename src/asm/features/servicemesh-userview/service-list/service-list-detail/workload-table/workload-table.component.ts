import {
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import {
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import { getSubset, RouterHttp } from '@asm/api/route-management';
import {
  DeploymentItem,
  getDeployments,
  MicroService,
  MicroServiceStore,
  ServiceListService,
} from '@asm/api/service-list';

import { get, isEqual } from 'lodash-es';
import { merge, Subject } from 'rxjs';
import { distinctUntilChanged, map, takeUntil } from 'rxjs/operators';

import { ConditionRuleDiaologComponent } from '../condition-rule-diaolog/condition-rule-diaolog.component';
import { AddServiceVersionDialogComponent } from './add-service-version-dialog/add-service-version-dialog.component';
import { CreateServiceVersionDialogComponent } from './create-service-version-dialog/create-service-version-dialog.component';
import { ServiceVersionDialogComponent } from './service-version-dialog/service-version-dialog.component';

@Component({
  selector: 'alo-workload-table',
  templateUrl: './workload-table.component.html',
  styleUrls: ['./workload-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WorkloadTableComponent implements OnInit, OnDestroy {
  @Input() data: MicroService;
  @Input() allowUpdate: boolean;
  @Output() refresh = new EventEmitter();
  @Input() routeParams: RouteParams;
  dialogRef: DialogRef;
  columns = ['workload', 'version', 'operating', 'actions'];
  disabled = false;
  allowRoutingUpdate = true;
  podSelectorData: DeploymentItem[] = [];
  private readonly onDestroy$$ = new Subject<void>();
  get deployments() {
    return getDeployments(this.data);
  }

  constructor(
    private readonly dialog: DialogService,
    private readonly api: ServiceListService,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly store: MicroServiceStore,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    this.getDeployments();
    merge(
      this.store.allowCreationRoute$.pipe(map(item => item.existRoute)),
      this.store.workloadInfo$,
    )
      .pipe(takeUntil(this.onDestroy$$), distinctUntilChanged(isEqual))
      .subscribe(() => {
        this.cdr.markForCheck();
      });

    this.k8sPermission
      .isAllowed({
        type: RESOURCE_TYPES.VIRTUALSERVICE,
        action: K8sResourceAction.UPDATE,
        namespace: this.routeParams.namespace,
        cluster: this.routeParams.cluster,
      })
      .pipe(takeUntil(this.onDestroy$$))
      .subscribe(res => {
        this.allowRoutingUpdate = res;
        this.cdr.markForCheck();
      });
  }

  getDeployments() {
    this.api
      .getRelation(this.routeParams.namespace, this.routeParams.cluster)
      .pipe(
        map(result => {
          return result.reduce((prev, curr) => {
            const matchLabels = get(
              curr.deployment,
              'spec.selector.matchLabels',
              {},
            );
            const labelVersion = get(matchLabels, 'version');
            if (labelVersion) {
              return [
                ...prev,
                {
                  name: this.k8sUtil.getName(curr.deployment),
                  labelVersion,
                },
              ];
            }
            return prev;
          }, []);
        }),
      )
      .subscribe(result => {
        this.podSelectorData = result;
      });
  }

  openVersionDialog() {
    const dialogRef = this.dialog.open(ServiceVersionDialogComponent, {
      size: DialogSize.Big,
      data: {
        routeParams: this.routeParams,
        detail: this.data,
      },
    });
    this.afterClosedDialog(dialogRef);
  }

  openCreateVersionDialog() {
    const dialogRef = this.dialog.open(CreateServiceVersionDialogComponent, {
      size: DialogSize.Big,
      data: {
        routeParams: this.routeParams,
        detail: this.data,
      },
    });
    this.afterClosedDialog(dialogRef);
  }

  afterClosedDialog(dialogRef: DialogRef<any>) {
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.refresh.next();
      }
    });
  }

  delete(name: string) {
    this.dialog
      .confirm({
        title: this.translate.get('delete_workload_title', { name }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        const {
          spec: { deployments, ...resSepc },
          ...rest
        } = this.data;
        const payload = {
          ...rest,
          spec: {
            ...resSepc,
            deployments: deployments.filter(item => item.name !== name),
          },
        };
        this.api.update(this.routeParams.cluster, payload).subscribe(() => {
          this.message.success(
            this.translate.get(`workload_remove_successfully`, { name }),
          );
          this.refresh.next();
        });
      })
      .catch(() => {});
  }

  openUpdateDialog(deployment: DeploymentItem) {
    this.dialog
      .open(AddServiceVersionDialogComponent, {
        data: {
          routeParams: this.routeParams,
          detail: this.data,
          deployment: deployment,
        },
      })
      .afterClosed()
      .subscribe(res => {
        this.dialogCloseCallback(res);
      });
  }

  openSetConditionalRules(deployment: { name: string; version: string }) {
    this.dialog
      .open(ConditionRuleDiaologComponent, {
        size: DialogSize.Big,
        data: {
          cluster: this.routeParams.cluster,
          deployment,
        },
      })
      .afterClosed()
      .subscribe(res => {
        this.dialogCloseCallback(res);
      });
  }

  dialogCloseCallback(res: boolean) {
    if (res) {
      this.refresh.emit();
    }
  }

  associatedRouteWeight = (deploy: DeploymentItem) => {
    return this.store.weightExistence.some(item => {
      return item.destination.host === deploy.name;
    });
  };

  existenceVersionPodSelector = (deploy: DeploymentItem) => {
    return this.podSelectorData.some(
      item => item.name === deploy.name && item.labelVersion,
    );
  };

  getUpdateOperation(item: DeploymentItem) {
    switch (true) {
      case this.associatedRouteWeight(item):
        return {
          disabled: true,
          tip: 'existence_route_workload_update_tip',
        };
      case this.associatedConditionRule(item):
        return {
          disabled: true,
          tip: 'existence_condition_rule_workload_update_tip',
        };
      case this.existenceVersionPodSelector(item):
        return {
          disabled: true,
          tip: 'version_ps_being_used_tip',
        };
      default:
        return { disabled: false };
        break;
    }
  }

  associatedConditionRule = (deploy: DeploymentItem) => {
    const conditions = get(
      this.store.virtualService,
      'spec.condition.http',
      [],
    ) as RouterHttp[];
    return conditions.some(item => {
      return item.route.some(
        route => getSubset(route.destination.subset) === deploy.version,
      );
    });
  };

  get itemDisabled() {
    return !this.store.routeCreatePermission.existRoute;
  }

  ngOnDestroy() {
    this.onDestroy$$.next();
  }
}
