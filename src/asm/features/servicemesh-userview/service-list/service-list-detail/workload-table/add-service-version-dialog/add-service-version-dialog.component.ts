import { TranslateService } from '@alauda/common-snippet';
import { DialogRef, DIALOG_DATA, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Directive,
  Inject,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  NgForm,
  NG_ASYNC_VALIDATORS,
  ValidationErrors,
} from '@angular/forms';
import { RouteParams } from '@app/typings';
import { SUBSET_PATTERN } from '@app/utils/patterns';
import {
  DeploymentItem,
  getDeployments,
  MicroService,
  ServiceListService,
} from '@asm/api/service-list';

import { cloneDeep, set } from 'lodash-es';
import { of } from 'rxjs';
@Component({
  selector: 'alo-add-service-version-dialog',
  templateUrl: './add-service-version-dialog.component.html',
  styleUrls: ['./add-service-version-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddServiceVersionDialogComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;

  subsetPattern = SUBSET_PATTERN;
  deployment: DeploymentItem = { name: '', version: '' };
  constructor(
    private readonly dialogRef: DialogRef<AddServiceVersionDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      routeParams: RouteParams;
      detail: MicroService;
      deployment: DeploymentItem;
    },
    private readonly message: MessageService,
    private readonly api: ServiceListService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.deployment = cloneDeep(this.data.deployment);
    this.cdr.markForCheck();
  }

  update() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      const { name } = this.deployment;
      const { routeParams, detail } = this.data;
      const deployments: DeploymentItem[] = getDeployments(detail);
      const index = deployments.findIndex(item => item.name === name);
      deployments[index] = this.deployment;
      const payload = set(detail, 'spec.deployments', deployments);
      this.api.update(routeParams.cluster, payload).subscribe(() => {
        this.message.success(
          this.translate.get(`update_service_version_successfully`),
        );
        this.cdr.markForCheck();
        this.dialogRef.close(true);
      });
    }
  }
}

@Directive({
  selector: '[aloCannotBeAllValidator]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: CannotBeAllValidatorDirective,
      multi: true,
    },
  ],
})
export class CannotBeAllValidatorDirective {
  @Input() deployment: DeploymentItem;
  @Input() detail: MicroService;
  // 验证修改的deployment是否存在相同版本以及 不能输入all
  validate(ctrl: AbstractControl): ValidationErrors | null {
    if (!ctrl.value) {
      return null;
    }
    const deployments: DeploymentItem[] = getDeployments(this.detail);
    const sameVersion = deployments.some(
      item => item.version === ctrl.value && item.name !== this.deployment.name,
    );
    if (sameVersion) {
      return of({ sameVersion: true });
    }
    return ctrl.value === 'all' ? of({ connotBeAll: true }) : of(null);
  }
}
