import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import {
  DialogRef,
  DialogService,
  DIALOG_DATA,
  MessageService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { RouteParams } from '@app/typings';
import {
  DeploymentItem,
  getDeployments,
  MicroService,
  MicroServiceStore,
  ServiceEntrance,
  ServiceListService,
} from '@asm/api/service-list';

import { get, set } from 'lodash-es';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
@Component({
  selector: 'alo-service-version-dialog',
  templateUrl: './service-version-dialog.component.html',
  styleUrls: ['./service-version-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceVersionDialogComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;

  submitting = false;
  module: {
    deployments: DeploymentItem[];
  };

  fillDeployments: DeploymentItem[];
  constructor(
    private readonly dialogRef: DialogRef<ServiceVersionDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      routeParams: RouteParams;
      add: boolean;
      detail: MicroService;
    },
    private readonly message: MessageService,
    private readonly api: ServiceListService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly store: MicroServiceStore,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialog: DialogService,
  ) {}

  ngOnInit() {
    this.getServiceRelation();
  }

  getServiceRelation() {
    // 在详情添加服务版本，需要获取当前svc是否存在了其他关联的deployment并回填到table中
    if (this.store.serviceName) {
      this.api
        .getRelation(
          this.data.routeParams.namespace,
          this.data.routeParams.cluster,
          this.store.serviceName,
        )
        .pipe(catchError(() => of([])))
        .subscribe((result: ServiceEntrance[]) => {
          const existedDeployments = (get(
            this.data,
            'detail.spec.deployments',
            [],
          ) as DeploymentItem[]).map(item => item.name);
          const deployments: DeploymentItem[] = (result || []).reduce(
            (prev, curr) => {
              const name = this.k8sUtil.getName(curr.deployment);
              const version: string = get(
                curr.deployment,
                'spec.template.metadata.labels.version',
                '',
              );
              if (existedDeployments.includes(name)) {
                return prev;
              }
              return [...prev, { name, version }];
            },
            [],
          );
          this.fillDeployments = deployments;
          this.module = { deployments };
          this.cdr.markForCheck();
        });
    } else {
      this.fillDeployments = [];
    }
  }

  confirm() {
    this.form.onSubmit(null);
    const {
      versionForm: { deployments = [] },
    } = this.form.value;
    if (this.form.valid) {
      this.associatedHandle().subscribe((result: ServiceEntrance[]) => {
        const payload = this.getModel(deployments);
        if (result) {
          const service = result[0].services;
          set(payload, 'spec.services', service);
          const deployments = getDeployments(payload);
          const { workloadNames } = this.api.getRelationDeployName(
            result,
            deployments,
          );
          if (workloadNames) {
            this.dialog.confirm({
              title: this.translate.get('workload_relation_tip', {
                name: this.store.selectedRelation.deployment.name,
                workloads: workloadNames,
              }),
              confirmText: this.translate.get('i_know'),
              cancelButton: false,
            });
            return;
          }
        }
        this.submitting = true;
        this.api.update(this.data.routeParams.cluster, payload).subscribe(
          () => {
            const names = deployments
              .map((item: DeploymentItem) => item.name)
              .join(',');
            this.handleSucc(names);
            this.dialogRef.close(true);
          },
          () => {
            this.submitting = false;
            this.cdr.markForCheck();
          },
        );
      });
    }
  }

  associatedHandle() {
    if (this.store.selectedRelation) {
      return this.api.getRelation(
        this.data.routeParams.namespace,
        this.data.routeParams.cluster,
        this.store.selectedRelation.service.name,
      );
    }
    return of(null);
  }

  getModel(deploys: DeploymentItem[]) {
    const {
      spec: { deployments = [], ...resSpece },
      ...res
    } = this.data.detail;
    return {
      ...res,
      spec: {
        ...resSpece,
        deployments: [...deployments, ...this.comparisonFillDeploy(deploys)],
      },
    };
  }

  comparisonFillDeploy(deploys: DeploymentItem[]) {
    return deploys.map((deploy, index) => ({
      name: deploy.name || this.fillDeployments[index].name,
      version: deploy.version,
    }));
  }

  private handleSucc(name: string) {
    this.submitting = false;
    this.cdr.markForCheck();
    this.message.success(
      this.translate.get('workload_add_successfully', {
        name: name,
      }),
    );
  }
}
