import {
  K8sPermissionService,
  K8sResourceAction,
  TranslateService,
} from '@alauda/common-snippet';
import { ConfirmType, DialogRef, DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import {
  DeploymentItem,
  ErrorCode,
  getDeployments,
  MicroService,
  MicroServiceStore,
  RouteActionType,
  ServiceItem,
  ServiceListService,
  TabNumberAction,
} from '@asm/api/service-list';
import { ServiceTopologyApiService } from '@asm/api/service-topology/service-topology-api.service';
import {
  ServiceNode,
  ServiceTopology,
} from '@asm/api/service-topology/service-topology-api.types';
import { GraphData } from '@asm/api/service-topology/utils';

import { get, isEmpty } from 'lodash-es';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import {
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { RouteDetailComponent } from './../../shared/route-component/route-detail/route-detail.component';
import { ServiceDialogComponent } from './service-dialog/service-dialog.component';
@Component({
  selector: 'alo-service-list-detail',
  templateUrl: './service-list-detail.component.html',
  styleUrls: ['./service-list-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceListDetailComponent implements OnInit, OnDestroy {
  private readonly onDestroy$$ = new Subject<void>();
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly api: ServiceListService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly topologyService: ServiceTopologyApiService,
    private readonly cdr: ChangeDetectorRef,
    public store: MicroServiceStore,
  ) {}

  @ViewChild('routeTemplate', { read: ViewContainerRef, static: true })
  routeTemplate: ViewContainerRef;

  @ViewChild('container', { read: ViewContainerRef, static: true })
  container: ViewContainerRef;

  @ViewChild('noEntranceTemplate', { read: TemplateRef, static: true })
  noEntranceTemplate: TemplateRef<any>;

  @ViewChild('noRouteTemplate', { read: TemplateRef, static: true })
  noRouteTemplate: TemplateRef<any>;

  routeDetailRef: ComponentRef<RouteDetailComponent>;
  dataModule: MicroService;
  nodeSelectedId: string;
  refresh$ = new BehaviorSubject(null);
  routeParams: RouteParams;
  noRoute = false;
  routeActionType = RouteActionType;
  routeDialogRef: DialogRef;
  workloadAbnormal = '';
  serviceName = '';
  graphData$: Observable<GraphData>;
  createServiceEntry = false;
  currentRelationship: { service: string; deployment: string };
  params$ = this.route.paramMap.pipe(
    filter(params => !!params.get('cluster')),
    map(params => ({
      name: params.get('name') || '',
      namespace: params.get('namespace') || '',
      cluster: params.get('cluster') || '',
      project: params.get('project') || '',
    })),
    tap(params => {
      this.routeParams = {
        project: params.project,
        cluster: params.cluster,
        namespace: params.namespace,
        name: params.name,
      };
    }),
    publishReplay(1),
    refCount(),
  );

  errorList$ = this.params$.pipe(
    switchMap(params =>
      this.api.errorchecks({
        ...params,
        resourcekind: 'microservice',
      }),
    ),
    map(res => {
      if (get(res, 'items[0].spec.errlist', []).length > 0) {
        return res.items[0].spec.errlist.reduce((prev, curr) => {
          if (curr.errorcode === ErrorCode.ErrorSvcRelate) {
            const presence = prev.findIndex(
              item => item.service === curr.relatename,
            );
            if (presence !== -1) {
              prev[presence].deployment = [
                ...prev[presence].deployment.split(','),
                curr.resourename,
              ].join(',');
              return prev;
            }
            return [
              ...prev,
              {
                service: curr.relatename,
                deployment: curr.resourename,
              },
            ];
          }
          return prev;
        }, []);
      }
      return null;
    }),
    publishReplay(1),
    refCount(),
  );

  creatableRoute$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.VIRTUALSERVICE,
        action: K8sResourceAction.CREATE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  permissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.MICROSERVICE,
        action: [K8sResourceAction.UPDATE, K8sResourceAction.DELETE],
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  serviceCreatePermissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.SERVICE,
        action: K8sResourceAction.CREATE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  detail$ = combineLatest([this.params$, this.refresh$]).pipe(
    switchMap(([params]) =>
      this.api.getDetail(params.cluster, params.namespace, params.name),
    ),
    tap((result: MicroService) => {
      this.dataModule = result;
      const deployments = getDeployments(result);
      const serviceName = get(result, 'spec.services[0].name', '');
      if (serviceName) {
        this.currentRelationship = {
          service: serviceName,
          deployment: deployments.map(item => item.name).join(','),
        };
      }
      this.serviceName = serviceName;
      this.store.allowCreationRouteAction({
        existRoute: !!serviceName,
      });
      this.store.serviceAction(serviceName);
      this.store.setDeployments(deployments);
      this.cdr.markForCheck();
    }),
    publishReplay(1),
    refCount(),
  );

  workloadTable$ = this.detail$.pipe(
    map((item: MicroService) => getDeployments(item)),
    publishReplay(1),
    refCount(),
  );

  serviceTable$ = this.detail$.pipe(
    map((item: MicroService) => {
      const { deployments, services = [] } = item.spec;
      if (services.length === 0) {
        this.store.tabAction(0, TabNumberAction.SERVICEENTRY);
        return [];
      }
      const { name } = services[0];
      this.store.tabAction(1, TabNumberAction.SERVICEENTRY);
      return {
        name,
        deployment: deployments,
      };
    }),
  );

  // 将 deployment 转换成关系图数据
  private combinationNodeData(
    deployments: DeploymentItem[],
    service: ServiceItem,
  ) {
    const defaultNode: ServiceNode[] = service
      ? [
          {
            ...this.api.convertToNode(
              { name: service.name },
              this.routeParams.name,
            ),
            ...this.store.nodeTagState,
          },
        ]
      : [];
    return deployments.reduce(
      (prev, curr) => {
        const { nodes, edges } = prev;
        return {
          nodes: [
            ...nodes,
            {
              ...this.api.convertToNode(curr, this.routeParams.name),
              ...this.store.nodeTagState,
            },
          ],
          ...(service
            ? {
                edges: [
                  ...edges,
                  this.api.convertToEdge(service.name, curr.name),
                ],
              }
            : {}),
        };
      },
      { nodes: [...defaultNode], edges: [] },
    );
  }

  private backfillService(nodes: ServiceNode[] = []) {
    const services = nodes.filter(item => item.node_type === 'service');
    const serviceName = isEmpty(services) ? '' : services[0].id;
    this.nodeSelectedId = serviceName;
    this.cdr.markForCheck();
  }

  ngOnInit() {
    this.initSubscribe();
    this.store.renderGraph$
      .pipe(takeUntil(this.onDestroy$$))
      .subscribe(result => {
        if (result) {
          this.graphData$ = this.detail$.pipe(
            map((item: MicroService) => {
              const {
                spec: { deployments = [], services = [] },
              } = item;
              const service = services[0];
              return this.topologyService.toSourceData(
                this.combinationNodeData(
                  deployments,
                  service,
                ) as ServiceTopology,
              );
            }),
            tap((result: GraphData) => {
              this.backfillService(result.nodes as ServiceNode[]);
              if (!isEmpty(result.lines)) {
                this.store.initRouteAction();
              } else {
                this.store.routeAction(RouteActionType.NOENTRY);
              }
            }),
          );
        }
      });
  }

  delete() {
    this.dialog
      .confirm({
        title: this.translate.get('microservice_delete_confirm_title', {
          name: this.routeParams.name,
        }),
        confirmType: ConfirmType.Danger,
        content: this.translate.get('microservice_delete_confirm_body'),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.api
          .delete(this.routeParams.cluster, this.dataModule)
          .subscribe(() => {
            this.router.navigate(['../'], {
              relativeTo: this.route,
            });
          });
      })
      .catch(() => false);
  }

  refresh() {
    this.refresh$.next(null);
  }

  triggerUpdateRoute(templateRef: TemplateRef<any>) {
    this.routeDialogRef = this.dialog.open(templateRef, {
      size: DialogSize.Large,
    });
  }

  triggerCreateRoute(templateRef: TemplateRef<any>) {
    this.store.routeAction(RouteActionType.CREATE);
    this.routeDialogRef = this.dialog.open(templateRef, {
      size: DialogSize.Large,
    });
  }

  routeDialogConfirm() {
    this.store.routeTriggerAction();
  }

  initSubscribe() {
    this.store.routeState$
      .pipe(filter(item => item === RouteActionType.DEFAULT))
      .subscribe(() => {
        if (this.routeDialogRef) {
          this.routeDialogRef.close();
        }
      });
  }

  activeCreateService() {
    const dialogRef = this.dialog.open(ServiceDialogComponent, {
      size: DialogSize.Big,
      data: {
        routeParams: this.routeParams,
        update: false,
        deployments: getDeployments(this.dataModule),
      },
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.refresh();
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy$$.next();
    this.store.initStore();
  }
}
