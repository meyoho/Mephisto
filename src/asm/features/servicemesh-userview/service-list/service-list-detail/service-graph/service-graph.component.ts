import { TranslateService } from '@alauda/common-snippet';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MicroServiceStore } from '@asm/api/service-list';
import { Placement } from '@asm/api/service-topology/config';
import { TipInfo } from '@asm/api/service-topology/service-topology-api.types';
import { GraphData, VisLine } from '@asm/api/service-topology/utils';
import { ZoomableService } from '@asm/features/servicemesh-userview/shared/commponents';
import { GraphComponent } from '@asm/features/servicemesh-userview/shared/commponents/graph/graph.component';
@Component({
  selector: 'alo-service-graph',
  templateUrl: './service-graph.component.html',
  styleUrls: ['./service-graph.component.scss'],
})
export class ServiceGraphComponent extends GraphComponent implements OnChanges {
  @Input() data: GraphData;
  @Input() selected: string;
  currentIds: Set<string> = new Set();
  defaultColor = '#009ce3';
  moveTag = false;
  lineInfo: TipInfo = {
    type: 'line',
    width: 145,
    height: 40,
    show: false,
    x: 0,
    y: 0,
  };

  constructor(
    public translate: TranslateService,
    public zoomService: ZoomableService,
    private readonly store: MicroServiceStore,
  ) {
    super(translate, zoomService);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data && changes.data.currentValue) {
      setTimeout(() => {
        this.render(changes.data.currentValue);
      });
    }
  }

  render({ nodes = [], lines, groupLines }: GraphData) {
    if (!nodes.length) {
      return;
    }
    this.renderGraph(nodes, lines, groupLines, {
      ranksep: 170,
      nodesep: 120,
      placement: lines.length > 1 ? Placement.TOP_START : Placement.AUTO,
    });
    if (this.selected) {
      this.currentIds = this.getSelectedLinkedNodeId(this.selected);
    }
  }

  getSelectedLinkedNodeId(id: string) {
    const lineIds = [...this.lineMap.keys()];
    const ids = lineIds.reduce((prev, curr) => {
      let result: string[] = [];
      if (curr.includes(id)) {
        result = [...curr.split('_'), curr];
      }
      return [...result, ...prev];
    }, []);
    ids.unshift(id);
    return new Set(ids);
  }

  lineMouseover() {
    if (this.store.workloadInfo && !this.moveTag) {
      this.moveTag = true;
      this.lineInfo.show = true;
      setTimeout(() => {
        this.moveTag = false;
      }, 5);
    }
  }

  lineMouseout() {
    if (this.store.workloadInfo && !this.moveTag) {
      this.moveTag = true;
      this.lineInfo.show = false;
      setTimeout(() => {
        this.moveTag = false;
      }, 5);
    }
  }

  lineMouseMove([e, line]: [MouseEvent, VisLine]) {
    if (this.store.workloadInfo) {
      const { target_id } = line;
      const version = this.nodeMap.get(target_id).label[1];
      this.lineInfo.x = Math.min(
        this.size.width - this.lineInfo.width,
        e.offsetX + 8,
      );
      this.lineInfo.y = Math.min(
        this.size.height - this.lineInfo.height,
        e.offsetY + 36,
      );
      const { count = 0, weight = 0 } = this.store.workloadInfo.get(version);
      this.lineInfo.data = [`流量权重：${weight}%`, `条件规则：${count}条`];
    }
  }
}
