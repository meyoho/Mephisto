import { StringMap } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import {
  ConnectionPool,
  OutlierDetection,
  StrategyType,
} from '@asm/api/service-list/service-list.types';

import { get } from 'lodash-es';
interface TableDetail {
  type: StrategyType;
  name: string;
  originalData: OutlierDetection | ConnectionPool;
}
const outlierDetectionTranslate = {
  consecutiveErrors: 'error_response_number',
  interval: 'inspection_cycle',
  baseEjectionTime: 'minimum_isolation_time',
  maxEjectionPercent: 'maximum_isolation_ratio',
};
const connectionPoolHttpTranslate = {
  maxConnections: 'max_connections',
  maxRetries: 'max_requests_retries',
  maxRequestsPerConnection: 'max_requests_per_connection',
  httpMaxPendingRequests: 'max_requests_pending',
  httpMaxRequests: 'max_requests',
};
const connectionPoolTcpTranslate = {
  maxConnections: 'max_connections',
  connectTimeout: 'connect_timeout',
};
@Component({
  selector: 'alo-strategy-table-detail',
  templateUrl: './strategy-table-detail.component.html',
  styleUrls: ['./strategy-table-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrategyTableDetailComponent implements OnInit {
  detailKeys: string[];
  translateVal: StringMap;
  @Input() data: TableDetail;
  @Input() describe: string;

  get values() {
    if (this.data.type === StrategyType.CONNECTIONPOOL) {
      return get(this.data.originalData, `spec.${this.describe}`);
    }
    return get(this.data.originalData, 'spec');
  }

  ngOnInit() {
    if (this.data.type === StrategyType.OUTLIERDETECTION) {
      this.detailKeys = Object.keys(outlierDetectionTranslate);
      this.translateVal = outlierDetectionTranslate;
    }
    if (this.data.type === StrategyType.CONNECTIONPOOL) {
      switch (this.describe) {
        case 'http':
          this.detailKeys = Object.keys(connectionPoolHttpTranslate);
          this.translateVal = connectionPoolHttpTranslate;
          break;
        case 'tcp':
          this.detailKeys = Object.keys(connectionPoolTcpTranslate);
          this.translateVal = connectionPoolTcpTranslate;
          break;
        default:
          break;
      }
    }
  }
}
