import {
  K8sUtilService,
  TOKEN_BASE_DOMAIN,
  TranslateService,
} from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RESOURCE_DEFINITIONS } from '@app/utils';
import { POSITIVE_INT_PATTERN } from '@app/utils/patterns';
import { dividedUnitNumber } from '@app/utils/unit';
import { MicroServiceStore } from '@asm/api/service-list';
import { ServiceListService } from '@asm/api/service-list/service-list.service';
import {
  ConnectionPool,
  ConnectionPoolHttp,
  ConnectionPoolTcp,
  getConnectionPoolTypeName,
} from '@asm/api/service-list/service-list.types';

import { get, has, omit, toNumber } from 'lodash-es';
@Component({
  templateUrl: './connectionPool-dialog.component.html',
  styleUrls: ['./connectionPool-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConnectionPoolDialogComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  positiveIntPattern = POSITIVE_INT_PATTERN;
  loading = false;
  update = false;
  get dialogText() {
    return !this.data.isUpdate
      ? {
          title: 'create_connection_pool_settings_http',
          confirm: 'create',
          confirm_message: 'connection_pool_create_successfully',
        }
      : {
          title: 'update_connection_pool_settings_http',
          confirm: 'update',
          confirm_message: 'connection_pool_update_successfully',
        };
  }

  httpViewModel: ConnectionPoolHttp = {
    maxRetries: 3,
    maxRequestsPerConnection: null,
    httpMaxPendingRequests: 1024,
    httpMaxRequests: 1024,
    maxConnections: 1024,
  };

  tcpViewModel: ConnectionPoolTcp = {
    maxConnections: 1024,
    connectTimeout: '',
    connectTimeoutUnit: 's',
  };

  defaultModel: ConnectionPool = {
    apiVersion: `${RESOURCE_DEFINITIONS.OUTLIERDETECTION.apiGroup}/${RESOURCE_DEFINITIONS.OUTLIERDETECTION.apiVersion}`,
    kind: 'ConnectionPool',
    metadata: {
      namespace: this.data.namespace,
      name: this.data.name,
      labels: {
        [`${this.asmBaseDomain}/hostname`]: this.data.name,
        ...(this.store.gatewayInfo.name
          ? { [`servicemesh.${this.baseDomain}/resource`]: 'gateway' }
          : {}),
      },
    },
    spec: {
      host: this.data.name,
      http: this.httpViewModel,
      tcp: this.tcpViewModel,
    },
  };

  constructor(
    private readonly dialogRef: DialogRef<ConnectionPoolDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      isUpdate: boolean;
      cluster: string;
      namespace: string;
      name: string;
      type: string;
    },
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly api: ServiceListService,
    private readonly k8sUtil: K8sUtilService,
    private readonly store: MicroServiceStore,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  ngOnInit() {
    this.getDetail();
  }

  getDetail() {
    const { cluster, namespace, name } = this.data;
    this.api
      .getConnectionPool({
        cluster,
        namespace,
        name,
      })
      .subscribe((result: ConnectionPool) => {
        this.update = true;
        this.defaultModel = result;
        this.httpViewModel = {
          ...get(result, 'spec.http', this.httpViewModel),
          maxConnections: get(result, 'spec.tcp.maxConnections'),
        };
        if (has(result, 'spec.tcp')) {
          const { maxConnections, connectTimeout } = get(
            result,
            'spec.tcp',
            this.tcpViewModel,
          );
          const [timeoutNumber, connectTimeoutUnit] = dividedUnitNumber(
            connectTimeout,
          );
          this.tcpViewModel = {
            maxConnections,
            connectTimeout: timeoutNumber,
            connectTimeoutUnit,
          };
        }
        this.cdr.markForCheck();
      });
  }

  save() {
    this.form.onSubmit(null);
    const { isUpdate, cluster } = this.data;
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    if (!isUpdate && !this.update) {
      this.api.createConnectionPool(cluster, this.getModel()).subscribe(
        result => {
          this.processResult(result);
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
    } else {
      this.api.updateConnectionPool(cluster, this.getModel()).subscribe(
        result => {
          this.processResult(result);
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
    }
  }

  processResult(data: ConnectionPool) {
    this.loading = false;
    this.message.success({
      content: this.translate.get(this.dialogText.confirm_message, {
        type: getConnectionPoolTypeName(this.data.type),
        name: this.k8sUtil.getName(data),
      }),
    });
    this.dialogRef.close(true);
  }

  getModel(): ConnectionPool {
    if (this.data.type === 'http') {
      const {
        maxRetries,
        maxRequestsPerConnection,
        httpMaxPendingRequests,
        httpMaxRequests,
        maxConnections,
      } = this.httpViewModel;
      const specRes = this.update
        ? get(this.defaultModel, 'spec')
        : omit(this.defaultModel.spec, 'tcp');
      return {
        ...this.defaultModel,
        spec: {
          ...specRes,
          http: {
            maxRetries: toNumber(maxRetries),
            ...(toNumber(maxRequestsPerConnection)
              ? {
                  maxRequestsPerConnection: toNumber(maxRequestsPerConnection),
                }
              : {}),
            httpMaxPendingRequests: toNumber(httpMaxPendingRequests),
            httpMaxRequests: toNumber(httpMaxRequests),
          },
          tcp: {
            maxConnections: toNumber(maxConnections),
          },
        },
      };
    }
    const {
      maxConnections,
      connectTimeout,
      connectTimeoutUnit,
    } = this.tcpViewModel;
    const specRes = this.update
      ? get(this.defaultModel, 'spec')
      : omit(this.defaultModel.spec, 'http');
    return {
      ...this.defaultModel,
      spec: {
        ...specRes,
        tcp: {
          maxConnections: toNumber(maxConnections),
          ...(connectTimeout
            ? { connectTimeout: `${connectTimeout}${connectTimeoutUnit}` }
            : {}),
        },
      },
    };
  }
}
