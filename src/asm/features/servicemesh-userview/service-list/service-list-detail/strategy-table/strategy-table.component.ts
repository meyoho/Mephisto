import {
  COMMON_WRITABLE_ACTIONS,
  K8sPermissionService,
  ObservableInput,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import { matchLabelsToString } from '@app/utils/resource-list';
import { SecurityStrategyService } from '@asm/api/security-strategy/security-strategy.service';
import { PolicyResponse } from '@asm/api/security-strategy/security-strategy.types';
import {
  ConnectionPool,
  ConnectionPoolType,
  getConnectionPoolTypeName,
  MicroServiceStore,
  OutlierDetection,
  ServiceListService,
  StrategyList,
  StrategyType,
  TabNumberAction,
} from '@asm/api/service-list';
import {
  strategyPure,
  TrafficPolicyService,
} from '@asm/api/traffic-policy/traffic-policy.service';
import { LoadBalancerCrd } from '@asm/api/traffic-policy/traffic-policy.types';
import { LoadBalancingDialogComponent } from '@asm/features/servicemesh-userview/load-balancing/dialog/load-balancing-dialog/load-balancing-dialog.component';
import { SecurityDialogComponent } from '@asm/features/servicemesh-userview/security-strategy/security-dialog/security-dialog.component';
import { securityPolicyRuleProcess } from '@asm/utils';

import { flattenDepth, get, has, omit } from 'lodash-es';
import { BehaviorSubject, combineLatest, forkJoin, Observable, of } from 'rxjs';
import {
  catchError,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import { ConnectionPoolDialogComponent } from './connectionPool-dialog/connectionPool-dialog.component';
import { OutlierDetectionDialogComponent } from './outlierDetection-dialog/outlierDetection-dialog.component';
@Component({
  selector: 'alo-strategy-table',
  templateUrl: './strategy-table.component.html',
  styleUrls: ['./strategy-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrategyTableComponent {
  columns = [
    'strategy_type',
    'strategy_details',
    'create_by',
    'created_at',
    'action',
  ];

  @ObservableInput(true)
  private readonly name$: Observable<string>;

  @Input() name: string;
  @Input() hiddenItem: string[] = [];
  strategyPure = strategyPure;
  params: RouteParams = {};
  strategyList: StrategyList[] = [];
  strategyType = StrategyType;
  currentType: string;
  refresh$ = new BehaviorSubject(null);
  isUpdate = false;
  getConnectionPoolTypeName = getConnectionPoolTypeName;
  params$ = combineLatest([
    this.route.paramMap,
    this.name$,
    this.refresh$,
  ]).pipe(
    filter(([_, name]) => !!name),
    map(([params, name]) => ({
      namespace: params.get('namespace') || '',
      cluster: params.get('cluster') || '',
      name: name,
    })),
    tap(params => {
      this.params = params;
    }),
    publishReplay(1),
    refCount(),
  );

  get policyExisted() {
    return [
      ...Object.values(StrategyType),
      ...Object.values(ConnectionPoolType),
    ].reduce((prev, value) => {
      const index = this.strategyList.findIndex(
        item => item.type === value || item.describe === value,
      );
      const data = this.strategyList[index];
      return {
        ...prev,
        ...(data ? { [data.describe ? data.describe : data.type]: true } : {}),
      };
    }, {});
  }

  get dialogConfig() {
    const config = {
      title: '',
      content: '',
      confirmText: this.translate.get('confirm'),
      cancelText: this.translate.get('cancel'),
      deleteContent: '',
    };
    switch (this.currentType) {
      case StrategyType.SAFETY:
        config.title = this.translate.get('safety_rules_delete_confirm_title', {
          name: this.params.name,
        });
        config.content = this.translate.get('safety_rules_delete_confirm_body');
        config.deleteContent = this.translate.get(
          'security_deleted_message_success',
          { name: this.params.name },
        );
        break;
      case StrategyType.LOADBALANCER:
        config.title = this.translate.get('load_balancing_title_dialog', {
          name: this.params.name,
        });
        config.deleteContent = this.translate.get(
          'loadbalancing_deleted_message_success',
          { name: this.params.name },
        );
        break;
      case StrategyType.OUTLIERDETECTION:
        config.deleteContent = this.translate.get(
          'outlier_detection_delete_successfully',
          { name: this.params.name },
        );
        config.title = this.translate.get(
          'outlier_detection_delete_confirm_title',
        );
    }
    return config;
  }

  constructor(
    private readonly route: ActivatedRoute,
    private readonly securityService: SecurityStrategyService,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly trafficAPi: TrafficPolicyService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly api: ServiceListService,
    private readonly store: MicroServiceStore,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  loadbalancersPermissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.LOADBALANCER,
        action: COMMON_WRITABLE_ACTIONS,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  safetyPermissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.POLOCY,
        action: COMMON_WRITABLE_ACTIONS,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  outlierDetectionPermissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.OUTLIERDETECTION,
        action: COMMON_WRITABLE_ACTIONS,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  connectionPoolPermissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.CONNECTIONPOOL,
        action: COMMON_WRITABLE_ACTIONS,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  menus = [
    {
      name: 'load_balancing',
      type: StrategyType.LOADBALANCER,
      permissions: this.loadbalancersPermissions$,
    },
    {
      name: 'outlier_detection',
      type: StrategyType.OUTLIERDETECTION,
      permissions: this.outlierDetectionPermissions$,
    },
    {
      name: 'connection_pool_settings',
      type: StrategyType.CONNECTIONPOOL,
      permissions: this.connectionPoolPermissions$,
      childer: [
        { name: 'HTTP/HTTP2', type: ConnectionPoolType.HTTP },
        // TODO: 先隐藏暂时不支持tcp
        // { name: 'TCP', type: ConnectionPoolType.TCP},
      ],
    },
    {
      name: 'safety',
      type: StrategyType.SAFETY,
      permissions: this.safetyPermissions$,
    },
  ];

  fetch = (params: RouteParams) => {
    if (!params) {
      this.store.renderGraph$.next(true);
    }
    return !params
      ? of([])
      : forkJoin([
          this.getSecurity(params),
          this.getLoadBalancer(params),
          this.getOutlierDetection(params),
          this.getConnectionPool(params),
        ]).pipe(
          map(result => {
            return flattenDepth(result as StrategyList[], 2).filter(
              item => item,
            );
          }),
          tap(result => {
            this.store.renderGraph$.next(true);
            this.strategyList = result;
            this.store.tabAction(result.length, TabNumberAction.STRATEGY);
            this.cdr.markForCheck();
          }),
        );
  };

  getSecurity(params: RouteParams) {
    const labelSelector = matchLabelsToString({
      [`${this.asmBaseDomain}/hostname`]: params.name,
    });
    return this.securityService
      .getPolicyList({
        ...params,
        labelSelector,
      })
      .pipe(
        catchError(() => of(null)),
        map(result => {
          const item = get(result, 'items[0]');
          this.store.nodeTagAction({ has_TLS: !!item });
          return item
            ? {
                type: StrategyType.SAFETY,
                name: 'safety',
                originalData: item,
                detail: securityPolicyRuleProcess(get(item, 'spec.peers')),
              }
            : null;
        }),
      );
  }

  getLoadBalancer(params: RouteParams) {
    return this.trafficAPi.getLoadbalancers(params).pipe(
      catchError(() => of(null)),
      map(item =>
        item
          ? {
              type: StrategyType.LOADBALANCER,
              name: 'load_balancing',
              originalData: item,
            }
          : null,
      ),
    );
  }

  getOutlierDetection(params: RouteParams) {
    return this.api.getOutlierDetection(params).pipe(
      catchError(() => of(null)),
      map(item =>
        item
          ? {
              type: StrategyType.OUTLIERDETECTION,
              name: 'outlier_detection',
              originalData: item,
            }
          : null,
      ),
      tap(result => {
        this.store.nodeTagAction({ has_CB: !!result });
      }),
    );
  }

  getConnectionPool(params: RouteParams) {
    return this.api.getConnectionPool(params).pipe(
      catchError(() => of(null)),
      map(item => {
        if (!item) {
          return null;
        }
        const model = {
          type: StrategyType.CONNECTIONPOOL,
          name: 'connection_pool_settings',
          originalData: this.reorganizationData(item),
        };
        const httpModel = get(item, 'spec.http')
          ? Object.assign(
              {
                describe: ConnectionPoolType.HTTP,
              },
              model,
            )
          : null;
        // TODO: 暂不支持隐藏TCP，最大连接数生效于http，https，tcp
        // const tcpModel = get(item, 'spec.tcp')
        const tcpModel = null
          ? Object.assign(
              {
                describe: ConnectionPoolType.TCP,
              },
              model,
            )
          : null;
        return item ? [httpModel || [], tcpModel || []] : null;
      }),
    );
  }

  reorganizationData(data: ConnectionPool) {
    const { spec, ...res } = data;
    const { http, tcp, ...resSpec } = spec;
    return {
      ...res,
      spec: {
        ...resSpec,
        http: {
          ...http,
          maxConnections: get(tcp, 'maxConnections'),
        },
      },
    };
  }

  openCreatePolicyDialog(type: StrategyType, name: string) {
    this.currentType = type;
    this.isUpdate = false;
    switch (type) {
      case StrategyType.LOADBALANCER:
        this.loadBalancerDialog();
        break;
      case StrategyType.SAFETY:
        this.safetyDialog();
        break;
      case StrategyType.OUTLIERDETECTION:
        this.outlierDetectionDialog();
        break;
      case StrategyType.CONNECTIONPOOL:
        this.connectionPoolDialog(name);
        break;
      default:
        break;
    }
  }

  openUpdateDialog(type: StrategyType, item: StrategyList) {
    this.currentType = type;
    this.isUpdate = true;
    this.openDialog(type, item);
  }

  openDialog(type: StrategyType, item: StrategyList) {
    switch (type) {
      case StrategyType.LOADBALANCER:
        this.loadBalancerDialog();
        break;
      case StrategyType.SAFETY:
        this.safetyDialog();
        break;
      case StrategyType.OUTLIERDETECTION:
        this.outlierDetectionDialog();
        break;
      case StrategyType.CONNECTIONPOOL:
        this.connectionPoolDialog(item.describe);
        break;
      default:
        break;
    }
  }

  safetyDialog() {
    const { namespace, cluster, name } = this.params;
    const dialogRef = this.dialog.open(SecurityDialogComponent, {
      data: {
        hideName: true,
        namespace,
        isUpdate: this.isUpdate,
        cluster,
        name,
        service: name,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh(result);
    });
  }

  loadBalancerDialog() {
    const { project, namespace, cluster, name } = this.params;
    const dialogRef = this.dialog.open(LoadBalancingDialogComponent, {
      data: {
        isUpdate: this.isUpdate,
        name: name,
        hideHost: true,
        project: project,
        namespace: namespace,
        cluster: cluster,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      this.refresh(result);
    });
  }

  outlierDetectionDialog() {
    const { cluster, name, namespace } = this.params;
    const dialogRef = this.dialog.open(OutlierDetectionDialogComponent, {
      data: {
        isUpdate: this.isUpdate,
        name: name,
        cluster: cluster,
        namespace,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      this.refresh(result);
    });
  }

  connectionPoolDialog(type: string) {
    const { cluster, name, namespace } = this.params;
    const dialogRef = this.dialog.open(ConnectionPoolDialogComponent, {
      data: {
        isUpdate: this.isUpdate,
        name: name,
        cluster: cluster,
        namespace,
        type,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      this.refresh(result);
    });
  }

  refresh(result: boolean) {
    if (result) {
      this.refresh$.next(null);
    }
  }

  openDeleteDialog(type: StrategyType, item: any) {
    this.currentType = type;
    switch (type) {
      case StrategyType.LOADBALANCER:
        this.loadBalancerDeleteDialog(item.originalData);
        break;
      case StrategyType.SAFETY:
        this.safetyDeleteDialog(item.originalData);
        break;
      case StrategyType.OUTLIERDETECTION:
        this.outlierDetectionDeleteDialog(item.originalData);
        break;
      case StrategyType.CONNECTIONPOOL:
        this.connectionPoolDeleteDialog(item.originalData, item.describe);
        break;
      default:
        break;
    }
  }

  loadBalancerDeleteDialog(data: LoadBalancerCrd) {
    this.dialog
      .confirm({
        ...this.dialogConfig,
      })
      .then(() => {
        this.trafficAPi.deleteLoadbalancer(this.params, data).subscribe(() => {
          this.deleteSuccProcessed();
        });
      })
      .catch(() => {});
  }

  safetyDeleteDialog(data: PolicyResponse) {
    this.dialog
      .confirm({
        ...this.dialogConfig,
      })
      .then(() => {
        this.securityService.delete(this.params.cluster, data).subscribe(() => {
          this.deleteSuccProcessed();
        });
      })
      .catch(() => {});
  }

  outlierDetectionDeleteDialog(data: OutlierDetection) {
    this.dialog
      .confirm({
        ...this.dialogConfig,
      })
      .then(() => {
        this.api
          .deleteOutlierDetection(this.params.cluster, data)
          .subscribe(() => {
            this.deleteSuccProcessed();
          });
      })
      .catch(() => {});
  }

  connectionPoolDeleteDialog(data: ConnectionPool, name: string) {
    const type = getConnectionPoolTypeName(name);
    this.dialog
      .confirm({
        title: this.translate.get('connection_pool_delete_confirm_title', {
          type,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        if (has(data, 'spec.http') && has(data, 'spec.tcp')) {
          const payload = omit(data, `spec.${name}`) as ConnectionPool;
          this.api
            .updateConnectionPool(this.params.cluster, payload)
            .subscribe(() => {
              this.deleteSuccProcessed(type);
            });
        } else {
          this.api
            .deleteConnectionPool(this.params.cluster, data)
            .subscribe(() => {
              this.deleteSuccProcessed(type);
            });
        }
      })
      .catch(() => {});
  }

  deleteSuccProcessed(type?: string) {
    if (!type) {
      this.message.success({ content: this.dialogConfig.deleteContent });
    } else {
      this.message.success({
        content: this.translate.get('connection_pool_delete_successfully', {
          name: this.params.name,
          type,
        }),
      });
    }
    this.refresh$.next(null);
  }

  getPermissions = (type: StrategyType) => {
    switch (type) {
      case StrategyType.LOADBALANCER:
        return this.loadbalancersPermissions$;
      case StrategyType.OUTLIERDETECTION:
        return this.outlierDetectionPermissions$;
      case StrategyType.CONNECTIONPOOL:
        return this.connectionPoolPermissions$;
      case StrategyType.SAFETY:
        return this.safetyPermissions$;
      default:
        break;
    }
  };

  get displayStrategyItem() {
    return this.menus.filter(item => !this.hiddenItem.includes(item.name));
  }
}
