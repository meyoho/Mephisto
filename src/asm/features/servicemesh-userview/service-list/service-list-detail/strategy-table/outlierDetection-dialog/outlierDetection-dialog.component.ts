import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RESOURCE_DEFINITIONS } from '@app/utils';
import { ISOLATION_RATIO, POSITIVE_INT_PATTERN } from '@app/utils/patterns';
import { dividedUnitNumber } from '@app/utils/unit';
import { ServiceListService } from '@asm/api/service-list/service-list.service';
import { OutlierDetection } from '@asm/api/service-list/service-list.types';

import { get, toNumber } from 'lodash-es';

@Component({
  templateUrl: './outlierDetection-dialog.component.html',
  styleUrls: ['./outlierDetection-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OutlierDetectionDialogComponent implements OnInit {
  @ViewChild('form', { static: true })
  form: NgForm;

  loading = false;
  percentagePattern = ISOLATION_RATIO;
  positiveIntPattern = POSITIVE_INT_PATTERN;
  viewModel = {
    interval: '10',
    intervalUnit: 's',
    consecutiveErrors: 5,
    baseEjectionTime: '30',
    baseEjectionTimeUnit: 's',
    maxEjectionPercent: 10,
  };

  get dialogText() {
    return !this.data.isUpdate
      ? {
          title: 'create_outlier_detection_policy',
          confirm: 'create',
          confirm_message: 'outlier_detection_create_successfully',
        }
      : {
          title: 'update_outlier_detection_policy',
          confirm: 'update',
          confirm_message: 'outlier_detection_update_successfully',
        };
  }

  defaultModel: OutlierDetection = {
    apiVersion: `${RESOURCE_DEFINITIONS.OUTLIERDETECTION.apiGroup}/${RESOURCE_DEFINITIONS.OUTLIERDETECTION.apiVersion}`,
    kind: 'OutlierDetection',
    metadata: {
      namespace: this.data.namespace,
      name: this.data.name,
      labels: {
        [`${this.asmBaseDomain}/hostname`]: this.data.name,
      },
    },
    spec: this.viewModel,
  };

  constructor(
    private readonly dialogRef: DialogRef<OutlierDetectionDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      isUpdate: boolean;
      cluster: string;
      namespace: string;
      name: string;
    },
    private readonly api: ServiceListService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  ngOnInit() {
    this.getDetail();
  }

  getDetail() {
    if (this.data.isUpdate) {
      const { cluster, namespace, name } = this.data;
      this.api
        .getOutlierDetection({
          cluster,
          namespace,
          name,
        })
        .subscribe((result: OutlierDetection) => {
          const {
            interval,
            consecutiveErrors,
            baseEjectionTime,
            maxEjectionPercent,
          } = get(result, 'spec');
          const [intervalNum, intervalUnit] = dividedUnitNumber(interval);
          const [baseEjectionTimeNum, baseEjectionTimeUnit] = dividedUnitNumber(
            baseEjectionTime,
          );

          this.defaultModel = result;
          this.viewModel = {
            interval: intervalNum,
            intervalUnit,
            consecutiveErrors,
            baseEjectionTime: baseEjectionTimeNum,
            baseEjectionTimeUnit,
            maxEjectionPercent,
          };
          this.cdr.markForCheck();
        });
    }
  }

  save() {
    this.form.onSubmit(null);
    const { isUpdate, cluster } = this.data;
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    if (!isUpdate) {
      this.api.createOutlierDetection(cluster, this.getModel()).subscribe(
        result => {
          this.processResult(result);
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
    } else {
      this.api.updateOutlierDetection(cluster, this.getModel()).subscribe(
        result => {
          this.processResult(result);
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
    }
  }

  processResult(data: OutlierDetection) {
    this.loading = false;
    this.message.success({
      content: this.translate.get(this.dialogText.confirm_message, {
        name: this.k8sUtil.getName(data),
      }),
    });
    this.dialogRef.close(true);
  }

  getModel(): OutlierDetection {
    const {
      interval,
      consecutiveErrors,
      maxEjectionPercent,
      intervalUnit,
      baseEjectionTime,
      baseEjectionTimeUnit,
    } = this.viewModel;
    return {
      ...this.defaultModel,
      spec: {
        host: this.data.name,
        interval: `${interval}${intervalUnit}`,
        consecutiveErrors: toNumber(consecutiveErrors),
        baseEjectionTime: `${baseEjectionTime}${baseEjectionTimeUnit}`,
        maxEjectionPercent: toNumber(maxEjectionPercent),
      },
    };
  }
}
