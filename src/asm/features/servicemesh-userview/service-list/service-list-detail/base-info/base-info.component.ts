import {
  K8sUtilService,
  TOKEN_BASE_DOMAIN,
  TranslateService,
} from '@alauda/common-snippet';
import { DialogRef, DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServiceListService } from '@asm/api/service-list/service-list.service';
import { MicroService } from '@asm/api/service-list/service-list.types';

import { set } from 'lodash-es';

@Component({
  selector: 'alo-base-info',
  templateUrl: './base-info.component.html',
  styleUrls: ['./base-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BaseInfoComponent {
  @Input() data: MicroService;
  @Input() permissions: { update: boolean };
  @Output() refresh = new EventEmitter();
  @ViewChild('form')
  form: NgForm;

  dialogRef: DialogRef;
  displayName = '';

  constructor(
    private readonly dialog: DialogService,
    private readonly api: ServiceListService,
    private readonly route: ActivatedRoute,
    private readonly k8sUtil: K8sUtilService,
    private readonly cdr: ChangeDetectorRef,
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {}

  openDialog(template: TemplateRef<any>) {
    this.displayName = this.k8sUtil.getDisplayName(this.data);
    this.dialogRef = this.dialog.open(template);
    this.cdr.markForCheck();
  }

  update() {
    const payload = set(
      this.data,
      `metadata.annotations['${this.baseDomain}/display-name']`,
      this.displayName,
    );
    this.api
      .update(this.route.snapshot.paramMap.get('cluster'), payload)
      .subscribe(result => {
        this.data = result;
        this.refresh.emit();
        this.message.success(
          this.translate.get(`update_display_name_successfully`),
        );
        this.cdr.markForCheck();
        this.dialogRef.close();
      });
  }
}
