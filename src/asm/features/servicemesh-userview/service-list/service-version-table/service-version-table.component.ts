import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import { ConfirmType, DialogService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Injector,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { RouteParams } from '@app/typings';
import { K8S_RESOURCE_NAME_BASE, SUBSET_PATTERN } from '@app/utils/patterns';
import {
  DeploymentItem,
  DeploymentRelation,
  getDeployments,
  MicroService,
  MicroServiceStore,
  ServiceEntrance,
  ServiceListService,
} from '@asm/api/service-list';

import { get, sortBy } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Observable } from 'rxjs';
import { map, publishReplay, refCount, tap } from 'rxjs/operators';

@Component({
  selector: 'alo-service-version-table',
  templateUrl: './service-version-table.component.html',
  styleUrls: ['./service-version-table.component.scss'],
})
export class ServiceVersionTableComponent extends BaseResourceFormArrayComponent
  implements OnInit {
  subsetPattern = SUBSET_PATTERN;
  namePattern = K8S_RESOURCE_NAME_BASE;
  deployments: DeploymentRelation[] = [];
  deployments$: Observable<any>;
  existedDeployments: string[] = [];
  @Input() fillDeployments: DeploymentItem[];
  @Input() routeParams: RouteParams;
  @Input() detail: MicroService;
  @Input() isCreate: boolean;
  @Input() submitted: boolean;
  @Output() backfillMicroService = new EventEmitter();
  get deploymentsName() {
    return this.form.value.map((item: DeploymentItem) => item.name);
  }

  constructor(
    injector: Injector,
    private readonly api: ServiceListService,
    private store: MicroServiceStore,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    private readonly dialog: DialogService,
  ) {
    super(injector);
  }

  ngOnInit() {
    if (this.isCreate) {
      this.store.serviceAction('');
    } else {
      this.store.disableMicroservice$.next(true);
      this.store.currentMicroService$.next(this.detail.metadata.name);
    }
    this.getAssociatedDeploy();
    this.initDeploy();
    // 回填关联当前svc的deployment,并禁用
    if (this.fillDeployments.length) {
      this.form.valueChanges
        .pipe(
          map(data => {
            const names = this.fillDeployments.map(item => item.name);
            data.forEach((item: DeploymentItem, index: number) => {
              if (names.includes(item.name)) {
                this.form.controls[index].get('name').disable();
              }
            });
          }),
        )
        .subscribe();
    }
  }

  getAssociatedDeploy() {
    this.api.getList(this.routeParams).subscribe(result => {
      const deployments = result.items.reduce((prev, curr: MicroService) => {
        const names = getDeployments(curr).map(
          (item: DeploymentItem) => item.name,
        );
        return [...prev, ...names];
      }, []);
      this.existedDeployments = deployments;
    });
  }

  initDeploy() {
    this.deployments$ = this.api
      .getRelation(this.routeParams.namespace, this.routeParams.cluster)
      .pipe(
        map((result: ServiceEntrance[]) => {
          return result.map(item => {
            const version: string = get(
              item.deployment,
              'spec.template.metadata.labels.version',
              '',
            );
            const matchLabels = get(
              item.deployment,
              'spec.selector.matchLabels',
              {},
            );
            const services = item.services || [];
            const names = services.map(item => item.name).join(',');
            return {
              deployment: {
                name: this.k8sUtil.getName(item.deployment),
                version: version,
                labelApp: get(matchLabels, 'app'),
                labelVersion: get(matchLabels, 'version'),
              },
              service: {
                name: names,
                total: services.length,
              },
            };
          });
        }),
        tap(result => {
          this.deployments = result;
          this.cdr.markForCheck();
        }),
        publishReplay(1),
        refCount(),
      );
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel() {
    return [
      {
        name: '',
        version: '',
      },
    ];
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      name: this.fb.control('', [Validators.required]),
      version: this.fb.control('', [
        Validators.pattern(this.subsetPattern.pattern),
        Validators.required,
        this.sameVersionValidator,
        this.cannotBeAllValidator,
      ]),
    });
  }

  cannotBeAllValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    return control.value === 'all' ? { connotBeAll: true } : null;
  };

  private getPreviousKeys(index: number) {
    return index ? this.formModel.slice(0, index) : this.formModel.slice(1);
  }


  sameVersionValidator = (
    control: AbstractControl,
  ): { [key: string]: boolean } => {
    if (!control.value) {
      return null;
    }
    const versions = getDeployments(this.detail).map(
      (item: DeploymentItem) => item.version,
    );
    const index = this.form.controls.indexOf(control.parent);
    const previousKeys = this.getPreviousKeys(index);
    const sameVersion = previousKeys.some(item => {
      return item.version === control.value;
    });
    const validatPass = versions.includes(control.value) || sameVersion;
    return validatPass
      ? {
          [this.getDisableControl(control.parent)
            ? 'sameVersionPodSelector'
            : 'sameVersion']: true,
        }
      : null;
  };

  getValidateTip = (control: AbstractControl) => {
    const workload = control.get('name');
    const version = control.get('version');
    return [
      {
        name: 'workload',
        disabled:
          (this.submitted || workload.touched) && workload.hasError('required'),
        tip: this.translate.get('not_null_tip', {
          name: this.translate.get('workload'),
        }),
      },
      {
        name: 'version',
        disabled:
          (this.submitted || version.touched) && version.hasError('required'),
        tip: this.translate.get('version_required_tip'),
      },
      {
        name: 'sameVersionPodSelector',
        disabled: version.hasError('sameVersionPodSelector'),
        tip: this.translate.get('version_existence_ps_being_used'),
      },
      {
        name: 'same_version',
        disabled: version.hasError('sameVersion'),
        tip: this.translate.get('same_version_tip'),
      },
      {
        name: 'version_connotBeAll',
        disabled: version.hasError('connotBeAll'),
        tip: this.translate.get('cannot_be_all'),
      },
      {
        name: 'version_pattern',
        disabled: version.hasError('pattern'),
        tip: `"${this.translate.get('version')}"${this.translate.get(
          this.subsetPattern.tip,
        )}`,
      },
    ].filter(item => item.disabled);
  };

  rowBackgroundColorFn(row: FormControl) {
    return row.invalid && row.touched ? '#fdefef' : '';
  }

  removeControl(index: number, control: AbstractControl) {
    if (control.value) {
      const { name, version } = control.value;
      this.removeAppPodSelector(name);
      const pass = this.deployments.some(item => {
        const { deployment, service } = item;
        return (
          deployment.name === name &&
          deployment.version === version &&
          service.total > 0
        );
      });
      if (pass) {
        this.store.serviceAction('');
      }
    }
    this.remove(index);
  }

  getDisableControl = (control: AbstractControl) => {
    return this.deployments.some(deploy => {
      let name = control.value.name;
      if (this.fillDeployments.length && !control.value.name) {
        name = get(
          this.fillDeployments.find(
            item => item.version === control.value.version,
          ),
          'name',
        );
      }
      return name === deploy.deployment.name && deploy.deployment.labelVersion;
    });
  };

  removeAppPodSelector(name: string) {
    if (name) {
      const data = this.deployments.find(item => item.deployment.name === name);
      const deployments = this.form.value.reduce(
        (prev: string[], curr: DeploymentItem) =>
          curr.name === name ? prev : [...prev, curr.name],
        [],
      );
      const existenceApp = this.deployments.some(item => {
        return (
          deployments.includes(item.deployment.name) && item.deployment.labelApp
        );
      });
      if (data.deployment.labelApp && !existenceApp) {
        this.backfillMicroService.emit('');
      }
    }
  }

  deployChanged(control: AbstractControl) {
    const name = control.get('name').value;
    const data = this.deployments.find(item => item.deployment.name === name);
    if (data.deployment.labelApp) {
      this.handleAppPodSelectorDialog(data.deployment, control);
    }
    if (
      !this.deployments.some(
        item =>
          item.deployment.labelApp &&
          this.deploymentsName.includes(item.deployment.name),
      )
    ) {
      this.backfillMicroService.emit('');
    }

    if (
      data &&
      data.deployment.version &&
      !data.deployment.labelApp &&
      !data.deployment.labelVersion
    ) {
      control.get('version').setValue(data.deployment.version);
    }
    // TODO: 只有 version 被 pod selector;
    if (!data.deployment.labelApp && data.deployment.labelVersion) {
      control.get('version').setValue(data.deployment.labelVersion);
    }
    if (get(data, 'service.total') === 1) {
      this.store.selectedRelation = data;
    }
    if (this.isCreate && get(data, 'service.total') === 1) {
      this.store.serviceAction(data.service.name);
    }
    this.cdr.markForCheck();
  }

  /**
   * 处理 app 被用作 pod selector 逻辑
   * deployment app 标签 value 无法修改为微服务名称。
   * 若创建微服务，必须 微服务名称 === app.value
   * 若其(matchLabels 存在 version)version标签被用作了pod selector，则回填版本，输入框置灰，不可更改
   */
  handleAppPodSelectorDialog(
    deployment: DeploymentItem,
    control: AbstractControl,
  ) {
    const { labelApp, labelVersion, name } = deployment;
    const setValue = () => {
      if (labelVersion) {
        control.get('version').setValue(labelVersion);
      }
      this.backfillMicroService.emit(labelApp);
    };
    if (
      labelApp === this.store.currentMicroService &&
      this.store.disableMicroservice
    ) {
      setValue();
      return;
    }
    this.dialog
      .confirm({
        title: this.translate.get('pod_Selector_dialog_title', { name }),
        confirmType: ConfirmType.Warning,
        content: this.translate.get('pod_Selector_dialog_context', {
          labelApp,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        setValue();
      })
      .catch(() => {
        control.get('name').setValue('');
        control.get('version').setValue('');
      });
  }

  sortDeployment = (deploymensts: DeploymentRelation[]) => {
    const res = (deploymensts || []).map(item => {
      return {
        ...item,
        ...this.deploymentDisabled(item),
      };
    });
    return sortBy(res, result => result.disabled);
  };

  deploymentDisabled = (item: { deployment: DeploymentItem; service: any }) => {
    const { service, deployment } = item;
    const deployments = this.deploymentsName;
    const fillDeployments = this.fillDeployments.map(item => item.name);
    switch (true) {
      case deployment.labelVersion &&
        (!this.subsetPattern.pattern.test(deployment.labelVersion) ||
          deployment.labelVersion.length > 52):
        return {
          name: this.translate.get('app_version_non_compliance_with_rules'),
          disabled: true,
        };
      case deployment.labelApp &&
        (!this.namePattern.pattern.test(deployment.labelApp) ||
          deployment.labelApp.length > 32):
        return {
          name: this.translate.get('app_version_non_compliance_with_rules'),
          disabled: true,
        };
      case deployment.labelApp &&
        this.store.disableMicroservice &&
        this.store.currentMicroService !== deployment.labelApp:
        return {
          name: this.translate.get('prohibited_app_label_tip'),
          disabled: true,
        };
      case service.total > 1:
        return {
          name: this.translate.get(
            `${
              this.store.serviceName
                ? 'multiple_entry_add_tip'
                : 'multiple_entry_tip'
            }`,
          ),
          disabled: true,
        };
      case service.total === 1 &&
        this.store.serviceName &&
        service.name !== this.store.serviceName:
        return {
          name: this.translate.get('exist_entry_tip'),
          disabled: true,
        };
      case [
        ...this.existedDeployments,
        ...deployments,
        ...fillDeployments,
      ].includes(deployment.name):
        return {
          name: this.translate.get('exit_deploy_tip'),
          disabled: true,
        };
      default:
        return {
          disabled: false,
        };
    }
  };

  versionChange() {
    // TODO: 修改 version 同步验证其他 version
    // 解决第一个和第二个存在相同版本，修改第一个版本后，第二个冲突提示还存在
    // 此方法比较消耗性能，每次更新都会重新检查所有，后续看是否有优化的可能
    this.form.controls.forEach(control => {
      control.get('version').updateValueAndValidity();
    });
  }
}
