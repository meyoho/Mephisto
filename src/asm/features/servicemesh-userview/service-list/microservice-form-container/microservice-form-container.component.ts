import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import { DialogService, MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  getDeployments,
  MicroService,
  MicroServiceStore,
  ServiceEntrance,
  ServiceListService,
} from '@asm/api/service-list';

import { set } from 'lodash-es';
import { of } from 'rxjs';
@Component({
  selector: 'alo-microservice-form-container',
  templateUrl: './microservice-form-container.component.html',
  styleUrls: ['./microservice-form-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MicroserviceFormContainerComponent implements OnInit {
  @Input() data: MicroService;
  @ViewChild('form')
  form: NgForm;

  microserviceModel: MicroService;
  namespace = this.route.snapshot.paramMap.get('namespace');
  cluster = this.route.snapshot.paramMap.get('cluster');
  name = this.route.snapshot.paramMap.get('name');
  submitting = false;
  notificationVisible = false;
  constructor(
    private readonly api: ServiceListService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly location: Location,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
    private readonly store: MicroServiceStore,
    private readonly dialog: DialogService,
  ) {}

  ngOnInit() {
    this.updateEntrance();
  }

  updateEntrance() {
    if (this.data) {
      this.microserviceModel = this.data;
    }
  }

  getServiceRelation() {
    if (this.store.serviceName) {
      return this.api.getRelation(
        this.namespace,
        this.cluster,
        this.store.serviceName,
      );
    }
    return of(null);
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    // 先判断加入的svc是否还存在其他deployment，存在弹出框提示
    this.getServiceRelation().subscribe((result: ServiceEntrance[]) => {
      const { microserviceForm } = this.form.value;
      if (result) {
        const service = result[0].services;
        // 统一平台、自定义服务入口，iscreatebysystem = true; 避免后续数据迁移及改动
        set(
          microserviceForm,
          'spec.services',
          service.map(item => ({ ...item, iscreatebysystem: true })),
        );
        const deployments = getDeployments(microserviceForm);
        const {
          workloadNames,
          addedDeployment,
        } = this.api.getRelationDeployName(result, deployments);
        if (workloadNames) {
          this.dialog.confirm({
            title: this.translate.get('workload_relation_tip', {
              name: addedDeployment[0],
              workloads: workloadNames,
            }),
            confirmText: this.translate.get('i_know'),
            cancelButton: false,
          });
          return;
        }
      }
      if (!this.data) {
        this.api.create(this.cluster, microserviceForm).subscribe(
          _ => {
            this.handleSucc(this.k8sUtil.getName(microserviceForm), 'created');
            const name = this.k8sUtil.getName(microserviceForm);
            this.router.navigate(['../', name], {
              relativeTo: this.route,
            });
          },
          () => {
            this.handleError();
          },
        );
      } else {
        this.api.update(this.cluster, microserviceForm).subscribe(
          _ => {
            const name = this.k8sUtil.getName(microserviceForm);
            this.handleSucc(name, 'update');
            this.router.navigate([`../../`, name], {
              relativeTo: this.route,
            });
          },
          () => {
            this.handleError();
          },
        );
      }
    });
  }

  private handleSucc(name: string, action: string) {
    this.submitting = false;
    this.cdr.markForCheck();
    this.message.success(
      this.translate.get(`microservice_${action}_successfully`, {
        name: name,
      }),
    );
  }

  private handleError() {
    this.submitting = false;
    this.cdr.markForCheck();
  }

  cancel() {
    this.location.back();
  }
}
