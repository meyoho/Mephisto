import { ChangeDetectionStrategy, Component } from '@angular/core';
@Component({
  templateUrl: './service-list-create.component.html',
  styleUrls: ['./service-list-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceListCreateComponent {
  constructor() {}
}
