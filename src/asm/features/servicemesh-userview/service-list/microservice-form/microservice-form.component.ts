import { KubernetesResource, TOKEN_BASE_DOMAIN } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouteParams } from '@app/typings';
import { RESOURCE_DEFINITIONS } from '@app/utils';
import { K8S_RESOURCE_NAME_BASE } from '@app/utils/patterns';
import { MicroServiceStore } from '@asm/api/service-list';

import { cloneDeep, get, omit } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { map, tap } from 'rxjs/operators';
@Component({
  selector: 'alo-microservice-form',
  templateUrl: './microservice-form.component.html',
  styleUrls: ['./microservice-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MicroserviceFormComponent extends BaseResourceFormGroupComponent
  implements OnInit, OnDestroy {
  namePattern = K8S_RESOURCE_NAME_BASE;
  routeParams: RouteParams;
  readonly = false;
  routeParams$ = this.route.paramMap.pipe(
    map(params => ({
      namespace: params.get('namespace'),
      cluster: params.get('cluster'),
      name: params.get('name'),
    })),
    tap(params => {
      this.routeParams = params;
    }),
  );

  @Input() submitted: boolean;
  constructor(
    injector: Injector,
    private readonly route: ActivatedRoute,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
    private readonly store: MicroServiceStore,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: this.fb.group({
        namespace: this.fb.control(''),
        name: this.fb.control(''),
        annotations: this.fb.group({
          displayName: '',
        }),
      }),
      spec: this.fb.group({
        deployments: this.fb.control([]),
      }),
    });
  }

  adaptResourceModel(resource: KubernetesResource = {}) {
    const annotations = get(resource, 'metadata.annotations');
    if (annotations) {
      annotations.displayName = annotations[`${this.baseDomain}/display-name`];
    }
    return { ...resource };
  }

  adaptFormModel(form: { [key: string]: any }) {
    const cloneForm = cloneDeep(form) || {};
    const displayName = get(cloneForm, 'metadata.annotations.displayName');
    let annotations = get(cloneForm, 'metadata.annotations');
    if (annotations) {
      annotations[`${this.baseDomain}/display-name`] = displayName;
      annotations = omit(annotations, 'displayName');
      cloneForm.metadata.annotations = annotations;
    }
    return cloneForm;
  }

  getDefaultFormModel() {
    const definition = RESOURCE_DEFINITIONS.MICROSERVICE;
    return {
      apiVersion: `${definition.apiGroup}/${definition.apiVersion}`,
      kind: 'MicroService',
      metadata: {
        name: '',
        namespace: this.routeParams.namespace,
        annotations: {
          displayName: '',
        },
      },
      spec: {
        deployments: [
          {
            name: '',
            version: '',
          },
        ],
      },
    };
  }

  ngOnInit() {
    super.ngOnInit();
  }

  backfillMicroService(name: string) {
    const nameControl = this.form.get('metadata.name');
    if (name) {
      nameControl.setValue(name);
    }
    this.readonly = !!name;
    this.store.disableMicroservice$.next(!!name);
  }

  nameChange(name: string) {
    this.store.currentMicroService$.next(name);
  }

  ngOnDestroy() {
    this.store.disableMicroservice$.next(false);
    this.store.currentMicroService$.next('');
  }
}
