import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FeatureSharedCommonModule } from '@app/features-shared/common';
import { SharedModule } from '@app/shared';

import { ProtocolModule } from '../protocol/protocol.module';
import { WhitelistModule } from '../whitelist/whitelist.module';
import { LoadBalancingModule } from './../load-balancing/load-balancing.module';
import { ServicemeshSharedModule } from './../shared/servicemesh-shared.module';
import { MicroserviceFormContainerComponent } from './microservice-form-container/microservice-form-container.component';
import { MicroserviceFormComponent } from './microservice-form/microservice-form.component';
import { ServiceListCreateComponent } from './service-list-create/service-list-create.component';
import { BaseInfoComponent } from './service-list-detail/base-info/base-info.component';
import { ConditionRuleContainerComponent } from './service-list-detail/condition-rule-diaolog/condition-rule-container/condition-rule-container.component';
import { ConditionRuleDiaologComponent } from './service-list-detail/condition-rule-diaolog/condition-rule-diaolog.component';
import { ServiceDialogComponent } from './service-list-detail/service-dialog/service-dialog.component';
import { ServiceFormComponent } from './service-list-detail/service-dialog/service-form/service-form.component';
import { ServicePortsComponent } from './service-list-detail/service-dialog/service-ports/service-ports.component';
import { ServiceGraphComponent } from './service-list-detail/service-graph/service-graph.component';
import { ServiceListDetailComponent } from './service-list-detail/service-list-detail.component';
import { ConnectionPoolDialogComponent } from './service-list-detail/strategy-table/connectionPool-dialog/connectionPool-dialog.component';
import { OutlierDetectionDialogComponent } from './service-list-detail/strategy-table/outlierDetection-dialog/outlierDetection-dialog.component';
import { StrategyTableDetailComponent } from './service-list-detail/strategy-table/strategy-table-detail/strategy-table-detail.component';
import { StrategyTableComponent } from './service-list-detail/strategy-table/strategy-table.component';
import {
  AddServiceVersionDialogComponent,
  CannotBeAllValidatorDirective,
  CreateServiceVersionContainerComponent,
  CreateServiceVersionDialogComponent,
  ImagesTableComponent,
  ServiceVersionDialogComponent,
  ServiceVersionFormComponent,
  WorkloadTableComponent,
} from './service-list-detail/workload-table';
import { DeployTagsComponent } from './service-list-table/deploy-tags/deploy-tags.component';
import { ServiceListTableComponent } from './service-list-table/service-list-table.component';
import { ServiceListComponent } from './service-list.component';
import { ServiceListRoutes } from './service-list.routing';
import { ServiceVersionTableComponent } from './service-version-table/service-version-table.component';

@NgModule({
  imports: [
    CommonModule,
    ServiceListRoutes,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    FeatureSharedCommonModule,
    ServicemeshSharedModule,
    LoadBalancingModule,
    ProtocolModule,
    WhitelistModule,
  ],
  declarations: [
    ServiceListComponent,
    ServiceListTableComponent,
    ServiceListCreateComponent,
    MicroserviceFormComponent,
    ServiceListDetailComponent,
    DeployTagsComponent,
    StrategyTableComponent,
    MicroserviceFormContainerComponent,
    ServiceGraphComponent,
    ServiceDialogComponent,
    ServicePortsComponent,
    ServiceFormComponent,
    StrategyTableDetailComponent,
    OutlierDetectionDialogComponent,
    ConnectionPoolDialogComponent,
    WorkloadTableComponent,
    BaseInfoComponent,
    ServiceVersionFormComponent,
    ServiceVersionDialogComponent,
    AddServiceVersionDialogComponent,
    CreateServiceVersionDialogComponent,
    CannotBeAllValidatorDirective,
    ServiceVersionTableComponent,
    CreateServiceVersionContainerComponent,
    ImagesTableComponent,
    ConditionRuleDiaologComponent,
    ConditionRuleContainerComponent,
  ],
  entryComponents: [
    ServiceDialogComponent,
    OutlierDetectionDialogComponent,
    ConnectionPoolDialogComponent,
    ServiceVersionDialogComponent,
    AddServiceVersionDialogComponent,
    CreateServiceVersionDialogComponent,
    ConditionRuleDiaologComponent,
  ],
  exports: [StrategyTableComponent],
})
export class ServiceListModule {}
