import { RouterModule, Routes } from '@angular/router';

import { ServiceListCreateComponent } from './service-list-create/service-list-create.component';
import { ServiceListDetailComponent } from './service-list-detail/service-list-detail.component';
import { ServiceListComponent } from './service-list.component';
const routes: Routes = [
  {
    path: '',
    component: ServiceListComponent,
  },
  {
    path: 'create',
    component: ServiceListCreateComponent,
  },
  {
    path: ':name',
    component: ServiceListDetailComponent,
  },
];
export const ServiceListRoutes = RouterModule.forChild(routes);
