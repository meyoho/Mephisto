import { TOKEN_BASE_DOMAIN } from '@alauda/common-snippet';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouteParams } from '@app/typings';
import { RESOURCE_DEFINITIONS } from '@app/utils';
import {
  K8S_RESOURCE_NAME_BASE,
  POSITIVE_INT_PATTERN,
  POSITIVE_PERCENTAGE_PATTERN,
} from '@app/utils/patterns';
import { Canary, CanaryService } from '@asm/api/canary';

import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { map, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'alo-canary-form',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CanaryFormComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  @Input() readonly = false;

  namePattern = K8S_RESOURCE_NAME_BASE;
  positiveIntPattern = POSITIVE_INT_PATTERN;
  positivePercentagePattern = POSITIVE_PERCENTAGE_PATTERN;
  flowIncreaseCycleUnits = ['s', 'm'];
  flowIncreaseCycleUnit = '';
  routeParams: RouteParams;
  namespace = '';
  isDisplayExplain = true;
  routeParams$ = this.route.paramMap.pipe(
    map(params => ({
      namespace: params.get('namespace'),
      cluster: params.get('cluster'),
      name: params.get('name'),
    })),
    tap(params => {
      this.routeParams = params;
      this.namespace = params.namespace;
    }),
  );

  deployments$ = this.routeParams$.pipe(
    switchMap(params => {
      return this.api.getDeployments(params);
    }),
    map(res => res.items),
  );

  @Input() submitted: boolean;
  constructor(
    injector: Injector,
    private readonly route: ActivatedRoute,
    private readonly api: CanaryService,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: this.fb.group({
        namespace: this.fb.control(''),
        name: this.fb.control(''),
        annotations: this.fb.group({
          displayName: '',
        }),
      }),
      spec: this.fb.group({
        targetRef: this.fb.group({
          apiVersion: this.fb.control(''),
          kind: this.fb.control(''),
          name: this.fb.control(''),
        }),
        metrics: this.fb.group({
          minRequestSuccessRate: this.fb.control('', [
            Validators.required,
            Validators.pattern(this.positivePercentagePattern.pattern),
          ]),
          maxRequestDuration: this.fb.control('', [
            Validators.required,
            Validators.pattern(this.positiveIntPattern.pattern),
          ]),
        }),
        analysis: this.fb.group({
          interval: this.fb.control('', [
            Validators.required,
            Validators.pattern(this.positiveIntPattern.pattern),
          ]),
          intervalUnit: this.fb.control(''),
          threshold: this.fb.control('', [
            Validators.required,
            Validators.pattern(this.positiveIntPattern.pattern),
          ]),
          maxWeight: this.fb.control(''),
          stepWeight: this.fb.control('', [
            Validators.required,
            Validators.pattern(this.positivePercentagePattern.pattern),
          ]),
        }),
      }),
    });
  }

  adaptResourceModel(resource: Canary) {
    const annotations = get(resource, 'metadata.annotations');
    return resource
      ? {
          ...resource,
          metadata: {
            ...resource.metadata,
            annotations: {
              displayName: annotations[`${this.baseDomain}/display-name`],
            },
          },
          spec: {
            ...resource.spec,
            metrics: {
              minRequestSuccessRate: resource.spec.metrics.find(
                metric => metric.name === 'request-success-rate',
              ).thresholdRange.min,
              maxRequestDuration: resource.spec.metrics.find(
                metric => metric.name === 'request-duration',
              ).thresholdRange.max,
            },
            analysis: {
              ...resource.spec.analysis,
              interval: resource.spec.analysis.interval.slice(0, -1),
              intervalUnit: resource.spec.analysis.interval.slice(-1),
            },
          },
        }
      : {};
  }

  adaptFormModel(form: { [key: string]: any }) {
    const metrics = get(form, 'spec.metrics');
    const analysis = get(form, 'spec.analysis');
    return {
      ...form,
      metadata: {
        ...form.metadata,
        annotations: {
          [`${this.baseDomain}/display-name`]: get(
            form,
            'metadata.annotations.displayName',
          ),
        },
      },
      spec: {
        ...form.spec,
        analysis: {
          interval: `${get(analysis, 'interval')}${get(
            analysis,
            'intervalUnit',
          )}`,
          threshold: Number(get(analysis, 'threshold')),
          maxWeight: Number(get(analysis, 'maxWeight')),
          stepWeight: Number(get(analysis, 'stepWeight')),
        },
        metrics: [
          {
            name: 'request-success-rate',
            thresholdRange: {
              min: Number(get(metrics, 'minRequestSuccessRate')),
            },
          },
          {
            name: 'request-duration',
            thresholdRange: { max: Number(get(metrics, 'maxRequestDuration')) },
          },
        ],
      },
    };
  }

  getDefaultFormModel() {
    const definition = RESOURCE_DEFINITIONS.CANARY;
    return {
      apiVersion: `${definition.apiGroup}/${definition.apiVersion}`,
      kind: 'CanaryDelivery',
      metadata: {
        name: '',
        namespace: this.namespace,
        annotations: {
          displayName: '',
        },
      },
      spec: {
        targetRef: {
          apiVersion: 'apps/v1',
          kind: 'Deployment',
          name: '',
        },
        metrics: {
          minRequestSuccessRate: '99',
          maxRequestDuration: '500',
        },
        analysis: {
          interval: '1',
          intervalUnit: 'm',
          threshold: '10',
          maxWeight: 100,
          stepWeight: '10',
        },
      },
    };
  }

  ngOnInit() {
    super.ngOnInit();
  }
}
