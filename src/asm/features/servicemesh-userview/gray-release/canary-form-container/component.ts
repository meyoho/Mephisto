import { K8sUtilService, TranslateService } from '@alauda/common-snippet';
import { MessageService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Canary, CanaryService } from '@asm/api/canary';

@Component({
  selector: 'alo-canary-form-container',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CanaryFormContainerComponent implements OnInit {
  @Input() data: Canary;
  @ViewChild('form')
  form: NgForm;

  canaryModel: Canary;
  cluster = this.route.snapshot.paramMap.get('cluster');
  submitting = false;
  notificationVisible = false;
  constructor(
    private readonly api: CanaryService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly message: MessageService,
    private readonly cdr: ChangeDetectorRef,
    private readonly location: Location,
    private readonly translate: TranslateService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  ngOnInit() {
    if (this.data) {
      this.canaryModel = this.data;
    }
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const { canaryForm } = this.form.value;
    this.submitting = true;
    if (!this.data) {
      this.api.create(this.cluster, canaryForm).subscribe(
        _ => {
          this.handleSuccess(this.k8sUtil.getName(canaryForm));
          const name = this.k8sUtil.getName(canaryForm);
          this.router.navigate(['../', name], {
            relativeTo: this.route,
          });
        },
        () => {
          this.handleError();
        },
      );
    } else {
      this.api.update(this.cluster, canaryForm).subscribe(
        _ => {
          const name = this.k8sUtil.getName(canaryForm);
          this.handleSuccess(name);
          this.router.navigate([`../../`, name], {
            relativeTo: this.route,
          });
        },
        () => {
          this.handleError();
        },
      );
    }
  }

  private handleSuccess(name: string) {
    this.message.success(
      this.translate.get(
        this.data
          ? 'canary_update_successfully'
          : 'canary_created_successfully',
        {
          name: name,
        },
      ),
    );
  }

  private handleError() {
    this.submitting = false;
    this.cdr.markForCheck();
  }

  cancel() {
    this.location.back();
  }
}
