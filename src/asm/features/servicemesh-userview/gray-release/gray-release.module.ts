import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FeatureSharedCommonModule } from '@app/features-shared/common';
import { SharedModule } from '@app/shared';

import { CanaryFormContainerComponent } from './canary-form-container/component';
import { CanaryFormComponent } from './canary-form/component';
import { CreateCanaryComponent } from './create-canary/component';
import { CanaryDetailComponent } from './detail/component';
import { GrayReleaseComponent } from './gray-release.component';
import { GrayReleaseRoutes } from './gray-release.routing';
import { ServiceListTableComponent } from './list-table/component';
import { RecordTagsComponent } from './list-table/record-tags/component';
import { GrayReleaseTypeSelectDialogComponent } from './list-table/type-select-dialog/component';
import { CanaryRecordDetailComponent } from './record-detail/component';
import { RecordLogDialogComponent } from './record-log-dialog/component';
import { RecordLogComponent } from './record-log/component';
import { ReleaseDialogComponent } from './release-dialog/component';
import { UpdateCanaryComponent } from './update-canary/component';

@NgModule({
  imports: [
    CommonModule,
    GrayReleaseRoutes,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    FeatureSharedCommonModule,
  ],
  declarations: [
    GrayReleaseComponent,
    ServiceListTableComponent,
    GrayReleaseTypeSelectDialogComponent,
    CreateCanaryComponent,
    CanaryFormComponent,
    CanaryFormContainerComponent,
    CanaryDetailComponent,
    UpdateCanaryComponent,
    ReleaseDialogComponent,
    CanaryRecordDetailComponent,
    RecordTagsComponent,
    RecordLogComponent,
    RecordLogDialogComponent,
  ],
  entryComponents: [
    GrayReleaseTypeSelectDialogComponent,
    ReleaseDialogComponent,
    RecordLogDialogComponent,
  ],
})
export class GrayReleaseModule {}
