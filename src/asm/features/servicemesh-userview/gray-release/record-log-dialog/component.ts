import { viewActions, yamlWriteOptions } from '@alauda/common-snippet';
import { DIALOG_DATA } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Canary } from '@asm/api/canary';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecordLogDialogComponent {
  editorActions = viewActions;
  editorOptions = yamlWriteOptions;
  yamlInputValue = '';
  submitting = false;

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      id: string;
      resource: Canary;
    },
  ) {}
}
