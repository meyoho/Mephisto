import {
  AsyncDataLoader,
  FeatureGateService,
  K8sPermissionService,
  K8sResourceAction,
  Reason,
  TranslateService,
} from '@alauda/common-snippet';
import {
  ConfirmType,
  DialogService,
  DialogSize,
  MessageService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Deployment, RouteParams } from '@app/typings';
import { RESOURCE_TYPES } from '@app/utils';
import {
  Canary,
  CanaryService,
  handleRecordStatus,
  Metric,
  parseToWorkloadStatus,
  RecordStatus,
} from '@asm/api/canary';

import { first } from 'lodash-es';
import { combineLatest, Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { RecordLogDialogComponent } from '../record-log-dialog/component';
import { ReleaseDialogComponent } from '../release-dialog/component';
@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CanaryDetailComponent {
  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly k8sPermission: K8sPermissionService,
    private readonly api: CanaryService,
    private readonly message: MessageService,
    private readonly fg: FeatureGateService,
  ) {}

  name: string;
  namespace: string;
  cluster: string;
  reason = Reason;
  targetRefName: string;
  primaryWorkload: Deployment;
  isDisplayReleaseRuleExplain = false;
  isDisplayServiceVersionExplain = false;
  columns = ['executeId', 'status', 'created_at', 'take_up_time'];
  private readonly reloadAction$$ = new Subject();
  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get('name'),
      namespace: params.get('namespace') || '',
      cluster: params.get('cluster') || '',
    })),
    tap(params => {
      this.name = params.name;
      this.namespace = params.namespace;
      this.cluster = params.cluster;
      this.cdr.markForCheck();
    }),
    publishReplay(1),
    refCount(),
  );

  parseToWorkloadStatus = (res: Deployment) =>
    parseToWorkloadStatus(RESOURCE_TYPES.DEPLOYMENT, res);

  fetch = (params: RouteParams) => {
    return this.api
      .watchCanaryDetail(params)
      .pipe(tap(res => (this.targetRefName = res.spec.targetRef.name)));
  };

  dataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: this.fetch,
    interval: 10 * 1000,
  });

  updated$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.CANARY,
        action: K8sResourceAction.UPDATE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  deleted$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.CANARY,
        action: K8sResourceAction.DELETE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  featGuardStatus$ = this.fg.isEnabled('asm-canary');

  workload$ = combineLatest([this.params$, this.dataLoader.data$]).pipe(
    switchMap(([params, _]) =>
      this.api.getDeployment(
        params.cluster,
        params.namespace,
        this.targetRefName,
      ),
    ),
    map(res => parseToWorkloadStatus(RESOURCE_TYPES.DEPLOYMENT, res)),
    publishReplay(1),
    refCount(),
  );

  primaryWorkload$ = combineLatest([
    this.params$,
    this.reloadAction$$.pipe(startWith(null as void)),
    this.dataLoader.data$,
  ]).pipe(
    switchMap(([params, _]) =>
      this.api.getDeployment(
        params.cluster,
        params.namespace,
        `${this.targetRefName}-primary`,
      ),
    ),
    tap(res => (this.primaryWorkload = res)),
    map(res => parseToWorkloadStatus(RESOURCE_TYPES.DEPLOYMENT, res)),
    publishReplay(1),
    refCount(),
  );

  getRecordStatusText = (status: RecordStatus) => {
    return handleRecordStatus(status).text;
  };

  getRecordStatusIcon = (status: RecordStatus) => {
    return handleRecordStatus(status).icon;
  };

  update() {
    this.router.navigate(['../', 'update', this.name], {
      relativeTo: this.route,
    });
  }

  delete(resource: Canary) {
    this.dialog
      .confirm({
        title: this.translate.get('canary_delete_confirm_title', {
          name: this.name,
        }),
        confirmType: ConfirmType.Danger,
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.api.delete(this.cluster, resource).subscribe(() => {
          this.message.success(
            this.translate.get('canary_deleted_successfully', {
              name: this.name,
            }),
          );
          this.router.navigate(['../'], {
            relativeTo: this.route,
          });
        });
      });
  }

  refresh() {
    this.dataLoader.reload();
  }

  getRequestSuccessRate = (metric: Metric[]) => {
    return metric.reduce((pre, curr) => {
      return curr.name === 'request-success-rate'
        ? curr.thresholdRange.min
        : pre;
    }, '-');
  };

  getRequestDuration = (metric: Metric[]) => {
    return metric.reduce((pre, curr) => {
      return curr.name === 'request-duration' ? curr.thresholdRange.max : pre;
    }, '-');
  };

  getTimeRange(startTime: string | number, endTime: string | number) {
    const duration = Number(endTime) - Number(startTime);
    if (duration <= 0) {
      return '';
    }
    const minute = parseInt('' + duration / 60)
      ? `${parseInt('' + duration / 60)}${this.translate.get('minute')}`
      : '';
    const second = `${duration % 60}${this.translate.get('second')} `;
    return `${minute} ${second}`;
  }

  release(name: string) {
    this.dialog
      .open(ReleaseDialogComponent, {
        data: {
          cluster: this.cluster,
          namespace: this.namespace,
          name: name,
        },
        size: DialogSize.Large,
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.message.success(this.translate.get('release_success'));
          this.refresh();
        }
      });
  }

  showLog(id: string, resource: Canary) {
    this.dialog.open(RecordLogDialogComponent, {
      data: { id: `#${id}`, resource },
      size: DialogSize.Large,
    });
  }

  getLatestRecord(resource: Canary) {
    return first(resource.spec?.records || []);
  }

  updateReplicas(num: number) {
    this.api
      .updateReplicas(num, this.cluster, this.primaryWorkload)
      .subscribe(_ => this.reloadAction$$.next());
  }

  isReleasing(resource: Canary) {
    const record = this.getLatestRecord(resource);
    return record
      ? handleRecordStatus(record.status).text === 'releasing'
      : false;
  }

  checkReleaseStatus(resource: Canary) {
    if (resource.status?.phase === 'Initializing') {
      return {
        enable: false,
        message: resource.status.conditions
          ? 'initialize_abnormal_cant_release'
          : 'initializing_cant_release',
      };
    } else if (this.isReleasing(resource)) {
      return {
        enable: false,
        message: 'releasing_cant_release',
      };
    } else {
      return {
        enable: true,
        message: '',
      };
    }
  }
}
