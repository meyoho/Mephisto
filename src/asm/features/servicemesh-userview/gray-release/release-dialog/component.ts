import { viewActions, yamlWriteOptions } from '@alauda/common-snippet';
import { DialogRef, DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { CanaryService } from '@asm/api/canary';

import { safeDump, safeLoad } from 'js-yaml';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReleaseDialogComponent implements OnInit {
  editorActions = viewActions;
  editorOptions = yamlWriteOptions;
  yamlInputValue = '';
  submitting = false;

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      cluster: string;
      namespace: string;
      name: string;
    },
    private readonly dialogRef: DialogRef<ReleaseDialogComponent>,
    private readonly api: CanaryService,
    private readonly cdr: ChangeDetectorRef,
  ) {}

  release() {
    this.submitting = true;
    this.api
      .updateDeployment(this.data.cluster, safeLoad(this.yamlInputValue))
      .subscribe(
        () => {
          this.dialogRef.close(true);
        },
        () => {
          this.submitting = false;
        },
      );
  }

  ngOnInit() {
    this.api
      .getDeployment(this.data.cluster, this.data.namespace, this.data.name)
      .subscribe(res => {
        this.yamlInputValue = safeDump(res, {
          lineWidth: 9999,
          sortKeys: true,
        });
        this.cdr.markForCheck();
      });
  }
}
