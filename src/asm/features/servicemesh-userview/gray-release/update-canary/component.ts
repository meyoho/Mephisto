import { AsyncDataLoader } from '@alauda/common-snippet';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CanaryService } from '@asm/api/canary';

import { map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  templateUrl: './template.html',
})
export class UpdateCanaryComponent {
  constructor(
    private readonly route: ActivatedRoute,
    private readonly api: CanaryService,
  ) {}

  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get('name') || '',
      namespace: params.get('namespace') || '',
      cluster: params.get('cluster') || '',
    })),
    publishReplay(1),
    refCount(),
  );

  fetch = (params: { namespace: string; name: string; cluster: string }) =>
    this.api.getCanaryDetail(params);

  dataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: this.fetch,
  });
}
