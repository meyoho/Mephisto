import { readonlyOptions, viewActions } from '@alauda/common-snippet';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { dateFormat } from '@app/utils/unit';
import { Canary, Event } from '@asm/api/canary';

@Component({
  selector: 'alo-record-log',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecordLogComponent {
  @Input() id: string;
  @Input() resource: Canary;
  editorActions = viewActions;
  editorOptions = readonlyOptions;

  getLogs = (resource: Canary) => {
    if (!resource.spec.records) {
      return '';
    }
    const record = this.getRecord(resource);
    return this.formatAllLogs(record.events);
  };

  getRecord(resource: Canary) {
    return resource.spec.records.find(item => `#${item.id}` === this.id);
  }

  private formatAllLogs(logs: Event[]) {
    return logs.map(line => this.formatLine(line)).join('\n');
  }

  private formatLine(line: Event) {
    return `${dateFormat(Number(line.timestamp))} ${line.message}`;
  }
}
