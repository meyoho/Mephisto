import { RouterModule, Routes } from '@angular/router';

import { CreateCanaryComponent } from './create-canary/component';
import { CanaryDetailComponent } from './detail/component';
import { GrayReleaseComponent } from './gray-release.component';
import { CanaryRecordDetailComponent } from './record-detail/component';
import { UpdateCanaryComponent } from './update-canary/component';

const routes: Routes = [
  {
    path: '',
    component: GrayReleaseComponent,
  },
  {
    path: 'canary',
    component: CreateCanaryComponent,
  },
  {
    path: ':name',
    component: CanaryDetailComponent,
  },
  {
    path: 'update/:name',
    component: UpdateCanaryComponent,
  },
  {
    path: ':name/:id',
    component: CanaryRecordDetailComponent,
  },
];
export const GrayReleaseRoutes = RouterModule.forChild(routes);
