import {
  baseDomain,
  K8sPermissionService,
  K8sResourceAction,
  K8sUtilService,
  TranslateService,
} from '@alauda/common-snippet';
import {
  ConfirmType,
  DialogService,
  DialogSize,
  MessageService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RESOURCE_TYPES } from '@app/utils';
import { Canary, CanaryService, handleRecordStatus } from '@asm/api/canary';

import { first } from 'lodash-es';
import { map, publishReplay, refCount, switchMap, tap } from 'rxjs/operators';

import { ReleaseDialogComponent } from '../release-dialog/component';

@Component({
  selector: 'alo-gray-release-table',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceListTableComponent {
  @Input()
  data: [];

  @Output()
  reload = new EventEmitter();

  prefix = baseDomain;
  columns = ['name', 'workload', 'releaseType', 'taskStatus', 'action'];
  cluster = '';
  namespace = '';
  params$ = this.route.paramMap.pipe(
    map(params => ({
      namespace: params.get('namespace') || '',
      cluster: params.get('cluster') || '',
    })),
    tap(params => {
      this.cluster = params.cluster;
      this.namespace = params.namespace;
    }),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly dialog: DialogService,
    private readonly translate: TranslateService,
    private readonly message: MessageService,
    private readonly api: CanaryService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly k8sUtil: K8sUtilService,
  ) {}

  updated$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.CANARY,
        action: K8sResourceAction.UPDATE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  deleted$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.CANARY,
        action: K8sResourceAction.DELETE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  delete(resource: Canary) {
    const name = this.k8sUtil.getName(resource);
    this.dialog
      .confirm({
        title: this.translate.get('canary_delete_confirm_title', {
          name: name,
        }),
        confirmType: ConfirmType.Danger,
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.api.delete(this.cluster, resource).subscribe(() => {
          this.message.success(
            this.translate.get('canary_deleted_successfully', {
              name: name,
            }),
          );
          this.reload.emit();
        });
      });
  }

  release(resource: Canary) {
    this.dialog
      .open(ReleaseDialogComponent, {
        data: {
          cluster: this.cluster,
          namespace: this.namespace,
          name: resource.spec.targetRef.name,
        },
        size: DialogSize.Large,
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.message.success(this.translate.get('release_success'));
          this.router.navigate(['../', this.k8sUtil.getName(resource)], {
            relativeTo: this.route,
          });
        }
      });
  }

  getLatestRecord(resource: Canary) {
    return first(resource.spec?.records || []);
  }

  isReleasing(resource: Canary) {
    const record = this.getLatestRecord(resource);
    return record
      ? handleRecordStatus(record.status).text === 'releasing'
      : false;
  }

  checkReleaseStatus(resource: Canary) {
    if (resource.status?.phase === 'Initializing') {
      return {
        enable: false,
        message: resource.status.conditions
          ? 'initialize_abnormal_cant_release'
          : 'initializing_cant_release',
      };
    } else if (this.isReleasing(resource)) {
      return {
        enable: false,
        message: 'releasing_cant_release',
      };
    } else {
      return {
        enable: true,
        message: '',
      };
    }
  }
}
