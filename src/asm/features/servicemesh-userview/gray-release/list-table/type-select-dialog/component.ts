import { DialogRef } from '@alauda/ui';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GrayReleaseTypeSelectDialogComponent {
  iconBase = 'icons/microservice/gray-release';
  types = [
    {
      title: 'canary_delivery',
      description: 'canary_delivery_description',
      icon: `${this.iconBase}/canary.svg`,
      link: 'canary',
    },
    {
      title: 'blue_green_deployments',
      description: 'blue_green_deployments_description',
      icon: `${this.iconBase}/blue-green.svg`,
      link: '',
    },
    {
      title: 'A_B_testing',
      description: 'A_B_testing_description',
      icon: `${this.iconBase}/AB.svg`,
      link: '',
    },
  ];

  constructor(
    private readonly dialogRef: DialogRef<GrayReleaseTypeSelectDialogComponent>,
  ) {}

  selectedType(link: string) {
    if (link) {
      this.dialogRef.close(link);
    }
  }
}
