import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { handleRecordStatus, Record } from '@asm/api/canary';

@Component({
  selector: 'alo-record-tags',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecordTagsComponent {
  @Input() records: Record[];
  @Input() name: string;
  handleRecordStatus = handleRecordStatus;
}
