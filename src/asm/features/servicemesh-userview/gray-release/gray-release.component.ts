import {
  FeatureGateService,
  K8sApiService,
  K8sPermissionService,
  K8sResourceAction,
  K8SResourceList,
  Reason,
  ResourceListParams,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RESOURCE_TYPES } from '@app/utils';
import { Canary, CanaryService } from '@asm/api/canary';

import { isEqual } from 'lodash-es';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import { GrayReleaseTypeSelectDialogComponent } from './list-table/type-select-dialog/component';

@Component({
  selector: 'alo-gray-release',
  templateUrl: './gray-release.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GrayReleaseComponent implements OnInit, OnDestroy {
  loading = false;
  reason = Reason;
  cluster: string;
  namespace: string;
  list: K8SResourceList<Canary>;
  params$ = combineLatest([this.route.paramMap, this.route.queryParamMap]).pipe(
    map(([params, queryParams]) => {
      return {
        keywords: queryParams.get('keywords'),
        namespace: params.get('namespace'),
        cluster: params.get('cluster'),
      };
    }),
    tap(res => {
      this.cluster = res.cluster;
      this.namespace = res.namespace;
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishReplay(1),
    refCount(),
  );

  creatable$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.MICROSERVICE,
        action: K8sResourceAction.CREATE,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  fetchParams$ = this.params$.pipe(
    map(params => ({
      namespace: params.namespace,
      cluster: params.cluster,
      keyword: params.keywords || '',
    })),
  );

  featGuardStatus$ = this.fg.isEnabled('asm-canary');

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly api: CanaryService,
    private readonly k8sPermission: K8sPermissionService,
    private readonly dialog: DialogService,
    private readonly fg: FeatureGateService,
    private readonly k8sApi: K8sApiService,
  ) {}

  fetch = (params: ResourceListParams) => {
    params.limit = '20';
    return this.api.getList(params);
  };

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords },
      queryParamsHandling: 'merge',
    });
  }

  createGrayRelease() {
    this.dialog
      .open(GrayReleaseTypeSelectDialogComponent, {
        size: DialogSize.Large,
      })
      .afterClosed()
      .subscribe(result => {
        if (result) {
          this.selectedType(result);
        }
      });
  }

  selectedType(link: string) {
    this.router.navigate(['./', link], {
      relativeTo: this.route,
    });
  }

  ngOnInit() {
    this.list = new K8SResourceList<Canary>({
      fetchParams$: this.fetchParams$,
      fetcher: this.fetch.bind(this),
      watcher: seed =>
        this.k8sApi.watchResourceChange(seed, {
          type: RESOURCE_TYPES.CANARY,
          cluster: this.cluster,
          namespace: this.namespace,
        }),
    });
  }

  ngOnDestroy() {
    this.list.destroy();
  }
}
