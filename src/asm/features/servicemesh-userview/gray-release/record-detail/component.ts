import { AsyncDataLoader } from '@alauda/common-snippet';
import { Status, StatusType } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouteParams } from '@app/typings';
import { Canary, CanaryService, handleRecordStatus } from '@asm/api/canary';

import { map, publishReplay, refCount, tap } from 'rxjs/operators';
@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CanaryRecordDetailComponent {
  handleRecordStatus = handleRecordStatus;
  name: string;
  namespace: string;
  cluster: string;
  id: string;
  params$ = this.route.paramMap.pipe(
    map(params => ({
      name: params.get('name'),
      id: params.get('id'),
      namespace: params.get('namespace') || '',
      cluster: params.get('cluster') || '',
    })),
    tap(params => {
      this.name = params.name;
      this.namespace = params.namespace;
      this.cluster = params.cluster;
      this.id = params.id;
      this.cdr.markForCheck();
    }),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly route: ActivatedRoute,
    private readonly cdr: ChangeDetectorRef,
    private readonly api: CanaryService,
  ) {}

  fetch = (params: RouteParams) => {
    // TODO: return this.api.watchCanaryDetail(params);
    return this.api.getCanaryDetail(params);
  };

  dataLoader = new AsyncDataLoader({
    params$: this.params$,
    fetcher: this.fetch,
    interval: 10 * 1000,
  });

  refresh() {
    this.dataLoader.reload();
  }

  getStatus = (resource: Canary): Status[] => {
    return [
      {
        scale: this.getCurrentWeight(resource),
        type: StatusType.Primary,
      },
      {
        scale: 100 - this.getCurrentWeight(resource),
        type: StatusType.Success,
      },
    ];
  };

  getRecord(resource: Canary) {
    return resource.spec.records.find(item => `#${item.id}` === this.id);
  }

  getCurrentWeight(resource: Canary) {
    const record = this.getRecord(resource);
    return record.currentWeight || 0;
  }
}
