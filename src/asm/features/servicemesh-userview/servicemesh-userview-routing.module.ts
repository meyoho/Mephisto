import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JaegerComponent } from './jaeger/jaeger.component';
import { SecurityStrategyComponent } from './security-strategy/security-strategy.component';
import { TrafficPolicyComponent } from './traffic-policy/traffic-policy.component';
import { UserGuardService } from './user-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'overview',
    pathMatch: 'full',
  },
  {
    path: 'overview',
    loadChildren: () =>
      import('./overview/overview.module').then(M => M.OverviewModule),
    canActivate: [UserGuardService],
  },
  {
    path: 'gray-release',
    loadChildren: () =>
      import('./gray-release/gray-release.module').then(
        M => M.GrayReleaseModule,
      ),
    canActivate: [UserGuardService],
  },
  {
    path: 'data-panel',
    loadChildren: () =>
      import('./data-panel/data-panel.module').then(M => M.DataPanelModule),
    canActivate: [UserGuardService],
  },
  {
    path: 'service-list',
    loadChildren: () =>
      import('./service-list/service-list.module').then(
        M => M.ServiceListModule,
      ),
    canActivate: [UserGuardService],
  },
  {
    path: 'jaeger',
    component: JaegerComponent,
    canActivate: [UserGuardService],
  },
  {
    path: 'service_topology',
    loadChildren: () =>
      import('./service-topology/service-topology.module').then(
        M => M.ServiceTopologyModule,
      ),
    canActivate: [UserGuardService],
  },
  {
    path: 'service-gateway',
    loadChildren: () =>
      import(
        '@asm/features/servicemesh-userview/service-gateway/gateway.module'
      ).then(M => M.ServiceGatewayModule),
    canActivate: [UserGuardService],
  },
  {
    path: 'security-strategy',
    component: SecurityStrategyComponent,
    canActivate: [UserGuardService],
  },
  {
    path: 'traffic-policy',
    component: TrafficPolicyComponent,
    canActivate: [UserGuardService],
  },
  {
    path: 'resource-list',
    loadChildren: () =>
      import('./resource-list/module').then(M => M.ResourceManageModule),
    canActivate: [UserGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServicemeshUserviewRoutingModule {}
