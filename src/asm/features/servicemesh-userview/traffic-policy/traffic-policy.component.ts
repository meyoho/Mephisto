import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'alo-traffic-policy',
  templateUrl: './traffic-policy.component.html',
  styleUrls: ['./traffic-policy.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrafficPolicyComponent {
  constructor() {}
}
