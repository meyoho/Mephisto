import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';

import { LoadBalancingDetailComponent } from './commponents/load-balancing-detail/load-balancing-detail.component';
import { LoadBalancingComponent } from './commponents/load-balancing/load-balancing.component';
import { DeleteLoadBalancingDialogComponent } from './dialog/delete-load-balancing-dialog/delete-load-balancing-dialog.component';
import { LoadBalancingDialogComponent } from './dialog/load-balancing-dialog/load-balancing-dialog.component';

@NgModule({
  imports: [CommonModule, FormsModule, SharedModule],
  declarations: [
    LoadBalancingDialogComponent,
    DeleteLoadBalancingDialogComponent,
    LoadBalancingComponent,
    LoadBalancingDetailComponent,
  ],
  exports: [
    LoadBalancingComponent,
    LoadBalancingDialogComponent,
    DeleteLoadBalancingDialogComponent,
    LoadBalancingDetailComponent,
  ],
  entryComponents: [
    LoadBalancingDialogComponent,
    DeleteLoadBalancingDialogComponent,
    LoadBalancingDetailComponent,
  ],
})
export class LoadBalancingModule {}
