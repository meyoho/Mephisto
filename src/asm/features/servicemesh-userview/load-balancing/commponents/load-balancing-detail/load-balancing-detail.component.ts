import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { strategyPure } from '@asm/api/traffic-policy/traffic-policy.service';
import { LoadBalancer } from '@asm/api/traffic-policy/traffic-policy.types';
import {
  ConsistentHash,
  labelName,
} from '@asm/api/traffic-policy/traffic-policy.types';

import { get } from 'lodash-es';
@Component({
  selector: 'alo-load-balancing-detail',
  templateUrl: './load-balancing-detail.component.html',
  styleUrls: ['./load-balancing-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancingDetailComponent implements OnChanges {
  @Input()
  loadBalancer: LoadBalancer;
  @Input()
  host: string;
  @Input()
  hideHost: boolean;
  strategyPure = strategyPure;
  constructor(private cdr: ChangeDetectorRef) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (get(changes, 'loadBalancer.currentValue')) {
      if (get(this.loadBalancer, 'consistentHash.minimumRingSize')) {
        delete this.loadBalancer.consistentHash.minimumRingSize;
        this.cdr.markForCheck();
      }
    }
  }

  getLabelName(consistentHash: ConsistentHash) {
    const key = Object.keys(consistentHash)[0];
    return labelName[key];
  }

  getValueName(item: { value: string | boolean }) {
    const value = get(item, 'value');
    return typeof value === 'boolean' || (value !== '' && value) ? value : '-';
  }
}
