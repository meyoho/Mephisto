import {
  COMMON_WRITABLE_ACTIONS,
  K8sPermissionService,
  KubernetesResource,
} from '@alauda/common-snippet';
import { DialogService, DialogSize } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RESOURCE_TYPES } from '@app/utils';
import {
  strategyPure,
  TrafficPolicyService,
} from '@asm/api/traffic-policy/traffic-policy.service';
import { DestinationRuleResponse } from '@asm/api/traffic-policy/traffic-policy.types';

import { get, isEqual } from 'lodash-es';
import { combineLatest, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import { DeleteLoadBalancingDialogComponent } from './../../dialog/delete-load-balancing-dialog/delete-load-balancing-dialog.component';
import { LoadBalancingDialogComponent } from './../../dialog/load-balancing-dialog/load-balancing-dialog.component';
@Component({
  selector: 'alo-load-balancing',
  templateUrl: './load-balancing.component.html',
  styleUrls: ['./load-balancing.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancingComponent {
  columns = ['host', 'strategy', 'create_by', 'creationTimestamp', 'action'];
  isUpdate: boolean;
  refresh$ = new Subject<void>();
  project: string;
  strategyPure = strategyPure;
  cluster: string;
  namespace: string;
  params$ = combineLatest([this.route.paramMap, this.route.queryParamMap]).pipe(
    map(([params, queryParams]) => ({
      keywords: queryParams.get('keywords') || '',
      sort: queryParams.get('sort') || 'creationTimestamp',
      direction: queryParams.get('direction') || 'desc',
      pageIndex: +(queryParams.get('page') || '1') - 1,
      pageSize: +(queryParams.get('page_size') || '10'),
      project: params.get('project'),
      namespace: params.get('namespace'),
      cluster: params.get('cluster') || '',
    })),
    tap(params => {
      this.project = params.project;
      this.cluster = params.cluster;
      this.namespace = params.namespace;
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.params$.pipe(
    map(params => params.keywords),
    publishReplay(1),
    refCount(),
  );

  sort$ = this.params$.pipe(
    map(params => ({
      active: params.sort,
      direction: params.direction,
    })),
    publishReplay(1),
    refCount(),
  );

  project$ = this.params$.pipe(
    map(params => params.project),
    tap(project => {
      this.project = project;
    }),
    publishReplay(1),
    refCount(),
  );

  namespace$ = this.params$.pipe(
    map(params => params.namespace),
    publishReplay(1),
    refCount(),
  );

  pageIndex$ = this.params$.pipe(
    map(params => params.pageIndex),
    publishReplay(1),
    refCount(),
  );

  pageSize$ = this.params$.pipe(
    map(params => params.pageSize),
    publishReplay(1),
    refCount(),
  );

  permissions$ = this.params$.pipe(
    switchMap(params =>
      this.k8sPermission.isAllowed({
        type: RESOURCE_TYPES.DESTINATIONRULE,
        action: COMMON_WRITABLE_ACTIONS,
        namespace: params.namespace,
        cluster: params.cluster,
      }),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private readonly dialog: DialogService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly apiService: TrafficPolicyService,
    private readonly k8sPermission: K8sPermissionService,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  sortChange(event: { active: string; direction: string }) {
    this.router.navigate([], {
      queryParams: { sort: event.active, direction: event.direction, page: 1 },
      queryParamsHandling: 'merge',
    });
  }

  fetch = (params: any) => {
    return this.apiService.getList(params);
  };

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords, page: 1 },
      queryParamsHandling: 'merge',
    });
  }

  pageChanged(param: string, value: number) {
    let page;
    if (param === 'page_size') {
      page = { page: 1 };
    }
    this.router.navigate([], {
      queryParams: { [param]: value, ...page },
      queryParamsHandling: 'merge',
    });
  }

  create() {
    this.isUpdate = false;
    const dialogRef = this.dialog.open(LoadBalancingDialogComponent, {
      data: {
        isUpdate: this.isUpdate,
        namespace: this.namespace,
        cluster: this.cluster,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh(result);
    });
  }

  update(item: DestinationRuleResponse) {
    this.isUpdate = true;
    const dialogRef = this.dialog.open(LoadBalancingDialogComponent, {
      data: {
        isUpdate: this.isUpdate,
        namespace: this.namespace,
        host: get(item, 'spec.host'),
        name: get(item, 'metadata.name'),
        cluster: this.cluster,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.refresh(result);
    });
  }

  delete(item: DestinationRuleResponse) {
    const dialogRef = this.dialog.open(DeleteLoadBalancingDialogComponent, {
      size: DialogSize.Small,
      data: {
        host: get(item, 'spec.host'),
        name: get(item, 'metadata.name'),
        namespace: this.namespace,
        cluster: this.cluster,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      this.refresh(result);
    });
  }

  refresh(res: boolean) {
    if (res) {
      this.refresh$.next();
    }
  }

  createByPure = (metadata: KubernetesResource['metadata']) => {
    return get(metadata.labels, `${this.asmBaseDomain}/creator`) || '-';
  };

  loadBalancerPure = (item: DestinationRuleResponse) => {
    return get(item, 'spec.trafficPolicy.loadBalancer') || {};
  };
}
