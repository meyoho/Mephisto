import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { TrafficPolicyService } from '@asm/api/traffic-policy/traffic-policy.service';
import {
  LoadBalancer,
  LoadBalancerCrd,
} from '@asm/api/traffic-policy/traffic-policy.types';
import { get, set } from 'lodash-es';
@Component({
  selector: 'alo-delete-load-balancing-dialog',
  templateUrl: './delete-load-balancing-dialog.component.html',
  styleUrls: ['./delete-load-balancing-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteLoadBalancingDialogComponent implements OnInit {
  submitting = false;
  loadBalancer: LoadBalancer;
  result: LoadBalancerCrd;
  constructor(
    private dialogRef: DialogRef<DeleteLoadBalancingDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      namespace: string;
      host: string;
      cluster: string;
    },
    private api: TrafficPolicyService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.api
      .getDetailLoadbalancer(this.data, this.data.host)
      .subscribe(result => {
        this.loadBalancer = get(result, 'spec');
        this.result = result;
        this.cdr.markForCheck();
      });
  }

  delete() {
    this.submitting = true;
    set(this.result.metadata, 'finalizers', [this.data.host]);
    const host = this.result.spec.host;
    const payload = { ...this.result, spec: { host } };
    this.api.updateLoadbalancer(this.data, payload).subscribe(
      () => {
        this.submitting = false;
        this.dialogRef.close(true);
      },
      () => {
        this.submitting = false;
        this.cdr.markForCheck();
      },
    );
  }
}
