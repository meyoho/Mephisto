import { TOKEN_BASE_DOMAIN, TranslateService } from '@alauda/common-snippet';
import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { TOKEN_ASM_BASE_DOMAIN } from '@app/services/services.module';
import { RESOURCE_DEFINITIONS } from '@app/utils';
import { INT_PATTERN_ZERO } from '@app/utils/patterns';
import { MicroServiceStore } from '@asm/api/service-list';
import { TrafficPolicyService } from '@asm/api/traffic-policy/traffic-policy.service';
import {
  ConsistentHash,
  DestinationRuleResponse,
  LoadBalancerCrd,
  LoadBalancerSpec,
  LoadBalancingStrategy,
} from '@asm/api/traffic-policy/traffic-policy.types';

import { cloneDeep, get, omit } from 'lodash-es';
@Component({
  selector: 'alo-load-balancing-dialog',
  templateUrl: './load-balancing-dialog.component.html',
  styleUrls: ['./load-balancing-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancingDialogComponent implements OnInit {
  loading = false;
  @ViewChild('form', { static: true })
  form: NgForm;

  services: DestinationRuleResponse[];
  service: DestinationRuleResponse;
  positiveIntPatternZero = INT_PATTERN_ZERO;
  resource = RESOURCE_DEFINITIONS.LOADBALANCER;
  originalData: LoadBalancerCrd = {
    apiVersion: `${this.resource.apiGroup}/${this.resource.apiVersion}`,
    kind: 'LoadBalancer',
    metadata: {
      namespace: this.data.namespace,
      name: this.data.name,
      labels: {
        [`${this.asmBaseDomain}/hostname`]: this.data.name,
        ...(this.store.gatewayInfo.name
          ? { [`servicemesh.${this.baseDomain}/resource`]: 'gateway' }
          : {}),
      },
    },
    spec: {
      host: this.data.name,
    },
  };

  get dialogText() {
    return !this.data.isUpdate
      ? {
          title: 'create_load_balancing',
          confirm: 'create',
          confirm_message: 'loadbalancing_created_message_success',
        }
      : {
          title: 'update_load_balancing',
          confirm: 'update',
          confirm_message: 'loadbalancing_updated_message_success',
        };
  }

  viewModel: LoadBalancerSpec = {
    host: this.data.name,
    simple: '',
  };

  consistentHash: ConsistentHash = {
    key: 'httpHeaderName',
    httpHeaderName: '',
    httpCookie: {
      name: '',
      path: '',
      ttl: '',
    },
    useSourceIp: true,
  };

  strategys = LoadBalancingStrategy;
  constructor(
    private readonly dialogRef: DialogRef<LoadBalancingDialogComponent>,
    @Inject(DIALOG_DATA)
    public data: {
      isUpdate: boolean;
      name: string;
      hideHost: boolean;
      cluster: string;
      namespace: string;
    },
    private readonly message: MessageService,
    private readonly translate: TranslateService,
    private readonly cdr: ChangeDetectorRef,
    private readonly api: TrafficPolicyService,
    private readonly store: MicroServiceStore,
    @Inject(TOKEN_BASE_DOMAIN) private readonly baseDomain: string,
    @Inject(TOKEN_ASM_BASE_DOMAIN) private readonly asmBaseDomain: string,
  ) {}

  ngOnInit() {
    this.setLoadbalancing();
  }

  save() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    const payload = this.getLoadbalancing();
    const { host } = this.viewModel;
    if (!this.data.isUpdate) {
      this.api.createLoadbalancer(this.data, payload).subscribe(
        () => {
          this.processResult(host);
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
    } else {
      this.api.updateLoadbalancer(this.data, payload).subscribe(
        () => {
          this.processResult(host);
        },
        () => {
          this.loading = false;
          this.cdr.markForCheck();
        },
      );
    }
  }

  processResult(host: string) {
    this.loading = false;
    this.message.success({
      content: this.translate.get(this.dialogText.confirm_message, {
        name: host,
      }),
    });
    this.dialogRef.close(true);
  }

  setLoadbalancing() {
    if (this.data.isUpdate) {
      this.viewModel.host = this.data.name;
      this.api
        .getDetailLoadbalancer(this.data, this.data.name)
        .subscribe(result => {
          this.originalData = result;
          const consistentHash: ConsistentHash = get(
            result,
            'spec.consistentHash',
          );
          if (consistentHash) {
            this.viewModel.simple = 'CONSISTENTHASH';
            this.consistentHash = { ...this.consistentHash, ...consistentHash };
            this.consistentHash.key = Object.keys(consistentHash).find(
              key => key !== 'minimumRingSize',
            );
            if (consistentHash.httpCookie) {
              this.consistentHash.httpCookie.ttl = parseInt(
                consistentHash.httpCookie.ttl as string,
                0,
              );
            }
          } else {
            this.viewModel = result.spec;
          }
          this.cdr.markForCheck();
        });
    }
  }

  getLoadbalancing() {
    let loadbalancing = cloneDeep(this.viewModel);
    const consistentHash: ConsistentHash = {};
    if (this.viewModel.simple === 'CONSISTENTHASH') {
      const loadBalancer = loadbalancing;
      const { key } = this.consistentHash;
      consistentHash[key] = cloneDeep(this.consistentHash[key]);
      consistentHash.minimumRingSize = 1024;
      if (key === 'httpCookie') {
        consistentHash[key].ttl = `${consistentHash[key].ttl}s`;
      }
      loadbalancing = {
        ...loadBalancer,
        consistentHash,
      };
      loadbalancing = omit(loadbalancing, 'simple') as LoadBalancerSpec;
    }
    return { ...this.originalData, spec: { ...loadbalancing } };
  }

  existedPure = (item: DestinationRuleResponse) => {
    return !!get(item, 'spec');
  };
}
