import { NgModule } from '@angular/core';

import { ApiModule } from './api/api.module';

@NgModule({
  imports: [ApiModule],
  providers: [],
  exports: [ApiModule],
})
export class ASMModule {
  constructor() {}
}
