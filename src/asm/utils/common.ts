import { PolicyMtls } from '@asm/api/security-strategy/security-strategy.types';
import { get } from 'lodash-es';

export enum PermissionType {
  Notdeployed = 'not_deployed',
  Unopened = 'unopened',
  NONamespace = 'no_namespace',
  Servicemesh = 'servicemesh',
}
export interface UrlParams {
  project?: string;
  cluster?: string;
  namespace?: string;
}
export const WORKSPACE_PREFIX = '/workspace';

export const URL_PREFIX = (params: UrlParams) => {
  const { project, cluster, namespace } = params;
  return `${WORKSPACE_PREFIX}/${project}/clusters/${cluster}/namespaces/${namespace}`;
};

export const permissionUrl = (type: PermissionType, params: UrlParams) => {
  switch (type) {
    case PermissionType.Notdeployed:
      return `${URL_PREFIX(params)}/unopened/true`;
    case PermissionType.Unopened:
      return `${URL_PREFIX(params)}/unopened/false`;
    case PermissionType.NONamespace:
      return `${WORKSPACE_PREFIX}/${params.project}/no-namespace`;
    case PermissionType.Servicemesh:
      return `${URL_PREFIX(params)}/servicemesh`;
    default:
      break;
  }
};

export const securityPolicyRuleProcess = (data: PolicyMtls[] = []) => {
  if (data.length === 0) {
    return '-';
  }
  const mode = data.map(item => get(item, 'mtls.mode'));
  if (mode[0] === 'STRICT' || !mode[0]) {
    return 'strict_mode';
  }
  return 'compatibility_mode';
};